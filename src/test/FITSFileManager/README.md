PJSR - PixInsight JavaScript Runtime
------------------------------------

This directory contains test cases and data for FITSFileManager.
The following scripts can be executed from the menu "SCRIPT/Execute script file ..." of Pixinsight:
  - FITFileManager-tests.js: Fully automated unit tests for many internal functions
  - FITSFileManager-fits-tests.js: Fully automated unit tests for FITS related functionalities
  - FITSFileManager-test-gui.js: Launch an interactive "config" windows to test the configuration UI

The result of all tests appears on the console log. The interactive test allows to play with the various configuration settings and checking the result in the console.

The script MakeSingleFileReleaseForTests.js can be used to create a single file release of FITSFileManager to distribute for testing.

###### Copyright (C) 2003-2021 Pleiades Astrophoto
