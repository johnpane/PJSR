// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// MakeSingleFileReleaseForTest.js 
// ----------------------------------------------------------------------------
//
// This file is part of FITSFileManager script version 1.6
//
// Copyright (c) 2012-2021 Jean-Marc Lugrin.
// Copyright (c) 2003-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

"use strict";

// Consolidate all scripts of one project and setup version,
// to make a single file easier for distribution for testing.

// USAGE:
// Adapt the following parameters:
//     VERSION
//     TARGET_ABSOLUTE_PATH 
// below and execute the script from PixInsight (SCRIPT/Execute Script File...), 
// Check the log on the console.
// The resulting script can be executed by SCRIPT/Execute Script File... for testing.
// This does not replace the normal distribution via the PI update mechanism.

#include <pjsr/DataType.jsh>

// The version (add -test at the end)
#define VERSION "1.6.1-test"
// The absolute path of the directory that will contain the consolidate script
#define TARGET_ABSOLUTE_PATH "C:\\TEMP"


// The input main file
#define MAIN_FILE_NAME "FITSFileManager.js"
// The path of the sources relative to this script
#define SOURCE_RELATIVE_DIR "../../scripts/FITSFileManager"


(function (version, mainFileName) {

   // Return a full normalized path to the directory containing the parameter file (or directory)
   let getDirectoryOfFileWithDriveLetter = function getDirectoryOfFileWithDriveLetter( a_file_path )
   {
      let unix_path = File.windowsPathToUnix( a_file_path );
      let pathNormalized = File.fullPath(unix_path);
      let directoryWithDrive = File.extractDrive( pathNormalized ) + File.extractDirectory(pathNormalized);
      //Console.writeln("*** getDirectoryOfFileWithDriveLetter\n    a_file_path '" + a_file_path + "\n    unix_path '" + unix_path + "'\n    pathNormalized '" + pathNormalized + "' \n    directoryWithDrive '" + directoryWithDrive +"'");
      return directoryWithDrive;
   }

   // Return a full normalized path of a directory or a file, received in unix or windows format
   let getDirectoryWithDriveLetter = function getDirectoryWithDriveLetter( a_directory_path )
   {
      let unix_path = File.windowsPathToUnix( a_directory_path );
      let pathNormalized = File.fullPath(unix_path);
      //Console.writeln("*** getDirectoryWithDriveLetter\n    a_directory_path '" + a_directory_path + "'\n    unix_path '" + unix_path  + "'\n    pathNormalized '" + pathNormalized +"'");
      return pathNormalized;
   }

   // Directories are relative to the directory of the current script
   let releaseScriptFilePath = #__FILE__ ;
   let releaseScriptDir = getDirectoryOfFileWithDriveLetter(releaseScriptFilePath);

   let sourceDir = getDirectoryWithDriveLetter(releaseScriptDir + "/" + SOURCE_RELATIVE_DIR);
   let sourceMainFilePath = getDirectoryWithDriveLetter(sourceDir + "/" + mainFileName);
   let targetDirectory = getDirectoryWithDriveLetter(TARGET_ABSOLUTE_PATH);

   // Read a file, return it as a list of lines
   let readTextFile = function(filePath) {
      if (!File.exists(filePath)) {
         throw "File not found: " +  File.fullPath(filePath);
      }
      let file = new File;
      file.openForReading(filePath);
      let buffer = file.read( DataType_ByteArray, file.size );
      file.close();

      let lines = buffer.toString().split( '\n' );
      return lines;
   }

   Console.writeln("Make release for '" + mainFileName + "', version '" + version  + "'");
   if (!File.directoryExists(targetDirectory)) {
      throw "Target directory not found: " +  File.fullPath(targetDirectory);
   }
   let mainFileNameOnly = File.extractName(mainFileName); // Name without extension
   let mainFileExtension = File.extractExtension(mainFileName);

   let targetFileName = mainFileNameOnly + "-" + version + mainFileExtension;
   let targetFilePath = targetDirectory + "/" + targetFileName;
   Console.writeln("  Target directory: " + targetDirectory);
   Console.writeln("  Target file name: " + targetFileName);


   let rootFileText = readTextFile(sourceMainFilePath);
   Console.writeln(mainFileName + " has " + rootFileText.length + " lines") ;

   let targetFile = new File;
   targetFile.createForWriting(targetFilePath);

   let includeRegExp1 = new RegExp("#include[ \\t]\"+(" + mainFileNameOnly + "-.+)\"");
   let includeRegExp2 = new RegExp("#include[ \\t]\"+(PJSR-logging\.jsh)\"");
   // let includeRegExp = /#include[ \t]"+(VaryParams-.+)"/;
   Console.writeln("  looking up include as " + includeRegExp1 + " or " + includeRegExp2);

   // For some reason adding the N makes the regexp not working....
   let defineVersionRegExp = /^#define[ \t]+VERSIO/;

   // To detect the title
   let defineTitleRegExp = /^#define[ \t]+TITLE[ \t]+(.+)/;

   // Log that we have a title
   let defineDebugRegExp = /^#define[ \t]+DEBUG(.*)/;

   let hasTitle = false;
   let hasVersion = false;

   let anyMessage = true;
   for (let i=0; i<rootFileText.length; i++) {
      // To show log on console
      if (anyMessage) {
         Console.flush();
         processEvents();
         anyMessage = false;
      }

       // Copy the text, adding the included files and updating the VERSION string
      let line = rootFileText[i];
      // Remove terminator characters
      line = line.replace(/[\r\n]*/g,"");
      let match = line.match(includeRegExp1);
      if (!match) match = line.match(includeRegExp2);
      if (match) {
         // Found an #include
         Console.writeln("  handling '"+ line + "'");
         anyMessage = true;
         
         let inludedFilePath = sourceDir + "/" + match[1];
         let includedText = readTextFile(inludedFilePath);
         Console.writeln("     including " + includedText.length + " lines from '" + match[1] + "'");
         targetFile.outTextLn("//================================================================");
         targetFile.outTextLn("// " + line.substring(1));
         targetFile.outTextLn("//================================================================");
         for (let j=0; j<includedText.length; j++) {
            // Remove terminator characters
            line = includedText[j].replace(/[\r\n]*/g,"");
            if (defineDebugRegExp.test(line)) {
               // Disable DEBUG
               Console.writeln("    handling '" + line + "'");
               Console.writeln("       Disabling DEBUG"); 
               targetFile.outTextLn("// " + line);
            } else {
               targetFile.outTextLn(line);
            }
         }
         targetFile.outTextLn("//==== end of include ============================================");

      } else if (defineVersionRegExp.test(line)) {
         hasVersion = true;
         anyMessage = true;
         // Update version
         Console.writeln("  handling '" + line + "'");
         Console.writeln("     writing updated version " + "#define VERSION \"" + version + "\"");
         targetFile.outTextLn("// Release version " + version + " created by makeRelease on " + new Date() );
         targetFile.outTextLn("#define VERSION \"" + version + "\"");

      } else if (defineDebugRegExp.test(line)) {
         // Disable DEBUG
         anyMessage = true;
         Console.writeln("  handling '" + line + "'");
         Console.writeln("     Disabling DEBUG"); 
         targetFile.outTextLn("// " + line);

      } else if (defineTitleRegExp.test(line)) {
         hasTitle = true;
         anyMessage = true;
         // Show title
         Console.writeln("  handling '" + line + "'");
         Console.writeln("     TITLE is defined"); //  as '" + TITLE + "'");
         targetFile.outTextLn(line);


      } else {
         targetFile.outTextLn(line);
      }

   }

   if (!hasTitle) {
      Console.writeln("#define TITLE is missing");
   }
   if (!hasVersion) {
      Console.writeln("#define VERSION is missing");
   }

   targetFile.close();
   Console.writeln("File '" + targetFilePath + "' created.");


})(VERSION, MAIN_FILE_NAME);

