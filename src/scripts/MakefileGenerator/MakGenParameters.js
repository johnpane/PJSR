// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// MakGenParameters.js - Released 2021-12-29T20:18:04Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Makefile Generator Script version 1.125
//
// Copyright (c) 2009-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Makefile Generator
 *
 * Automatic generation of PCL makefiles and projects for FreeBSD, Linux,
 * macOS and Windows platforms.
 *
 * Copyright (c) 2009-2021, Pleiades Astrophoto S.L. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Project generation parameters.
 */

function GeneratorParameters()
{
   /*
    * Identifier of this project
    */
   this.id = "";

   /*
    * Project type
    * One of: "Module" "StaticLibrary" "DynamicLibrary" "Executable" "X11Installer" "CoreAux" "Core"
    */
   this.type = "";

   /*
    * Project platform
    * One of: "FreeBSD" "Linux" "MacOSX" "Windows"
    */
   this.platform = "";

   /*
    * Project architecture
    * One of: "x64" "arm64"
    * For GCC and Clang only. Ignored for Windows projects.
    */
   this.architecture = "x64";

   /*
    * Compatibility with old hardware.
    */
   this.compatibility = false;

   /*
    * If true, we are generating makefiles for an official PixInsight
    * distribution.
    * In an official distribution, the PixInsight Core application and all
    * modules and auxiliary executables are digitally signed with our corporate
    * code signing certificates.
    */
   this.official = false;

   /*
    * If true, sign modules and executables with the codesign and signtool
    * utilities on macOS and Windows, respectively.
    */
   this.signed = DEFAULT_SIGNED_CODE;

   /*
    * When signing code, use this identity for codesign's -s argument. For the
    * signtool on Windows, the signing identity is selected automatically.
    */
   this.signingIdentity = DEFAULT_CODE_SIGNING_IDENTITY;

   /*
    * Remove existing build directories before generating new makefiles.
    */
   this.cleanUpPreviousBuilds = true;

   /*
    * GCC debug builds. If true, generate debug makefiles.
    * For GNU/GCC only; ignored for Windows projects.
    */
   this.gccDebug = false;

   /*
    * GCC optimization level.
    * One of: "0", "1", "2", "3", "s"
    * For GNU/GCC only; ignored for Windows projects.
    */
   this.gccOptimization = OPTIMIZATION_DEFAULT_STR;

   /*
    * GCC link-time optimization (-flto). Experimental option - disabled by
    * default because it produces very slow code in our tests.
    */
   this.gccLinkTimeOptimization = false;

   /*
    * Do not strip binaries. If true, the -rdynamic flag will be used to enable
    * backtraces with demangled function names. If false, the -s flag will be
    * used to strip all executable files (Linux only).
    */
   this.gccUnstrippedBinaries = true;

   /*
    * GCC C++ compiler executable suffix for Linux builds.
    * Set this to an empty string to use the system default compiler.
    */
   this.gccSuffixLinux = DEFAULT_GCC_VERSION_SUFFIX_LINUX;

   /*
    * Include -arch xxx and -Xarch_xxx compiler and linker options for macOS
    * makefiles.
    */
   this.osxArchOptions = true;

   /*
    * OS X sysroot SDK version.
    */
   this.osxSDKVersion = DEFAULT_OSX_SDK_VERSION;

   /*
    * Generate code using AVX2 and FMA instructions.
    */
   this.useAVX2 = DEFAULT_USE_AVX2_FMA;

   /*
    * Support CUDA GPU acceleration.
    */
   this.cudaSupport = DEFAULT_CUDA_SUPPORT;

   /*
    * PCL diagnostics level
    * 0=disabled, 1=preconditions, 2=preconditions+checks
    */
   this.diagnostics = 0;

   /*
    * Add -fvisibility=hidden option for GNU/GCC makefiles
    * Ignored for module projects
    */
   this.hidden = false;

   /*
    * Windows-specific: module definition file
    */
   this.winDefFile = "";

   /*
    * Windows-specific: UAC execution level = requireAdministrator
    * For internal use ONLY (required by the update system)
    */
   this.uacAdmin = false;

   /*
    * Additional preprocessor definitions
    */
   this.extraDefinitions = new Array;

   /*
    * Additional preprocessor definitions - Windows-specific
    */
   this.winExtraDefinitions = new Array;

   /*
    * Additional include search directories
    */
   this.extraIncludeDirs = new Array;

   /*
    * Additional library search directories
    */
   this.extraLibDirs = new Array;

   /*
    * Additional library dependencies
    */
   this.extraLibraries = new Array;

   /*
    * If nonempty, additional library dependencies for Windows. In such case
    * this.extraLibraries will be ignored for generation of Windows MSVC
    * projects.
    *
    * Rationale - For example, we have libfoo.so on UNIX/Linux (instead of the
    * expected liblibfoo.so) and libfoo.dll (along with libfoo.lib) on Windows.
    * In such case we have to specify "foo" on UNIX/Linux and "libfoo" on
    * Windows because of the default 'lib' prefix applied by ld.
    */
   this.winExtraLibraries = new Array;

   /*
    * Additional include search directories - Windows-specific
    */
   this.winExtraIncludeDirs = new Array;

   //

   this.isModule = function()
   {
      return this.type == "Module";
   };

   this.isOfficialModule = function()
   {
      return this.official && this.isModule();
   };

   this.isDynamicLibrary = function()
   {
      return this.type == "DynamicLibrary";
   };

   this.isOfficialDynamicLibrary = function()
   {
      return this.official && this.isDynamicLibrary();
   };

   this.isStaticLibrary = function()
   {
      return this.type == "StaticLibrary";
   };

   this.isExecutable = function()
   {
      return this.type == "Executable" || this.isCoreExecutable();
   };

   this.isOfficialExecutable = function()
   {
      return this.official && this.isExecutable();
   };

   this.isCore = function()
   {
      return this.type == "Core";
   };

   this.isCoreAux = function()
   {
      return this.type == "CoreAux";
   };

   this.isCoreExecutable = function()
   {
      return this.type == "CoreExecutable";
   };

   this.isX11Installer = function()
   {
      return this.type == "X11Installer";
   };

   this.isHostPlatform = function()
   {
      return this.platform == "Host";
   };

   this.isLinuxPlatform = function()
   {
      return this.platform == "Linux" || this.isHostPlatform();
   };

   this.isFreeBSDPlatform = function()
   {
      return this.platform == "FreeBSD";
   };

   this.isMacOSXPlatform = function()
   {
      return this.platform == "MacOSX";
   };

   this.isWindowsPlatform = function()
   {
      return this.platform == "Windows";
   };

   this.isX64Project = function()
   {
      return this.architecture == "x64";
   };

   this.isARM64Project = function()
   {
      return this.architecture == "arm64";
   };

   this.isCompatibilityProject = function()
   {
      return this.compatibility;
   };

   this.isAVX2SupportProject = function()
   {
      return this.useAVX2 && !this.isCompatibilityProject();
   };

   this.isCUDASupportProject = function()
   {
      // ### FIXME. For now, we only support CUDA on Linux.
      return this.cudaSupport && !this.isCompatibilityProject() && this.isLinuxPlatform();
   };

   this.validate = function()
   {
      /*
       * Ensure validity of project type, platform and architecture.
       */
      this.id = this.id.trim();
      if ( this.id.length == 0 )
         throw new Error( "Empty project identifier." );
      if ( !this.isModule() && !this.isStaticLibrary() && !this.isDynamicLibrary() && !this.isExecutable() && !this.isCore() && !this.isCoreAux() && !this.isX11Installer() )
         throw new Error( "Invalid project type: " + this.type );
      if ( !this.isLinuxPlatform() && !this.isFreeBSDPlatform() && !this.isMacOSXPlatform() && !this.isWindowsPlatform() )
         throw new Error( "Unknown platform: " + this.platform );
      if ( !this.isX64Project() && !this.isARM64Project() )
         throw new Error( "Unknown architecture: " + this.architecture );
      if ( this.signed )
         if ( this.signingIdentity.length == 0 )
            throw new Error( "Empty signing identity." );
   };

   this.isSourceFile = function( fileName )
   {
      let ext = File.extractExtension( fileName );
      if ( ext == ".cpp" || ext == ".c" || ext == ".cxx" || this.isMacOSXPlatform() && ext == ".mm" )
         return true;
      if ( ext == ".cu" )
         return this.isCUDASupportProject();
      return false;
   };

   this.mainTarget = function()
   {
      if ( this.isModule() || this.isDynamicLibrary() )
      {
         let s = this.id;

         if ( this.isModule() )
            s += "-pxm";
         else
            s += "-pxi";

         if ( this.isLinuxPlatform() || this.isFreeBSDPlatform() )
            s += ".so";
         else if ( this.isMacOSXPlatform() )
            s += ".dylib";
         else if ( this.isWindowsPlatform() )
            s += ".dll";

         if ( this.isDynamicLibrary() )
            if ( this.isLinuxPlatform() || this.isFreeBSDPlatform() || this.isMacOSXPlatform() )
               s = "lib" + s;

         return s;
      }

      if ( this.isStaticLibrary() )
      {
         let s = this.id;

         if ( this.isLinuxPlatform() || this.isFreeBSDPlatform() || this.isMacOSXPlatform() )
            s = "lib" + s + "-pxi.a";
         else if ( this.isWindowsPlatform() )
            s += "-pxi.lib";

         return s;
      }

      if ( this.isExecutable() || this.isCoreAux() || this.isX11Installer() )
      {
         if ( this.isWindowsPlatform() )
            return this.id + ".exe";
         return this.id;
      }

      if ( this.isCore() )
         return this.isWindowsPlatform() ? "PixInsight.exe" : "PixInsight";

      return null;
   };

   this.macOSXAppName = function()
   {
      return "PixInsight.app";
   };

   this.platformMacroId = function()
   {
      if ( this.isFreeBSDPlatform() )
         return "__PCL_FREEBSD";
      if ( this.isLinuxPlatform() )
         return "__PCL_LINUX";
      if ( this.isMacOSXPlatform() )
         return "__PCL_MACOSX";
      if ( this.isWindowsPlatform() )
         return "__PCL_WINDOWS";

      return null;
   };

   this.platformBuildDirectory = function( baseDirectory )
   {
      if ( this.isFreeBSDPlatform() )
         return baseDirectory + "/freebsd";
      if ( this.isLinuxPlatform() )
         return baseDirectory + (this.isHostPlatform() ? "/host" : "/linux");
      if ( this.isMacOSXPlatform() )
         return baseDirectory + "/macosx";
      if ( this.isWindowsPlatform() )
         return baseDirectory + "/windows";

      return null;
   };

   this.dependsOnQt = function()
   {
      if ( this.isCore() || this.isCoreAux() )
         return true;
      for ( let i = 0; i < this.extraLibraries.length; ++i )
         if ( this.extraLibraries[i].startsWith( "Qt" ) )
            return true;
      return false;
   };

   this.validateGnuCxxBuild = function()
   {
      if ( this.isWindowsPlatform() )
         throw new Error( "Unsupported environment: Windows with GCC or Clang compiler." );
      if ( !this.isLinuxPlatform() && !this.isFreeBSDPlatform() && !this.isMacOSXPlatform() )
         throw new Error( "Unsupported environment: Unknown platform with GCC or Clang compiler." );
   };

   this.validateMSVCxxBuild = function()
   {
      if ( !this.isWindowsPlatform() )
         throw new Error( "Unsupported environment: VC++ compiler on non-Windows platform." );
   };

   this.gccStandardBuildDirectory = function( baseDirectory )
   {
      if ( this.isWindowsPlatform() )
         return null;
      return this.platformBuildDirectory( baseDirectory ) + "/g++";
   };

   this.gccCompatibilityBuildDirectory = function( baseDirectory )
   {
      if ( this.isWindowsPlatform() )
         return null;
      return this.platformBuildDirectory( baseDirectory ) + "/g++c";
   };

   this.gccBuildDirectory = function( baseDirectory )
   {
      return this.isCompatibilityProject() ?
         this.gccCompatibilityBuildDirectory( baseDirectory ) :
         this.gccStandardBuildDirectory( baseDirectory );
   };

   this.gccQtMkspecsDirectory = function()
   {
      let s = this.qtDirectory() + "/qtbase/mkspecs/";
      if ( this.isMacOSXPlatform() )
         s += "macx-g++";
      else if ( this.isFreeBSDPlatform() )
         s += "freebsd-clang";
      else // Linux
         s += "linux-g++-64";
      return s;
   };

   this.gccMakefile = function()
   {
      let makefile = "makefile-" + this.architecture;
      if ( this.gccDebug )
         makefile += "-debug";
      return makefile;
   };

   this.vccStandardBuildDirectory = function( baseDirectory, vcVersion )
   {
      if ( !this.isWindowsPlatform() )
         return null;
      return baseDirectory + "/windows/vc" + vcVersion.toString();
   };

   this.vccCompatibilityBuildDirectory = function( baseDirectory, vcVersion )
   {
      if ( !this.isWindowsPlatform() )
         return null;
      return baseDirectory + "/windows/vc" + vcVersion.toString() + "c";
   };

   this.vccBuildDirectory = function( baseDirectory, vcVersion )
   {
      return this.isCompatibilityProject() ?
         this.vccCompatibilityBuildDirectory( baseDirectory, vcVersion ) :
         this.vccStandardBuildDirectory( baseDirectory, vcVersion );
   };

   this.vccProject = function( vcVersion )
   {
      return this.id + ".vcxproj"; // VC++ >= 2010
   };

   this.gccCxxCompiler = function()
   {
      if ( this.isMacOSXPlatform() || this.isFreeBSDPlatform() )
         return "clang++";
      if ( this.isHostPlatform() )
         return "g++" + DEFAULT_GCC_VERSION_SUFFIX_HOST;
      return "g++" + this.gccSuffixLinux;
   };

   this.gccCCompiler = function()
   {
      if ( this.isMacOSXPlatform() || this.isFreeBSDPlatform() )
         return "clang";
      if ( this.isHostPlatform() )
         return "gcc" + DEFAULT_GCC_VERSION_SUFFIX_HOST;
      return "gcc" + this.gccSuffixLinux;
   };

   this.cudaCCompiler = function()
   {
      return "nvcc";
   };

   this.gccArchFlags = function()
   {
      if ( this.isMacOSXPlatform() && this.osxArchOptions )
         return " -arch x86_64";
      return " -m64";
   };

   this.gccPICFlags = function()
   {
      // PIC is always enabled for OS X. On Linux and FreeBSD, it must be enabled for 64-bit builds.
      return (this.isMacOSXPlatform() || this.isLinuxPlatform() || this.isFreeBSDPlatform()) ? " -fPIC" : "";
   };

   this.gccCompileArgs = function( isCpp )
   {
      if ( isCpp == undefined ) // assume C++ compilation by default
         isCpp = true;

      let s = "-c -pipe -pthread" + this.gccArchFlags() + this.gccPICFlags();

      if ( this.isMacOSXPlatform() )
      {
         s += " -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX" + this.osxSDKVersion + ".sdk";
         s += " -mmacosx-version-min=" + MIN_OSX_VERSION;
      }

      s += " -D_REENTRANT -D" + this.platformMacroId();
      if ( this.isCore() )
      {
         s += " -D__PCL_BUILDING_PIXINSIGHT_APPLICATION -D__PCL_QT_INTERFACE" + // PCL
              " -DXP_UNIX" + // SpiderMonkey
              " -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE" +
              " -DQT_EDITION=QT_EDITION_OPENSOURCE" + // Qt/LGPL
              " -DQT_NO_EXCEPTIONS -DQT_NO_DEBUG -DQT_SHARED";
      }
      else
      {
         if ( this.isCoreAux() )
            s += " -D__PCL_QT_INTERFACE" + // PCL
                 " -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE" +
                 " -DQT_EDITION=QT_EDITION_OPENSOURCE" + // Qt/LGPL
                 " -DQT_NO_EXCEPTIONS -DQT_NO_DEBUG -DQT_SHARED";

         for ( let i = 0; i < this.extraDefinitions.length; ++i )
            s += " -D\"" + this.extraDefinitions[i] + "\"";
      }

      if ( this.diagnostics != 0 )
         s += " -D__PCL_DIAGNOSTICS_LEVEL=" + this.diagnostics.toString();

      if ( this.isCompatibilityProject() )
         s += " -D__PCL_COMPATIBILITY";
      if ( this.isAVX2SupportProject() )
         s += " -D__PCL_AVX2 -D__PCL_FMA";
      if ( this.isCUDASupportProject() )
         s += " -D__PCL_CUDA";

      s += " -I\"$(PCLINCDIR)\" -I\"$(PCLSRCDIR)/3rdparty\"";

      if ( this.isCore() || this.isCoreAux() )
      {
         if ( this.isFreeBSDPlatform() )
         {
            s += " -I\"" + this.qtDirectory() + "/usr/local/include/qt5\"";
            if ( this.isCore() )
               s += " -I\"" + this.qtDirectory() + "/usr/local/include/qt5/QtWebEngineWidgets\"";
         }
         else
         {
            s += " -I\"" + this.qtDirectory() + "/qtbase/include\"";
            s += " -I\"" + this.qtDirectory() + "/qtbase/include/QtCore\"";
            s += " -I\"" + this.qtDirectory() + "/qtsvg/include\"";
            if ( this.isCore() )
            {
               s += " -I\"" + this.qtDirectory() + "/qtwebchannel/include\"";
               s += " -I\"" + this.qtDirectory() + "/qtwebengine/include\"";
               s += " -I\"" + this.qtDirectory() + "/qtwebengine/include/QtWebEngineWidgets\"";
               s += " -I\"" + this.qtDirectory() + "/qtdeclarative/include\"";
               s += " -I\"" + this.qtDirectory() + "/qtlocation/include\"";
            }
            if ( this.isLinuxPlatform() )
               s += " -I\"" + this.qtDirectory() + "/qtx11extras/include\"";
            if ( this.isMacOSXPlatform() )
               s += " -I\"" + this.qtDirectory() + "/qtmacextras/include\"";
         }
         s += " -I\"" + this.gccQtMkspecsDirectory() + "\"";

         if ( this.isCore() )
         {
            s += " -I\"$(PCLSRCDIR)/core/Components\" -I\"$(PCLSRCDIR)/core\"";

            // SpiderMonkey include directories are architecture- and platform-dependent
            s += " -I\"$(PCLINCDIR)/js/";
            if ( this.isFreeBSDPlatform() )
               s += "freebsd";
            if ( this.isLinuxPlatform() )
               s += "linux";
            if ( this.isMacOSXPlatform() )
               s += "macosx";
            s += "/" + this.architecture + "\"";
         }

         s += " -I\"$(PCLINCDIR)/pcl\"";

         if ( this.isFreeBSDPlatform() )
            s += " -I/usr/local/include -I/usr/X11R6/include";
      }

      if ( this.isModule() || this.isDynamicLibrary() || this.isExecutable() )
         if ( this.dependsOnQt() )
         {
            if ( this.isFreeBSDPlatform() )
               s += " -I/usr/local/include -I\"" + this.qtDirectory() + "/usr/local/include/qt5\"";
            else
               s += " -I\"" + this.qtDirectory() + "/qtbase/include\"";
            s += " -I\"" + this.gccQtMkspecsDirectory() + "\"";
         }

      // CUDA runtime on Linux
      if ( !this.isCompatibilityProject() )
         if ( this.isLinuxPlatform() )
            s += " -I\"$(CUDA_HOME)/include\"";

      if ( !this.isCore() )
         for ( let i = 0; i < this.extraIncludeDirs.length; ++i )
            s += " -I\"" + this.extraIncludeDirs[i] + "\"";

      if ( this.gccDebug )
         s += " -O0 -g";
      else
      {
         // -Ofast not supported by clang on macOS.
         let optimization = this.gccOptimization;
         if ( this.isMacOSXPlatform() )
            if ( optimization == "fast" )
               optimization = "3";

         // The AVX2 instruction set was first available for the Intel Haswell
         // microarchitecture.
         if ( this.isAVX2SupportProject() )
            s += " -march=haswell";

         // - Optimize for AMD Family 17h core based CPUs (Zen version 2
         //   microarchitecture) on Linux and FreeBSD builds.
         // - Optimize for the Intel Skylake microarchitecture on macOS.
         if ( !this.isCompatibilityProject() )
            if ( this.isMacOSXPlatform() )
               s += " -mtune=skylake";
            else
               s += " -mtune=znver2";

         if ( this.isLinuxPlatform() )
            s += " -mfpmath=sse";

         // Compatibility builds only require SSSE3 instruction set support on
         // Linux/UNIX. Regular builds require SSE4.2 and optionally AVX2/FMA3.
         if ( this.isCompatibilityProject() )
            s += " -mssse3";
         else
            s += " -msse4.2";

         // Since core version 1.8.8-7, we can generate code with AVX2 and FMA3
         // instructions optionally in all official builds.
         if ( this.isAVX2SupportProject() )
            s += " -mavx2 -mfma";

         // Inline string operations for small blocks with runtime checks, and
         // use library calls for large blocks.
         s += " -minline-all-stringops";

         // GCC optimization level
         s += " -O" + optimization;

         // Make all functions susceptible to inlining under -O2 optimization
         // (already enabled for -O3).
         if ( optimization == "2" )
            s += " -finline-functions";

         // On OS X, omitting frame pointers is *not allowed* and leads to a
         // nightmare of crashes.
         if ( !this.isMacOSXPlatform() )
            s += " -fomit-frame-pointer"; // this may not be enabled by default on x64

         // Generate separate function and data sections, for removal of unused
         // sections during the link phase (see the --gc-sections linker flag).
         s += " -ffunction-sections -fdata-sections";

         if ( optimization == "2" || optimization == "3" )
         {
            // Fast math always enabled for optimized builds.
            s += " -ffast-math";

            // Link-time optimization available for modules, PCL, and
            // third-party shared libraries.
            // ### N.B.: Use of this option is experimental and not recommended
            // without extensive testing. It poses a risk to generate unstable
            // binaries without a significant performance improvement.
            if ( this.gccLinkTimeOptimization )
               if ( this.isModule() || this.official && (this.isStaticLibrary() || this.isDynamicLibrary()) )
                  s += " -flto";
         }
      }

      // Hidden visibility enabled for modules by default.
      // Clang on macOS also requires these flags enabled for executables.
      // ### TODO: Make this a user-selectable option.
      if ( this.hidden || this.isModule() || (this.isCore() || this.isCoreAux() || this.isExecutable()) && this.isMacOSXPlatform() )
      {
         s += " -fvisibility=hidden";
         if ( isCpp )
            s += " -fvisibility-inlines-hidden";
      }

      // Generate code that allows trapping instructions to throw exceptions.
      // This is not available on Clang.
      if ( !this.isMacOSXPlatform() && !this.isFreeBSDPlatform() )
         s += " -fnon-call-exceptions";

      // We require C++17 support since core version 1.8.7.
      if ( isCpp )
      {
         s += " -std=c++17";
         if ( this.isMacOSXPlatform() )
            s += " -stdlib=libc++"; // required since Xcode 6
      }
      else
         s += " -std=c99";

      // - Enable all warnings.
      // - Suppress some useless warnings related to parentheses.
      s += " -Wall -Wno-parentheses";

      if ( isCpp )
      {
         // Clang generates *really* useless and nasty warnings about different
         // representations of empty structures in C and C++.
         if ( this.isMacOSXPlatform() || this.isFreeBSDPlatform() )
            s += " -Wno-extern-c-compat";

         // SpiderMonkey on g++ and clang generates warnings about applying
         // offsetof() to non-POD types.
         if ( this.isCore() )
            s += " -Wno-invalid-offsetof";
      }
      else
      {
         // Some of our 3rd-party support libraries are slightly non C99
         // compliant. In particular, they call some undeclared functions...
         // Without suppressing these warnings these libraries don't build with
         // clang, which
         s += " -Wno-error=implicit-function-declaration";
      }

      // Manage dependencies.
      s += " -MMD -MP -MF\"$(@:%.o=%.d)\" -o\"$@\" \"$<\"";

      return s;
   };

   this.cudaCompileArgs = function()
   {
      return "-c --use_fast_math --extra-device-vectorization --optimize 3" +
             " --compiler-options '-fPIC' -Xptxas=\"-v\"" +
             " -DNAME=__PCL_LINUX -I\"$(PCLINCDIR)\" -I\"$(CUDA_HOME)/include\"" +
             " -MMD -MP -MF \"$(@:%.o=%.d)\" -o \"$@\" \"$<\"";
   };

   this.gccBuildCommand = function()
   {
      if ( this.isStaticLibrary() )
         return "ar r $(OBJ_DIR)/" + this.mainTarget() + " $(OBJ_FILES)";

      let s = this.gccCxxCompiler() + this.gccArchFlags() + this.gccPICFlags();

      if ( this.isMacOSXPlatform() )
      {
         // The documentation for install_name_tool points out:
         //    "For this tool to work when the install names or rpaths are
         //    larger the binary should be built with the ld
         //    -headerpad_max_install_names option."
         // See: http://stackoverflow.com/questions/2092378/
         //             macosx-how-to-collect-dependencies-into-a-local-bundle
         s += " -headerpad_max_install_names";
         s += " -Wl,-syslibroot,/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX" + this.osxSDKVersion + ".sdk";
         s += " -mmacosx-version-min=" + MIN_OSX_VERSION;
         s += " -stdlib=libc++";
      }
      else
      {
         // On Linux:
         // - Always use the gold linker.
         // - Use the --enable-new-dtags linker option to prevent rpath issues.
         //   See: https://news.ycombinator.com/item?id=14222349
         if ( this.isLinuxPlatform() )
            s += " -pthread -Wl,-fuse-ld=gold -Wl,--enable-new-dtags"
         // On Linux and FreeBSD, mark all executables and shared objects as
         // not requiring an executable stack.
         s += " -Wl,-z,noexecstack";
         // GCC and Clang on Darwin don't support linker optimizations.
         s += " -Wl,-O1";
      }

      // Exclude unused sections.
      s += " -Wl," + (this.isMacOSXPlatform() ? "-dead_strip" : "--gc-sections");

      // Be compatible with old Linux distributions without CXXABI_1.3.8, such
      // as RHEL 7. This rpath allows us to use a libstdc++.so.6.0.22 or newer
      // created by installing GCC >= 4.9. Note that the updater1 program is
      // executed by root (through an SUID bit) on regular installations, so
      // the process does not inherit the core application's environment.
      if ( this.isLinuxPlatform() )
         if ( this.mainTarget() == "PixInsightUpdater" )
            s += " -Wl,-rpath,/usr/local/lib64/:/lib64/";

      // Either strip all symbols, or allow for significant backtraces with
      // demangled function names.
      if ( this.gccUnstrippedBinaries )
         s += " -rdynamic";
      else if ( !this.isMacOSXPlatform() ) // -s declared obsolete in latest versions of clang
         s += " -s";

      if ( this.isModule() || this.isDynamicLibrary() )
         s += (this.isMacOSXPlatform()) ?
               " -dynamiclib -install_name @executable_path/" + this.mainTarget() : " -shared";
      if ( this.isMacOSXPlatform() )
         if ( this.isCore() || this.isCoreAux() || this.dependsOnQt() )
            s += " -rpath @executable_path/../Frameworks";

      // Link-time optimization for building modules and third-party shared
      // libraries. ### Warning: this option is experimental.
      if ( this.gccLinkTimeOptimization )
         if ( this.isModule() || this.official && (this.isStaticLibrary() || this.isDynamicLibrary()) )
            if ( this.gccOptimization == "2" || this.gccOptimization == "3" || this.gccOptimization == "fast" )
               s += " -flto";

      s += " -L\"" + this.libDirectory() + "\"";

      if ( this.isLinuxPlatform() || this.isFreeBSDPlatform() )
         if ( this.isCore() || this.isCoreAux() || this.isModule() || this.isDynamicLibrary() || this.isExecutable() )
            s += " -L\"" + this.binDirectory() + "/lib\"";

      if ( this.isCore() || this.isCoreAux() ||
           (this.isModule() || this.isDynamicLibrary() || this.isExecutable()) && this.dependsOnQt() )
      {
         if ( this.isFreeBSDPlatform() )
         {
            s += " -L/usr/local/lib -L\"" + this.qtDirectory() + "/usr/local/lib/qt5\"";
            if ( this.isCore() || this.isCoreAux() )
               s += " -L/usr/X11R6/lib";
         }
         else
         {
            s += " -L\"" + this.qtDirectory() + "/qtbase/lib\"";
            if ( this.isMacOSXPlatform() )
               s += " -F\"" + this.qtDirectory() + "/qtbase/lib\"";
         }
      }

      if ( this.isModule() || this.isDynamicLibrary() || this.isExecutable() || this.isCoreAux() )
         for ( let i = 0; i < this.extraLibDirs.length; ++i )
            s += " -L\"" + this.extraLibDirs[i] + "\"";

      s += " -o $(OBJ_DIR)/" + this.mainTarget() + " $(OBJ_FILES)";

      if ( this.isModule() || this.isDynamicLibrary() )
         if ( this.isMacOSXPlatform() )
         {
            s += " -framework CoreFoundation"; // required since PI 1.8.0
            for ( let i = 0; i < this.extraLibraries.length; ++i )
               if ( this.extraLibraries[i].startsWith( "Qt" ) )
                  s += " -framework " + this.extraLibraries[i].replace( "Qt5", "Qt" );
            if ( this.isModule() )
               s += " -lpthread -lPCL-pxi -llz4-pxi -lzlib-pxi -lRFC6234-pxi -llcms-pxi -lcminpack-pxi";
            for ( let i = 0; i < this.extraLibraries.length; ++i )
               if ( !this.extraLibraries[i].startsWith( "Qt" ) )
                  s += " -l" + this.extraLibraries[i];
         }
         else
         {
            if ( this.isModule() )
               s += " -lpthread -lPCL-pxi -llz4-pxi -lzlib-pxi -lRFC6234-pxi -llcms-pxi -lcminpack-pxi";
            for ( let i = 0; i < this.extraLibraries.length; ++i )
               s += " -l" + this.extraLibraries[i];
         }

      if ( this.isExecutable() || this.isX11Installer() )
      {
         if ( this.isMacOSXPlatform() )
            s += " -framework AppKit -framework ApplicationServices";
         s += " -lpthread -lPCL-pxi -llz4-pxi -lzlib-pxi -lRFC6234-pxi -llcms-pxi -lcminpack-pxi";
         for ( let i = 0; i < this.extraLibraries.length; ++i )
            s += " -l" + this.extraLibraries[i];
      }

      if ( this.isCore() )
      {
         // System and Qt libraries.
         if ( this.isMacOSXPlatform() )
         {
            // Cocoa-based core application, Qt/Mac >= 5.12.2
            s += " -lz -lidn2 -framework Carbon -framework AppKit -framework ApplicationServices -framework Security" +
                 " -framework DiskArbitration -framework IOKit -framework OpenGL -framework AGL -framework SystemConfiguration" +
                 " -lQt5UiTools -framework QtSensors -framework QtPositioning -framework QtMultimediaWidgets" +
                 " -framework QtMultimedia -framework QtOpenGL -framework QtSql -framework QtQml -framework QtQuick" +
                 " -framework QtWebChannel -framework QtWebEngine -framework QtWebEngineCore -framework QtWebEngineWidgets" +
                 " -framework QtPrintSupport -framework QtMacExtras -framework QtWidgets -framework QtGui -framework QtSvg" +
                 " -framework QtXml -framework QtNetwork -framework QtCore";
         }
         else
         {
            // X11-Linux/FreeBSD core application, Qt/X11 >= 5.12.2
            s += " -lX11 -lpthread -lssl -lcrypto";
            if ( this.isFreeBSDPlatform() )
               s += " -lidn2 -lnghttp2 -lexecinfo";
            s += " -lQt5Sensors -lQt5Positioning -lQt5MultimediaWidgets -lQt5Multimedia -lQt5OpenGL -lQt5Sql" +
                 " -lQt5Qml -lQt5Quick -lQt5WebChannel -lQt5WebEngine -lQt5WebEngineCore -lQt5WebEngineWidgets" +
                 " -lQt5PrintSupport -lQt5X11Extras -lQt5Widgets -lQt5Gui -lQt5Svg -lQt5Xml -lQt5Network -lQt5Core";
         }

         // CUDA runtime on Linux
         if ( !this.isCompatibilityProject() )
            if ( this.isLinuxPlatform() )
               s += " -lcudart";

         // SpiderMonkey >= 1.8.7 since core version 1.8.0.853
         // PixInsight Class Library (PCL)
         // LZ4
         // Zlib
         // RFC6234
         // cURL
         // Little CMS
         s += " -lmozjs" + CORE_JS_ENGINE_VERSION + " -lPCL-pxi -llz4-pxi -lzlib-pxi -lRFC6234-pxi -lcurl-pxi -llcms-pxi";
      }

      if ( this.isCoreAux() )
      {
         if ( this.isMacOSXPlatform() )
            s += " -framework AppKit -framework ApplicationServices" +
                 " -framework DiskArbitration -framework IOKit -framework OpenGL -framework AGL" +
                 " -framework QtWidgets -framework QtGui -framework QtSvg -framework QtCore";
         else
            s += " -lQt5Widgets -lQt5Gui -lQt5Svg -lQt5Core";

         s += " -lPCL-pxi";
         for ( let i = 0; i < this.extraLibraries.length; ++i )
            s += " -l" + this.extraLibraries[i];
      }

      return s;
   };

   this.qtDirectory = function()
   {
      return "$(QTDIR64)";
   };

   this.libDirectory = function()
   {
      return this.isCompatibilityProject() ? "$(PCLLIBDIR64C)" : "$(PCLLIBDIR64)";
   };

   this.binDirectory = function()
   {
      return this.isCompatibilityProject() ? "$(PCLBINDIR64C)" : "$(PCLBINDIR64)";
   };

   this.archToken = function()
   {
      if ( this.isCompatibilityProject() )
         return "x64c";
      return this.architecture;
   };

   this.destinationDirectory = function()
   {
      // Static libraries, e.g. the PCL library.
      if ( this.isStaticLibrary() )
         return this.libDirectory();

      // Installer program on FreeBSD and Linux.
      if ( this.isX11Installer() )
         return this.binDirectory() + "/../..";

      // Core executables inside the application bundle on macOS.
      if ( this.isMacOSXPlatform() )
         if ( this.isCore() || this.isCoreAux() || this.isCoreExecutable() )
            return "$(PCLDIR)/dist/" + this.archToken() + "/PixInsight/" + this.macOSXAppName() + "/Contents/MacOS";

      // Everything else on the bin distribution directory on all platforms.
      return this.binDirectory();
   };
}

// ----------------------------------------------------------------------------
// EOF MakGenParameters.js - Released 2021-12-29T20:18:04Z
