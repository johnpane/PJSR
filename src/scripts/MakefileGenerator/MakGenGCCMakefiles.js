// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// MakGenGCCMakefiles.js - Released 2021-12-29T20:18:04Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Makefile Generator Script version 1.125
//
// Copyright (c) 2009-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Makefile Generator
 *
 * Automatic generation of PCL makefiles and projects for FreeBSD, Linux,
 * macOS and Windows platforms.
 *
 * Copyright (c) 2009-2021, Pleiades Astrophoto S.L. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Generation of makefiles for GCC and Clang compilers.
 */

/*
 * GNU/GCC and Clang makefiles for 64-bit targets.
 */
function GnuCxxAll( F, P )
{
   P.validate();
   P.validateGnuCxxBuild();

   let buildDirectory = P.gccBuildDirectory( F.baseDirectory );

   let makefilePath = buildDirectory + "/Makefile";

   console.writeln( "<end><cbr><br>==> Generating makefile:" );
   console.writeln( makefilePath );
   console.flush();

   let tmp = P.architecture;
   P.architecture = "x64";
   let makefileX64 = P.gccMakefile();
   P.architecture = "arm64";
   let makefileARM64 = P.gccMakefile();
   P.architecture = tmp;

   let hasMakefileX64 = File.exists( buildDirectory + '/' + makefileX64 );
   let hasMakefileARM64 = File.exists( buildDirectory + '/' + makefileARM64 );

   if ( !(hasMakefileX64 || hasMakefileARM64) )
      throw new Error( "Internal: No makefile has been generated before calling GnuCxxAll()" );

   let f = new File;
   f.createForWriting( makefilePath );

   f.outTextLn( "######################################################################" );
   f.outTextLn( "# PixInsight Makefile Generator Script v" + VERSION );
   f.outTextLn( "# Copyright (C) 2009-" + YEAR + " Pleiades Astrophoto" );
   f.outTextLn( "######################################################################" );
   f.outTextLn( "# Generated on .... " + (new Date( Date.now() )).toISOString() );
   f.outTextLn( "# Project id ...... " + P.id );
   f.outTextLn( "# Project type .... " + P.type );
   f.outTextLn( "# Platform ........ " + P.platform + "/g++" + (P.compatibility ? " (compatibility)" : "") );
   f.outTextLn( "# Configuration ... " + (P.gccDebug ? "Debug" : "Release") + "/all" );
   f.outTextLn( "######################################################################" );
   f.outTextLn( '' );

   f.outTextLn( "#" );
   f.outTextLn( "# Targets" );
   f.outTextLn( "#" );
   f.outTextLn( '' );
   f.outTextLn( ".PHONY: all" );
   f.outTextLn( "all: " );
   if ( hasMakefileX64 )
      f.outTextLn( "\t$(MAKE) -f ./" + makefileX64 + " --no-print-directory" );
   if ( hasMakefileARM64 )
      f.outTextLn( "\t$(MAKE) -f ./" + makefileARM64 + " --no-print-directory" );
   f.outTextLn( '' );
   f.outTextLn( ".PHONY: clean" );
   f.outTextLn( "clean:" );
   if ( hasMakefileX64 )
      f.outTextLn( "\t$(MAKE) -f ./" + makefileX64 + " --no-print-directory clean" );
   if ( hasMakefileARM64 )
      f.outTextLn( "\t$(MAKE) -f ./" + makefileARM64 + " --no-print-directory clean" );
   f.outTextLn( '' );

   f.flush();
   f.close();
}

/*
 * GNU/G++ makefile
 */
function GnuCxx( F, P )
{
   P.validate();
   P.validateGnuCxxBuild();

   if ( P.cleanUpPreviousBuilds )
   {
      let stdDir = P.gccStandardBuildDirectory( F.baseDirectory );
      let cmpDir = P.gccCompatibilityBuildDirectory( F.baseDirectory );
      if ( File.directoryExists( stdDir ) )
         removeDirectory( stdDir );
      if ( File.directoryExists( cmpDir ) )
         removeDirectory( cmpDir );
   }

   let buildDirectory = P.gccBuildDirectory( F.baseDirectory );
   let makefilePath = buildDirectory + '/' + P.gccMakefile();

   console.writeln( "<end><cbr><br>==> Generating makefile:" );
   console.writeln( makefilePath );
   console.flush();

   let objectPrefix = P.architecture + (P.gccDebug ? "/Debug" : "/Release");
   let objectDirectory = buildDirectory + '/' + objectPrefix;

   let target = "$(OBJ_DIR)/" + P.mainTarget();

   let sources = new Array;
   let objects = new Array;
   let dependencies = new Array;
   for ( let i = 0; i < F.sources.length; ++i )
   {
      let s = F.sources[i];
      if ( s.hasSourceFiles() )
      {
         let prefix = s.directory;
         let objDir = objectDirectory;
         if ( s.directory.length > 0 )
         {
            prefix += '/';
            objDir += '/' + s.directory;
         }
         createDirectoryIfNotExists( objDir, true );
         File.createFileForWriting( objDir + "/.keep" ).close();

         for ( let j = 0; j < s.files.length; ++j )
            if ( P.isSourceFile( s.files[j] ) )
            {
               sources.push( "../../" + prefix + s.files[j] );
               let n = "./" + objectPrefix + '/' + prefix + File.extractName( s.files[j] );
               objects.push( n + ".o" );
               dependencies.push( n + ".d" );
            }
      }
   }

   let f = new File;
   f.createForWriting( makefilePath );

   f.outTextLn( "######################################################################" );
   f.outTextLn( "# PixInsight Makefile Generator Script v" + VERSION );
   f.outTextLn( "# Copyright (C) 2009-" + YEAR + " Pleiades Astrophoto" );
   f.outTextLn( "######################################################################" );
   f.outTextLn( "# Generated on .... " + (new Date( Date.now() )).toISOString() );
   f.outTextLn( "# Project id ...... " + P.id );
   f.outTextLn( "# Project type .... " + P.type );
   f.outTextLn( "# Platform ........ " + P.platform + "/g++" + (P.compatibility ? " (compatibility)" : "") );
   f.outTextLn( "# Configuration ... " + (P.gccDebug ? "Debug" : "Release") + '/' + P.architecture );
   if ( P.extraDefinitions.length > 0 )
   {
      f.outTextLn( "# --------------------------------------------------------------------" );
      f.outTextLn( "# Additional preprocessor definitions:" );
      for ( let i = 0; i < P.extraDefinitions.length; ++i )
         f.outTextLn( "# " + P.extraDefinitions[i] );
   }
   if ( P.extraIncludeDirs.length > 0 )
   {
      f.outTextLn( "# --------------------------------------------------------------------" );
      f.outTextLn( "# Additional include search directories:" );
      for ( let i = 0; i < P.extraIncludeDirs.length; ++i )
         f.outTextLn( "# " + P.extraIncludeDirs[i] );
   }
   if ( P.extraLibDirs.length > 0 )
   {
      f.outTextLn( "# --------------------------------------------------------------------" );
      f.outTextLn( "# Additional library search directories:" );
      for ( let i = 0; i < P.extraLibDirs.length; ++i )
         f.outTextLn( "# " + P.extraLibDirs[i] );
   }
   if ( P.extraLibraries.length > 0 )
   {
      f.outTextLn( "# --------------------------------------------------------------------" );
      f.outTextLn( "# Additional libraries:" );
      for ( let i = 0; i < P.extraLibraries.length; ++i )
         f.outTextLn( "# " + P.extraLibraries[i] );
   }
   f.outTextLn( "######################################################################" );
   f.outTextLn( '' );

   /*
    * Perform a greedy replacement of relevant environment variables in the
    * OBJ_DIR value. Without this, we would be propagating our own build
    * directories everywhere!
    */
   f.outTextLn( "OBJ_DIR=\"" + objectDirectory.replace(
                                 RegExp( '^' + PCLSRCDIR ), "$(PCLSRCDIR)" ).replace(
                                    RegExp( '^' + PCLDIR ), "$(PCLDIR)" ) + "\"" );
   f.outTextLn( '' );

   f.outTextLn( ".PHONY: all" );
   f.outTextLn( "all: " + target );
   f.outTextLn( '' );

   f.outTextLn( "#" );
   f.outTextLn( "# Source files" );
   f.outTextLn( "#" );
   f.outTextLn( '' );
   f.outTextLn( "SRC_FILES= \\" );
   for ( let i = 0; ; )
   {
      f.outText( sources[i] );
      if ( ++i == sources.length )
         break;
      f.outTextLn( " \\" );
   }
   f.outTextLn( '' );
   f.outTextLn( '' );

   f.outTextLn( "#" );
   f.outTextLn( "# Object files" );
   f.outTextLn( "#" );
   f.outTextLn( '' );
   f.outTextLn( "OBJ_FILES= \\" );
   for ( let i = 0; ; )
   {
      f.outText( objects[i] );
      if ( ++i == objects.length )
         break;
      f.outTextLn( " \\" );
   }
   f.outTextLn( '' );
   f.outTextLn( '' );

   f.outTextLn( "#" );
   f.outTextLn( "# Dependency files" );
   f.outTextLn( "#" );
   f.outTextLn( '' );
   f.outTextLn( "DEP_FILES= \\" );
   for ( let i = 0; ; )
   {
      f.outText( dependencies[i] );
      if ( ++i == dependencies.length )
         break;
      f.outTextLn( " \\" );
   }
   f.outTextLn( '' );
   f.outTextLn( '' );

   f.outTextLn( "#" );
   f.outTextLn( "# Rules" );
   f.outTextLn( "#" );
   f.outTextLn( '' );
   f.outTextLn( "-include $(DEP_FILES)" );
   f.outTextLn( '' );
   f.outTextLn( target + ": $(OBJ_FILES)" );
   f.outTextLn( "\t" + P.gccBuildCommand() );
   f.outTextLn( "\t$(MAKE) -f ./" + P.gccMakefile() + " --no-print-directory post-build" );
   f.outTextLn( '' );
   f.outTextLn( ".PHONY: clean" );
   f.outTextLn( "clean:" );
   f.outTextLn( "\trm -f $(OBJ_FILES) $(DEP_FILES) " + target );
   f.outTextLn( '' );
   f.outTextLn( ".PHONY: post-build" );
   f.outTextLn( "post-build:" );
	f.outTextLn( "\tcp " + target + ' ' + P.destinationDirectory() );
   if ( P.isMacOSXPlatform() )
   {
      let appDir = "$(PCLDIR)/dist/" + P.architecture;
      let appBundle = appDir + "/PixInsight/" + P.macOSXAppName();

      if ( P.isCore() )
      {
         /*
          * Since OS X 10.9.5, all shared objects must be on the Frameworks
          * application bundle directory.
          */
         f.outTextLn( "\tinstall_name_tool" +
                      " -change @executable_path/libmozjs-24.dylib" +
                      " @executable_path/../Frameworks/libmozjs-24.dylib " +
                      appBundle + "/Contents/MacOS/PixInsight" );

         /*
          * Regenerate Qt frameworks and resources.
          */
         f.outTextLn( "\trm -rf " + appBundle + "/Contents/Frameworks/Qt*" );
         f.outTextLn( "\trm -rf " + appBundle + "/Contents/PlugIns" );
         f.outTextLn( "\trm -f " + appBundle + "/Contents/Resources/qt.conf" );
         f.outTextLn( "\tmacdeployqt " + appBundle );
         f.outTextLn( "\tinstall_name_tool -add_rpath @executable_path/../../../../../../../../Frameworks " +
            appBundle + "/Contents/Frameworks/QtWebEngineCore.framework/" +
                        "Helpers/QtWebEngineProcess.app/Contents/MacOS/QtWebEngineProcess" );

         /*
          * Update creation time for the core application bundle.
          * ### N.B.: This must be done *before* signing, as file times are
          * part of the code signature.
          */
         f.outTextLn( "\ttouch " + appBundle );

         /*
          * Since OS X 10.9.5, the --resource-rules argument to codesign is no
          * longer supported. From now on every file inside an application
          * bundle has to be signed without exceptions---even plain text files
          * have to be part of signed code. Fortunately, the --deep argument
          * works fine to simplify things by doing this recursively.
          *
          * The --options=runtime option enables the hardened runtime, which is
          * required for application notarization by Apple since macOS 10.15.
          *
          * The entitlements.plist file modifies default code signing
          * protection policies to allow us execute JIT-compiled code generated
          * dynamically by the JavaScript engine. This is necessary for
          * application notarization since macOS 10.15.3.
          */
         f.outTextLn( "\tcodesign --deep --options=runtime -s pleiades -f "
                    + "--entitlements ../../Components/Application/entitlements.plist -v --timestamp " + appBundle );

         /*
          * The updater1 program requires administrative privileges on macOS
          * since core version 1.8.5.
          * ### N.B.: Since core version 1.8.6.1471, the SUID bit is set by a
          * post-installation script embedded in the installer package.
          */
         //f.outTextLn( "\tchmod +s " + appBundle + "/Contents/MacOS/PixInsightUpdater" );
      }
      else if ( P.isModule() || P.isDynamicLibrary() || P.isExecutable() )
      {
         for ( let i = 0; i < P.extraLibraries.length; ++i )
            if ( !P.extraLibraries[i].startsWith( "Qt" ) )
               for ( let j = 0; j < P.extraLibDirs.length; ++j )
               {
                  let libFile = "lib" + P.extraLibraries[i] + ".dylib";
                  f.outTextLn( "\tinstall_name_tool -change " + P.extraLibDirs[j] + "/" + libFile +
                               " " + "@loader_path/" + libFile +
                               " " + P.destinationDirectory() + "/" + P.mainTarget() );
               }
         /*
          * In official distributions, all modules and executables are
          * digitally signed on macOS and Windows platforms.
          * ### N.B.: Updater executables are already signed within the core
          * application bundle on macOS.
          */
         if ( P.official )
            f.outTextLn( "\tcodesign --deep --options=runtime -s pleiades -f -v --timestamp " + P.destinationDirectory() + "/" + P.mainTarget() );
         else if ( P.signed )
            f.outTextLn( "\tcodesign --deep --options=runtime -s " + P.signingIdentity + " -f -v --timestamp " + P.destinationDirectory() + "/" + P.mainTarget() );
      }
   }
   f.outTextLn( '' );
   if ( F.cppFileCount > 0 )
      for ( let i = 0; i < F.sources.length; ++i )
      {
         let s = F.sources[i];
         if ( s.cppFileCount > 0 )
         {
            let prefix = s.directory;
            if ( s.directory.length > 0 )
               prefix += '/';
            f.outTextLn( "./" + objectPrefix + '/' + prefix + "%.o: " + "../../" + prefix + "%.cpp" );
            f.outTextLn( "\t" + P.gccCxxCompiler() + ' ' + P.gccCompileArgs() );
            f.outTextLn( "\t@echo \' \'" );
         }
      }
   if ( F.cxxFileCount > 0 )
      for ( let i = 0; i < F.sources.length; ++i )
      {
         let s = F.sources[i];
         if ( s.cxxFileCount > 0 )
         {
            let prefix = s.directory;
            if ( s.directory.length > 0 )
               prefix += '/';
            f.outTextLn( "./" + objectPrefix + '/' + prefix + "%.o: " + "../../" + prefix + "%.cxx" );
            f.outTextLn( "\t" + P.gccCxxCompiler() + ' ' + P.gccCompileArgs() );
            f.outTextLn( "\t@echo \' \'" );
         }
      }
   if ( F.cFileCount > 0 )
      for ( let i = 0; i < F.sources.length; ++i )
      {
         let s = F.sources[i];
         if ( s.cFileCount > 0 )
         {
            let prefix = s.directory;
            if ( s.directory.length > 0 )
               prefix += '/';
            f.outTextLn( "./" + objectPrefix + '/' + prefix + "%.o: " + "../../" + prefix + "%.c" );
            f.outTextLn( "\t" + P.gccCCompiler() + ' ' + P.gccCompileArgs( false/*isCpp*/ ) );
            f.outTextLn( "\t@echo \' \'" );
         }
      }
   if ( F.cuFileCount > 0 )
      if ( P.isCUDASupportProject() )
         for ( let i = 0; i < F.sources.length; ++i )
         {
            let s = F.sources[i];
            if ( s.cuFileCount > 0 )
            {
               let prefix = s.directory;
               if ( s.directory.length > 0 )
                  prefix += '/';
               f.outTextLn( "./" + objectPrefix + '/' + prefix + "%.o: " + "../../" + prefix + "%.cu" );
               f.outTextLn( "\t" + P.cudaCCompiler() + ' ' + P.cudaCompileArgs() );
               f.outTextLn( "\t@echo \' \'" );
            }
         }
   if ( F.mmFileCount > 0 )
      for ( let i = 0; i < F.sources.length; ++i )
      {
         let s = F.sources[i];
         if ( s.mmFileCount > 0 )
         {
            let prefix = s.directory;
            if ( s.directory.length > 0 )
               prefix += '/';
            f.outTextLn( "./" + objectPrefix + '/' + prefix + "%.o: " + "../../" + prefix + "%.mm" );
            f.outTextLn( "\t" + P.gccCxxCompiler() + ' ' + P.gccCompileArgs() );
            f.outTextLn( "\t@echo \' \'" );
         }
      }
   f.outTextLn( '' );

   f.flush();
   f.close();
}

// ----------------------------------------------------------------------------
// EOF MakGenGCCMakefiles.js - Released 2021-12-29T20:18:04Z
