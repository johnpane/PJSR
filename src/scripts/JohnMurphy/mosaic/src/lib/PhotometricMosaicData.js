/* global Parameters, View, APERTURE_GROWTH, APERTURE_ADD, APERTURE_GAP, APERTURE_BKG_DELTA, APERTURE_GROWTH_OVERLAP, APERTURE_GROWTH_TARGET, DataType_UCString, Settings, KEYPREFIX, DataType_Float, DataType_Int32, DataType_Boolean, PIXEL_SIZE_DEFAULT, FOCAL_LENGTH_DEFAULT */

// Version 1.0 (c) John Murphy 20th-Oct-2019
//
// ======== #license ===============================================================
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// =================================================================================
//"use strict";
#define KEYPREFIX "PhotometricMosaic"
#define APERTURE_ADD 1
#define APERTURE_GROWTH 0.25
#define APERTURE_GROWTH_OVERLAP 0.25
#define APERTURE_GROWTH_TARGET 1.0
#define APERTURE_GAP 2
#define APERTURE_BKG_DELTA 10
#define LINEAR_RANGE 0.7

// -----------------------------------------------------------------------------
// Form/Dialog data
// -----------------------------------------------------------------------------
function PhotometricMosaicData() {
    // Used to populate the contents of a saved process icon
    // Also used at the end of our script to populate the history entry.
    this.saveParameters = function () {
        Parameters.clear();
        if (this.referenceView.isMainView) {
            Parameters.set("referenceView", this.referenceView.fullId);
        }
        if (this.targetView.isMainView) {
            Parameters.set("targetView", this.targetView.fullId);
        }
        Parameters.set("replaceRefImage", this.replaceRefImage);
        Parameters.set("pixelSize", this.pixelSize);
        Parameters.set("focalLength", this.focalLength);
        
        // Star Detection
        Parameters.set("refLogStarDetection", this.refLogStarDetection);
        Parameters.set("tgtLogStarDetection", this.tgtLogStarDetection);
        
        // Photometric Star Search
        Parameters.set("starFluxTolerance", this.starFluxTolerance);
        Parameters.set("starSearchRadius", this.starSearchRadius);
        
        // Photometric Scale
        Parameters.set("apertureGrowthRate", this.apertureGrowthRate);
        Parameters.set("apertureAdd", this.apertureAdd);
        Parameters.set("apertureGap", this.apertureGap);
        Parameters.set("apertureBgDelta", this.apertureBgDelta);
        Parameters.set("limitPhotoStarsPercent", this.limitPhotoStarsPercent);
        Parameters.set("linearRangeRef", this.linearRangeRef);
        Parameters.set("linearRangeTgt", this.linearRangeTgt);
        Parameters.set("outlierRemovalPercent", this.outlierRemovalPercent);
        Parameters.set("useAutoPhotometry", this.useAutoPhotometry);
        
        // Join Region
        Parameters.set("joinSize", this.joinSize);
        Parameters.set("joinPosition", this.joinPosition);
        Parameters.set("joinOutlierPercent", this.joinOutlierPercent);
        
        // Replace/Update Region
        Parameters.set("cropTargetPreviewLeft", this.cropTargetPreviewRect.x0);
        Parameters.set("cropTargetPreviewTop", this.cropTargetPreviewRect.y0);
        Parameters.set("cropTargetPreviewWidth", this.cropTargetPreviewRect.width);
        Parameters.set("cropTargetPreviewHeight", this.cropTargetPreviewRect.height);
        Parameters.set("useCropTargetToReplaceRegion", this.useCropTargetToReplaceRegion);
        
        // Gradient Sample Generation
        Parameters.set("sampleStarGrowthRate", this.sampleStarGrowthRate);
        Parameters.set("sampleStarGrowthRateTarget", this.sampleStarGrowthRateTarget);
        Parameters.set("limitSampleStarsPercent", this.limitSampleStarsPercent);
        Parameters.set("sampleSize", this.sampleSize);
        Parameters.set("maxSamples", this.maxSamples);
        Parameters.set("useAutoSampleGeneration", this.useAutoSampleGeneration);
        for (let i=0; i<this.manualRejectionCircles.length; i++){
            let mrc = this.manualRejectionCircles[i];
            Parameters.set("manualRejectionCircle" + i + "_x", mrc.x);
            Parameters.set("manualRejectionCircle" + i + "_y", mrc.y);
            Parameters.set("manualRejectionCircle" + i + "_overlapRadius", mrc.overlapRadius);
            Parameters.set("manualRejectionCircle" + i + "_targetRadius", mrc.targetRadius);
        }
        
        // Adjust Scale
        Parameters.set("adjustScale0", this.adjustScale[0]);
        Parameters.set("adjustScale1", this.adjustScale[1]);
        Parameters.set("adjustScale2", this.adjustScale[2]);
        Parameters.set("adjustScaleLineOffset", this.adjustScaleLineOffset);
        
        // Gradient Correction (Overlap region)
        Parameters.set("overlapGradientSmoothness", this.overlapGradientSmoothness);
        Parameters.set("taperLength", this.taperLength);
        Parameters.set("useAutoTaperLength", this.useAutoTaperLength);
        
        // Gradient Correction (Target image)
        Parameters.set("useTargetGradientCorrection", this.useTargetGradientCorrection);
        Parameters.set("targetGradientSmoothness", this.targetGradientSmoothness);
        
        // Mosaic Star Mask
        Parameters.set("limitMaskStarsPercent", this.limitMaskStarsPercent);
        Parameters.set("maskStarGrowthRate", this.maskStarGrowthRate);
        Parameters.set("maskStarGrowthLimit", this.maskStarGrowthLimit);
        Parameters.set("maskStarRadiusAdd", this.maskStarRadiusAdd);
        Parameters.set("useAutoMaskStarSize", this.useAutoMaskStarSize);
        
        // Mosaic Join Mode
        Parameters.set("useMosaicOverlay", this.useMosaicOverlay);
        Parameters.set("useMosaicRandom", this.useMosaicRandom);
        Parameters.set("useMosaicAverage", this.useMosaicAverage);
        Parameters.set("createJoinMask", this.createJoinMask);
        
        Parameters.set("smallScreen", this.smallScreen);
        Parameters.set("graphWidth", this.graphWidth);
        Parameters.set("graphHeight", this.graphHeight);
        Parameters.set("extraControls", EXTRA_CONTROLS);
    };

    // Reload our script's data from a process icon
    this.loadParameters = function () {
        if (Parameters.has("referenceView")) {
            let viewId = Parameters.getString("referenceView");
            this.referenceView = View.viewById(viewId);
        }
        if (Parameters.has("targetView")) {
            let viewId = Parameters.getString("targetView");
            this.targetView = View.viewById(viewId);
        }
        if (Parameters.has("replaceRefImage"))
            this.replaceRefImage = Parameters.getBoolean("replaceRefImage");
        
        this.hasPixelScale = true;
        if (Parameters.has("pixelSize")) {
            this.pixelSize = Parameters.getReal("pixelSize");
        } else {
            this.hasPixelScale = false;
        }
        if (Parameters.has("focalLength")) {
            this.focalLength = Math.round(Parameters.getReal("focalLength"));
        } else {
            this.hasPixelScale = false;
        }
        
        // Star Detection
        if (Parameters.has("refLogStarDetection"))
            this.refLogStarDetection = Parameters.getReal("refLogStarDetection");
        if (Parameters.has("tgtLogStarDetection"))
            this.tgtLogStarDetection = Parameters.getReal("tgtLogStarDetection");
        
        // Photometric Star Search
        if (Parameters.has("starFluxTolerance"))
            this.starFluxTolerance = Parameters.getReal("starFluxTolerance");
        if (Parameters.has("starSearchRadius"))
            this.starSearchRadius = Parameters.getReal("starSearchRadius");
        
        // Photometric Scale
        if (Parameters.has("apertureGrowthRate"))
            this.apertureGrowthRate = Parameters.getReal("apertureGrowthRate");
        if (Parameters.has("apertureAdd"))
            this.apertureAdd = Parameters.getInteger("apertureAdd");
        if (Parameters.has("apertureGap"))
            this.apertureGap = Parameters.getInteger("apertureGap");
        if (Parameters.has("apertureBgDelta"))
            this.apertureBgDelta = Parameters.getInteger("apertureBgDelta");
        if (Parameters.has("limitPhotoStarsPercent"))
            this.limitPhotoStarsPercent = Parameters.getReal("limitPhotoStarsPercent");
        if (Parameters.has("linearRangeRef"))
            this.linearRangeRef = Parameters.getReal("linearRangeRef");
        if (Parameters.has("linearRangeTgt"))
            this.linearRangeTgt = Parameters.getReal("linearRangeTgt");
        if (Parameters.has("outlierRemovalPercent"))
            this.outlierRemovalPercent = Parameters.getReal("outlierRemovalPercent");
        if (Parameters.has("useAutoPhotometry"))
            this.useAutoPhotometry = Parameters.getBoolean("useAutoPhotometry");
        
        // Join Region
        if (Parameters.has("joinSize"))
            this.joinSize = Parameters.getReal("joinSize");
        if (Parameters.has("joinPosition"))
            this.joinPosition = Parameters.getInteger("joinPosition");
        if (Parameters.has("joinOutlierPercent"))
            this.joinOutlierPercent = Parameters.getReal("joinOutlierPercent");
        
        // Replace/Update Region
        {
            let x = 0;
            let y = 0;
            let w = 1;
            let h = 1;
            if (Parameters.has("cropTargetPreviewLeft")){
                x = Parameters.getInteger("cropTargetPreviewLeft");
            }
            if (Parameters.has("cropTargetPreviewTop")){
                y = Parameters.getInteger("cropTargetPreviewTop");
            }
            if (Parameters.has("cropTargetPreviewWidth")){
                w = Parameters.getInteger("cropTargetPreviewWidth");
            }
            if (Parameters.has("cropTargetPreviewHeight")){
                h = Parameters.getInteger("cropTargetPreviewHeight");
            }
            this.cropTargetPreviewRect = new Rect(x, y, x + w, y + h);
            
            if (Parameters.has("useCropTargetToReplaceRegion"))
                this.useCropTargetToReplaceRegion = Parameters.getBoolean("useCropTargetToReplaceRegion");
        }
        
        // Gradient Sample Generation
        if (Parameters.has("sampleStarGrowthRate"))
            this.sampleStarGrowthRate = Parameters.getReal("sampleStarGrowthRate");
        if (Parameters.has("sampleStarGrowthRateTarget"))
            this.sampleStarGrowthRateTarget = Parameters.getReal("sampleStarGrowthRateTarget");
        if (Parameters.has("limitSampleStarsPercent"))
            this.limitSampleStarsPercent = Parameters.getReal("limitSampleStarsPercent");
        if (Parameters.has("sampleSize"))
            this.sampleSize = Parameters.getInteger("sampleSize");
        if (Parameters.has("maxSamples"))
            this.maxSamples = Parameters.getInteger("maxSamples");
        if (Parameters.has("useAutoSampleGeneration"))
            this.useAutoSampleGeneration = Parameters.getBoolean("useAutoSampleGeneration");
        for (let i=0; ; i++){
            if (Parameters.has("manualRejectionCircle" + i + "_x") &&
                    Parameters.has("manualRejectionCircle" + i + "_y") &&
                    Parameters.has("manualRejectionCircle" + i + "_overlapRadius") &&
                    Parameters.has("manualRejectionCircle" + i + "_targetRadius")){
                
                let x = Parameters.getReal("manualRejectionCircle" + i + "_x");
                let y = Parameters.getReal("manualRejectionCircle" + i + "_y");
                let overlapR = Parameters.getInteger("manualRejectionCircle" + i + "_overlapRadius");
                let targetR = Parameters.getInteger("manualRejectionCircle" + i + "_targetRadius");
                this.manualRejectionCircles.push(new ManualRejectionCircle(x, y, overlapR, targetR));
            } else {
                break;
            }
        }
        
        // Adjust Scale
        if (Parameters.has("adjustScale0"))
            this.adjustScale[0] = Parameters.getReal("adjustScale0");
        if (Parameters.has("adjustScale1"))
            this.adjustScale[1] = Parameters.getReal("adjustScale1");
        if (Parameters.has("adjustScale2"))
            this.adjustScale[2] = Parameters.getReal("adjustScale2");
        if (Parameters.has("adjustScaleLineOffset"))
            this.adjustScaleLineOffset = Parameters.getInteger("adjustScaleLineOffset");
        
        // Gradient Correction (Overlap region)
        if (Parameters.has("overlapGradientSmoothness"))
            this.overlapGradientSmoothness = Parameters.getReal("overlapGradientSmoothness");
        if (Parameters.has("taperLength"))
            this.taperLength = Parameters.getInteger("taperLength");
        if (Parameters.has("useAutoTaperLength"))
            this.useAutoTaperLength = Parameters.getBoolean("useAutoTaperLength");
        
        // Gradient Correction (Target image)
        if (Parameters.has("useTargetGradientCorrection"))
            this.useTargetGradientCorrection = Parameters.getBoolean("useTargetGradientCorrection");
        if (Parameters.has("targetGradientSmoothness"))
            this.targetGradientSmoothness = Parameters.getReal("targetGradientSmoothness");
        
        // Mosaic Star Mask
        if (Parameters.has("limitMaskStarsPercent"))
            this.limitMaskStarsPercent = Parameters.getReal("limitMaskStarsPercent");
        if (Parameters.has("maskStarGrowthRate"))
            this.maskStarGrowthRate = Parameters.getReal("maskStarGrowthRate");
        if (Parameters.has("maskStarGrowthLimit"))
            this.maskStarGrowthLimit = Parameters.getInteger("maskStarGrowthLimit");
        if (Parameters.has("maskStarRadiusAdd"))
            this.maskStarRadiusAdd = Parameters.getReal("maskStarRadiusAdd");
        if (Parameters.has("useAutoMaskStarSize"))
            this.useAutoMaskStarSize = Parameters.getBoolean("useAutoMaskStarSize");
        
        // Mosaic Join Mode
        if (Parameters.has("useMosaicOverlay"))
            this.useMosaicOverlay = Parameters.getBoolean("useMosaicOverlay");
        if (Parameters.has("useMosaicRandom"))
            this.useMosaicRandom = Parameters.getBoolean("useMosaicRandom");
        if (Parameters.has("useMosaicAverage"))
            this.useMosaicAverage = Parameters.getBoolean("useMosaicAverage");
        if (Parameters.has("createJoinMask"))
            this.createJoinMask = Parameters.getBoolean("createJoinMask");
        
        if (Parameters.has("smallScreen"))
            this.smallScreen = Parameters.getBoolean("smallScreen");
        if (Parameters.has("graphWidth"))
            this.graphWidth = Parameters.getInteger("graphWidth");
        if (Parameters.has("graphHeight"))
            this.graphHeight = Parameters.getInteger("graphHeight");
        
        if (Parameters.has("extraControls")){
            EXTRA_CONTROLS = Parameters.getBoolean("extraControls");
        }

    };

    // Initialise the scripts data
    this.setParameters = function () {
        this.pixelSize = PIXEL_SIZE_DEFAULT;
        this.focalLength = FOCAL_LENGTH_DEFAULT;
        this.hasPixelScale = false;
        
        this.referenceView = new View();
        this.targetView = new View();
        this.replaceRefImage = true;
        
        // Star Detection
        this.refLogStarDetection = -1;
        this.tgtLogStarDetection = -1;
        
        // Photometric Star Search
        this.starFluxTolerance = 1.5;
        this.starSearchRadius = 2.5;
        
        // Photometric Scale
        this.apertureGrowthRate = APERTURE_GROWTH;
        this.apertureAdd = APERTURE_ADD;
        this.apertureGap = APERTURE_GAP;
        this.apertureBgDelta = APERTURE_BKG_DELTA;
        this.limitPhotoStarsPercent = 100;
        this.outlierRemovalPercent = 2;
        this.useAutoPhotometry = true;
        this.adjustScale = [];
        this.adjustScale[0] = 1;
        this.adjustScale[1] = 1;
        this.adjustScale[2] = 1;
        this.adjustScaleLineOffset = 0;
        
        // Join Region
        this.joinSize = 90;
        this.joinPosition = 0;
        this.joinOutlierPercent = 2.0;
        
        // Replace/Update Region
        this.cropTargetPreviewRect = new Rect(0, 0, 0, 0);
        this.useCropTargetToReplaceRegion = false;
        
        // Gradient Sample Generation
        this.sampleStarGrowthRate = APERTURE_GROWTH_OVERLAP;
        this.sampleStarGrowthRateTarget = APERTURE_GROWTH_TARGET;
        this.limitSampleStarsPercent = 35;
        this.sampleSize = 20;
        this.maxSamples = 3000;
        this.useAutoSampleGeneration = true;
        this.manualRejectionCircles = [];
        
        // Gradient Correction (Overlap region)
        this.overlapGradientSmoothness = -1;
        this.taperLength = 200;
        this.useAutoTaperLength = true;
        
        // Gradient Correction (Target image)
        this.useTargetGradientCorrection = true;
        this.targetGradientSmoothness = 2;
        
        // Mosaic Star Mask
        this.limitMaskStarsPercent = 10;
        this.maskStarGrowthRate = APERTURE_GROWTH;
        this.maskStarGrowthLimit = 400;
        this.maskStarRadiusAdd = APERTURE_ADD + 3;
        this.useAutoMaskStarSize = true;
        
        // Mosaic Join Mode
        this.useMosaicOverlay = true;
        this.useMosaicRandom = false;
        this.useMosaicAverage = false;
        this.createJoinMask = true;
        
        this.graphWidth = 1200; // gradient and photometry graph width
        this.graphHeight = 800; // gradient and photometry graph height
        
        this.smallScreen = false;
        
        if (this.cache !== undefined){
            this.cache.invalidate();
        }
        this.cache = new MosaicCache();
        this.cache.setUserInputData(this.referenceView.fullId, this.targetView.fullId, 
                this.refLogStarDetection, this.tgtLogStarDetection);
        this.linearRangeRef = this.cache.getLinearRangeRef();
        this.linearRangeTgt = this.cache.getLinearRangeTgt();
    };

    // Used when the user presses the reset button
    this.resetParameters = function (photometricMosaicDialog) {
        // Reset the script's data
        this.setParameters();
        photometricMosaicDialog.referenceImage_ViewList.currentView = this.referenceView;
        photometricMosaicDialog.targetImage_ViewList.currentView = this.targetView;
        photometricMosaicDialog.setPixelScaleFields();
        photometricMosaicDialog.replaceRefCheckBox.checked = this.replaceRefImage;
        
        // Star Detection
        photometricMosaicDialog.refLogStarDetection_Control.setValue(this.refLogStarDetection);
        photometricMosaicDialog.tgtLogStarDetection_Control.setValue(this.tgtLogStarDetection);
        
        // Photometric Star Search
        photometricMosaicDialog.starFluxTolerance_Control.setValue(this.starFluxTolerance);
        photometricMosaicDialog.starSearchRadius_Control.setValue(this.starSearchRadius);
        
        // Photometric Scale
        photometricMosaicDialog.apertureGrowthRate_Control.setValue(this.apertureGrowthRate);
        photometricMosaicDialog.apertureAdd_Control.setValue(this.apertureAdd);
        photometricMosaicDialog.apertureGap_Control.setValue(this.apertureGap);
        photometricMosaicDialog.apertureBgDelta_Control.setValue(this.apertureBgDelta);
        photometricMosaicDialog.limitPhotoStarsPercent_Control.setValue(this.limitPhotoStarsPercent);
        photometricMosaicDialog.linearRangeRef_Control.setValue(this.linearRangeRef);
        photometricMosaicDialog.linearRangeTgt_Control.setValue(this.linearRangeTgt);
        photometricMosaicDialog.outlierRemoval_Control.setValue(this.outlierRemovalPercent);
        photometricMosaicDialog.setPhotometryAutoValues(this.useAutoPhotometry);
        
        // Mosaic Join Mode
        photometricMosaicDialog.mosaicOverlay_Control.checked = this.useMosaicOverlay;
        photometricMosaicDialog.mosaicRandom_Control.checked = this.useMosaicRandom;
        photometricMosaicDialog.mosaicAverage_Control.checked = this.useMosaicAverage;
        photometricMosaicDialog.joinOutlier_Control.setValue(this.joinOutlierPercent);
        photometricMosaicDialog.joinMask_CheckBox.checked = this.createJoinMask;
        
        // Join Region
        photometricMosaicDialog.joinSize_Control.setValue(this.joinSize);
        photometricMosaicDialog.joinPosition_Control.setValue(this.joinPosition);
        
        // Replace/Update Region
        photometricMosaicDialog.rectangleX0_Control.setValue(this.cropTargetPreviewRect.x0);
        photometricMosaicDialog.rectangleY0_Control.setValue(this.cropTargetPreviewRect.y0);
        photometricMosaicDialog.rectangleWidth_Control.setValue(this.cropTargetPreviewRect.width);
        photometricMosaicDialog.rectangleHeight_Control.setValue(this.cropTargetPreviewRect.height);
        photometricMosaicDialog.previewImage_ViewList.currentView = new View();
        photometricMosaicDialog.enableReplaceUpdateRegion(this.useCropTargetToReplaceRegion);
        
        // Gradient Sample Generation
        photometricMosaicDialog.sampleStarGrowthRate_Control.setValue(this.sampleStarGrowthRate);
        photometricMosaicDialog.sampleStarGrowthRateTarget_Control.setValue(this.sampleStarGrowthRateTarget);
        photometricMosaicDialog.limitSampleStarsPercent_Control.setValue(this.limitSampleStarsPercent);
        photometricMosaicDialog.sampleSize_Control.setValue(this.sampleSize);
        if (EXTRA_CONTROLS){
            photometricMosaicDialog.maxSamples_Control.setValue(this.maxSamples);
        }
        photometricMosaicDialog.setSampleGenerationAutoValues(this.useAutoSampleGeneration);
        
        // Adjust Scale
        photometricMosaicDialog.resetAdjustScaleControls();
        
        // Gradient Correction (Overlap region)
        photometricMosaicDialog.overlapGradientSmoothness_Control.setValue(this.overlapGradientSmoothness);
        photometricMosaicDialog.taperLength_Control.setValue(this.taperLength);
        photometricMosaicDialog.autoTaperLengthCheckBox.checked = this.useAutoTaperLength;
        photometricMosaicDialog.setTaperLengthAutoValue(this);
        
        // Gradient Correction (Target image)
        photometricMosaicDialog.targetGradientSmoothness_Control.setValue(this.targetGradientSmoothness);
        photometricMosaicDialog.setTargetGradientFlag(this.useTargetGradientCorrection);
        
        photometricMosaicDialog.smallScreenToggle.checked = this.smallScreen;
    };
    
    // Initialise the script's data
    this.setParameters();
}

/**
 * Save all script parameters as settings keys.
 * @param {PhotometricMosaicData} data 
 */
function saveSettings(data){
    resetSettings();
    if (data.referenceView.isMainView) {
        Settings.write( KEYPREFIX+"/referenceView", DataType_UCString, data.referenceView.fullId);
    }
    if (data.targetView.isMainView) {
        Settings.write( KEYPREFIX+"/targetView", DataType_UCString, data.targetView.fullId);
    }
    
    Settings.write( KEYPREFIX+"/replaceRefImage", DataType_Boolean, data.replaceRefImage );
    Settings.write( KEYPREFIX+"/pixelSize", DataType_Float, data.pixelSize );
    Settings.write( KEYPREFIX+"/focalLength", DataType_Int32, data.focalLength );
    
    // Star Detection
    Settings.write( KEYPREFIX+"/refLogStarDetection", DataType_Float, data.refLogStarDetection );
    Settings.write( KEYPREFIX+"/tgtLogStarDetection", DataType_Float, data.tgtLogStarDetection );
    
        // Photometric Star Search
    Settings.write( KEYPREFIX+"/starFluxTolerance", DataType_Float, data.starFluxTolerance );
    Settings.write( KEYPREFIX+"/starSearchRadius", DataType_Float, data.starSearchRadius );

    // Photometric Scale
    Settings.write( KEYPREFIX+"/apertureGrowthRate", DataType_Float, data.apertureGrowthRate );
    Settings.write( KEYPREFIX+"/apertureAdd", DataType_Int32, data.apertureAdd );
    Settings.write( KEYPREFIX+"/apertureGap", DataType_Int32, data.apertureGap );
    Settings.write( KEYPREFIX+"/apertureBgDelta", DataType_Int32, data.apertureBgDelta );
    Settings.write( KEYPREFIX+"/limitPhotoStarsPercent", DataType_Float, data.limitPhotoStarsPercent );
    Settings.write( KEYPREFIX+"/linearRangeRef", DataType_Float, data.linearRangeRef );
    Settings.write( KEYPREFIX+"/linearRangeTgt", DataType_Float, data.linearRangeTgt );
    Settings.write( KEYPREFIX+"/outlierRemovalPercent", DataType_Float, data.outlierRemovalPercent );
    Settings.write( KEYPREFIX+"/useAutoPhotometry", DataType_Boolean, data.useAutoPhotometry );

    // Join Region
    Settings.write( KEYPREFIX+"/joinSize", DataType_Float, data.joinSize );
    Settings.write( KEYPREFIX+"/joinPosition", DataType_Int32, data.joinPosition );
    Settings.write( KEYPREFIX+"/joinOutlierPercent", DataType_Float, data.joinOutlierPercent );
    
    // Replace/Update Region
    Settings.write( KEYPREFIX+"/cropTargetPreviewLeft", DataType_Int32, data.cropTargetPreviewRect.x0);
    Settings.write( KEYPREFIX+"/cropTargetPreviewTop", DataType_Int32, data.cropTargetPreviewRect.y0);
    Settings.write( KEYPREFIX+"/cropTargetPreviewWidth", DataType_Int32, data.cropTargetPreviewRect.width);
    Settings.write( KEYPREFIX+"/cropTargetPreviewHeight", DataType_Int32, data.cropTargetPreviewRect.height);
    Settings.write( KEYPREFIX+"/useCropTargetToReplaceRegion", DataType_Boolean, data.useCropTargetToReplaceRegion);
    
    // Gradient Sample Generation
    Settings.write( KEYPREFIX+"/sampleStarGrowthRate", DataType_Float, data.sampleStarGrowthRate );
    Settings.write( KEYPREFIX+"/sampleStarGrowthRateTarget", DataType_Float, data.sampleStarGrowthRateTarget );
    Settings.write( KEYPREFIX+"/limitSampleStarsPercent", DataType_Float, data.limitSampleStarsPercent );
    Settings.write( KEYPREFIX+"/sampleSize", DataType_Int32, data.sampleSize );
    Settings.write( KEYPREFIX+"/extraControls", DataType_Boolean, EXTRA_CONTROLS );
    if (EXTRA_CONTROLS){
        Settings.write( KEYPREFIX+"/maxSamples", DataType_Int32, data.maxSamples );
    }
    Settings.write( KEYPREFIX+"/useAutoSampleGeneration", DataType_Boolean, data.useAutoSampleGeneration );
    for (let i=0; i<data.manualRejectionCircles.length; i++){
        let mrc = data.manualRejectionCircles[i];
        Settings.write( KEYPREFIX+"/manualRejectionCircle" + i + "_x", DataType_Float, mrc.x);
        Settings.write( KEYPREFIX+"/manualRejectionCircle" + i + "_y", DataType_Float, mrc.y);
        Settings.write( KEYPREFIX+"/manualRejectionCircle" + i + "_overlapRadius", DataType_Int32, mrc.overlapRadius);
        Settings.write( KEYPREFIX+"/manualRejectionCircle" + i + "_targetRadius", DataType_Int32, mrc.targetRadius);
    }
        
    // Adjust Scale
    Settings.write( KEYPREFIX+"/adjustScale0", DataType_Float, data.adjustScale[0]);
    Settings.write( KEYPREFIX+"/adjustScale1", DataType_Float, data.adjustScale[1]);
    Settings.write( KEYPREFIX+"/adjustScale2", DataType_Float, data.adjustScale[2]);
    Settings.write( KEYPREFIX+"/adjustScaleLineOffset", DataType_Int32, data.adjustScaleLineOffset);
    
    // Gradient Correction (Overlap region)
    Settings.write( KEYPREFIX+"/overlapGradientSmoothness", DataType_Float, data.overlapGradientSmoothness );
    Settings.write( KEYPREFIX+"/taperLength", DataType_Int32, data.taperLength );
    Settings.write( KEYPREFIX+"/useAutoTaperLength", DataType_Boolean, data.useAutoTaperLength );
    
    // Gradient Correction (Target image)
    Settings.write( KEYPREFIX+"/useTargetGradientCorrection", DataType_Boolean, data.useTargetGradientCorrection );
    Settings.write( KEYPREFIX+"/targetGradientSmoothness", DataType_Float, data.targetGradientSmoothness );
    
    // Mosaic Star Mask
    Settings.write( KEYPREFIX+"/limitMaskStarsPercent", DataType_Float, data.limitMaskStarsPercent );
    Settings.write( KEYPREFIX+"/maskStarGrowthRate", DataType_Float, data.maskStarGrowthRate );
    Settings.write( KEYPREFIX+"/maskStarGrowthLimit", DataType_Int32, data.maskStarGrowthLimit );
    Settings.write( KEYPREFIX+"/maskStarRadiusAdd", DataType_Float, data.maskStarRadiusAdd );
    Settings.write( KEYPREFIX+"/useAutoMaskStarSize", DataType_Boolean, data.useAutoMaskStarSize );
    
    // Mosaic Join Mode
    Settings.write( KEYPREFIX+"/useMosaicOverlay", DataType_Boolean, data.useMosaicOverlay );
    Settings.write( KEYPREFIX+"/useMosaicRandom", DataType_Boolean, data.useMosaicRandom );
    Settings.write( KEYPREFIX+"/useMosaicAverage", DataType_Boolean, data.useMosaicAverage );
    Settings.write( KEYPREFIX+"/createJoinMask", DataType_Boolean, data.createJoinMask );
    
    Settings.write( KEYPREFIX+"/smallScreen", DataType_Boolean, data.smallScreen );
    
    Settings.write( KEYPREFIX+"/graphWidth", DataType_Int32, data.graphWidth );
    Settings.write( KEYPREFIX+"/graphHeight", DataType_Int32, data.graphHeight );
    
    console.writeln("\nSaved settings");
}

// A function to delete all previously stored settings keys for this script.
function resetSettings(){
   Settings.remove( KEYPREFIX );
}

/**
 * Restore all script parameters from settings keys.
 * @param {PhotometricMosaicData} data 
 */
function restoreSettings(data){
    let keyValue;
    keyValue = Settings.read( KEYPREFIX+"/referenceView", DataType_UCString );
    if ( Settings.lastReadOK ){
        let viewId = keyValue;
        data.referenceView = View.viewById(viewId);
    }
    keyValue = Settings.read( KEYPREFIX+"/targetView", DataType_UCString );
    if ( Settings.lastReadOK ){
        let viewId = keyValue;
        data.targetView = View.viewById(viewId);
    }
    
    keyValue = Settings.read( KEYPREFIX+"/replaceRefImage", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.replaceRefImage = keyValue;
    
    data.hasPixelScale = true;
    keyValue = Settings.read( KEYPREFIX+"/pixelSize", DataType_Float );
    if ( Settings.lastReadOK ){
        data.pixelSize = keyValue;
    } else {
        data.hasPixelScale = false;
    }
    keyValue = Settings.read( KEYPREFIX+"/focalLength", DataType_Int32 );
    if ( Settings.lastReadOK ){
        data.focalLength = keyValue;
    } else {
        data.hasPixelScale = false;
    }
    
    // Star Detection
    keyValue = Settings.read( KEYPREFIX+"/refLogStarDetection", DataType_Float );
    if ( Settings.lastReadOK )
        data.refLogStarDetection = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/tgtLogStarDetection", DataType_Float );
    if ( Settings.lastReadOK )
        data.tgtLogStarDetection = keyValue;
    
    // Photometric Star Search
    keyValue = Settings.read( KEYPREFIX+"/starFluxTolerance", DataType_Float );
    if ( Settings.lastReadOK )
        data.starFluxTolerance = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/starSearchRadius", DataType_Float );
    if ( Settings.lastReadOK )
        data.starSearchRadius = keyValue;
    
    // Photometric Scale
    keyValue = Settings.read( KEYPREFIX+"/apertureGrowthRate", DataType_Float );
    if ( Settings.lastReadOK )
        data.apertureGrowthRate = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/apertureAdd", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.apertureAdd = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/apertureGap", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.apertureGap = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/apertureBgDelta", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.apertureBgDelta = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/limitPhotoStarsPercent", DataType_Float );
    if ( Settings.lastReadOK )
        data.limitPhotoStarsPercent = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/linearRangeRef", DataType_Float );
    if ( Settings.lastReadOK )
        data.linearRangeRef = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/linearRangeTgt", DataType_Float );
    if ( Settings.lastReadOK )
        data.linearRangeTgt = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/outlierRemovalPercent", DataType_Float );
    if ( Settings.lastReadOK )
        data.outlierRemovalPercent = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/useAutoPhotometry", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useAutoPhotometry = keyValue;
    
    // Join Region
    keyValue = Settings.read( KEYPREFIX+"/joinSize", DataType_Float );
    if ( Settings.lastReadOK )
        data.joinSize = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/joinPosition", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.joinPosition = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/joinOutlierPercent", DataType_Float );
    if ( Settings.lastReadOK )
        data.joinOutlierPercent = keyValue;
    
    // Replace/Update Region
    {
        let x = 0;
        let y = 0;
        let w = 1;
        let h = 1;
        keyValue = Settings.read( KEYPREFIX+"/cropTargetPreviewLeft", DataType_Int32 );
        if ( Settings.lastReadOK )
            x = keyValue;
        keyValue = Settings.read( KEYPREFIX+"/cropTargetPreviewTop", DataType_Int32 );
        if ( Settings.lastReadOK )
            y = keyValue;
        keyValue = Settings.read( KEYPREFIX+"/cropTargetPreviewWidth", DataType_Int32 );
        if ( Settings.lastReadOK )
            w = keyValue;
        keyValue = Settings.read( KEYPREFIX+"/cropTargetPreviewHeight", DataType_Int32 );
        if ( Settings.lastReadOK )
            h = keyValue;
        data.cropTargetPreviewRect = new Rect(x, y, x + w, y + h);

        keyValue = Settings.read( KEYPREFIX+"/useCropTargetToReplaceRegion", DataType_Boolean );
        if ( Settings.lastReadOK )
            data.useCropTargetToReplaceRegion = keyValue;
    }

    // Gradient Sample Generation
    keyValue = Settings.read( KEYPREFIX+"/sampleStarGrowthRate", DataType_Float );
    if ( Settings.lastReadOK )
        data.sampleStarGrowthRate = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/sampleStarGrowthRateTarget", DataType_Float );
    if ( Settings.lastReadOK )
        data.sampleStarGrowthRateTarget = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/limitSampleStarsPercent", DataType_Float );
    if ( Settings.lastReadOK )
        data.limitSampleStarsPercent = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/sampleSize", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.sampleSize = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/extraControls", DataType_Boolean );
    if ( Settings.lastReadOK )
        EXTRA_CONTROLS = keyValue;
    if (EXTRA_CONTROLS){
        keyValue = Settings.read( KEYPREFIX+"/maxSamples", DataType_Int32 );
        if ( Settings.lastReadOK )
            data.maxSamples = keyValue;
    }
    keyValue = Settings.read( KEYPREFIX+"/useAutoSampleGeneration", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useAutoSampleGeneration = keyValue;
    for (let i=0; ; i++){
        let x, y, overlapR, targetR;
        keyValue = Settings.read( KEYPREFIX+"/manualRejectionCircle" + i + "_x", DataType_Float );
        if ( Settings.lastReadOK )
            x = keyValue;
        keyValue = Settings.read( KEYPREFIX+"/manualRejectionCircle" + i + "_y", DataType_Float );
        if ( Settings.lastReadOK )
            y = keyValue;
        keyValue = Settings.read( KEYPREFIX+"/manualRejectionCircle" + i + "_overlapRadius", DataType_Int32 );
        if ( Settings.lastReadOK )
            overlapR = keyValue;
        keyValue = Settings.read( KEYPREFIX+"/manualRejectionCircle" + i + "_targetRadius", DataType_Int32 );
        if ( Settings.lastReadOK )
            targetR = keyValue;
        
        if (x && y && overlapR && targetR){
            data.manualRejectionCircles.push(new ManualRejectionCircle(x, y, overlapR, targetR));
        } else {
            break;
        }
    }
    
    // Adjust Scale
    keyValue = Settings.read( KEYPREFIX+"/adjustScale0", DataType_Float );
    if ( Settings.lastReadOK )
        data.adjustScale[0] = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/adjustScale1", DataType_Float );
    if ( Settings.lastReadOK )
        data.adjustScale[1] = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/adjustScale2", DataType_Float );
    if ( Settings.lastReadOK )
        data.adjustScale[2] = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/adjustScaleLineOffset", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.adjustScaleLineOffset = keyValue;
    
    // Gradient Correction (Overlap region)
    keyValue = Settings.read( KEYPREFIX+"/overlapGradientSmoothness", DataType_Float );
    if ( Settings.lastReadOK )
        data.overlapGradientSmoothness = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/taperLength", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.taperLength = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/useAutoTaperLength", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useAutoTaperLength = keyValue;
    
    // Gradient Correction (Target image)
    keyValue = Settings.read( KEYPREFIX+"/useTargetGradientCorrection", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useTargetGradientCorrection = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/targetGradientSmoothness", DataType_Float );
    if ( Settings.lastReadOK )
        data.targetGradientSmoothness = keyValue;
    
    // Mosaic Star Mask
    keyValue = Settings.read( KEYPREFIX+"/limitMaskStarsPercent", DataType_Float );
    if ( Settings.lastReadOK )
        data.limitMaskStarsPercent = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/maskStarGrowthRate", DataType_Float );
    if ( Settings.lastReadOK )
        data.maskStarGrowthRate = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/maskStarGrowthLimit", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.maskStarGrowthLimit = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/maskStarRadiusAdd", DataType_Float );
    if ( Settings.lastReadOK )
        data.maskStarRadiusAdd = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/useAutoMaskStarSize", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useAutoMaskStarSize = keyValue;
    
    // Mosaic Join Mode
    keyValue = Settings.read( KEYPREFIX+"/useMosaicOverlay", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useMosaicOverlay = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/useMosaicRandom", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useMosaicRandom = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/useMosaicAverage", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.useMosaicAverage = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/createJoinMask", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.createJoinMask = keyValue;
    
    keyValue = Settings.read( KEYPREFIX+"/graphWidth", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.graphWidth = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/graphHeight", DataType_Int32 );
    if ( Settings.lastReadOK )
        data.graphHeight = keyValue;
    keyValue = Settings.read( KEYPREFIX+"/smallScreen", DataType_Boolean );
    if ( Settings.lastReadOK )
        data.smallScreen = keyValue;
}
