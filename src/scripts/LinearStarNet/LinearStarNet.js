// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// LinearStarnet.js - Released 2021-05-07T10:10:02Z
// ----------------------------------------------------------------------------
//
// This file is part of LinearStarNet Script version 1.4
//
// Copyright (c) 2021 Edoardo Luca Radice. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 _     _                       _____ _             _   _      _
| |   (_)                     /  ___| |           | \ | |    | |
| |    _ _ __   ___  __ _ _ __\ `--.| |_ __ _ _ __|  \| | ___| |_
| |   | | '_ \ / _ \/ _` | '__|`--. \ __/ _` | '__| . ` |/ _ \ __|
| |___| | | | |  __/ (_| | |  /\__/ / || (_| | |  | |\  |  __/ |_
\_____/_|_| |_|\___|\__,_|_|  \____/ \__\__,_|_|  \_| \_/\___|\__|

*/
#feature-id    Utilities > LinearStarNet

#feature-info  A script that applies StarNet Process to a linear image appling a semi-reversible Histogram transformation.
#define VERSION "1.4"
#define TITLE "LinearStarNet"

#include <pjsr/DataType.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/ColorSpace.jsh>


// Returns a push button with given text and onClick function.
function pushButtonWithTextOnClick(parent, text_, onClick_) {
    var button = new PushButton(parent);

    button.text = text_;
    button.onClick = onClick_;

    return button;
}



/*
______             _ _           _        _   _ _
|  _  \           | (_)         | |      | | | (_)
| | | |_   _ _ __ | |_  ___ __ _| |_ ___ | | | |_  _____      __
| | | | | | | '_ \| | |/ __/ _` | __/ _ \| | | | |/ _ \ \ /\ / /
| |/ /| |_| | |_) | | | (_| (_| | ||  __/\ \_/ / |  __/\ V  V /
|___/  \__,_| .__/|_|_|\___\__,_|\__\___| \___/|_|\___| \_/\_/
            | |
            |_|
*/

function DuplicateView(source,NewViewID) {

// Duplicazione della view "source"

   NewViewID=NewViewID.trim()
   NewViewID=(NewViewID!="") ? NewViewID : source.id + "_clone";


   this.cloneImageWindow = new ImageWindow(
      source.image.width,            // larghezza dell'immagine da clonare
      source.image.height,           // altezza dell'immagine da clonare
      source.image.numberOfChannels, // numero di canali dell'immagine da clonare
      source.image.bitsPerSample,    // bit per pixel dell'immagine da clonare
      source.image.sampleType == SampleType_Real, // true se l'immagine ha valori in virgola mobile, false se ha valori interi
      source.image.colorSpace != ColorSpace_Gray, // true se lo spazio colore e' RGB, false se' scala di grigi
      NewViewID
   );

// una volta creata la view che conterra' l'immagine duplicata dobbiamo copiare l'immagime
   this.cloneImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile ); // "blocco" la vista per processarla
   this.cloneImageWindow.mainView.image.selectedPoint = new Point( 0, 0 ); // posiziono il punto di lavoro nell'origine
   this.cloneImageWindow.mainView.image.apply( source.image ); // applico l'immagine originale sulla vista
   this.cloneImageWindow.mainView.image.resetSelections(); // resetto ogni selezione (ridondante, non servirebbe)
   this.cloneImageWindow.mainView.endProcess(); // "sblocco" la vista al termine dell'operazione di copia
   NewViewID=this.cloneImageWindow.mainView.id;


   this.cloneImageWindow.visible=true;
   this.cloneImageWindow.show;
   this.cloneImageWindow.bringToFront;
   var NewView= new View(NewViewID);
   return NewView


}



/*
______                              _
| ___ \                            | |
| |_/ /_ _ _ __ __ _ _ __ ___   ___| |_ ___ _ __ ___
|  __/ _` | '__/ _` | '_ ` _ \ / _ \ __/ _ \ '__/ __|
| | | (_| | | | (_| | | | | | |  __/ ||  __/ |  \__ \
\_|  \__,_|_|  \__,_|_| |_| |_|\___|\__\___|_|  |___/
*/

// The script's parameters prototype.
function LSNParPrototype() {
   this.setDefaults = function()
   {
      this.targetView = null;
      this.mtfShadowClip =-3.5;
      this.mtfBG =0.1;
      this.ShowClipMap = true;
      this.OnlyTryClip = false
      this.SNStride =0;
      this.SNStars=false;
      this.CopyView=false;
      this.Pc0 = 0;
      this.Pm = 0.5;

   };

    this.setParameters = function () {
        Parameters.clear();
        Parameters.set("mtfShadowClip", this.mtfShadowClip);
        Parameters.set("mtfBG", this.mtfBG);
        Parameters.set("ShowClipMap", this.ShowClipMap);
        Parameters.set("OnlyTryClip", this.OnlyTryClip);
        Parameters.set("SNStride", this.SNStride);
        Parameters.set("SNStars", this.SNStars);
        Parameters.set("CopyView", this.CopyView);
        Parameters.set("Pc0", this.Pc0);
        Parameters.set("Pm", this.Pm);

    }

    this.getParameters = function () {
        this.mtfShadowClip = Parameters.has("mtfShadowClip") ? Parameters.getReal("mtfShadowClip") : -3.5;
        this.mtfBG = Parameters.has("mtfBG") ? Parameters.getReal("mtfBG") : 0.1;
        this.ShowClipMap = Parameters.has("ShowClipMap") ? Parameters.getBoolean("ShowClipMap") : true;
        this.OnlyTryClip = Parameters.has("OnlyTryClip") ? Parameters.getBoolean("OnlyTryClip") : false;
        this.SNStride = Parameters.has("SNStride") ? Parameters.getReal("SNStride") : 0;
        this.SNStars = Parameters.has("SNStars") ? Parameters.getBoolean("SNStars") : false;
        this.CopyView = Parameters.has("CopyView") ? Parameters.getBoolean("CopyView") : false;
        this.Pc0 = Parameters.has("Pc0") ? Parameters.getReal("Pc0") : 0;
        this.Pm = Parameters.has("Pm") ? Parameters.getReal("Pm") : 0.5;
    }
}
var LSNPar = new LSNParPrototype();
LSNPar.setDefaults();
LSNPar.getParameters();


/*
 _   _                 _____      _             __
| | | |               |_   _|    | |           / _|
| | | |___  ___ _ __    | | _ __ | |_ ___ _ __| |_ __ _  ___ ___
| | | / __|/ _ \ '__|   | || '_ \| __/ _ \ '__|  _/ _` |/ __/ _ \
| |_| \__ \  __/ |     _| || | | | ||  __/ |  | || (_| | (_|  __/
 \___/|___/\___|_|     \___/_| |_|\__\___|_|  |_| \__,_|\___\___|

*/

// The script's parameters dialog prototype.
function LSNDialogPrototype() {
    this.__base__ = Dialog;
    this.__base__();

    var labelMinWidth = Math.round(this.font.width("Amount:") + 2.0 * this.font.width('M'));

    var sliderMaxValue = 256;
    var sliderMinWidth = 256;

    this.windowTitle = TITLE;

    this.titlePane = new Label(this);

    this.titlePane.frameStyle = FrameStyle_Box;
    this.titlePane.margin = 4;
    this.titlePane.wordWrapping = true;
    this.titlePane.useRichText = true;
    this.titlePane.text =
        "<p><b>" + TITLE + " Version " + VERSION + "</b> &mdash; " +
        "This script applies StarNet process to a linear image applying a semi-reversible Histogram transformation." +
        "<p>A linear monochrome image or a color calibrated RGB image is expected as input.</p> " +
        "<p>Thanks to Juan Conejero and Roberto Sartori for their inspiring code.</p> " +
        "<p>Copyright &copy; 2021 Edoardo Luca Radice. All Rights Reserved.</p>";


   // Target image selection sizer and group
    this.targetView = new VerticalSizer;
    this.targetView.margin = 6;
    this.targetView.spacing = 4;

    this.viewList = new ViewList(this);
    this.viewList.getMainViews();
    if (LSNPar.targetView !== null && LSNPar.targetView.isView) {
        this.viewList.currentView = LSNPar.targetView;
    }
    else {
        LSNPar.targetView = this.viewList.currentView;;
    }
    this.viewList.onViewSelected = function (view) { LSNPar.targetView = view; }

    this.targetView.add(this.viewList);


    var CopyViewCheckBox = new CheckBox(this);

    CopyViewCheckBox.text = "Create a new view";
    CopyViewCheckBox.toolTip = "If selected StarNet will be executed on a copy of the selected view";
    CopyViewCheckBox.checked = LSNPar.CopyView;
    CopyViewCheckBox.onCheck = function(checked) {
         LSNPar.CopyView=checked;
    };
    this.targetView.add(CopyViewCheckBox);


    this.TargetGroup = new GroupBox(this);
    this.TargetGroup.title = "Target view";
    this.TargetGroup.sizer = this.targetView;

   //MTF Parameters Sizer

    this.parameterPane = new VerticalSizer;

    this.parameterPane.margin = 6;
    this.parameterPane.spacing = 4;

    this.mtfShadowClipControl = new NumericControl(this);
    this.mtfShadowClipControl.label.text = "Shadows Clipping:";
    this.mtfShadowClipControl.label.minWidth = labelMinWidth;
    this.mtfShadowClipControl.slider.setRange(0, sliderMaxValue);
    this.mtfShadowClipControl.slider.minWidth = sliderMinWidth;
    this.mtfShadowClipControl.setRange(-10, 0);
    this.mtfShadowClipControl.setPrecision(2);
    this.mtfShadowClipControl.setValue(LSNPar.mtfShadowClip);
    this.mtfShadowClipControl.onValueUpdated = function (value)
    {
       LSNPar.mtfShadowClip = value;
    }
    this.mtfShadowClipControl.toolTip =
    "<p>Shadows clipping point. as from STF Auto Stretch parameter</p>"+
    "<p>A higher value (less negative) means more contrast .</p>";

   //Add the Shadow Clip numeric control
    this.parameterPane.add(this.mtfShadowClipControl);


    this.mtfBGControl = new NumericControl( this );
    this.mtfBGControl.label.text = "Target Background:";
    this.mtfBGControl.label.minWidth = labelMinWidth;
    this.mtfBGControl.slider.setRange( 0, sliderMaxValue );
    this.mtfBGControl.slider.setScaledMinWidth( 300 );
    this.mtfBGControl.setRange( 0.0, 1.0 );
    this.mtfBGControl.setPrecision( 2 );
    this.mtfBGControl.setValue( LSNPar.mtfBG );
    this.mtfBGControl.toolTip =
        "<p>Target background. as from STF Auto Stretch parameter</p>"+
        "<p>A higher value produces a brighter image for sta</p>";
    this.mtfBGControl.onValueUpdated = function( value )
    {
       LSNPar.mtfBG = value;
    };

   //Add the Target background numeric control
    this.parameterPane.add( this.mtfBGControl );


   //Display checkboxes in an horizontal sizer
    this.MTFCBPane = new HorizontalSizer;
    this.MTFCBPane.margin = 6;
    this.MTFCBPane.spacing = 4;


    this.showClippinMapCheckBox = new CheckBox(this);
    this.showClippinMapCheckBox.text = "Show clipping map";
    this.showClippinMapCheckBox.toolTip = "If selected a map with clipped pixel will be generated";
    this.showClippinMapCheckBox.checked = LSNPar.ShowClipMap;
    this.showClippinMapCheckBox.onCheck = function(checked) {
          LSNPar.ShowClipMap=checked;
    };

   //Add the clipping map checkbox
    this.MTFCBPane.add(this.showClippinMapCheckBox);
    this.MTFCBPane.addSpacing(10);


    this.TryClipCheckBox = new CheckBox(this);
    this.TryClipCheckBox.text = "Only calculate clipping map";
    this.TryClipCheckBox.toolTip =
         "If selected the StarNet process will not run and only the cipping map will be calculated"+
         "<br>Check this to try MTF parameters before executing time consuming StarNet process";
    this.TryClipCheckBox.checked = LSNPar.OnlyTryClip;
    this.TryClipCheckBox.onCheck = function(checked) {
          LSNPar.OnlyTryClip=checked;
    };

   //Add the calculate only the clipping map checkbox

    this.MTFCBPane.add(this.TryClipCheckBox);

    this.parameterPane.add( this.MTFCBPane );


    this.MTFGroup = new GroupBox(this);
    this.MTFGroup.title = "Auto MTF Parameters";
    this.MTFGroup.sizer = this.parameterPane;


    //Display Starnet parameters
    this.StarNetPane = new HorizontalSizer;
    this.StarNetPane.margin = 6;
    this.StarNetPane.spacing = 4;


    this.StridePane = new VerticalSizer;
    this.StridePane.margin = 6;
    this.StridePane.spacing = 4;


    this.Stridelabel = new Label(this);

    this.Stridelabel.frameStyle = FrameStyle_Flat;
    this.Stridelabel.margin = 0;
    this.Stridelabel.wordWrapping = false;
    this.Stridelabel.useRichText = true;
    this.Stridelabel.text ="StarNet Stride:";
    this.Stridelabel.textAlignement =10;

    this.StridePane.add( this.Stridelabel );

   // Starnet Stride
   //
    this.StrideListCombo = new ComboBox(this);
    var StrideList=new Array("128","64","32","16","8");
    for(var i=0; i < StrideList.length; ++i)
    {
       this.StrideListCombo.addItem(StrideList[i]);
    }
    this.StrideListCombo.currentItem=LSNPar.SNStride;
    this.StrideListCombo.onItemSelected = function( index )
    {
       LSNPar.SNStride=index;
    }

    this.StridePane.add( this.StrideListCombo );
    this.StarNetPane.add( this.StridePane );


    this.StarMaskCheckBox = new CheckBox(this);

    this.StarMaskCheckBox.text = "Create star mask";
    this.StarMaskCheckBox.toolTip = "Generate a linear starmask along with a linear starless image";
    this.StarMaskCheckBox.checked = LSNPar.SNStars;
    this.StarMaskCheckBox.onCheck = function(checked) {
          LSNPar.SNStars=checked;
    };

    this.StarNetPane.add( this.StarMaskCheckBox );


    this.StarNetGroup = new GroupBox(this);
    this.StarNetGroup.title = "StarNet Parameters";
    this.StarNetGroup.sizer = this.StarNetPane;



    this.buttonPane = new HorizontalSizer;
    this.buttonPane.spacing = 4;
    this.buttonPane.margin = 6;


    this.newInstanceButton = new ToolButton( this );
    this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
    this.newInstanceButton.setScaledFixedSize( 24, 24 );
    this.newInstanceButton.toolTip = "New Instance";
    this.newInstanceButton.onMousePress = function()
    {
       this.hasFocus = true;
       this.pushed = false;
       LSNPar.setParameters();
       this.dialog.newInstance();
    };

   this.buttonPane.add( this.newInstanceButton );



    this.buttonPane.addStretch();
    this.buttonPane.add(pushButtonWithTextOnClick(this, "Execute", function () {
//        LSNPar.exit = false;
        this.dialog.done(5);
    }));
    this.buttonPane.add(pushButtonWithTextOnClick(this, "Close", function () {
//        LSNPar.exit = true;
        this.dialog.done(6);
    }));


   //Add all the sizers to the Form sizer
    this.sizer = new VerticalSizer;

    this.sizer.margin = 6;
    this.sizer.spacing = 6;
    this.sizer.add(this.titlePane);
    this.sizer.add(this.TargetGroup);
    this.sizer.add(this.MTFGroup);
    this.sizer.add(this.StarNetGroup);
    this.sizer.add(this.buttonPane);

    this.adjustToContents();
    this.setFixedSize();


}
LSNDialogPrototype.prototype = new Dialog;



/*

______ _           _             _    __                                        _
|  ___(_)         | |           | |  / _|                                      | |
| |_   _ _ __   __| |  _ __ ___ | |_| |_   _ __   __ _ _ __ __ _ _ __ ___   ___| |_ ___ _ __ ___
|  _| | | '_ \ / _` | | '_ ` _ \| __|  _| | '_ \ / _` | '__/ _` | '_ ` _ \ / _ \ __/ _ \ '__/ __|
| |   | | | | | (_| | | | | | | | |_| |   | |_) | (_| | | | (_| | | | | | |  __/ ||  __/ |  \__ \
\_|   |_|_| |_|\__,_| |_| |_| |_|\__|_|   | .__/ \__,_|_|  \__,_|_| |_| |_|\___|\__\___|_|  |___/
                                          | |
                                          |_|

 * Find mtf parameters
 * Inspired by Juan Conejero (PTeam) AutoSTF Script
 */
function FindMtfParams( view, shadowsClipping, targetBackground )
{

   var n = view.image.isColor ? 3 : 1;
// if the image is RGB I use a rgb Linked transformation because I expect it is a color calibrated image.

   view.image.resetSelections();

      // Noninverted image

      var c0 = 0;
      var m = 0;
      for ( var c = 0; c < n; ++c )
      {
         view.image.selectedChannel = c;
         var median = view.image.median();
         var avgDev = view.image.avgDev();
         c0 += median + shadowsClipping*avgDev;
         m  += median;
      }
      view.image.resetSelections();
      c0 = Math.range( c0/n, 0.0, 1.0 );
      LSNPar.Pc0 = c0;
      m = Math.mtf( targetBackground, m/n - c0 );
      LSNPar.Pm = m;

   console.writeln( "<end><cbr/><b>", view.fullId, "</b>:" );
   for ( var c = 0; c < n; ++c )
   {
      console.writeln( "channel #", c );
      console.writeln( format( "c0 = %.6f", c0 ) );
      console.writeln( format( "m  = %.6f", m ) );
   }
   console.writeln( "<end><cbr/>" );
}


/*
 *

  ___              _      ___  ______________
 / _ \            | |     |  \/  |_   _|  ___|
/ /_\ \_ __  _ __ | |_   _| .  . | | | | |_
|  _  | '_ \| '_ \| | | | | |\/| | | | |  _|
| | | | |_) | |_) | | |_| | |  | | | | | |
\_| |_/ .__/| .__/|_|\__, \_|  |_/ \_/ \_|
      | |   | |       __/ |
      |_|   |_|      |___/
*/
function ApplyMTF(IDView, MPar, c0Par, IsDirect) {

      if (IsDirect) {
         var exp = "mtf("+ MPar + "," + IDView + "-" + c0Par + ")";
      }
      else {
         var exp = "mtf(1-"+ MPar + "," + IDView + ")+" + c0Par;
      }

      let MTF = new PixelMath;
      MTF.expression = exp;
      return MTF

}


/*
 _____       _     _                  _
/  ___|     | |   | |                | |
\ `--. _   _| |__ | |_ _ __ __ _  ___| |_
 `--. \ | | | '_ \| __| '__/ _` |/ __| __|
/\__/ / |_| | |_) | |_| | | (_| | (__| |_
\____/ \__,_|_.__/ \__|_|  \__,_|\___|\__|


*/

function Subtract(IDViewA, IDViewB, offset) {

      let SUB = new PixelMath;
      SUB.expression =IDViewA + "-" + IDViewB + "+" + offset ;
      return SUB

}





/*
 _     _                       _____ _             _   _      _     _____ ___________ _____
| |   (_)                     /  ___| |           | \ | |    | |   /  __ \  _  |  _  \  ___|
| |    _ _ __   ___  __ _ _ __\ `--.| |_ __ _ _ __|  \| | ___| |_  | /  \/ | | | | | | |__
| |   | | '_ \ / _ \/ _` | '__|`--. \ __/ _` | '__| . ` |/ _ \ __| | |   | | | | | | |  __|
| |___| | | | |  __/ (_| | |  /\__/ / || (_| | |  | |\  |  __/ |_  | \__/\ \_/ / |/ /| |___
\_____/_|_| |_|\___|\__,_|_|  \____/ \__\__,_|_|  \_| \_/\___|\__|  \____/\___/|___/ \____/


*/

function DoLinearStarnet()
{

   FindMtfParams( LSNPar.targetView, LSNPar.mtfShadowClip, LSNPar.mtfBG, true )

   if (LSNPar.ShowClipMap) { //Create a clipping map to verify how much clipping occurs

      ClippingMap=DuplicateView(LSNPar.targetView,LSNPar.targetView.id+"_ClippingMap");

      Delin = ApplyMTF(ClippingMap.id, LSNPar.Pm, LSNPar.Pc0, true)
      Relin = ApplyMTF(ClippingMap.id, LSNPar.Pm, LSNPar.Pc0, false)
      CreateMap=Subtract(LSNPar.targetView.id, ClippingMap.id, 0.5)

      let BinarizeMap = new PixelMath;
      BinarizeMap.expression = "iif($T != 0.5 , 1 , 0)";

      let P = new ProcessContainer;
      P.add( Delin );
      P.add( Relin );
      P.add( CreateMap );
      P.add( BinarizeMap );

      P.executeOn( ClippingMap, true );
   }


   if (LSNPar.OnlyTryClip) return //if the Only Try Clip checkbox is selectet the script ends here



   //Define Target view
   if (LSNPar.CopyView) {

      //Apply the transformation on a copy of the selected view
      SNTarget=DuplicateView(LSNPar.targetView,LSNPar.targetView.id+"_starless");
   }
   else
   {
   //Apply the transformation on the selected view
      var SNTarget=new View(LSNPar.targetView.id);
   }


   if (LSNPar.SNStars) {// Copy the original file to create, at the end of star removal, the linear starmap
      var SNStarmap= new View();
      SNStarmap=DuplicateView(LSNPar.targetView,LSNPar.targetView.id+"_starmap");
   }


//Delinearize: apply a semi-reversible midtone transfer function
// if mtfShadowClip is large enought (in absoute value) clipping in the shadow should be minimal

    Delin = ApplyMTF(SNTarget.id, LSNPar.Pm, LSNPar.Pc0, true)

// perform StarNet
   let SN = new StarNet;
   switch (LSNPar.SNStride) {
      case 0:
         SN.stride = StarNet.prototype.Stride_128;
         break;
      case 1:
         SN.stride = StarNet.prototype.Stride_64;
         break;
      case 2:
         SN.stride = StarNet.prototype.Stride_32;
         break;
      case 3:
         SN.stride = StarNet.prototype.Stride_16;
         break;
      case 4:
         SN.stride = StarNet.prototype.Stride_8;
         break;
   }
  SN.mask = false;

//relinearize: invert the midtone transfer function m -> 1-m  and c0 is added to restore background level

   Relin = ApplyMTF(SNTarget.id, LSNPar.Pm, LSNPar.Pc0, false)


   //Bring to front the target image
   SNTarget.window.bringToFront();


   //Embed the single processes in a ProcessContainer
   let P = new ProcessContainer;
   P.add( Delin );
   P.add( SN );
   P.add( Relin );
   P.executeOn( SNTarget, true );

   if (LSNPar.SNStars) {// Create the linear starmap
      CalcStarMap=Subtract(SNStarmap.id,SNTarget.id,0);
      CalcStarMap.executeOn( SNStarmap, true );
   }

}



/*
 _ __ ___   __ _ _ _ __
| '_ ` _ \ / _` | | '_ \
| | | | | | (_| | | | | |
|_| |_| |_|\__,_|_|_| |_|

*/
function main()
{
   console.show();

   if ( Parameters.isViewTarget )
   {
      // Script has been launched on a view so we are in Target context, execute and exit
      LSNPar.getParameters();
      LSNPar.targetView=Parameters.targetView;
      DoLinearStarnet();
      return;
   }

   if ( Parameters.isGlobalTarget )
   {
      // Script has been launched in global context, get the parameters from the parameters table to override defaults
       LSNPar.getParameters();
   }


   //Both Global or immediate context
   // Prepare the dialog

   //the active view
   LSNPar.targetView = ImageWindow.activeWindow.currentView;
   let parametersDialog = new LSNDialogPrototype();
   LSNPar.exit = false;
   let retVal=parametersDialog.execute();
   if (retVal!=5) return; //the user didn't click the "Execute button"

      // do the job
   DoLinearStarnet();

}

main();

// ----------------------------------------------------------------------------
// EOF LinearStarnet.js - Released 2021-05-07T10:10:02Z
