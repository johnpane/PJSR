===============================================================================
CorrectMagentaStars Script Changelog
===============================================================================

2021.05.05 - v1.2

- Allowed using of Masks.

- Addded Show Documentation button

-------------------------------------------------------------------------------
2019.11.11 - v1.1

- Added instance icon.

- scnrAmount parameter saturated in [0, 1] range.

- Target view reported in console on execution.

-------------------------------------------------------------------------------
v1.0

- First release.
