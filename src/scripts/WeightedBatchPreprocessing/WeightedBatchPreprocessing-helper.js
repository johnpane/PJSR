// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-helper.js - Released 2022-03-15T20:50:04Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.4.2
//
// Copyright (c) 2019-2022 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * Helper routines
 */

// ----------------------------------------------------------------------------
// Extensions to the Array object
//
// NB: Define new methods of the Array object as nonenumerable properties with
//     Object.defineProperty() to prevent problems with "for...in" constructs.
// ----------------------------------------------------------------------------
var helperFunctions = () => (
{

   /**
    * Returns true if the version string a is lower than b.
    *
    * @param {*} a
    * @param {*} b
    * @return {*}
    */
   versionLT: ( a, b ) =>
   {
      let at = a.split( "." ).map( i => parseInt( i ) );
      let bt = b.split( "." ).map( i => parseInt( i ) );
      for ( let i = 0; i < Math.max( at.length, bt.length ); i++ )
      {
         let ai = ( at[ i ] || 0 );
         let bi = ( bt[ i ] || 0 );
         if ( bi > ai )
            return true;
         if ( bi < ai )
            return false;
      }
      return false;
   },
   /*
    * Returns an array of enabled target frames.
    * Used to build the input for ImageCalibration/ImageIntegration/StarAlignment
    */
   enableTargetFrames: function( array, ncolumns, enableDrizzle, enableLN )
   {
      let target = new Array;
      for ( let i = 0; i < array.length; ++i )
      {
         target[ i ] = new Array( ncolumns );
         for ( let j = 0; j < ncolumns - 1; ++j )
            target[ i ][ j ] = true;
         target[ i ][ ncolumns - 1 ] = array[ i ];
         if ( enableDrizzle )
            target[ i ][ ncolumns ] = File.changeExtension( array[ i ], '.xdrz' );
         if ( enableLN )
            target[ i ][ ncolumns + 1 ] = File.changeExtension( array[ i ], '.xnml' );
      }
      return target;
   },

   /**
    * Checks if a valid Cosmetic Corection icon name is provided by checking the
    * existence of a correspondent valid icon on the workspace.
    *
    * @param {*} name the Cosmetic Corerction icon name to be checked
    * @return {*} true if a valid Cosmetic Correction icon exists, false otherwise
    */
   validCCIconName: function( name )
   {
      let valid = false;
      let icons = ProcessInstance.iconsByProcessId( "CosmeticCorrection" );
      for ( let i = 0; i < icons.length; ++i )
      {
         if ( name == icons[ i ] )
            return true;
      }
      return false;
   },

   // ----------------------------------------------------------------------------
   // String Utils
   // ----------------------------------------------------------------------------

   /**
    * Returns a h:mm:s formatted string representing the value as a duration
    *
    * @param {*} value
    */
   formatTimeDuration: function( duration )
   {
      let hours = Math.floor( duration / 3600 );
      let min = Math.floor( ( duration - hours * 3600 ) / 60 );
      let sec = duration % 60;
      return format( "%4i", hours ) + "h " + format( "%2i", min ) + "min " + format( "%2i", sec ) + "sec";
   },

   /*
    * Returns a clean filter name.
    */
   cleanFilterName: function( str )
   {
      let sanitizeRegExp = new RegExp( "[^" + KEYWORD_VALUE_CHARSET + "]", "gi" );
      return str.replace( sanitizeRegExp, '-' );
   },

   /*
    * Returns true if this string is empty.
    */
   isEmptyString: function( str )
   {
      return str == undefined || str.length <= 0;
   },

   /*
    * Returns true if this string contains the specified substring.
    */
   stringHas: function( str, s )
   {
      return str.indexOf( s ) > -1;
   },

   /*
    * Add the given padding to a string
    */
   paddedStringNumber: function( str, padding )
   {
      let intPart = str.split( '.' );
      let P = Math.max( 0, padding - intPart[ 0 ].length );
      let Pad = " ".repeat( P );
      return Pad + str;
   },

   // ----------------------------------------------------------------------------
   // File Utils
   // ----------------------------------------------------------------------------

   /*
    * Creates a directory if it does not exist.
    * Returns the directory path.
    */
   existingDirectory: function( dir )
   {
      if ( !File.directoryExists( dir ) )
         File.createDirectory( dir );
      return dir;
   },

   readFileKeywords: function( filePath )
   {
      let ext = File.extractExtension( filePath ).toLowerCase();
      let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
      if ( F.isNull ) // shouldn't happen
         return {
            success: false,
            message: "No installed file format can read \'" + ext + "\' files."
         };
      let f = new FileFormatInstance( F );
      if ( f.isNull )
         return {
            success: false,
            message: "Unable to instantiate file format: " + F.name
         };

      let info = f.open( filePath, "verbosity 0" ); // do not fill the console with useless messages
      if ( !info || ( info && info.length <= 0 ) )
         return {
            success: false,
            message: "Unable to open input file: " + filePath
         };

      let keywords = [];
      if ( F.canStoreKeywords )
         keywords = f.keywords;

      f.close();

      return {
         success: true,
         keywords: keywords
      };
   },

   /*
    * Returns the name and extension components of a path specification.
    */
   extractNameAndExtension: function( path )
   {
      return File.extractName( path ) + File.extractExtension( path );
   },

   // ----------------------------------------------------------------------------
   // Smart Naming Helpers
   // ----------------------------------------------------------------------------

   smartNaming:
   {
      lastMatching: function( text, regexp )
      {
         let matches = text.match( regexp )
         if ( matches != null && matches.length > 0 )
         {
            return matches[ matches.length - 1 ];
         }
         return undefined;
      },

      /*
       * Extract the master property from the file Path.
       *
       * fileName must contain the "master" string in its name in order to be recognized
       * as a master file from SmartNaming.
       *
       * NOTE: the chec is case-insensitive
       */
      isMasterFromPath: function( filePath )
      {
         let regexp = /(MASTER)/gi;
         return this.lastMatching( filePath, regexp ) != undefined;
      },

      /*
       * Extract the image type from the last matching pattern occurrence in its
       * filePath.
       *
       * filePath must contain one pr more of BIAS, DARK, DARKS, FLAT, FLATS, LIGHT
       * or LIGHTS. The last of the sequence will be taken. It is useful to check the
       * whole path since instead of renaming single files it's possible to put all
       * light files into an enclosing folder with the word "lights" in the name.
       *
       * NOTE: negative look behind is not supported in JS so the first char
       * before the keywords is matched and removed in the switch.
       */
      geImageTypeFromPath: function( filePath )
      {
         let regexp = /(?![a-z0-9]).{0,1}(BIAS|DARK|DARKS|FLAT|FLATS|LIGHT|LIGHTS)(?![a-z0-9])/gi;
         let fileType = this.lastMatching( filePath, regexp );
         if ( fileType )
         {
            switch ( fileType.substr( 1 ).toUpperCase() )
            {
               case 'BIAS':
                  return ImageType.BIAS;
               case 'DARK':
               case 'DARKS':
                  return ImageType.DARK;
               case 'FLAT':
               case 'FLATS':
                  return ImageType.FLAT;
               case 'LIGHT':
               case 'LIGHTS':
                  return ImageType.LIGHT;
            }
         }

         return ImageType.UNKNOWN;
      },

      sanitizeKeywordValue: function( value )
      {
         let sanitizeRegExp = new RegExp( "[^" + KEYWORD_VALUE_CHARSET + "]", "gi" );
         return value.replace( sanitizeRegExp, '-' );
      },

      /*
       * Extract the exposure time from the last matching pattern occurrence in its filePath.
       * Exposure can be specified by means of he
       */
      getExposureTimeFromPath: function( filePath )
      {
         // match the format EXPTIME_10.0 or EXPOSURE_2
         let regexp = /(EXPTIME|EXPOSURE)(_|-| )[0-9]+(\.[0-9]*)?/gi;
         let match = this.lastMatching( filePath, regexp );
         if ( match )
         {
            let sanitizedStrValue = match.replace( /(EXPTIME|EXPOSURE)(_|-| )/gi, '' );
            let value = Number( sanitizedStrValue );
            return value !== NaN ? value : 0
         }
         // find any number followed by 's' or 'sec' ore '_secs' like 2s, 2.1_secs or 2.2sec
         let postfixes = [ 's', 'sec', '_secs' ];
         regexp = /[0-9]+(\.[0-9]*)?(?=(s|sec|_secs)[^a-zA-Z0-9])/gi;
         let matches = regexp.exec( filePath );
         if ( matches !== null )
         {
            let sanitizedStr = matches[ 0 ];
            postfixes.forEach( postfix =>
            {
               sanitizedStr = sanitizedStr.replace( postfix, '' );
            } );
            let value = Number( sanitizedStr );
            return value !== NaN ? value : 0
         }
         return 0;
      },

      /*
       * Extract the binning from the last matching pattern occurrence in its filePath.
       */
      getBinningFromPath: function( filePath )
      {
         let regexp = /(XBINNING|CCDBINX|BINNING)(_|-| )[0-9]+/gi;
         let match = this.lastMatching( filePath, regexp );
         if ( match )
         {
            let sanitizedStrValue = match.replace( /(XBINNING|CCDBINX|BINNING)(_|-| )/gi, '' );
            let value = Number( sanitizedStrValue );
            return value !== NaN ? value : 1
         }
         return 1
      },

      /*
       * Extract the filter name from the last matching pattern occurrence in its filePath.
       * Poissible valid combinations within the path are:
       * - FILTER NebulaBooster
       * - FILTER-Ha
       * - INSFLNAM_L
       */
      getFilterFromPath: function( filePath )
      {
         let regexp = /(FILTER|INSFLNAM)(_|-| )[- a-zA-Z0-9]+/gi;
         let match = this.lastMatching( filePath, regexp );
         if ( match )
            return match.replace( /(FILTER|INSFLNAM)(_|-| )/gi, '' );
         return undefined;
      },

      /*
       * Extract a custom key-value from the last matching pattern occurrence in its filePath.
       * Value is limited to contiguous alphanumeric characters wrapped by by -,_ or space:
       */
      getCustomKeyValueFromPath: function( key, filePath )
      {
         // remove the extension
         let extension = File.extractExtension( filePath );
         let sanitizedFilePath = filePath.slice( 0, -extension.length );
         // search the existence of the key.value pair
         let regexp = new RegExp( "[^a-zA-Z0-9](" + key + ")(_|-| )[" + KEYWORD_VALUE_CHARSET + "]+", "gi" );
         let match = this.lastMatching( sanitizedFilePath, regexp );
         if ( match )
         {
            let subsRegExp = new RegExp( "[^a-zA-Z0-9](" + key + ")(_|-| )", "gi" );
            let sanitizeRegExp = new RegExp( "[^" + KEYWORD_VALUE_CHARSET + "]", "gi" );
            return match.replace( subsRegExp, '' ).replace( sanitizeRegExp, "-" );
         }
         return '';
      },
   },

   // ----------------------------------------------------------------------------
   // Extensions to the Parameters object
   // ----------------------------------------------------------------------------
   parameters:
   {
      indexedId: function( id, index )
      {
         return id + '_' + ( index + 1 ).toString(); // make indexes one-based
      },
      hasIndexed: function( id, index )
      {
         return Parameters.has( this.indexedId( id, index ) );
      },
      getBooleanIndexed: function( id, index )
      {
         return Parameters.getBoolean( this.indexedId( id, index ) );
      },
      getIntegerIndexed: function( id, index )
      {
         return Parameters.getInteger( this.indexedId( id, index ) );
      },
      getRealIndexed: function( id, index )
      {
         return Parameters.getReal( this.indexedId( id, index ) );
      },
      getStringIndexed: function( id, index )
      {
         return Parameters.getString( this.indexedId( id, index ) );
      },
      getUIntIndexed: function( id, index )
      {
         return Parameters.getUInt( this.indexedId( id, index ) );
      },
      getStringList: function( id )
      {
         let list = new Array();
         if ( Parameters.has( id ) )
         {
            let s = Parameters.getString( id );
            list = s.split( ':' );
            for ( let i = 0; i < list.length; ++i )
               list[ i ] = list[ i ].trim();
         }
         return list;
      },
      setIndexed: function( id, index, value )
      {
         return Parameters.set( this.indexedId( id, index ), value );
      },
   },
   factory:
   {
      CPTreeBox: function( parent )
      {
         let treeBox = new TreeBox( parent );
         treeBox.rootDecoration = false;
         treeBox.headerVisible = true;
         treeBox.alternateRowColor = true;
         treeBox.numberOfColumns = 1;
         return treeBox;
      },
      CPHeader: function( parent, title )
      {
         let label = new Label( parent );
         label.text = "<b>" + title + "</b>";
         label.margin = 8;
         label.spacing = 8;
         label.useRichText = true;
         return label;
      },
      CPMasterSelectionControl: function( parent, title )
      {
         let control = new Control( parent );
         let label = new Label( control );
         label.useRichText = true;
         label.text = title;
         label.setFixedWidth( 34 );
         label.textAlignment = TextAlign_VertCenter;
         let combo = new ComboBox( control );
         combo.setVariableWidth();
         control.sizer = new HorizontalSizer;
         control.sizer.add( label );
         control.sizer.add( combo );
         return combo;
      }
   }
} );

var WBPPUtils = ( function()
{
   this.instance = null;

   function createInstance()
   {
      return helperFunctions();
   }

   return {
      shared: function()
      {
         if ( !this.instance )
            this.instance = createInstance();
         return this.instance;
      }
   };
} )();

/*
 * FileList
 *
 * Recursively search a directory tree for all existing files with the
 * specified file extensions.
 */
function FileList( dirPath, extensions, verbose )
{
   /*
    * Regenerate this file list for the specified base directory and file
    * extensions.
    */
   this.regenerate = function( dirPath, extensions, verbose )
   {
      // Security check: Do not allow climbing up a directory tree.
      if ( dirPath.indexOf( ".." ) >= 0 )
         throw new Error( "FileList: Attempt to redirect outside the base directory: " + dirPath );

      // The base directory is the root of our search tree.
      this.baseDirectory = File.fullPath( dirPath );
      if ( this.baseDirectory.length == 0 )
         throw new Error( "FileList: No base directory has been specified." );

      // The specified directory can optionally end with a separator.
      if ( this.baseDirectory[ this.baseDirectory.length - 1 ] == '/' )
         this.baseDirectory.slice( this.baseDirectory.length - 1, -1 );

      // Security check: Do not try to search on a nonexisting directory.
      if ( !File.directoryExists( this.baseDirectory ) )
         throw new Error( "FileList: Attempt to search a nonexistent directory: " + this.baseDirectory );

      // If no extensions have been specified we'll look for all existing files.
      if ( extensions == undefined || extensions == null || extensions.length == 0 )
         extensions = [ '' ];

      if ( verbose )
      {
         console.writeln( "<end><cbr><br>==> Finding files from base directory:" );
         console.writeln( this.baseDirectory );
      }

      // Find all files with the required extensions in our base tree recursively.
      this.files = [];
      for ( let i = 0; i < extensions.length; ++i )
         this.files = this.files.concat( searchDirectory( this.baseDirectory + "/*" + extensions[ i ], true /*recursive*/ ) );

      // // Delete baseDirectory + separator from the beginning of all file paths.
      // var d = this.baseDirectory + '/';
      // for ( let i = 0; i < this.files.length; ++i )
      // {
      //    if ( this.files[ i ].indexOf( d ) != 0 )
      //       throw new Error( "<* Panic *> Inconsistent directory search: " + this.files[ i ] );
      //    this.files[ i ] = this.files[ i ].slice( d.length, this.files[ i ].length );
      // }
   };

   this.baseDirectory = "";
   this.files = [];
   this.index = [];

   if ( dirPath != undefined )
      this.regenerate( dirPath, extensions, verbose );

   if ( verbose )
   {
      console.writeln( "<end><cbr>" + this.files.length + " file(s) found:" );
      for ( let i = 0; i < this.files.length; ++i )
         console.writeln( this.files[ i ] );
   }
}

FileList.prototype = new Object;

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-helper.js - Released 2022-03-15T20:50:04Z
