// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-GUI.js - Released 2022-03-15T20:50:04Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.4.2
//
// Copyright (c) 2019-2022 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * Graphical user interface
 */

/* beautify ignore:start */
#include <pjsr/Color.jsh>
#include <pjsr/DataType.jsh>
#include <pjsr/FontFamily.jsh>
#include "WeightedBatchPreprocessing-graph.js"
/* beautify ignore:end */

// ----------------------------------------------------------------------------
/**
 * Base TreeBox Control.
 *
 * @param {*} parent
 */
function StyledTreeBox( parent )
{
   this.__base__ = TreeBox;
   if ( parent )
      this.__base__( parent );
   else
      this.__base__();

   this.alternateRowColor = true;
}

StyledTreeBox.prototype = new TreeBox;

// ----------------------------------------------------------------------------

/**
 * Describes the properties of a treeBox like header names and column widhts for
 * a given image type.
 *
 * @param {ImageType} imageType
 */
function GroupTreeBoxDescriptor( imageType )
{
   this.imageType = imageType;

   /**
    * Returns the headers for be shown for the given mode
    *
    * @param {WBPPGroupingMode} mode
    * @returns
    */
   this.headers = ( mode ) =>
   {
      let headers = [];
      switch ( mode )
      {
         case WBPPGroupingMode.PRE:
            switch ( this.imageType )
            {
               case ImageType.BIAS:
                  headers = [ "BIAS", "Bin" ];
                  break;
               case ImageType.DARK:
                  headers = [ "DARK", "Bin", "Exposure", "Contains\nBias" ];
                  break;
               case ImageType.FLAT:
                  headers = [ "FLAT", "Bin", "Exposure", "Filter", "STATUS", "Bias", "Dark", "Optimize\nDark", "CFA\nImages", "CFA\nScaling" ];
                  break;
               case ImageType.LIGHT:
                  headers = [ "LIGHT", "Bin", "Exposure", "Filter", "STATUS", "Bias", "Dark", "Flat", "Optimize\nDark", "Output\nPedestal (DN)", "Cosmetic\nCorrection", "CFA\nImages" ];
                  break;
            }
            break;

         case WBPPGroupingMode.POST:
            switch ( this.imageType )
            {
               case ImageType.LIGHT:
                  headers = [ "MASTER LIGHT", "Bin", "Exposures", "Filter", "Color Space", "Integration Time" ];
                  break;
            }
            break;
      }

      return headers;
   };
}

// ----------------------------------------------------------------------------

function PresetsControl( title, parent )
{
   this.__base__ = ParametersControl;
   this.__base__( title, parent, false /* expand */ , false /* deactivable */ , true /* collapsable */ );

   let label = new Label( this );
   label.useRichText = true;
   label.wordWrapping = true;
   label.text = "<p>Configure WBPP for maximum speed or quality.</p>"

   let selectButton = new PushButton( this );
   selectButton.text = "SELECT";
   selectButton.onClick = function()
   {
      let presetsDialog = new PresetsDialog;
      let preset = presetsDialog.execute();
      engine.applyPreset( preset );
      this.dialog.updateControls();
   }

   this.add( label );
   this.add( selectButton );
}
PresetsControl.prototype = new Control;

// ----------------------------------------------------------------------------

/**
 * This control allows to add/edit/remove a list of keywords and provide
 * an event-handler "onKeywordsUpdated" that is triggered when the list of
 * keywords changes.
 * By default, keyboard input is sanitized and capitalized, only alphanumeric
 * characters are allowed.
 *
 * @param {*} title
 * @param {*} parent
 */
function KeywordsEditor( title, parent )
{
   this.__base__ = ParametersControl;
   this.__base__( title, parent, false /* expandable */ , true /* deactivable */ , true /* collapsable */ );

   // Internal state
   this.keywordSelected = false;

   // Keyword input
   this.keywordEdit = new Edit( this );
   this.keywordEdit.setScaledFixedHeight( 20 );

   this.keywordEdit.onTextUpdated = ( newText ) =>
   {
      let caretPosition = this.keywordEdit.caretPosition;
      this.keywordEdit.text = newText.replace( /[^a-z0-9-]/gmi, '' ).toUpperCase();
      this.keywordEdit.caretPosition = caretPosition + ( ( this.keywordEdit.text.length == newText.length ) ? 0 : -1 );
   };

   this.keywordEdit.onLoseFocus = () =>
   {
      this.keywordEdit.text = "";
      this.keywordSelected = false;
      this.updateToolButtons();
      this.selectionWasUpdated = false;
      this.refreshTreeBox();
   }

   // confirm button
   this.confirmButton = new ToolButton( this );
   this.confirmButton.setScaledFixedSize( 20, 20 );
   this.confirmButton.toolTip = "<p>By adding or updating a keyword, all the frame files will be" +
      "reprocessed and regrouped accordingly. Any setting that has been manually changed will be " +
      "set back to its default value.</p>";

   this.confirmButton.onClick = () =>
   {
      if ( this.keywordSelected )
         this.replaceCurrentKeyword(); // modify the selected keyword
      else
         this.addNewKeyword(); // add the typed keyword the selected keyword
   };

   // keyword list
   this.keywordsTreeBox = new TreeBox( this );
   this.keywordsTreeBox.alternateRowColor = true;
   this.keywordsTreeBox.multipleSelection = false;
   this.keywordsTreeBox.indentSize = 0;
   this.keywordsTreeBox.setScaledFixedHeight( 120 );

   // headers
   this.keywordsTreeBox.headerVisible = true;
   this.keywordsTreeBox.headerSorting = false;
   this.keywordsTreeBox.numberOfColumns = 3;
   this.keywordsTreeBox.setHeaderText( 0, "Keyword" );
   this.keywordsTreeBox.setHeaderText( 1, "Pre" );
   this.keywordsTreeBox.setHeaderText( 2, "Post" );
   this.keywordsTreeBox.setHeaderAlignment( 0, TextAlign_Center );
   this.keywordsTreeBox.setHeaderAlignment( 1, TextAlign_Center );
   this.keywordsTreeBox.setHeaderAlignment( 2, TextAlign_Center );

   this.selectionWasUpdated = false;

   this.keywordsTreeBox.onNodeSelectionUpdated = ( programmatically ) =>
   {
      if ( !programmatically )
         this.selectionWasUpdated = true;
      if ( this.keywordsTreeBox.currentNode )
      {
         this.keywordSelected = true;
         this.keywordEdit.text = this.keywordsTreeBox.currentNode.text( 0 );
         this.updateToolButtons();
      }
   };

   this.keywordsTreeBox.onNodeClicked = () =>
   {
      if ( !this.selectionWasUpdated )
      {
         this.keywordEdit.text = "";
         this.keywordsTreeBox.currentNode.selected = false;
         this.keywordSelected = false;
         this.updateToolButtons();
      }
      this.selectionWasUpdated = false;
   };

   // keyword sort buttons
   this.moveUp = new ToolButton( this );
   this.moveUp.setScaledFixedSize( 20, 20 );
   this.moveUp.icon = this.scaledResource( ":/arrows/arrow-up.png" );
   this.moveUp.onClick = () => this.moveSelectedKeyword( true /* up */ );
   this.moveDown = new ToolButton( this );
   this.moveDown.setScaledFixedSize( 20, 20 );
   this.moveDown.icon = this.scaledResource( ":/arrows/arrow-down.png" );
   this.moveDown.onClick = () => this.moveSelectedKeyword( false /* up */ );

   // pre/post process switches
   this.switchWorkingMode = new ToolButton( this );
   this.switchWorkingMode.setScaledFixedSize( 20, 20 );
   this.switchWorkingMode.icon = this.scaledResource( ":/icons/processes.png" );
   this.switchWorkingMode.onClick = () =>
   {
      this.switchKeyword();
   };

   // cancel button
   this.cancelButton = new ToolButton( this );
   this.cancelButton.setScaledFixedSize( 20, 20 );
   this.cancelButton.toolTip = "<p>By removing a keyword, all the frame files will be" +
      "reprocessed and regrouped accordingly. Any setting that has been manually changed will be " +
      "set back to its default value.</p>";
   this.cancelButton.onClick = () => this.removeSelectedKeyword();
   this.cancelButton.icon = this.scaledResource( ":/toolbar/image-purge.png" );

   this.buttonsSizer = new VerticalSizer;
   this.buttonsSizer.spacing = 4;
   this.buttonsSizer.add( this.moveUp );
   this.buttonsSizer.add( this.moveDown );
   this.buttonsSizer.add( this.switchWorkingMode );
   this.buttonsSizer.add( this.cancelButton );
   this.buttonsSizer.addStretch();

   this.treeBoxAndButtonsSizer = new HorizontalSizer;
   this.treeBoxAndButtonsSizer.spacing = 4;
   this.treeBoxAndButtonsSizer.add( this.keywordsTreeBox );
   this.treeBoxAndButtonsSizer.add( this.buttonsSizer )

   // layout
   this.editRowSizer = new HorizontalSizer;
   this.editRowSizer.spacing = 4;
   this.editRowSizer.add( this.keywordEdit );
   this.editRowSizer.add( this.confirmButton );

   this.add( this.editRowSizer );
   this.add( this.treeBoxAndButtonsSizer );

   // AUX

   this.onActivateStatusChanged = ( active ) =>
   {
      engine.groupingKeywordsEnabled = active;
      this.onKeywordsUpdated();
   };

   this.refreshTreeBox = () =>
   {
      this.keywordsTreeBox.clear();
      engine.keywords.list.forEach( keyword =>
      {
         let nodeTree = new TreeBoxNode( this.keywordsTreeBox );
         nodeTree.setText( 0, keyword.name );
         if ( keyword.mode & WBPPGroupingMode.PRE )
         {
            nodeTree.setIcon( 1, this.scaledResource( ":/browser/enabled.png" ) );
         }
         else
         {
            nodeTree.setIcon( 1, this.scaledResource( "" ) );
         }

         if ( keyword.mode & WBPPGroupingMode.POST )
         {
            nodeTree.setIcon( 2, this.scaledResource( ":/browser/enabled.png" ) );
         }
         else
         {
            nodeTree.setIcon( 2, this.scaledResource( "" ) );
         }
      } );
      this.keywordsTreeBox.adjustColumnWidthToContents( 0 );
      this.keywordsTreeBox.adjustColumnWidthToContents( 1 );
      this.keywordsTreeBox.adjustColumnWidthToContents( 2 );
   };

   this.updateToolButtons = () =>
   {
      this.moveDown.enabled = this.keywordSelected;
      this.moveUp.enabled = this.keywordSelected;
      this.switchWorkingMode.enabled = this.keywordSelected;
      this.cancelButton.enabled = this.keywordSelected;

      if ( !this.keywordSelected )
         this.confirmButton.icon = this.scaledResource( ":/icons/add.png" );
      else
         this.confirmButton.icon = this.scaledResource( ":/script-editor/modified-document.png" );
   };

   this.addNewKeyword = () =>
   {
      if ( this.keywordEdit.text.length == 0 )
      {
         // must type the keyword before adding it
         ( new MessageBox(
            "No keyword specified. Type your keyword and then click the add button.",
            "ERROR",
            StdIcon_Warning,
            StdButton_Ok
         ) ).execute();
         return;
      }

      // add the keyword
      let errorStr = engine.keywords.add( this.keywordEdit.text );
      if ( errorStr )
      {
         ( new MessageBox(
            errorStr,
            "ERROR",
            StdIcon_Warning,
            StdButton_Ok
         ) ).execute();
         return;
      }
      // success
      this.keywordEdit.text = "";
      this.refreshTreeBox();
      this.updateToolButtons();

      // event handler management
      if ( this.onKeywordsUpdated )
         this.onKeywordsUpdated();
   };

   this.replaceCurrentKeyword = () =>
   {
      let oldKeyword = this.keywordsTreeBox.currentNode.text( 0 );
      if ( this.keywordEdit.text.length == 0 )
      {
         ( new MessageBox(
            "New keyword cannot be empty.",
            "ERROR",
            StdIcon_Warning,
            StdButton_Ok
         ) ).execute();
         return;
      }

      engine.keywords.replace( oldKeyword, this.keywordEdit.text );

      // done
      this.keywordSelected = false;
      this.keywordEdit.text = "";
      this.refreshTreeBox();
      this.updateToolButtons();

      // event handler management
      if ( this.onKeywordsUpdated )
         this.onKeywordsUpdated();
   };

   this.moveSelectedKeyword = ( up ) =>
   {
      let name = this.keywordsTreeBox.currentNode.text( 0 );

      engine.keywords.move( name, up );
      this.refreshTreeBox();
      if ( this.onKeywordsUpdated )
         this.onKeywordsUpdated();
      this.selectKeyword( name );
   };

   this.switchKeyword = () =>
   {
      if ( !this.keywordsTreeBox.currentNode )
         return;
      let name = this.keywordsTreeBox.currentNode.text( 0 );
      engine.keywords.switchMode( name )
      this.refreshTreeBox();
      if ( this.onKeywordsUpdated )
         this.onKeywordsUpdated();
      this.selectKeyword( name );
   };

   this.removeSelectedKeyword = () =>
   {
      let keyword = this.keywordsTreeBox.currentNode.text( 0 );
      if ( keyword )
      {
         engine.keywords.remove( keyword );
         this.keywordSelected = false;
         this.keywordEdit.text = "";
         this.refreshTreeBox();
         this.updateToolButtons();
      }

      // event handler management
      if ( this.onKeywordsUpdated )
         this.onKeywordsUpdated();
   };

   this.selectKeyword = ( name ) =>
   {
      for ( let i = 0; i < this.keywordsTreeBox.numberOfChildren; i++ )
      {
         let node = this.keywordsTreeBox.child( i );
         if ( node.text( 0 ) == name )
         {
            this.keywordsTreeBox.currentNode = this.keywordsTreeBox.child( i )
            break;
         }
      }
      this.keywordsTreeBox.onNodeSelectionUpdated( true /* programmatically */ )
   };

   this.reset = () =>
   {
      this.keywordSelected = false;
      this.keywordEdit.text = "";
      this.update();
   }

   this.update = () =>
   {
      this.refreshTreeBox();
      this.updateToolButtons();
   };

   // init
   this.update();
}

KeywordsEditor.prototype = new Control;

// ----------------------------------------------------------------------------

/**
 * Controls that contains the settings to calibrate a group.
 *
 * @param {*} parent
 */
function CalibrationSettingsControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Calibration Settings", parent );

   // utils
   let
   {
      factory
   } = WBPPUtils.shared();

   let warningOnPrecalibratedDarks =
      "<p>By disabling this option, you are going to apply an image calibration task that deviates from our recommended " +
      "best practices. Pre-calibrated master dark frames may suffer from severe clipping of negative pixel values that " +
      "occur after bias subtraction. This is especially problematic with DSLR or CMOS cameras.</p>" +
      "<p>Our recommended practice is to <i>never</i> use bias-subtracted dark frames or master dark frames. In this way " +
      "bias subtraction will be applied internally by the ImageCalibration process with no possibility of truncation, " +
      "and the use of pedestals will be unnecessary.</p>";

   // -------------------------------------------------------
   // CONTAINS BIAS
   this.containsBiasCheckBox = new CheckBox( this );
   this.containsBiasCheckBox.text = "Contains Bias";
   this.containsBiasCheckBox.enabled = true;
   this.containsBiasCheckBox.toolTip = "<p>Disable this option when using pre-calibrated master dark frames, " +
      "i.e. master dark frames with the bias signal subtracted.</p>"
   this.containsBiasCheckBox.onCheck = ( checked ) =>
   {
      let group = this.parent.selectedGroup;
      group.containsBias = checked;
      this.updateWithGroup( group );
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
      if ( !checked )
         ( new MessageBox( warningOnPrecalibratedDarks, TITLE, StdIcon_Warning, StdButton_Ok ) ).execute();
   };

   // -------------------------------------------------------
   // CONTAINS DARK
   this.optimizeMasterDarkCheckBox = new CheckBox( this );
   this.optimizeMasterDarkCheckBox.text = "Optimize Master Dark";
   this.optimizeMasterDarkCheckBox.toolTip = "<p>Enable this option to apply <i>dark frame optimization</i> during calibration " +
      "of flat and light frames.</p>" +
      "<p>The dark frame optimization routine computes dark scaling factors to minimize the noise induced by dark subtraction.</p>";
   this.optimizeMasterDarkCheckBox.enabled = true;
   this.optimizeMasterDarkCheckBox.onCheck = ( checked ) =>
   {
      let group = this.parent.selectedGroup;
      group.optimizeMasterDark = checked;
      this.updateWithGroup( group );
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
   };

   // -------------------------------------------------------
   // MASTER CHECKBOXES
   this.masterDarkCheckbox = new CheckBox( this );
   this.masterFlatCheckbox = new CheckBox( this );

   this.masterDarkCheckbox.text = "Dark:";
   this.masterFlatCheckbox.text = "Flat:";

   this.masterDarkCheckbox.setScaledFixedWidth( 50 );
   this.masterFlatCheckbox.setScaledFixedWidth( 50 );

   this.masterDarkCheckbox.onCheck = ( checked ) =>
   {
      this.parent.selectedGroup.forceNoDark = !checked;
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
   };
   this.masterFlatCheckbox.onCheck = ( checked ) =>
   {
      this.parent.selectedGroup.forceNoFlat = !checked;
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
   };

   // -------------------------------------------------------
   // MASTER COMBO BOXES
   this.masterDarkCombo = new ComboBox( this );
   this.masterFlatCombo = new ComboBox( this );

   // assign the override group preference or clear it
   this.masterDarkCombo.onItemSelected = ( itemIndex ) =>
   {
      let selectedGroup = this.parent.selectedGroup;
      if ( itemIndex == 0 )
         selectedGroup.overrideDark = undefined;
      else
         selectedGroup.overrideDark = this.masterDarkCombo.__groups__[ itemIndex - 1 ];
      // refresh table
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
   };

   // assign the override group preference or clear it
   this.masterFlatCombo.onItemSelected = ( itemIndex ) =>
   {
      let selectedGroup = this.parent.selectedGroup;
      if ( itemIndex == 0 )
         selectedGroup.overrideFlat = undefined;
      else
         selectedGroup.overrideFlat = this.masterFlatCombo.__groups__[ itemIndex - 1 ];
      // refresh table
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
   };

   // -------------------------------------------------------
   // LAYOUT THE masters checkbox and dropdown
   let darkSizer = new HorizontalSizer;
   darkSizer.spacing = 4;
   darkSizer.add( this.masterDarkCheckbox );
   darkSizer.add( this.masterDarkCombo );

   let flatSizer = new HorizontalSizer;
   flatSizer.spacing = 4;
   flatSizer.add( this.masterFlatCheckbox );
   flatSizer.add( this.masterFlatCombo );

   // -------------------------------------------------------
   // LAYOUT THE CONTROLS

   this.add( this.containsBiasCheckBox );
   this.add( darkSizer );
   this.add( flatSizer );
   this.add( this.optimizeMasterDarkCheckBox );

   // -------------------------------------------------------
   // HELPERS

   // the control gets updated given the group
   this.updateWithGroup = ( group ) =>
   {
      if ( !group )
      {
         this.hide();
         return;
      }
      this.showControlsForGroup( group );
      this.containsBiasCheckBox.checked = group.containsBias;
      this.masterDarkCheckbox.checked = !group.forceNoDark;
      this.masterFlatCheckbox.checked = !group.forceNoFlat;
      this.optimizeMasterDarkCheckBox.checked = group.optimizeMasterDark;
   };

   // show/hide the proper options depending on the given group
   this.showControlsForGroup = ( group ) =>
   {
      if ( !group )
         return;

      this.containsBiasCheckBox.visible = ( group.hasMaster && group.imageType == ImageType.DARK );

      this.masterDarkCheckbox.visible = !group.hasMaster && ( group.imageType == ImageType.FLAT || group.imageType == ImageType.LIGHT );
      this.masterFlatCheckbox.visible = ( group.imageType == ImageType.LIGHT );

      this.masterDarkCombo.visible = this.masterDarkCheckbox.visible;
      this.masterFlatCombo.visible = this.masterFlatCheckbox.visible;

      this.optimizeMasterDarkCheckBox.visible = !group.hasMaster && ( group.imageType == ImageType.FLAT || group.imageType == ImageType.LIGHT );

      if ( this.masterDarkCombo.visible )
         this.enableDarkFor( group );

      if ( this.masterFlatCombo.visible )
         this.enableFlatFor( group );
   };

   // hides all options
   this.hide = () =>
   {
      this.containsBiasCheckBox.visible = false;
      this.masterDarkCheckbox.visible = false;
      this.masterFlatCheckbox.visible = false;
      this.masterDarkCombo.visible = false;
      this.masterFlatCombo.visible = false;
      this.optimizeMasterDarkCheckBox.visible = false;
   };

   // prepare the Dark dropdown populating the compatible master darks for the given group
   this.enableDarkFor = ( group ) =>
   {
      // get compatible darks
      let groupList = engine.getCalibrationGroupsMatching(
      {
         imageType: ImageType.DARK,
         binning: group.binning
      } );
      // empty the list
      this.masterDarkCombo.clear();
      // fill the list
      this.masterDarkCombo.addItem( "Auto" );
      groupList.forEach( group => this.masterDarkCombo.addItem( group.dropdownShortString() ) );
      this.masterDarkCombo.__groups__ = groupList;

      if ( group.overrideDark )
      {
         for ( let i = 0; i < groupList.length; i++ )
            if ( groupList[ i ].id == group.overrideDark.id )
            {
               this.masterDarkCombo.currentItem = i + 1;
               break;
            }
      }
      else
         this.masterDarkCombo.currentItem = 0;

      // optimize master dark checkbox
      this.optimizeMasterDarkCheckBox.checked = group.optimizeMasterDark;
   };

   // prepare the Flat dropdown populating the compatible master flats for the given group
   this.enableFlatFor = ( group ) =>
   {
      // get compatible Flats
      let groupList = engine.getCalibrationGroupsMatching(
      {
         imageType: ImageType.FLAT,
         binning: group.binning
      } );
      // empty the list
      this.masterFlatCombo.clear();
      // fill the list
      this.masterFlatCombo.addItem( "Auto" );
      groupList.forEach( group => this.masterFlatCombo.addItem( group.dropdownShortString() ) );
      this.masterFlatCombo.__groups__ = groupList;

      if ( group.overrideFlat )
      {
         for ( let i = 0; i < groupList.length; i++ )
            if ( group.overrideFlat && groupList[ i ].id == group.overrideFlat.id )
            {
               this.masterFlatCombo.currentItem = i + 1;
               break;
            }
      }
      else
         this.masterFlatCombo.currentItem = 0;
   };

   // start with all options hidden
   this.hide();
}
CalibrationSettingsControl.prototype = new Control;

/**
 * Controls that contains the pedestal settings of a calibration group.
 *
 * @param {*} parent
 */
function PedestalSettingsControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Output Pedestal Settings", parent );

   let labelWidth = this.font.width( "Value (DN):m" );

   // -------------------------------------------------------
   // LIGHT OUTPUT PEDESTAL MODE
   let pedestalModeTooltip = "<p>This parameter defines how to calculate and apply output pedestals.</p>" +
      "<p><b>Literal value</b> allows you to specify a pedestal value in 16-bit data number units (DN) with the " +
      "<i>value</i> parameter below.</p>" +
      "<p><b>Automatic</b> will apply an automatically calculated pedestal value to frames that have negative, zero " +
      "or insignificant (to machine epsilon) minimum pixel values after calibration. Automatic pedestals are computed " +
      "as statistically significant, robust mean values to ensure positivity of the calibrated image.</p>" +
      "<p>See the <i>value</i> parameter for more information on output pedestals.</p>";

   this.pedestalModeLabel = new Label( this );
   this.pedestalModeLabel.text = "Mode:";
   this.pedestalModeLabel.setFixedWidth( labelWidth );
   this.pedestalModeLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.pedestalModeLabel.toolTip = pedestalModeTooltip;

   this.pedestalModeComboBox = new ComboBox( this );
   this.pedestalModeComboBox.toolTip = pedestalModeTooltip;
   this.pedestalModeComboBox.addItem( "Automatic" );
   this.pedestalModeComboBox.addItem( "Literal value" );

   this.pedestalModeComboBox.onItemSelected = ( value ) =>
   {
      let selectedGroup = this.parent.selectedGroup;
      selectedGroup.lightOutputPedestalMode = value;
      this.updateWithGroup( selectedGroup );
      this.parent.update();
   };

   // -------------------------------------------------------
   // LIGHT OUTPUT PEDESTAL VALUE
   let lightOutputPedestalTooltip = "<p>The <i>output pedestal</i> is a small quantity expressed in " +
      "the 16-bit unsigned integer range (from 0 to 65535). It is added at the end of the calibration process " +
      "and its purpose is to prevent negative values that can occur sometimes as a result of overscan and bias " +
      "subtraction. Under normal conditions, you should need no pedestal to calibrate science frames because " +
      "mean sky background levels should be sufficiently high as to avoid negative values. The default value is zero.</p>";

   this.lightOutputPedestalLabel = new Label( this );
   this.lightOutputPedestalLabel.text = "Value (DN):";
   this.lightOutputPedestalLabel.setFixedWidth( labelWidth );
   this.lightOutputPedestalLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.lightOutputPedestalLabel.toolTip = lightOutputPedestalTooltip;

   this.lightOutputPedestalSpinBox = new SpinBox( this );
   this.lightOutputPedestalSpinBox.minValue = 0;
   this.lightOutputPedestalSpinBox.maxValue = 999;
   this.lightOutputPedestalSpinBox.toolTip = lightOutputPedestalTooltip;

   this.lightOutputPedestalSpinBox.onValueUpdated = ( value ) =>
   {
      let selectedGroup = this.parent.selectedGroup;
      selectedGroup.lightOutputPedestal = value;
      this.updateWithGroup( selectedGroup );
      this.parent.update();
   };

   // -------------------------------------------------------
   // LIGHT OUTPUT PEDESTAL THRESHOLD
   let lightOutputPedestalThresholdTooltip =
      "<p>Minimum fraction of negative or insignificant calibrated pixels to trigger generation of an automatic output pedestal.</p>" +
      "<p>This parameter represents a fraction of the total image pixels in the [0,1] range. When the image has more than this " +
      "fraction of negative or insignificant pixels after calibration and the <i>output pedestal mode</i> is set to <i>automatic</i>, " +
      "the process will compute a robust, statistically significant additive pedestal to ensure positivity of the calibrated image.</p>" +
      "<p>The default value is 0.001, which represents a 0.1% of the total pixels.</p>";

   this.lightOutputPedestalThresholdControl = new NumericEdit( this );
   this.lightOutputPedestalThresholdControl.label.text = "Threshold:";
   this.lightOutputPedestalThresholdControl.label.setFixedWidth( labelWidth );
   this.lightOutputPedestalThresholdControl.label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.lightOutputPedestalThresholdControl.label.toolTip = lightOutputPedestalThresholdTooltip;
   this.lightOutputPedestalThresholdControl.edit.setVariableWidth();
   this.lightOutputPedestalThresholdControl.setReal( true );
   this.lightOutputPedestalThresholdControl.setRange( 0, 0.01 );
   this.lightOutputPedestalThresholdControl.setPrecision( 4 );


   this.lightOutputPedestalThresholdControl.toolTip = lightOutputPedestalThresholdTooltip;

   this.lightOutputPedestalThresholdControl.onValueUpdated = ( value ) =>
   {
      let selectedGroup = this.parent.selectedGroup;
      selectedGroup.lightOutputPedestalThreshold = value;
      // this.updateWithGroup( selectedGroup );
      this.parent.update();
   };

   // -------------------------------------------------------
   // APPLY TO ALL LIGHT FRAMES
   this.pedestalThresholdApplyToAllButton = new PushButton( this );
   this.pedestalThresholdApplyToAllButton.text = "Apply to all light frames";
   this.pedestalThresholdApplyToAllButton.tooltip = "Apply the output pedestal threshold value of the current group to all" +
      +" other groups.";
   this.pedestalThresholdApplyToAllButton.onClick = () =>
   {
      let selectedGroup = this.parent.selectedGroup;
      engine.applyOutputPedestalThreshold( selectedGroup.lightOutputPedestalMode, selectedGroup.lightOutputPedestal, selectedGroup.lightOutputPedestalThreshold );
      this.updateWithGroup( this.parent.selectedGroup );
      this.parent.update();
      // expicitly redraw the post-process dialog
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
   };

   //

   let pedestalModeSizer = new HorizontalSizer;
   pedestalModeSizer.spacing = 4;
   pedestalModeSizer.add( this.pedestalModeLabel );
   pedestalModeSizer.add( this.pedestalModeComboBox, 100 );

   let outputPedestalSizer = new HorizontalSizer;
   outputPedestalSizer.spacing = 4;
   outputPedestalSizer.add( this.lightOutputPedestalLabel );
   outputPedestalSizer.add( this.lightOutputPedestalSpinBox );
   outputPedestalSizer.addStretch();

   let outputPedestalThresholdSizer = new HorizontalSizer;
   outputPedestalThresholdSizer.add( this.lightOutputPedestalThresholdControl );
   outputPedestalThresholdSizer.addStretch();

   this.add( pedestalModeSizer );
   this.add( outputPedestalSizer );
   this.add( outputPedestalThresholdSizer );
   this.add( this.pedestalThresholdApplyToAllButton );

   // -------------------------------------------------------
   // HELPERS

   // the control gets updated given the group
   this.updateWithGroup = ( group ) =>
   {
      if ( !group )
      {
         this.hide();
         return;
      }

      this.showControlsForGroup( group );
      let pedestalModeAuto = group.lightOutputPedestalMode == WBPPPedestalMode.AUTO;
      this.lightOutputPedestalSpinBox.value = pedestalModeAuto ? 0 : group.lightOutputPedestal;
      this.pedestalModeComboBox.currentItem = group.lightOutputPedestalMode;
      this.lightOutputPedestalThresholdControl.setValue( group.lightOutputPedestalThreshold );

      this.lightOutputPedestalLabel.enabled = group.lightOutputPedestalMode == WBPPPedestalMode.LITERAL;
      this.lightOutputPedestalSpinBox.enabled = group.lightOutputPedestalMode == WBPPPedestalMode.LITERAL;
      this.lightOutputPedestalThresholdControl.enabled = group.lightOutputPedestalMode == WBPPPedestalMode.AUTO;
   };

   // show/hide the proper options depending on the given group
   this.showControlsForGroup = ( group ) =>
   {
      if ( !group )
         return;

      this.pedestalModeLabel.visible = group.imageType == ImageType.LIGHT;
      this.pedestalModeComboBox.visible = group.imageType == ImageType.LIGHT;

      this.lightOutputPedestalLabel.visible = group.imageType == ImageType.LIGHT;
      this.lightOutputPedestalSpinBox.visible = group.imageType == ImageType.LIGHT;

      this.lightOutputPedestalThresholdControl.visible = group.imageType == ImageType.LIGHT;
      this.pedestalThresholdApplyToAllButton.visible = group.imageType == ImageType.LIGHT;
   };

   // hides all options
   this.hide = () =>
   {
      this.pedestalModeLabel.visible = false;
      this.pedestalModeComboBox.visible = false;
      this.lightOutputPedestalLabel.visible = false;
      this.lightOutputPedestalSpinBox.visible = false;
      this.lightOutputPedestalThresholdControl.visible = false;
      this.pedestalThresholdApplyToAllButton.visible = false;
   };

   // start with all options hidden
   this.hide();
}
PedestalSettingsControl.prototype = new Control;

/**
 * Controls that contains the pedestal settings of a calibration group.
 *
 * @param {*} parent
 */
function CosmeticCorrectionSettingsControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Cosmetic Correction", parent );

   let labelWidth = this.font.width( "Template:m" );

   // -------------------------------------------------------
   // Cosmetic Correction template icon
   let templateIconIdToolTip = "<p>Identifier of an existing CosmeticCorrection icon " +
      "that will be used as a template to apply a cosmetic correction process to the " +
      "calibrated frames. Cosmetic correction will be applied just after calibration, " +
      "before deBayering (when appropriate) and registration.</p>";

   this.CCLabel = new Label( this );
   this.CCLabel.text = "Template:";
   this.CCLabel.setFixedWidth( labelWidth );
   this.CCLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.CCLabel.toolTip = templateIconIdToolTip;

   this.CCComboBox = new ComboBox( this );
   this.CCComboBox.toolTip = templateIconIdToolTip;
   this.CCComboBox.addItem( "<none>" );
   let icons = ProcessInstance.iconsByProcessId( "CosmeticCorrection" );
   for ( let i = 0; i < icons.length; ++i )
      this.CCComboBox.addItem( icons[ i ] );

   this.CCComboBox.onItemSelected = ( value ) =>
   {
      let templateName = this.CCComboBox.itemText( value )
      let selectedGroup = this.parent.selectedGroup;
      selectedGroup.CCTemplate = value == 0 ? "" : templateName;
      this.updateWithGroup( selectedGroup );
      this.parent.update();
   };

   // -------------------------------------------------------
   // APPLY TO ALL LIGHT FRAMES
   this.CCApplyToAllButton = new PushButton( this );
   this.CCApplyToAllButton.text = "Apply to all light frames";
   this.CCApplyToAllButton.tooltip = "Apply the output pedestal threshold value of the current group to all" +
      +" other groups.";
   this.CCApplyToAllButton.onClick = () =>
   {
      let selectedGroup = this.parent.selectedGroup;
      engine.applyCCTemplate( selectedGroup.CCTemplate );
      this.updateWithGroup( this.parent.selectedGroup );
      this.parent.update();
   };

   //

   let CCTemplateSizer = new HorizontalSizer;
   CCTemplateSizer.spacing = 4;
   CCTemplateSizer.add( this.CCLabel );
   CCTemplateSizer.add( this.CCComboBox );

   this.add( CCTemplateSizer );
   this.add( this.CCApplyToAllButton );

   // -------------------------------------------------------
   // HELPERS

   // the control gets updated given the group
   this.updateWithGroup = ( group ) =>
   {
      if ( !group )
      {
         this.hide();
         return;
      }

      this.showControlsForGroup( group );

      let currentItem = 0;
      for ( let i = 1; i < this.CCComboBox.numberOfItems; i++ )
      {
         if ( this.CCComboBox.itemText( i ) == group.CCTemplate )
         {
            currentItem = i;
            break;
         }
      }
      this.CCComboBox.currentItem = currentItem;
   };

   // show/hide the proper options depending on the given group
   this.showControlsForGroup = ( group ) =>
   {
      if ( !group )
         return;

      this.CCLabel.visible = group.imageType == ImageType.LIGHT;
      this.CCComboBox.visible = group.imageType == ImageType.LIGHT;
      this.CCApplyToAllButton.visible = group.imageType == ImageType.LIGHT;
   };

   // hides all options
   this.hide = () =>
   {
      this.CCLabel.visible = false;
      this.CCComboBox.visible = false;
      this.CCApplyToAllButton.visible = false;
   };

   // start with all options hidden
   this.hide();
}
CosmeticCorrectionSettingsControl.prototype = new Control;

/**
 * Control that contains the debayer settings of a group
 */

function DebayerSettingsControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "CFA Settings", parent );

   // -------------------------------------------------------
   // IS CFA
   this.isCFACheckBox = new CheckBox( this );
   this.isCFACheckBox.text = "CFA images";
   this.isCFACheckBox.toolTip = "<p>When checked, the WBPP script works under the assumption that all " +
      "the frames in this group (calibration and light frames) have been mosaiced with a Color Filter Array (CFA) pattern, " +
      "such as a Bayer pattern. When this option is enabled, an additional deBayering task will be performed on light frames prior to image " +
      "registration using the <i>Bayer/mosaic pattern</i> and <i>DeBayer method</i> parameters.</p>";
   this.isCFACheckBox.enabled = true;
   this.isCFACheckBox.onCheck = ( checked ) =>
   {
      let group = this.parent.selectedGroup;
      group.isCFA = checked;
      this.updateWithGroup( group );
      engine.reconstructPostProcessGroups();
      this.parent.treeBoxRowSelectionUpdated( this.parent.selectedNode.parentTree );
      // expicitly redraw the post-process dialog manually to avoid the loss of focus on
      // the currently selected group
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
   };

   // -------------------------------------------------------
   // BAYER PATTERN
   this.bayerPatternLabel = new Label( this );
   this.bayerPatternLabel.text = "Bayer/mosaic pattern:";
   this.bayerPatternLabel.minWidth = this.dialog.labelWidth1;
   this.bayerPatternLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.bayerPatternComboBox = new ComboBox( this );
   this.bayerPatternComboBox.addItem( "Auto" );
   this.bayerPatternComboBox.addItem( "RGGB" );
   this.bayerPatternComboBox.addItem( "BGGR" );
   this.bayerPatternComboBox.addItem( "GBRG" );
   this.bayerPatternComboBox.addItem( "GRBG" );
   this.bayerPatternComboBox.addItem( "GRGB" );
   this.bayerPatternComboBox.addItem( "GBGR" );
   this.bayerPatternComboBox.addItem( "RGBG" );
   this.bayerPatternComboBox.addItem( "BGRG" );
   this.bayerPatternComboBox.onItemSelected = ( itemIndex ) =>
   {
      this.parent.selectedGroup.CFAPattern = itemIndex;
      this.updateWithGroup( this.parent.selectedGroup );
      this.parent.update();
   };

   this.bayerPatternSizer = new HorizontalSizer;
   this.bayerPatternSizer.spacing = 4;
   this.bayerPatternSizer.add( this.bayerPatternLabel );
   this.bayerPatternSizer.add( this.bayerPatternComboBox, 100 );

   //

   this.debayerMethodLabel = new Label( this );
   this.debayerMethodLabel.text = "DeBayer method:";
   this.debayerMethodLabel.minWidth = this.dialog.labelWidth1;
   this.debayerMethodLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.debayerMethodComboBox = new ComboBox( this );
   this.debayerMethodComboBox.addItem( "SuperPixel" );
   this.debayerMethodComboBox.addItem( "Bilinear" );
   this.debayerMethodComboBox.addItem( "VNG" );
   this.debayerMethodComboBox.onItemSelected = ( itemIndex ) =>
   {
      this.parent.selectedGroup.debayerMethod = itemIndex;
      this.updateWithGroup( this.parent.selectedGroup );
      this.parent.update();
   };

   this.debayerMethodSizer = new HorizontalSizer;
   this.debayerMethodSizer.spacing = 4;
   this.debayerMethodSizer.add( this.debayerMethodLabel );
   this.debayerMethodSizer.add( this.debayerMethodComboBox, 100 );

   // -------------------------------------------------------
   // SEPARATE CFA FLAT SCALING  FACTORS
   this.separateCFAFlatScalingFactorsCheckBox = new CheckBox( this );
   this.separateCFAFlatScalingFactorsCheckBox.text = "Separate CFA flat scaling factors";
   this.separateCFAFlatScalingFactorsCheckBox.toolTip = "<p>When this option is enabled and the master flat frame is a single-channnel " +
      "image mosaiced with a Color Filter Array (CFA), such as a Bayer or X-Trans filter pattern, three separate master " +
      "flat scaling factors are computed for red, green and blue CFA components, respectively. When this option is disabled, " +
      "a single scaling factor is computed for the whole master flat frame, ignoring CFA components.</p>";
   this.separateCFAFlatScalingFactorsCheckBox.onCheck = ( checked ) =>
   {
      let group = this.parent.selectedGroup;
      group.separateCFAFlatScalingFactors = checked;
      this.updateWithGroup( group );
      this.parent.update();
   };

   // -------------------------------------------------------

   this.cfaSettingsApplyToAllButton = new PushButton( this );
   this.cfaSettingsApplyToAllButton.text = "Apply globally";
   this.cfaSettingsApplyToAllButton.tooltip = "Apply the Debayer settings of the current group to the rest of the" +
      +" groups in the project.";
   this.cfaSettingsApplyToAllButton.onClick = () =>
   {
      let imageType = this.parent.selectedGroup.imageType;
      let response = new MessageBox( "This operation will apply the same CFA settings of the selected group to all " +
         StackEngine.imageTypeToString( imageType ) + " groups in the session. Do you want to proceed?",
         "Apply CFA Settings", StdIcon_Question, StdButton_Yes, StdButton_No ).execute();
      if ( response == StdButton_Yes )
      {
         let imageType = this.parent.selectedGroup.imageType;
         engine.applyCFASettings( imageType, this.isCFACheckBox.checked, this.bayerPatternComboBox.currentItem, this.debayerMethodComboBox.currentItem, this.separateCFAFlatScalingFactorsCheckBox.checked );
         engine.reconstructPostProcessGroups();
         this.updateWithGroup( this.parent.selectedGroup );
         this.parent.update();
         // expicitly redraw the post-process dialog
         this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
      }
   };

   // -------------------------------------------------------
   // LAYOUT THE CONTROLS
   this.add( this.isCFACheckBox );
   this.add( this.bayerPatternSizer );
   this.add( this.debayerMethodSizer );
   this.add( this.separateCFAFlatScalingFactorsCheckBox );
   this.add( this.cfaSettingsApplyToAllButton );

   // -------------------------------------------------------
   // HELPERS
   this.updateWithGroup = ( group ) =>
   {
      if ( !group )
      {
         this.hide();
         return;
      }

      this.showControlsForGroup( group );
      this.isCFACheckBox.checked = group.isCFA;
      this.bayerPatternComboBox.currentItem = group.CFAPattern;
      this.debayerMethodComboBox.currentItem = group.debayerMethod;
      this.separateCFAFlatScalingFactorsCheckBox.checked = group.separateCFAFlatScalingFactors && group.isCFA;
   };

   this.showControlsForGroup = ( group ) =>
   {
      if ( !group )
      {
         this.hide();
         return;
      }

      this.isCFACheckBox.visible = ( group.imageType == ImageType.FLAT || group.imageType == ImageType.LIGHT );
      this.bayerPatternComboBox.visible = group.imageType == ImageType.LIGHT;
      this.debayerMethodComboBox.visible = group.imageType == ImageType.LIGHT;
      this.bayerPatternLabel.visible = this.bayerPatternComboBox.visible;
      this.debayerMethodLabel.visible = this.debayerMethodComboBox.visible;
      this.cfaSettingsApplyToAllButton.visible = this.isCFACheckBox.visible;

      this.bayerPatternComboBox.enabled = group.isCFA;
      this.debayerMethodComboBox.enabled = group.isCFA;

      this.separateCFAFlatScalingFactorsCheckBox.visible = group.imageType == ImageType.FLAT;
      this.separateCFAFlatScalingFactorsCheckBox.enabled = group.isCFA;

      this.cfaSettingsApplyToAllButton.text = ( group.imageType == ImageType.LIGHT ) ? "Apply to all light frames" : "Apply to all flat frames";
   };

   this.hide = () =>
   {
      this.isCFACheckBox.visible = false;
      this.bayerPatternLabel.visible = false;
      this.debayerMethodLabel.visible = false;
      this.bayerPatternComboBox.visible = false;
      this.debayerMethodComboBox.visible = false;
      this.separateCFAFlatScalingFactorsCheckBox.visible = false;
      this.cfaSettingsApplyToAllButton.visible = false;
   };

   this.hide();
}

DebayerSettingsControl.prototype = new Control;

// -----------------------------------------------------------------------------
/**
 * This dialog depicts the calibration flow diagram for the specified groups
 * and options.
 *
 * The calibration flow is generically represented by a group (light) that
 * needs to be bias-subtracted, dark-subtracted and flat-divided. If dark is
 * marked to be calibrated then the bias is subtracted to the Dark before
 * subtracting the dark from the light. If Dark needs to be optimized then the
 * dark is multiplied by a proper constant before being subtracted from the
 * light (but after the bias subtraction).
 */
function CalibrationFlowDialog( bias, dark, flat, light, darkIsCalibrated, darkIsOptimized )
{
   this.__base__ = Dialog;
   this.__base__();

   // dynamic window sizing
   this.layoutTimer = new Timer( 0 /*interval*/ , false /*periodic*/ );
   this.layoutTimer.onTimeout = () =>
   {
      let rect = this.graphRect.inflatedBy( this.logicalPixelsToPhysical( 16 ) );
      this.graph.setFixedSize( rect.width, rect.height );
      this.ensureLayoutUpdated();
      this.adjustToContents();
      this.setFixedSize();
   };

   // title
   this.label = new Label( this );
   this.label.text = light ? "<b>Calibration Flow Diagram</b><br>" + light.toString() : "";
   this.label.textAlignment = TextAlign_Center;
   this.label.useRichText = true;

   // graph
   this.graphGenerator = new GraphGenerator( this );
   this.graph = new ScrollBox( this );
   this.graph.backgroundColor = 0xFFFFFFFF;
   this.graph.viewport.onPaint = ( x0, y0, x1, y1 ) =>
   {
      this.graphRect = this.graphGenerator.generateGraph( this.graph.viewport, bias, dark, flat, light, darkIsCalibrated, darkIsOptimized );
      // Schedule a single timer event that will be delivered as soon as we
      // return to the event loop. This is necessary because we cannot layout
      // the control from an onPaint() event handler, since doing that would
      // lead to infinite recursion.
      this.layoutTimer.start();
   };

   // layout
   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.label );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.graph );
}

CalibrationFlowDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/**
 * Controls that shows the controls for the post processing configurations
 *
 * @param {*} parent
 */
function PostProcessingConfiguration( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Configuration", parent );

   //

   this.debayerOutputLabel = new Label( this );
   this.debayerOutputLabel.text = "Debayer:";
   this.debayerOutputLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.debayerOutputComboBox = new ComboBox( this );
   this.debayerOutputComboBox.addItem( "Combined RGB color" );
   this.debayerOutputComboBox.addItem( "Separate RGB channels" );
   this.debayerOutputComboBox.addItem( "Combined + Separated" );
   // this.debayerOutputComboBox.addItem( "Both" );
   this.debayerOutputComboBox.onItemSelected = function( item )
   {
      engine.debayerOutputMethod = item;
      engine.reconstructGroups();
      parent.dialog.refreshTreeBoxes();

   };

   this.debayerOutputComboBox.toolTip = this.debayerOutputLabel.toolTip =
      "<p>Demosaiced/interpolated images can be generated as combined RGB color images, " +
      "as separate RGB channels stored as monochrome images, or applying both options at the same time.</p>" +
      "<p>Generation of single RGB color images is the default option.<br>" +
      "Separate RGB channel images can be useful for correction of non-isotropic channel misalignments " +
      "such as those caused by chromatic aberration and atmospheric dispersion, by computing image registration " +
      "transformations with distortion correction among all color components of a data set. This procedure is " +
      "compatible with normal image integrations as well as drizzle integrations.</p>" +
      "<p>Separate channel file names will carry the _R, _G and _B suffixes, respectively for the red, green and blue components.";

   this.debayerOutputSizer = new HorizontalSizer;
   this.debayerOutputSizer.spacing = 4;
   this.debayerOutputSizer.add( this.debayerOutputLabel );
   this.debayerOutputSizer.add( this.debayerOutputComboBox, 100 );

   //
   let lightExposureTooltip = "<p>Light frames with exposure times differing less than this value " +
      "(in seconds) will be grouped into the same master.</p>";

   this.lightExposureToleranceLabel = new Label( this );
   this.lightExposureToleranceLabel.text = "Exposure tolerance:";
   this.lightExposureToleranceLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.lightExposureToleranceLabel.toolTip = lightExposureTooltip;

   this.lightExposureToleranceSpinBox = new SpinBox( this );
   this.lightExposureToleranceSpinBox.minValue = 0;
   this.lightExposureToleranceSpinBox.maxValue = 3600;
   this.lightExposureToleranceSpinBox.toolTip = lightExposureTooltip;
   this.lightExposureToleranceSpinBox.onValueUpdated = function( value )
   {
      engine.lightExposureTolerancePost = value;
      engine.reconstructGroups();
      parent.dialog.refreshTreeBoxes();
   };

   this.groupLightsWithDifferentExposureSizer = new HorizontalSizer;
   this.groupLightsWithDifferentExposureSizer.spacing = 4;
   this.groupLightsWithDifferentExposureSizer.add( this.lightExposureToleranceLabel );
   this.groupLightsWithDifferentExposureSizer.addStretch();
   this.groupLightsWithDifferentExposureSizer.add( this.lightExposureToleranceSpinBox );

   // layout
   this.add( this.debayerOutputSizer );
   this.add( this.groupLightsWithDifferentExposureSizer );

   this.update = () =>
   {
      this.debayerOutputComboBox.currentItem = engine.debayerOutputMethod;
      this.lightExposureToleranceSpinBox.value = engine.lightExposureTolerancePost;
   }

   this.update();
}

PostProcessingConfiguration.prototype = new Dialog;

/**
 * Controls that shows the status of the post processing steps
 *
 * @param {*} parent
 */
function PostProcessingSteps( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Active  Steps", parent );

   // Lineat Pattern Subtraction
   this.isLPSEnabled = new CheckBox( this );
   this.isLPSEnabled.text = "Linear Pattern Subtraction";
   this.isLPSEnabled.toolTip = "<p>When checked, Linear Pattern Subtraction will be performed.</p>";
   this.isLPSEnabled.onCheck = () =>
   {
      this.update();
   }

   // Image weighting
   this.isSubframeWeightingEnabled = new CheckBox( this );
   this.isSubframeWeightingEnabled.text = "Subframe Weighting";
   this.isSubframeWeightingEnabled.toolTip = "<p>When checked, weights will be computed and stored into the light frames FITS headers.</p>";
   this.isSubframeWeightingEnabled.onCheck = () =>
   {
      this.update();
   }

   // Image Registration
   this.isImageRegistrationEnabled = new CheckBox( this );
   this.isImageRegistrationEnabled.text = "Image Registration";
   this.isImageRegistrationEnabled.toolTip = "<p>When checked, light frames star alignment will be performed.</p>";
   this.isImageRegistrationEnabled.onCheck = () =>
   {
      this.update();
   }

   // Local Normalization
   this.isLocalNormalizationEnabled = new CheckBox( this );
   this.isLocalNormalizationEnabled.text = "Local Normalization";
   this.isLocalNormalizationEnabled.toolTip = "<p>When checked, local normalization will be used as normalization method.</p>";
   this.isLocalNormalizationEnabled.onCheck = () =>
   {
      this.update();
   }

   // Image Integration
   this.isImageIntegrationEnabled = new CheckBox( this );
   this.isImageIntegrationEnabled.text = "Image Integration";
   this.isImageIntegrationEnabled.toolTip = "<p>When checked, master light frames will be generated.</p>";
   this.isImageIntegrationEnabled.onCheck = () =>
   {
      this.update();
   }

   // layout
   this.add( this.isLPSEnabled );
   this.add( this.isSubframeWeightingEnabled );
   this.add( this.isImageRegistrationEnabled );
   this.add( this.isLocalNormalizationEnabled );
   this.add( this.isImageIntegrationEnabled );

   this.update = () =>
   {
      let makeBold = ( comp ) =>
      {
         let font = comp.font;
         font.bold = comp.enabled;
         comp.font = font;
      }

      this.isLPSEnabled.checked = engine.linearPatternSubtraction;
      this.isLPSEnabled.enabled = engine.linearPatternSubtraction;
      makeBold( this.isLPSEnabled );
      this.isSubframeWeightingEnabled.checked = engine.subframeWeightingEnabled;
      this.isSubframeWeightingEnabled.enabled = engine.subframeWeightingEnabled;
      makeBold( this.isSubframeWeightingEnabled );
      this.isImageRegistrationEnabled.checked = engine.imageRegistration;
      this.isImageRegistrationEnabled.enabled = engine.imageRegistration;
      makeBold( this.isImageRegistrationEnabled );
      this.isLocalNormalizationEnabled.checked = engine.localNormalization;
      this.isLocalNormalizationEnabled.enabled = engine.localNormalization;
      makeBold( this.isLocalNormalizationEnabled );
      this.isImageIntegrationEnabled.checked = engine.integrate;
      this.isImageIntegrationEnabled.enabled = engine.integrate;
      makeBold( this.isImageIntegrationEnabled );
   }

   this.update();
}

PostProcessingSteps.prototype = new Dialog;

// ----------------------------------------------------------------------------
/**
 * Main TAB view containing the list of bias/dark/flat/light tree boxes and the
 * group options / calibration diagram button on the right side.
 */
function CalibrationPanel( parent, viewMode )
{
   this.__base__ = Control;
   this.__base__( parent );
   this.viewMode = viewMode;

   // styles
   this.defaultFontColor = 0xFF000000;
   this.calibratedByColor = 0xFF333333;
   this.calibratesColorAuto = 0xFF55AA55;
   this.calibratesColorManual = 0xFF154C99;
   this.disabledColor = 0x88BBBBBB;
   this.fontSize = 8;
   this.optionsPanelWidth = parent.logicalPixelsToPhysical( 220 );

   // utils
   let
   {
      factory
   } = WBPPUtils.shared();

   // global maps of the group.id with the note in the tree boxes
   this.groupNodeMap = {};
   this.selectedGroup = undefined;
   this.selectedNode = undefined;

   // active calibration groups
   this.graphCalibrationBias = null;
   this.graphCalibrationDark = null;
   this.graphCalibrationFlat = null;
   this.graphCalibrationLight = null;

   // tree box descriptors
   this.biasDescriptor = new GroupTreeBoxDescriptor( ImageType.BIAS );
   this.darkDescriptor = new GroupTreeBoxDescriptor( ImageType.DARK );
   this.flatDescriptor = new GroupTreeBoxDescriptor( ImageType.FLAT );
   this.lightDescriptor = new GroupTreeBoxDescriptor( ImageType.LIGHT );

   // ---
   // GUI
   // ---

   // create the tree box listing the groups
   this.biasTreeBox = factory.CPTreeBox( parent );
   this.darkTreeBox = factory.CPTreeBox( parent );
   this.flatTreeBox = factory.CPTreeBox( parent );
   this.lightTreeBox = factory.CPTreeBox( parent );
   this.treeBoxes = [ this.biasTreeBox, this.darkTreeBox, this.flatTreeBox, this.lightTreeBox ];

   // Tree Box sizer
   let treeBoxesSizer = new VerticalSizer;
   treeBoxesSizer.spacing = 4;
   treeBoxesSizer.add( this.biasTreeBox );
   treeBoxesSizer.add( this.darkTreeBox );
   treeBoxesSizer.add( this.flatTreeBox );
   treeBoxesSizer.add( this.lightTreeBox );

   // Logic to be performed when a row is selected in a treebox
   this.treeBoxRowSelectionUpdated = ( treeBox ) =>
   {
      // do nothing if view mode is POST
      if ( this.viewMode == WBPPKeywordMode.POST )
         return undefined;

      // record that the selection has been changed
      this.selectionWasUpdated = true;
      // reset all tree boxes to their default appearance
      this.resetTableAppearance( this.disabledColor );
      // search for the selected node and retrieve the group
      if ( treeBox.selectedNodes.length == 0 )
      {
         this.unselectAllNodesExcept();
         this.update();
         return undefined;
      }
      // save the currently selected node and group
      this.selectedNode = treeBox.selectedNodes[ 0 ];
      this.selectedGroup = engine.groupsManager.getGroupByID( this.selectedNode.__groupID__ );
      // get the selected node on the current treeBox
      let nodeFont = this.selectedNode.font( 0 );
      nodeFont.bold = true;
      let nodeColor = this.selectedNode.textColor( 0 );
      this.setNodeFontBold( this.selectedNode, true );
      this.setNodeFontColor( this.selectedNode, nodeColor );
      // unselect all nodes in all treeBoxes except the currently selected one
      this.unselectAllNodesExcept( this.selectedNode );
      // get the array of groups that are calibrated by this group and
      // highlight the rows corresponding to the calibrated groups
      engine.getGroupsCalibratedBy( this.selectedGroup ).forEach( cg =>
      {
         let n = this.groupNodeMap[ cg.id ];
         this.setNodeFontBold( n, true );
         this.setNodeFontColor( n, this.calibratedByColor );
      } );

      // initialize the current graph groups
      this.graphCalibrationBias = null;
      this.graphCalibrationDark = null;
      this.graphCalibrationFlat = null;
      this.graphCalibrationLight = this.selectedGroup.isMaster ? null : this.selectedGroup;
      // get the array of groups that calibrates the selected group
      let calibGroups = this.selectedGroup.isMaster ?
      {} : engine.getCalibrationGroupsFor( this.selectedGroup );
      Object.keys( calibGroups ).forEach( key =>
      {
         if ( calibGroups[ key ] )
         {
            let cg = calibGroups[ key ];
            let n = this.groupNodeMap[ cg.id ];
            this.setNodeFontBold( n, true );
            if ( ( this.selectedGroup.overrideDark && this.selectedGroup.overrideDark.id == cg.id ) ||
               ( this.selectedGroup.overrideFlat && this.selectedGroup.overrideFlat.id == cg.id ) )
            {
               this.setNodeFontColor( n, this.calibratesColorManual );
            }
            else
            {
               this.setNodeFontColor( n, this.calibratesColorAuto );
            }
            // prepare the current calibration files
            if ( !this.selectedGroup.isMaster )
               switch ( cg.imageType )
               {
                  case ImageType.BIAS:
                     this.graphCalibrationBias = cg;
                     break;
                  case ImageType.DARK:
                     this.graphCalibrationDark = cg;
                     break;
                  case ImageType.FLAT:
                     this.graphCalibrationFlat = cg;
                     break;
               }
         }
      } );
      // update all nodes content
      this.update();
      // enable edit group and show graph button
      this.updateShowGraphButton();
      // show the controls for the selected group
      this.calibrationSettingsControl.updateWithGroup( this.selectedGroup );
      this.pedestalSettingsControl.updateWithGroup( this.selectedGroup );
      this.cosmeticCorrectionSettingsControl.updateWithGroup( this.selectedGroup );
      this.debayerSettingsControl.updateWithGroup( this.selectedGroup );
      return undefined;
   };

   // Logic called after "treeBoxRowSelectionUpdated" is completed
   // this completes the lifacycle of select-mouse up and manages
   // the case where we click on an already selected group
   // to disable it
   this.onNodeClicked = () =>
   {
      // do nothing on post-process view mode
      if ( this.viewMode == WBPPKeywordMode.POST )
         return undefined;

      if ( !this.selectionWasUpdated )
      {
         this.resetTableAppearance( this.defaultFontColor );
         this.unselectAllNodesExcept();
         this.graphCalibrationBias = null;
         this.graphCalibrationDark = null;
         this.graphCalibrationFlat = null;
         this.graphCalibrationLight = null;
         this.calibrationSettingsControl.hide();
         this.pedestalSettingsControl.hide();
         this.cosmeticCorrectionSettingsControl.hide();
         this.debayerSettingsControl.hide();
      }
      this.selectionWasUpdated = false;
      this.updateShowGraphButton();
      return undefined;
   };

   // update the whole Control Panel data
   this.update = () =>
   {
      this.setTreeBoxesVisibility();

      // update all nodes content
      engine.groupsManager.groupsForMode( this.viewMode ).forEach( group =>
      {
         let node = this.groupNodeMap[ group.id ];
         if ( node )
            this.fillGroupRowData( group, node );
      } );
      // show the status if needed
      let status = this.selectedGroup ? this.selectedGroup.status() :
      {};
      this.statusTextBox.visible = ( status.errors || status.warnings ) != undefined;
      this.statusTextBox.text = "<b><u>" + ( status.errors ? "ERRORS" : "WARNINGS" ) + "</u></b><br><br>" + ( status.errors || status.warnings || "" );
      this.statusTextBox.foregroundColor = status.errors ? 0xFFFF0000 : 0xFF333333;
      this.statusTextBox.home();
      this.updateShowGraphButton();
      this.postProcessStatus.update();

   };

   this.setTreeBoxesVisibility = () =>
   {
      [ this.biasTreeBox, this.darkTreeBox, this.flatTreeBox ].forEach( treeBox =>
      {
         treeBox.visible = this.viewMode == WBPPKeywordMode.PRE;
      } );
   };

   // add one column for each keyword after the status column
   this.setColumnHeadersForMode = ( mode ) =>
   {
      [
      {
         treeBox: this.biasTreeBox,
         descriptor: this.biasDescriptor
      },
      {
         treeBox: this.darkTreeBox,
         descriptor: this.darkDescriptor
      },
      {
         treeBox: this.flatTreeBox,
         descriptor: this.flatDescriptor
      },
      {
         treeBox: this.lightTreeBox,
         descriptor: this.lightDescriptor
      } ].forEach( obj =>
      {
         let headers = obj.descriptor.headers( this.viewMode );

         obj.treeBox.numberOfColumns = headers.length;
         for ( let i = 0; i < headers.length; i++ )
         {
            obj.treeBox.setHeaderAlignment( i, TextAlign_Center );
            if ( headers[ i ] )
               obj.treeBox.setHeaderText( i, headers[ i ] );
            else
               obj.treeBox.setHeaderText( i, "" );
         }
      } );

      // CUSTOM: in POST-PROCESS mode we add the total integration time to the header
      if ( this.viewMode == WBPPGroupingMode.POST )
      {
         // MANUALLY SET
         let totalIntegrationTime = WBPPUtils.shared().formatTimeDuration( engine.groupsManager.totalIntegrationTime() );
         this.lightTreeBox.setHeaderText( 5, "Integration Time\n" + totalIntegrationTime );
      }

      let keywordsForMode = engine.keywords.keywordsForMode( mode );
      let nKeywords = keywordsForMode.length;
      let biasBaseColumns = this.biasDescriptor.headers( mode ).length;
      this.biasTreeBox.numberOfColumns = biasBaseColumns + nKeywords + 1;
      let darkBaseColumns = this.darkDescriptor.headers( mode ).length;
      this.darkTreeBox.numberOfColumns = darkBaseColumns + nKeywords + 1;
      let flatBaseColumns = this.flatDescriptor.headers( mode ).length;
      this.flatTreeBox.numberOfColumns = flatBaseColumns + nKeywords + 1;
      let lightBaseColumns = this.lightDescriptor.headers( mode ).length;
      this.lightTreeBox.numberOfColumns = lightBaseColumns + nKeywords + 1;

      keywordsForMode.forEach( ( kw, i ) =>
      {
         this.biasTreeBox.setHeaderText( biasBaseColumns + i, kw.name );
         this.biasTreeBox.setHeaderAlignment( biasBaseColumns + i, TextAlign_Center );

         this.darkTreeBox.setHeaderText( darkBaseColumns + i, kw.name );
         this.darkTreeBox.setHeaderAlignment( darkBaseColumns + i, TextAlign_Center );

         this.flatTreeBox.setHeaderText( flatBaseColumns + i, kw.name );
         this.flatTreeBox.setHeaderAlignment( flatBaseColumns + i, TextAlign_Center );

         this.lightTreeBox.setHeaderText( lightBaseColumns + i, kw.name );
         this.lightTreeBox.setHeaderAlignment( lightBaseColumns + i, TextAlign_Center );
      } );

      // remove the header title from the last column
      this.biasTreeBox.setHeaderText( biasBaseColumns + nKeywords, "" );
      this.darkTreeBox.setHeaderText( darkBaseColumns + nKeywords, "" );
      this.flatTreeBox.setHeaderText( flatBaseColumns + nKeywords, "" );
      this.lightTreeBox.setHeaderText( lightBaseColumns + nKeywords, "" );
   };

   // enable/disable show Graph Button
   this.updateShowGraphButton = () =>
   {
      this.showGraphButton.enabled =
         this.viewMode == WBPPKeywordMode.PRE &&
         this.selectedGroup != undefined &&
         ( [ ImageType.BIAS, ImageType.DARK ].indexOf( this.selectedGroup.imageType ) == -1 );
   };

   // stores the state of the selection to support the logic of select/unselect an item by
   // clicking it multiple times
   this.selectionWasUpdated = false;

   // set the treeBox event handlers for all treeBoxes
   this.treeBoxes.forEach( treeBox =>
   {
      treeBox.onNodeSelectionUpdated = () =>
      {
         this.treeBoxRowSelectionUpdated( treeBox );
      };
      treeBox.onNodeClicked = this.onNodeClicked;
   } );

   // show edit button
   this.calibrationSettingsControl = new CalibrationSettingsControl( this );
   this.calibrationSettingsControl.setFixedWidth( this.optionsPanelWidth );
   this.calibrationSettingsControl.visible = this.viewMode == WBPPGroupingMode.PRE;

   this.pedestalSettingsControl = new PedestalSettingsControl( this );
   this.pedestalSettingsControl.setFixedWidth( this.optionsPanelWidth );
   this.pedestalSettingsControl.visible = this.viewMode == WBPPGroupingMode.PRE;

   this.cosmeticCorrectionSettingsControl = new CosmeticCorrectionSettingsControl( this );
   this.cosmeticCorrectionSettingsControl.setFixedWidth( this.optionsPanelWidth );
   this.cosmeticCorrectionSettingsControl.visible = this.viewMode == WBPPGroupingMode.PRE;

   this.debayerSettingsControl = new DebayerSettingsControl( this );
   this.debayerSettingsControl.setFixedWidth( this.optionsPanelWidth );
   this.debayerSettingsControl.visible = this.viewMode == WBPPGroupingMode.PRE;

   // show calibration flow button
   this.showGraphButton = new PushButton( this );
   this.showGraphButton.text = "Show Calibration Diagram";
   this.showGraphButton.enabled = false;
   this.showGraphButton.onClick = () =>
   {
      // show a dialog depicting the calibration flow
      let calibrationFlowDialog = new CalibrationFlowDialog(
         this.graphCalibrationBias,
         this.graphCalibrationDark,
         this.graphCalibrationFlat,
         this.graphCalibrationLight,
         this.selectedGroup.containsBias,
         this.selectedGroup.optimizeMasterDark );
      calibrationFlowDialog.execute();
   };
   this.showGraphButton.visible = this.viewMode == WBPPGroupingMode.PRE;

   // show the post process configurations
   this.postProcessConfig = new PostProcessingConfiguration( this );
   this.postProcessConfig.setFixedWidth( this.optionsPanelWidth );
   this.postProcessConfig.visible = this.viewMode == WBPPGroupingMode.POST;

   // show the post process steps
   this.postProcessStatus = new PostProcessingSteps( this );
   this.postProcessStatus.setFixedWidth( this.optionsPanelWidth );
   this.postProcessStatus.visible = this.viewMode == WBPPGroupingMode.POST;

   this.showGraphButtonSizer = new HorizontalSizer;
   this.showGraphButtonSizer.margin = 6;
   this.showGraphButtonSizer.add( this.showGraphButton );

   // the status textbox reporting the detected issues of the selected groups
   this.statusTextBox = new TextBox( this );
   this.statusTextBox.readOnly = true;
   this.statusTextBox.setFixedWidth( this.optionsPanelWidth );
   this.statusTextBox.setMinHeight( 0 );

   // bottom sizer
   let optionsSizer = new VerticalSizer;
   optionsSizer.spacing = 4;

   if ( this.viewMode == WBPPGroupingMode.PRE )
   {
      optionsSizer.add( this.calibrationSettingsControl );
      optionsSizer.add( this.pedestalSettingsControl );
      optionsSizer.add( this.cosmeticCorrectionSettingsControl );
      optionsSizer.add( this.debayerSettingsControl );
      optionsSizer.add( this.statusTextBox );
      optionsSizer.add( this.showGraphButtonSizer );
   }
   else
   {
      optionsSizer.add( this.postProcessConfig );
      optionsSizer.add( this.postProcessStatus );
   }
   optionsSizer.addStretch();

   // layout elements
   this.sizer = new HorizontalSizer;
   this.sizer.margin = 4;
   this.sizer.spacing = 4;
   this.sizer.add( treeBoxesSizer );
   this.sizer.add( optionsSizer );

   // -------------------
   // AUXILIARY FUNCTIONS
   // -------------------

   // reset font and color or the treeboxes to their default value
   this.resetTableAppearance = ( defaultColor ) =>
   {
      this.setTreeBoxesVisibility();
      [ this.biasTreeBox, this.darkTreeBox, this.flatTreeBox, this.lightTreeBox ].forEach( treeBox =>
      {
         // do not update hodden treeboxe
         if ( treeBox.visible )
            for ( let i = 0; i < treeBox.numberOfChildren; i++ )
            {
               // remove bold font from all table
               let node = treeBox.child( i );
               this.setNodeFontSize( node, this.fontSize );
               this.setNodeFontBold( node, false );
               this.setNodeFontColor( node, defaultColor );

               let group = engine.groupsManager.getGroupByID( node.__groupID__ );

               // if group is master than enphasize the first column
               if ( group.hasMaster )
               {
                  let font = node.font( 0 );
                  font.bold = true;
                  node.setFont( 0, font );
               }
            }
      } );

      // hide the status box
      this.statusTextBox.visible = false;
   };

   this.setNodeFontSize = ( node, size ) =>
   {
      if ( !node.parentTree )
         return;
      for ( let j = 0; j < node.parentTree.numberOfColumns; j++ )
      {
         let font = node.font( j );
         font.pointSize = size;
         node.setFont( j, font );
      }
   };

   // sets the given font and color on the provided node
   this.setNodeFontColor = ( node, color ) =>
   {
      if ( !node.parentTree )
         return;
      for ( let j = 0; j < node.parentTree.numberOfColumns; j++ )
      {
         node.setTextColor( j, color );
      }
   };

   // enable/disable the node font bold property
   this.setNodeFontBold = ( node, bold ) =>
   {
      if ( !node.parentTree )
         return;
      for ( let j = 0; j < node.parentTree.numberOfColumns; j++ )
      {
         let font = node.font( j );
         font.bold = bold;
         node.setFont( j, font );
      }
   };

   // unselects all nodes extcept the node provided, if no node is providede then
   // all nodes will be unselected
   this.unselectAllNodesExcept = ( exceptNode ) =>
   {
      [ this.biasTreeBox, this.darkTreeBox, this.flatTreeBox, this.lightTreeBox ].forEach( treeBox =>
      {
         for ( let i = 0; i < treeBox.numberOfChildren; i++ )
         {
            let node = treeBox.child( i );
            if ( node !== exceptNode )
               node.selected = false;
         }
      } );

      if ( !exceptNode )
      {
         this.selectedNode = null;
         this.selectedGroup = null;
         this.showGraphButton.enabled = false;
      }
   };

   // redraw (regenerate fron scratch) the whole content of the treeBoxes.
   // to be called when new group is added or removed
   this.redraw = () =>
   {
      // clear the panel
      this.clearTable();

      this.setColumnHeadersForMode( this.viewMode );

      // get groups (only pre-process)
      let biasGroups = engine.getSortedGroupsOfType( ImageType.BIAS, this.viewMode );
      let darkGroups = engine.getSortedGroupsOfType( ImageType.DARK, this.viewMode );
      let flatGroups = engine.getSortedGroupsOfType( ImageType.FLAT, this.viewMode );
      let lightGroups = engine.getSortedGroupsOfType( ImageType.LIGHT, this.viewMode );
      // filter out hidden groups
      lightGroups = lightGroups.filter( group => !group.isHidden );
      biasGroups.__imageType__ = ImageType.BIAS;
      darkGroups.__imageType__ = ImageType.DARK;
      flatGroups.__imageType__ = ImageType.FLAT;
      lightGroups.__imageType__ = ImageType.LIGHT;

      let prepareTreeGroup = ( groups, treeBox ) =>
      {
         for ( let i = 0; i < groups.length; i++ )
         {
            let node = new TreeBoxNode( treeBox );
            // fill the Group <-> Node mapping
            this.groupNodeMap[ groups[ i ].id ] = node;
            node.__groupID__ = groups[ i ].id
            this.fillGroupRowData( groups[ i ], node );
         }

         let height;
         if ( groups.__imageType__ == ImageType.BIAS )
            height = 38 + Math.max( 2, groups.length ) * 24; // in logical pixels
         else
            height = 60 + Math.max( 2, groups.length ) * 26;

         if ( groups.__imageType__ == ImageType.LIGHT )
            treeBox.setVariableHeight();
         else if ( groups.length <= 5 )
            treeBox.setScaledFixedHeight( height );
         else
            treeBox.setScaledMaxHeight( height );

         for ( let i = 0; i < treeBox.numberOfColumns; ++i )
            treeBox.adjustColumnWidthToContents( i );
         processEvents();
         // Leave enough space in each column to change font weight to bold
         // without clipping cell text contents.
         for ( let i = 0; i < treeBox.numberOfColumns; ++i )
            treeBox.setColumnWidth( i, treeBox.columnWidth( i ) + this.logicalPixelsToPhysical( 16 ) );
      };

      prepareTreeGroup( biasGroups, this.biasTreeBox );
      prepareTreeGroup( darkGroups, this.darkTreeBox );
      prepareTreeGroup( flatGroups, this.flatTreeBox );
      prepareTreeGroup( lightGroups, this.lightTreeBox );

      this.resetTableAppearance( this.defaultFontColor );
      this.calibrationSettingsControl.hide();
      this.pedestalSettingsControl.hide();
      this.cosmeticCorrectionSettingsControl.hide();
      this.debayerSettingsControl.hide();

      // deselect all and refresh
      this.selectedGroup = undefined;
      this.selectedNode = undefined;
      this.update();
   };

   // removes all nodes from all treeBoxes and cleans the node-group map
   this.clearTable = function()
   {
      this.treeBoxes.forEach( treeBox => treeBox.clear() );
      this.groupNodeMap = {};
   };

   // Populate the row corresponding to the given group
   this.fillGroupRowData = function( group, node )
   {

      let
      {
         validCCIconName
      } = WBPPUtils.shared();

      let name = group.hasMaster ? "master" + StackEngine.imageTypeToString( group.imageType ) :
         group.fileItems.length + " frame" + ( ( group.fileItems.length > 1 ) ? "s" : "" );
      let binning = group.binning + "x" + group.binning;
      let exposure = ( group.imageType != ImageType.BIAS ) ? group.exposureToString() : "";
      let filter = group.filter;
      let containsBias = group.containsBias;
      let optimizeMasterDark = group.optimizeMasterDark;
      let hasMaster = group.hasMaster;
      let exactDarkExposureTime = false; // group.imageType == ImageType.FLAT;
      let lightOutputPedestal = group.lightOutputPedestal;

      let checkGreenIcon = ":/browser/enabled.png";
      let disabledIcon = ":/browser/disabled.png"
      let warningIcon = ":/icons/warning.png";
      let cancelIcon = ":/icons/cancel.png";
      let blackCrosslIcon = ":/workspace/close-inactive.png";
      let linkIcon = ":/icons/link.png";
      let rgbIcon = ":/toolbar/image-display-rgb.png";
      let grayIcon = ":/toolbar/image-display-value.png";
      let colorPicureIcon = ":/icons/picture-colors.png";
      let contrastPicureIcon = ":/icons/picture-contrast.png";
      let forceNoMaster = ":/icons/cancel.png";
      let invalidCC = ":/icons/warning.png";

      let darkOptimizationYES = ":/icons/function.png";
      let keywordIndex = -1;

      if ( this.viewMode == WBPPKeywordMode.POST )
      {
         // if max and min exposure differs by more than 1 sec then show both as a range
         exposure = group.exposuresMinMaxRangeString();
         switch ( group.imageType )
         {
            case ImageType.LIGHT:
               // name, binning, exposure and filter
               [ name, binning, exposure, filter ].forEach( ( text, i ) => node.setText( i, text ) );
               node.setAlignment( 2, TextAlign_Right );
               // starting index of light-flat specific columns
               let index = 4;

               // Color Space
               if ( group.isCFA )
               {
                  node.setIcon( index, this.scaledResource( rgbIcon ) );
                  node.setText( index, "RGB" );
               }
               else
               {
                  node.setIcon( index, this.scaledResource( grayIcon ) );
                  node.setText( index, group.associatedRGBchannel || "Gray" );
               }
               index++;

               // exposure time
               node.setText( index, WBPPUtils.shared().formatTimeDuration( group.totalExposureTime() ) );
               index++;

               keywordIndex = index;
         }
      }
      else // PRE
      {
         switch ( group.imageType )
         {
            case ImageType.BIAS:
               [ name, binning ].forEach( ( text, i ) => node.setText( i, text ) );
               keywordIndex = 2;
               break;

            case ImageType.DARK:
               [ name, binning, exposure ].forEach( ( text, i ) => node.setText( i, text ) );

               // contains Bias
               if ( hasMaster )
               {
                  if ( containsBias )
                     node.setIcon( 3, this.scaledResource( checkGreenIcon ) );
                  else
                     node.setIcon( 3, this.scaledResource( disabledIcon ) );
               }
               else
               {
                  node.setIcon( 3, this.scaledResource( checkGreenIcon ) );
               }

               node.setAlignment( 2, TextAlign_Right );
               keywordIndex = 4;
               break;

            case ImageType.FLAT:
            case ImageType.LIGHT:
               // name, binning, exposure and filter
               [ name, binning, exposure, filter ].forEach( ( text, i ) => node.setText( i, text ) );
               node.setAlignment( 2, TextAlign_Right );

               // starting index of light-flat specific columns
               let index = 4;

               // Status - Flat Group
               // Status - Light Group
               let status = group.status();
               if ( status.errors )
                  node.setIcon( index, this.scaledResource( cancelIcon ) );
               else if ( status.warnings )
                  node.setIcon( index, this.scaledResource( warningIcon ) );
               else
                  node.setIcon( index, this.scaledResource( checkGreenIcon ) );
               index++;

               let calibrationgroups = engine.getCalibrationGroupsFor( group );

               // bias
               if ( !group.hasMaster )
                  if ( calibrationgroups.masterBias )
                     node.setIcon( index, this.scaledResource( checkGreenIcon ) );
                  else
                     node.setIcon( index, this.scaledResource( blackCrosslIcon ) );
               index++;

               // dark
               if ( !group.hasMaster )
                  if ( group.forceNoDark )
                     node.setIcon( index, this.scaledResource( forceNoMaster ) );
                  else if ( group.overrideDark )
                  node.setIcon( index, this.scaledResource( linkIcon ) );
               else if ( calibrationgroups.masterDark )
                  node.setIcon( index, this.scaledResource( checkGreenIcon ) );
               else
                  node.setIcon( index, this.scaledResource( blackCrosslIcon ) );
               index++;

               // flat
               if ( group.imageType == ImageType.LIGHT )
               {
                  if ( !group.hasMaster )
                  {
                     if ( group.forceNoFlat )
                        node.setIcon( index, this.scaledResource( forceNoMaster ) );
                     else if ( group.overrideFlat )
                        node.setIcon( index, this.scaledResource( linkIcon ) );
                     else if ( calibrationgroups.masterFlat )
                        node.setIcon( index, this.scaledResource( checkGreenIcon ) );
                     else
                        node.setIcon( index, this.scaledResource( blackCrosslIcon ) );
                  }
                  index++;
               }

               // optimized dark
               if ( !hasMaster )
               {
                  if ( optimizeMasterDark )
                  {
                     node.setIcon( index, this.scaledResource( darkOptimizationYES ) );
                     node.setText( index, "Yes" );
                  }
                  else
                  {
                     node.setIcon( index, this.scaledResource( blackCrosslIcon ) );
                     node.setText( index, "" );
                  }
               }
               index++;

               // light output pedestal - LIGHT Group
               if ( group.imageType == ImageType.LIGHT )
               {
                  if ( group.lightOutputPedestalMode == WBPPPedestalMode.AUTO )
                     node.setText( index, format( "AUTO (%.2f%%)", 100 * group.lightOutputPedestalThreshold ) );
                  else
                     node.setText( index, lightOutputPedestal.toString() );

                  node.setAlignment( index, TextAlign_Right );
                  index++;
               }

               // light output cosmetic correction - LIGHT Group
               if ( group.imageType == ImageType.LIGHT )
               {
                  if ( group.CCTemplate && group.CCTemplate.length > 0 )
                  {

                     if ( validCCIconName( group.CCTemplate ) )
                     {
                        node.setIcon( index, this.scaledResource( checkGreenIcon ) );
                     }
                     else
                     {
                        node.setIcon( index, this.scaledResource( invalidCC ) );
                     }
                     node.setText( index, group.CCTemplate );

                  }
                  else
                  {
                     node.setText( index, "" );
                     node.setIcon( index, this.scaledResource( blackCrosslIcon ) );
                  }
                  index++;
               }

               // CFA Images - Pattern and Methos
               if ( group.isCFA )
               {
                  node.setIcon( index, this.scaledResource( rgbIcon ) );
                  if ( group.imageType == ImageType.FLAT )
                     node.setText( index, "Yes" );
                  else
                     node.setText( index, group.CFAPatternString() );
               }
               else
               {
                  node.setIcon( index, this.scaledResource( grayIcon ) );
                  node.setText( index, "No" );
               }
               index++;

               // CFA scaling - FLAT Group
               if ( group.imageType == ImageType.FLAT )
               {
                  if ( group.isCFA )
                  {
                     if ( group.separateCFAFlatScalingFactors )
                     {
                        node.setIcon( index, this.scaledResource( colorPicureIcon ) );
                        node.setText( index, "Yes" );
                     }
                     else
                     {
                        node.setIcon( index, this.scaledResource( contrastPicureIcon ) );
                        node.setText( index, "No" );
                     }
                  }
                  else
                  {
                     node.setIcon( index, this.scaledResource( blackCrosslIcon ) );
                     node.setText( index, "" );
                  }
                  index++;
               }

               keywordIndex = index;
         }
      }

      // fill the keyword values
      engine.keywords.keywordsForMode( this.viewMode ).forEach( keyword =>
      {
         let value = group.keywords[ keyword.name ] || "-";
         node.setText( keywordIndex, value );
         node.setToolTip( keywordIndex, value );
         keywordIndex++;
      } );
   };

   // initialize
   this.redraw();
}

CalibrationPanel.prototype = new Control;

// ----------------------------------------------------------------------------
/**
 * Base control for grouping controls in an optionally collapsable panel.
 */
function ParametersControl( title, parent, expand, deactivable, collapsable )
{
   this.__base__ = Control;
   if ( parent )
   {
      this.__base__( parent );
      if ( !parent.parameterControls )
         parent.parameterControls = new Array;
      parent.parameterControls.push( this );
   }
   else
      this.__base__();

   if ( expand )
   {
      this.expand = expand;
      this.hide();
   }
   else
      this.expand = false;

   // start always non collapsed
   this.collapsed = false;

   let r = Color.red( this.dialog.backgroundColor );
   let g = Color.green( this.dialog.backgroundColor );
   let b = Color.blue( this.dialog.backgroundColor );
   let r1 = ( r > 16 ) ? r - 16 : r + 16;
   let g1 = ( g > 16 ) ? g - 16 : g + 16;
   let b1 = ( b > 16 ) ? b - 16 : b + 16;
   let r2 = ( r > 16 ) ? r - 8 : r + 16;
   let g2 = ( g > 16 ) ? g - 8 : g + 16;
   let b2 = ( b > 16 ) ? b - 8 : b + 16;

   // Force this generic control (=QWidget) to inherit its dialog's font.
   this.font = this.dialog.font;

   // title bar construction
   if ( deactivable == true )
   {
      this.activableCheckbox = new CheckBox( this );
      this.activableCheckbox.setScaledFixedSize( 20, 20 );
      this.activableCheckbox.toolTip = "Enable or disable the grouping by the listed keywords";
      this.activableCheckbox.checked = true;
      this.activableCheckbox.onCheck = ( value ) =>
      {
         this.contents.enabled = value;
         // invoke the callback if registered
         if ( this.onActivateStatusChanged )
            this.onActivateStatusChanged( value );
      };
   }

   this.titleLabel = new Label( this );
   this.titleLabel.text = title ? title : "";
   this.titleLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   if ( this.expand )
   {
      this.closeButton = new ToolButton( this );
      this.closeButton.icon = this.scaledResource( ":/icons/close.png" );
      this.closeButton.setScaledFixedSize( 20, 20 );
      this.closeButton.toolTip = "Back";
      this.closeButton.onClick = function()
      {
         this.parent.parent.hide();
      };
   }
   else
      this.closeButton = null;

   // prepare the title bar control
   this.titleBar = new Control( this );
   this.titleBarSheetExpanded =
      "QWidget#" + this.titleBar.uniqueId + " {" +
      "border: 1px solid gray;" +
      "border-bottom: none;" +
      "background-color: " + Color.rgbColorToHexString( Color.rgbaColor( r1, g1, b1 ) ) + ";" +
      "}" +
      "QLabel {" +
      "color: blue;" +
      "padding-top: 2px;" +
      "padding-bottom: 2px;" +
      "padding-left: 4px;" +
      "}" +
      "QLabel:disabled {" +
      "color: gray;" +
      "}";
   this.titleBarSheetCollapsed =
      "QWidget#" + this.titleBar.uniqueId + " {" +
      "border: 1px solid gray;" +
      "background-color: " + Color.rgbColorToHexString( Color.rgbaColor( r1, g1, b1 ) ) + ";" +
      "}" +
      "QLabel {" +
      "color: blue;" +
      "padding-top: 2px;" +
      "padding-bottom: 2px;" +
      "padding-left: 4px;" +
      "}" +
      "QLabel:disabled {" +
      "color: gray;" +
      "}"

   this.titleBar.styleSheet = this.scaledStyleSheet( this.titleBarSheetExpanded );
   this.titleBar.sizer = new HorizontalSizer;

   // add the activate checkbox if needed
   if ( this.activableCheckbox )
   {
      this.titleBar.sizer.addSpacing( 8 );
      this.titleBar.sizer.add( this.activableCheckbox );
   }

   // add the title checkbox if needed
   this.titleBar.sizer.add( this.titleLabel );
   this.titleBar.sizer.addStretch();

   if ( collapsable )
   {
      this.collapseButton = new ToolButton( this );
      this.collapseButton.icon = this.scaledResource( ":/auto-hide/expand.png" );
      this.collapseButton.setScaledFixedSize( 20, 20 );
      this.collapseButton.toolTip = "Group's expand/collapse toggle button.";
      this.collapseButton.onClick = () =>
      {
         this.collapsed = !this.collapsed;
         this.contents.visible = !this.collapsed;
         if ( this.collapsed )
            this.titleBar.styleSheet = this.scaledStyleSheet( this.titleBarSheetCollapsed );
         else
            this.titleBar.styleSheet = this.scaledStyleSheet( this.titleBarSheetExpanded );
      };
      this.titleBar.sizer.add( this.collapseButton );
      this.titleBar.sizer.addSpacing( 4 );
   }

   // add the close button if needed
   if ( this.expand )
   {
      this.titleBar.sizer.add( this.closeButton );
      this.titleBar.sizer.addSpacing( 4 );
   }

   // prepare the contents container
   this.contents = new Control( this );
   this.contents.styleSheet = this.scaledStyleSheet(
      "QWidget#" + this.contents.uniqueId + " {" +
      "border: 1px solid gray;" +
      "background-color: " + Color.rgbColorToHexString( Color.rgbaColor( r2, g2, b2 ) ) + ";" +
      "}" );

   this.contents.sizer = new VerticalSizer;
   this.contents.sizer.margin = 6;
   this.contents.sizer.spacing = 6;
   this.contents.sizer.addSpacing( 8 );

   this.sizer = new VerticalSizer;
   this.sizer.add( this.titleBar );
   this.sizer.add( this.contents );

   // HELPERS

   this.add = function( control )
   {
      this.contents.sizer.add( control );
   };

   this.onShow = function()
   {
      if ( this.expand )
         for ( let i = 0; i < this.parent.parameterControls.length; ++i )
         {
            let sibling = this.parent.parameterControls[ i ];
            if ( sibling.uniqueId != this.uniqueId )
               sibling.hide();
         }
   };

   this.onHide = function()
   {
      if ( this.expand )
         for ( let i = 0; i < this.parent.parameterControls.length; ++i )
         {
            let sibling = this.parent.parameterControls[ i ];
            if ( !sibling.expand && sibling.uniqueId != this.uniqueId )
               sibling.show();
         }
   };

   this.activate = () =>
   {
      if ( this.activableCheckbox )
      {
         this.activableCheckbox.checked = true
         this.titleLabel.enabled = true;
      };
      this.contents.enabled = true;

   };

   this.deactivate = () =>
   {
      if ( this.activableCheckbox )
      {
         this.activableCheckbox.checked = false
         this.titleLabel.enabled = false;
      };
      this.contents.enabled = false;
   };
}

ParametersControl.prototype = new Control;

// ----------------------------------------------------------------------------

function OverscanRectControl( parent, overscan, sourceRegion )
{
   this.__base__ = Control;
   if ( parent )
      this.__base__( parent );
   else
      this.__base__();

   let editWidth1 = 7 * this.font.width( "0" );

   this.label = new Label( this );
   this.label.text = ( ( overscan < 0 ) ? "Image" : ( sourceRegion ? "Source" : "Target" ) ) + " region:";
   this.label.minWidth = this.dialog.labelWidth1;
   this.label.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   let fullName = "Overscan " + ( ( overscan < 0 ) ? "image" : "#" + ( overscan + 1 ).toString() + ( sourceRegion ? " source" : " target" ) ) + " region";

   this.leftControl = new NumericEdit( this );
   this.leftControl.overscan = overscan;
   this.leftControl.sourceRegion = sourceRegion;
   this.leftControl.label.hide();
   this.leftControl.setReal( false );
   this.leftControl.setRange( 0, 9999999 )
   this.leftControl.edit.setFixedWidth( editWidth1 );
   this.leftControl.toolTip = "<p>Left coordinate of the " + fullName + " in CCD pixels.</p>"
   this.leftControl.onValueUpdated = function( value )
   {
      let ivalue = Math.trunc( value );
      if ( this.overscan < 0 )
         engine.overscan.imageRect.x0 = ivalue;
      else if ( this.sourceRegion )
         engine.overscan.overscan[ this.overscan ].sourceRect.x0 = ivalue;
      else
         engine.overscan.overscan[ this.overscan ].targetRect.x0 = ivalue;
   };

   this.topControl = new NumericEdit( this );
   this.topControl.overscan = overscan;
   this.topControl.sourceRegion = sourceRegion;
   this.topControl.label.hide();
   this.topControl.setReal( false );
   this.topControl.setRange( 0, 9999999 )
   this.topControl.edit.setFixedWidth( editWidth1 );
   this.topControl.toolTip = "<p>Top coordinate of the " + fullName + " in CCD pixels.</p>"
   this.topControl.onValueUpdated = function( value )
   {
      let ivalue = Math.trunc( value );
      if ( this.overscan < 0 )
         engine.overscan.imageRect.y0 = ivalue;
      else if ( this.sourceRegion )
         engine.overscan.overscan[ this.overscan ].sourceRect.y0 = ivalue;
      else
         engine.overscan.overscan[ this.overscan ].targetRect.y0 = ivalue;
   };

   this.widthControl = new NumericEdit( this );
   this.widthControl.overscan = overscan;
   this.widthControl.sourceRegion = sourceRegion;
   this.widthControl.label.hide();
   this.widthControl.setReal( false );
   this.widthControl.setRange( 0, 9999999 )
   this.widthControl.edit.setFixedWidth( editWidth1 );
   this.widthControl.toolTip = "<p>Width of the " + fullName + " in CCD pixels.</p>"
   this.widthControl.onValueUpdated = function( value )
   {
      let ivalue = Math.trunc( value );
      if ( this.overscan < 0 )
         engine.overscan.imageRect.x1 = engine.overscan.imageRect.x0 + ivalue;
      else if ( this.sourceRegion )
         engine.overscan.overscan[ this.overscan ].sourceRect.x1 = engine.overscan.overscan[ this.overscan ].sourceRect.x0 + ivalue;
      else
         engine.overscan.overscan[ this.overscan ].targetRect.x1 = engine.overscan.overscan[ this.overscan ].targetRect.x0 + ivalue;
   };

   this.heightControl = new NumericEdit( this );
   this.heightControl.overscan = overscan;
   this.heightControl.sourceRegion = sourceRegion;
   this.heightControl.label.hide();
   this.heightControl.setReal( false );
   this.heightControl.setRange( 0, 9999999 )
   this.heightControl.edit.setFixedWidth( editWidth1 );
   this.heightControl.toolTip = "<p>Height of the " + fullName + " in CCD pixels.</p>"
   this.heightControl.onValueUpdated = function( value )
   {
      let ivalue = Math.trunc( value );
      if ( this.overscan < 0 )
         engine.overscan.imageRect.y1 = engine.overscan.imageRect.y0 + ivalue;
      else if ( this.sourceRegion )
         engine.overscan.overscan[ this.overscan ].sourceRect.y1 = engine.overscan.overscan[ this.overscan ].sourceRect.y0 + ivalue;
      else
         engine.overscan.overscan[ this.overscan ].targetRect.y1 = engine.overscan.overscan[ this.overscan ].targetRect.y0 + ivalue;
   };

   this.toolTip = "<p>" + fullName + ".</p>";

   this.sizer = new HorizontalSizer;
   this.sizer.spacing = 4;
   this.sizer.add( this.label );
   this.sizer.add( this.leftControl );
   this.sizer.add( this.topControl );
   this.sizer.add( this.widthControl );
   this.sizer.add( this.heightControl );
   this.sizer.addStretch();
}

OverscanRectControl.prototype = new Control;

// ----------------------------------------------------------------------------

function OverscanRegionControl( parent, overscan )
{
   this.__base__ = Control;
   if ( parent )
      this.__base__( parent );
   else
      this.__base__();

   if ( overscan < 0 )
   {
      this.imageRectControl = new OverscanRectControl( this, overscan, false );

      this.sizer = new VerticalSizer;
      this.sizer.add( this.imageRectControl );
   }
   else
   {
      this.applyCheckBox = new CheckBox( this );
      this.applyCheckBox.overscan = overscan;
      this.applyCheckBox.text = "Overscan #" + ( overscan + 1 ).toString();
      this.applyCheckBox.toolTip = "<p>Enable overscan region #" + ( overscan + 1 ).toString() + ".</p>";
      this.applyCheckBox.onCheck = function( checked )
      {
         engine.overscan.overscan[ this.overscan ].enabled = checked;
         let biasPage = this.dialog.tabBox.pageControlByIndex( ImageType.BIAS );
         biasPage.overscanControl.updateControls();
      };

      this.applySizer = new HorizontalSizer;
      this.applySizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
      this.applySizer.add( this.applyCheckBox );
      this.applySizer.addStretch();

      this.sourceRectControl = new OverscanRectControl( this, overscan, true );
      this.targetRectControl = new OverscanRectControl( this, overscan, false );

      this.sizer = new VerticalSizer;
      this.sizer.spacing = 4;
      this.sizer.add( this.applySizer );
      this.sizer.add( this.sourceRectControl );
      this.sizer.add( this.targetRectControl );
   }
}

OverscanRegionControl.prototype = new Control;

// ----------------------------------------------------------------------------

function OverscanControl( parent, expand )
{
   this.__base__ = ParametersControl;
   this.__base__( "Overscan", parent, expand );

   //

   this.imageControl = new OverscanRegionControl( this, -1 );
   this.overscanControls = new Array;
   this.overscanControls.push( new OverscanRegionControl( this, 0 ) );
   this.overscanControls.push( new OverscanRegionControl( this, 1 ) );
   this.overscanControls.push( new OverscanRegionControl( this, 2 ) );
   this.overscanControls.push( new OverscanRegionControl( this, 3 ) );

   this.add( this.imageControl );
   this.add( this.overscanControls[ 0 ] );
   this.add( this.overscanControls[ 1 ] );
   this.add( this.overscanControls[ 2 ] );
   this.add( this.overscanControls[ 3 ] );

   this.updateControls = function()
   {
      this.imageControl.imageRectControl.leftControl.setValue( engine.overscan.imageRect.x0 );
      this.imageControl.imageRectControl.topControl.setValue( engine.overscan.imageRect.y0 );
      this.imageControl.imageRectControl.widthControl.setValue( engine.overscan.imageRect.width );
      this.imageControl.imageRectControl.heightControl.setValue( engine.overscan.imageRect.height );

      for ( let i = 0; i < 4; ++i )
      {
         let enabled = engine.overscan.overscan[ i ].enabled;
         this.overscanControls[ i ].applyCheckBox.checked = enabled;
         this.overscanControls[ i ].sourceRectControl.leftControl.setValue( engine.overscan.overscan[ i ].sourceRect.x0 );
         this.overscanControls[ i ].sourceRectControl.topControl.setValue( engine.overscan.overscan[ i ].sourceRect.y0 );
         this.overscanControls[ i ].sourceRectControl.widthControl.setValue( engine.overscan.overscan[ i ].sourceRect.width );
         this.overscanControls[ i ].sourceRectControl.heightControl.setValue( engine.overscan.overscan[ i ].sourceRect.height );
         this.overscanControls[ i ].sourceRectControl.enabled = enabled;
         this.overscanControls[ i ].targetRectControl.leftControl.setValue( engine.overscan.overscan[ i ].targetRect.x0 );
         this.overscanControls[ i ].targetRectControl.topControl.setValue( engine.overscan.overscan[ i ].targetRect.y0 );
         this.overscanControls[ i ].targetRectControl.widthControl.setValue( engine.overscan.overscan[ i ].targetRect.width );
         this.overscanControls[ i ].targetRectControl.heightControl.setValue( engine.overscan.overscan[ i ].targetRect.height );
         this.overscanControls[ i ].targetRectControl.enabled = enabled;
      }

      this.contents.enabled = engine.overscan.enabled;
   };

   this.parentOnShow = this.onShow;

   this.onShow = function()
   {
      let biasPage = this.dialog.tabBox.pageControlByIndex( ImageType.BIAS );
      this.setFixedWidth( biasPage.imageIntegrationControl.width );
      this.parentOnShow();
   };
}

OverscanControl.prototype = new Control;

// ----------------------------------------------------------------------------

function BiasOverscanControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Overscan", parent );

   //

   this.applyCheckBox = new CheckBox( this );
   this.applyCheckBox.text = "Apply";
   this.applyCheckBox.toolTip = "<p>Apply overscan correction.</p>";
   this.applyCheckBox.onCheck = function( checked )
   {
      engine.overscan.enabled = checked;
      let biasPage = this.dialog.tabBox.pageControlByIndex( ImageType.BIAS );
      biasPage.biasOverscanControl.updateControls();
      biasPage.overscanControl.updateControls();
   };

   this.applySizer = new HorizontalSizer;
   this.applySizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.applySizer.add( this.applyCheckBox );
   this.applySizer.addStretch();

   //

   this.editButton = new PushButton( this );
   this.editButton.text = "Overscan parameters...";
   this.editButton.icon = this.scaledResource( ":/icons/arrow-right.png" );
   this.editButton.toolTip = "<p>Edit overscan parameters.</p>";
   this.editButton.onClick = function()
   {
      let biasPage = this.dialog.tabBox.pageControlByIndex( ImageType.BIAS );
      biasPage.overscanControl.show();
   };

   this.editSizer = new HorizontalSizer;
   this.editSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.editSizer.add( this.editButton );
   this.editSizer.addStretch();

   //

   this.add( this.applySizer );
   this.add( this.editSizer );

   this.updateControls = function()
   {
      this.applyCheckBox.checked = engine.overscan.enabled;
      this.editButton.enabled = engine.overscan.enabled;
   };
}

BiasOverscanControl.prototype = new Control;

// ----------------------------------------------------------------------------

function ImageIntegrationControl( parent, imageType, expand )
{
   this.__base__ = ParametersControl;
   this.__base__( "Image Integration", parent, expand );

   this.imageType = imageType;

   let IIlabelWidth = this.font.width( "Large-scale pixel rejection: " + "M" );

   //

   this.combinationLabel = new Label( this );
   this.combinationLabel.text = "Combination:";
   this.combinationLabel.minWidth = IIlabelWidth;
   this.combinationLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.combinationComboBox = new ComboBox( this );
   this.combinationComboBox.addItem( "Average" );
   this.combinationComboBox.addItem( "Median" );
   this.combinationComboBox.addItem( "Minimum" );
   this.combinationComboBox.addItem( "Maximum" );
   this.combinationComboBox.onItemSelected = function( item )
   {
      engine.combination[ this.parent.parent.imageType ] = item;
   };

   this.combinationLabel.toolTip = this.combinationComboBox.toolTip =
      "<p><b>Average</b> combination provides the best signal-to-noise ratio in the integrated result.</p>" +
      "<p><b>Median</b> combination provides more robust rejection of outliers, but at the cost of more noise.</p>";

   this.combinationSizer = new HorizontalSizer;
   this.combinationSizer.spacing = 4;
   this.combinationSizer.add( this.combinationLabel );
   this.combinationSizer.add( this.combinationComboBox, 100 );

   //

   this.rejectionAlgorithmLabel = new Label( this );
   this.rejectionAlgorithmLabel.text = "Rejection algorithm:";
   this.rejectionAlgorithmLabel.minWidth = IIlabelWidth;
   this.rejectionAlgorithmLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.rejectionAlgorithmComboBox = new ComboBox( this );
   let names = engine.rejectionNames();
   names.forEach( item => this.rejectionAlgorithmComboBox.addItem( item ) );
   this.rejectionAlgorithmComboBox.onItemSelected = function( item )
   {
      engine.rejection[ this.parent.parent.imageType ] = engine.rejectionFromIndex( item );
      this.parent.parent.updateControls();
   };

   this.rejectionAlgorithmLabel.toolTip = this.rejectionAlgorithmComboBox.toolTip =
      "<p><b>Percentile clipping</b> rejection is excellent to integrate reduced sets of images, such as " +
      "3 to 6 images. This is a single-pass algorithm that rejects pixels outside a fixed range of values " +
      "relative to the median of each pixel stack.</p>" +

      "<p><b>Winsorized sigma clipping</b> is similar to the normal sigma clipping algorithm, but uses a " +
      "special iterative procedure based on Huber's method of robust estimation of parameters through " +
      "<i>Winsorization</i>. This algorithm can yield superior rejection of outliers with better preservation " +
      "of significant data for large sets of images.</p>" +

      "<p><b>Linear fit clipping</b> fits each pixel stack to a straigtht line. The linear fit is optimized " +
      "in the twofold sense of minimizing average absolute deviation and maximizing inliers. This rejection " +
      "algorithm is more robust than sigma clipping for large sets of images, especially in presence of " +
      "additive sky gradients of varying intensity and spatial distribution. For the best performance, use " +
      "this algorithm for large sets of at least 15 images. Five images is the minimum required.</p>" +
      "<p>The <b>Generalized Extreme Studentized Deviate (ESD) Test</b> rejection algorithm is an " +
      "implementation of the method described by Bernard Rosner in his 1983 paper <i>Percentage " +
      "Points for a Generalized ESD Many-Outlier procedure</i>, adapted to the image integration task.<br>" +

      "<p>The <b>Generalized Extreme Studentized Deviate (ESD)</b> algorithm assumes that each pixel stack, in the absence of outliers, follows an approximately " +
      "normal (Gaussian) distribution. It aims to avoiding <i>masking</i>, a serious issue that occurs when " +
      "an outlier goes undetected because its value is similar to another outlier. The performance of this algorithm" +
      "can be excellent for large data sets of 25 or more images, and especially for very large sets of " +
      "50 or more frames. The minimum required is 3 images.</p>" +

      "<p>The <b> Robust Chauvenet Rejection(RCR)</b> is based on the algorithms described inn Maples, M.P., " +
      "Reichart, D. E. et al., <i>Robust Chauvenet Outlier Rejection</i>, Astrophysical Journal Supplement Series, " +
      "2018, 238, A2, 1-49. Our implementation applies three successive stages of rejection with decreasingly robust/" +
      "increasingly precise measures of central tendency and sample deviation, rejecting a single pixel at each iteration" +
      "for maximum stability. This is a single=parameter pixel rejection algorithm introduced since version 1.8.8-13 of " +
      "PixInsight.</p > " +

      "<p><b>Auto</b> selects the best algorithm depending on the amount of images in the group: ";

   this.rejectionAlgorithmSizer = new HorizontalSizer;
   this.rejectionAlgorithmSizer.spacing = 4;
   this.rejectionAlgorithmSizer.add( this.rejectionAlgorithmLabel );
   this.rejectionAlgorithmSizer.add( this.rejectionAlgorithmComboBox, 100 );

   //


   this.percentileLowControl = new NumericControl( this );
   this.percentileLowControl.label.text = "Percentile low:";
   this.percentileLowControl.label.minWidth = IIlabelWidth;
   this.percentileLowControl.setRange( 0, 1 );
   this.percentileLowControl.slider.setRange( 0, 1000 );
   this.percentileLowControl.slider.scaledMinWidth = 200;
   this.percentileLowControl.setPrecision( 2 );
   this.percentileLowControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.percentileLowControl.toolTip = "<p>Low clipping factor for the percentile clipping rejection algorithm.</p>";
   this.percentileLowControl.onValueUpdated = function( value )
   {
      engine.percentileLow[ this.parent.parent.imageType ] = value;
   };

   //

   this.percentileHighControl = new NumericControl( this );
   this.percentileHighControl.label.text = "Percentile high:";
   this.percentileHighControl.label.minWidth = IIlabelWidth;
   this.percentileHighControl.setRange( 0, 1 );
   this.percentileHighControl.slider.setRange( 0, 1000 );
   this.percentileHighControl.slider.scaledMinWidth = 200;
   this.percentileHighControl.setPrecision( 2 );
   this.percentileHighControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.percentileHighControl.toolTip = "<p>High clipping factor for the percentile clipping rejection algorithm.</p>";
   this.percentileHighControl.onValueUpdated = function( value )
   {
      engine.percentileHigh[ this.parent.parent.imageType ] = value;
   };

   //

   this.sigmaLowControl = new NumericControl( this );
   this.sigmaLowControl.label.text = "Sigma low:";
   this.sigmaLowControl.label.minWidth = IIlabelWidth;
   this.sigmaLowControl.setRange( 0, 10 );
   this.sigmaLowControl.slider.setRange( 0, 1000 );
   this.sigmaLowControl.slider.scaledMinWidth = 200;
   this.sigmaLowControl.setPrecision( 2 );
   this.sigmaLowControl.setValue( 4.0 );
   this.sigmaLowControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.sigmaLowControl.toolTip = "<p>Low clipping factor for the sigma clipping rejection algorithms.</p>";
   this.sigmaLowControl.onValueUpdated = function( value )
   {
      engine.sigmaLow[ this.parent.parent.imageType ] = value;
   };

   //

   this.sigmaHighControl = new NumericControl( this );
   this.sigmaHighControl.label.text = "Sigma high:";
   this.sigmaHighControl.label.minWidth = IIlabelWidth;
   this.sigmaHighControl.setRange( 0, 10 );
   this.sigmaHighControl.slider.setRange( 0, 1000 );
   this.sigmaHighControl.slider.scaledMinWidth = 200;
   this.sigmaHighControl.setPrecision( 2 );
   this.sigmaHighControl.setValue( 2.0 );
   this.sigmaHighControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.sigmaHighControl.toolTip = "<p>High clipping factor for the sigma clipping rejection algorithms.</p>";
   this.sigmaHighControl.onValueUpdated = function( value )
   {
      engine.sigmaHigh[ this.parent.parent.imageType ] = value;
   };

   //

   this.linearFitLowControl = new NumericControl( this );
   this.linearFitLowControl.label.text = "Linear fit low:";
   this.linearFitLowControl.label.minWidth = IIlabelWidth;
   this.linearFitLowControl.setRange( 0, 10 );
   this.linearFitLowControl.slider.setRange( 0, 1000 );
   this.linearFitLowControl.slider.scaledMinWidth = 200;
   this.linearFitLowControl.setPrecision( 2 );
   this.linearFitLowControl.setValue( 5.0 );
   this.linearFitLowControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.linearFitLowControl.toolTip = "<p>Low clipping factor for the linear fit clipping rejection algorithm.</p>";
   this.linearFitLowControl.onValueUpdated = function( value )
   {
      engine.linearFitLow[ this.parent.parent.imageType ] = value;
   };

   //

   this.linearFitHighControl = new NumericControl( this );
   this.linearFitHighControl.label.text = "Linear fit high:";
   this.linearFitHighControl.label.minWidth = IIlabelWidth;
   this.linearFitHighControl.setRange( 0, 10 );
   this.linearFitHighControl.slider.setRange( 0, 1000 );
   this.linearFitHighControl.slider.scaledMinWidth = 200;
   this.linearFitHighControl.setPrecision( 2 );
   this.linearFitHighControl.setValue( 2.5 );
   this.linearFitHighControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.linearFitHighControl.toolTip = "<p>High clipping factor for the linear fit clipping rejection algorithm.</p>";
   this.linearFitHighControl.onValueUpdated = function( value )
   {
      engine.linearFitHigh[ this.parent.parent.imageType ] = value;
   };

   //

   if ( ImageIntegration.prototype.Rejection_ESD )
   {
      this.ESD_OutliersControl = new NumericControl( this );
      this.ESD_OutliersControl.label.text = "ESD outliers:";
      this.ESD_OutliersControl.label.minWidth = IIlabelWidth;
      this.ESD_OutliersControl.setRange( 0, 1 );
      this.ESD_OutliersControl.slider.setRange( 0, 1000 );
      this.ESD_OutliersControl.slider.scaledMinWidth = 200;
      this.ESD_OutliersControl.setPrecision( 2 );
      this.ESD_OutliersControl.setValue( 0.3 );
      this.ESD_OutliersControl.edit.setFixedWidth( this.dialog.numericEditWidth );
      this.ESD_OutliersControl.toolTip = "<p>Expected maximum fraction of outliers for the generalized ESD rejection algorithm.</p>" +
         "<p>For example, a value of 0.2 applied to a stack of 10 pixels means that the ESD algorithm will be limited to detect a maximum of " +
         "two outlier pixels, or in other words, only 0, 1 or 2 outliers will be detectable in such case. The default value is 0.3, which allows the algorithm " +
         "to detect up to a 30% of outlier pixels in each pixel stack.</p>";
      this.ESD_OutliersControl.onValueUpdated = function( value )
      {
         engine.ESD_Outliers[ this.parent.parent.imageType ] = value;
      };
   }

   if ( ImageIntegration.prototype.Rejection_ESD )
   {
      this.ESD_SignificanceControl = new NumericControl( this );
      this.ESD_SignificanceControl.label.text = "ESD significance:";
      this.ESD_SignificanceControl.label.minWidth = IIlabelWidth;
      this.ESD_SignificanceControl.setRange( 0, 1 );
      this.ESD_SignificanceControl.slider.setRange( 0, 1000 );
      this.ESD_SignificanceControl.slider.scaledMinWidth = 200;
      this.ESD_SignificanceControl.setPrecision( 2 );
      this.ESD_SignificanceControl.setValue( 0.05 );
      this.ESD_SignificanceControl.edit.setFixedWidth( this.dialog.numericEditWidth );
      this.ESD_SignificanceControl.toolTip = "<p>Probability of making a type 1 error (false positive) in the generalized ESD rejection algorithm.</p>" +
         "<p>This is the significance level of the outlier detection hypothesis test. For example, a significance level of 0.01 means that a 1% chance " +
         "of being wrong when rejecting the null hypothesis (that there are no outliers in a given pixel stack) is acceptable. The default value is 0.05 " +
         "(5% significance level).</p>";
      this.ESD_SignificanceControl.onValueUpdated = function( value )
      {
         engine.ESD_Significance[ this.parent.parent.imageType ] = value;
      };
   }

   if ( ImageIntegration.prototype.Rejection_RCR )
   {
      this.RCR_Limit = new NumericControl( this );
      this.RCR_Limit.label.text = "RCR Limit:";
      this.RCR_Limit.label.minWidth = IIlabelWidth;
      this.RCR_Limit.setRange( 0, 1 );
      this.RCR_Limit.slider.setRange( 0, 100 );
      this.RCR_Limit.slider.scaledMinWidth = 200;
      this.RCR_Limit.setPrecision( 2 );
      this.RCR_Limit.setValue( 0.1 );
      this.RCR_Limit.edit.setFixedWidth( this.dialog.numericEditWidth );
      this.RCR_Limit.toolTip = "<p>Limit for the altered Chauvenet rejection criterion.</p > " +
         "<p>The larger the value of this parameter, the more pixels will be rejected by the Robust " +
         "Chauvenet Rejection algorithm.</p>";
      this.RCR_Limit.onValueUpdated = function( value )
      {
         engine.RCR_Limit[ this.parent.parent.imageType ] = value;
      };
   }

   this.add( this.combinationSizer );
   this.add( this.rejectionAlgorithmSizer );
   this.add( this.percentileLowControl );
   this.add( this.percentileHighControl );
   this.add( this.sigmaLowControl );
   this.add( this.sigmaHighControl );
   this.add( this.linearFitLowControl );
   this.add( this.linearFitHighControl );
   if ( ImageIntegration.prototype.Rejection_ESD )
   {
      this.add( this.ESD_OutliersControl );
      this.add( this.ESD_SignificanceControl );
   }
   if ( ImageIntegration.prototype.Rejection_RCR )
   {
      this.add( this.RCR_Limit );
   }

   if ( this.imageType == ImageType.FLAT )
   {
      this.flatLargeScaleRejectionCheckBox = new CheckBox( this );
      this.flatLargeScaleRejectionCheckBox.text = "Large-scale pixel rejection";
      this.flatLargeScaleRejectionCheckBox.toolTip = "<p>Apply large-scale pixel rejection, high pixel sample values. " +
         "Useful to improve rejection of stars for integration of sky flats.</p>";
      this.flatLargeScaleRejectionCheckBox.onCheck = function( checked )
      {
         engine.flatsLargeScaleRejection = checked;
         this.parent.parent.updateControls();
      };

      this.flatLargeScaleRejectionSizer = new HorizontalSizer;
      this.flatLargeScaleRejectionSizer.addUnscaledSpacing( IIlabelWidth + this.logicalPixelsToPhysical( 4 ) );
      this.flatLargeScaleRejectionSizer.add( this.flatLargeScaleRejectionCheckBox );
      this.flatLargeScaleRejectionSizer.addStretch();

      //

      this.flatLargeScaleRejectionLayersLabel = new Label( this );
      this.flatLargeScaleRejectionLayersLabel.text = "Large-scale layers:";
      this.flatLargeScaleRejectionLayersLabel.minWidth = IIlabelWidth;
      this.flatLargeScaleRejectionLayersLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

      this.flatLargeScaleRejectionLayersSpinBox = new SpinBox( this );
      this.flatLargeScaleRejectionLayersSpinBox.minValue = 1;
      this.flatLargeScaleRejectionLayersSpinBox.maxValue = 6;
      this.flatLargeScaleRejectionLayersSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
      this.flatLargeScaleRejectionLayersSpinBox.onValueUpdated = function( value )
      {
         engine.flatsLargeScaleRejectionLayers = value;
      };

      this.flatLargeScaleRejectionLayersLabel.toolTip = this.flatLargeScaleRejectionLayersSpinBox.toolTip =
         "<p>Large-scale pixel rejection, number of protected small-scale wavelet layers. " +
         "Increase to restrict large-scale rejection to larger structures of contiguous rejected pixels.</p>";

      this.flatLargeScaleRejectionLayersSizer = new HorizontalSizer;
      this.flatLargeScaleRejectionLayersSizer.spacing = 4;
      this.flatLargeScaleRejectionLayersSizer.add( this.flatLargeScaleRejectionLayersLabel );
      this.flatLargeScaleRejectionLayersSizer.add( this.flatLargeScaleRejectionLayersSpinBox );
      this.flatLargeScaleRejectionLayersSizer.addStretch();

      //

      this.flatLargeScaleRejectionGrowthLabel = new Label( this );
      this.flatLargeScaleRejectionGrowthLabel.text = "Large-scale growth:";
      this.flatLargeScaleRejectionGrowthLabel.minWidth = IIlabelWidth;
      this.flatLargeScaleRejectionGrowthLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

      this.flatLargeScaleRejectionGrowthSpinBox = new SpinBox( this );
      this.flatLargeScaleRejectionGrowthSpinBox.minValue = 1;
      this.flatLargeScaleRejectionGrowthSpinBox.maxValue = 20;
      this.flatLargeScaleRejectionGrowthSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
      this.flatLargeScaleRejectionGrowthSpinBox.onValueUpdated = function( value )
      {
         engine.flatsLargeScaleRejectionGrowth = value;
      };

      this.flatLargeScaleRejectionGrowthLabel.toolTip = this.flatLargeScaleRejectionGrowthSpinBox.toolTip =
         "<p>Large-scale pixel rejection, growth of large-scale pixel rejection structures. " +
         "Increase to extend rejection to more adjacent pixels.</p>";

      this.flatLargeScaleRejectionGrowthSizer = new HorizontalSizer;
      this.flatLargeScaleRejectionGrowthSizer.spacing = 4;
      this.flatLargeScaleRejectionGrowthSizer.add( this.flatLargeScaleRejectionGrowthLabel );
      this.flatLargeScaleRejectionGrowthSizer.add( this.flatLargeScaleRejectionGrowthSpinBox );
      this.flatLargeScaleRejectionGrowthSizer.addStretch();

      //

      this.add( this.flatLargeScaleRejectionSizer );
      this.add( this.flatLargeScaleRejectionLayersSizer );
      this.add( this.flatLargeScaleRejectionGrowthSizer );
   }

   if ( this.imageType == ImageType.LIGHT )
   {
      this.lightLargeScaleRejectionLayersCheckboxesLabel = new Label( this );
      this.lightLargeScaleRejectionLayersCheckboxesLabel.text = "Large-scale pixel rejection:";
      this.lightLargeScaleRejectionLayersCheckboxesLabel.minWidth = IIlabelWidth;
      this.lightLargeScaleRejectionLayersCheckboxesLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

      this.lightLargeScaleRejectionHighCheckBox = new CheckBox( this );
      this.lightLargeScaleRejectionHighCheckBox.text = "High";
      this.lightLargeScaleRejectionHighCheckBox.toolTip = "<p>Apply large-scale pixel rejection, high pixel sample values.";
      this.lightLargeScaleRejectionHighCheckBox.onCheck = function( checked )
      {
         engine.lightsLargeScaleRejectionHigh = checked;
         this.parent.parent.updateControls();
      };

      this.lightLargeScaleRejectionLowCheckBox = new CheckBox( this );
      this.lightLargeScaleRejectionLowCheckBox.text = "Low";
      this.lightLargeScaleRejectionLowCheckBox.toolTip = "<p>Apply large-scale pixel rejection, low pixel sample values.";
      this.lightLargeScaleRejectionLowCheckBox.onCheck = function( checked )
      {
         engine.lightsLargeScaleRejectionLow = checked;
         this.parent.parent.updateControls();
      };

      //

      this.lightLargeScaleRejectionLayersLabel = new Label( this );
      this.lightLargeScaleRejectionLayersLabel.text = "Large-scale layers:";
      this.lightLargeScaleRejectionLayersLabel.minWidth = IIlabelWidth;
      this.lightLargeScaleRejectionLayersLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

      this.lightLargeScaleRejectionLayersHighSpinBox = new SpinBox( this );
      this.lightLargeScaleRejectionLayersHighSpinBox.minValue = 1;
      this.lightLargeScaleRejectionLayersHighSpinBox.maxValue = 6;
      this.lightLargeScaleRejectionLayersHighSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
      this.lightLargeScaleRejectionLayersHighSpinBox.onValueUpdated = function( value )
      {
         engine.lightsLargeScaleRejectionLayersHigh = value;
      };

      this.lightLargeScaleRejectionLayersLowSpinBox = new SpinBox( this );
      this.lightLargeScaleRejectionLayersLowSpinBox.minValue = 1;
      this.lightLargeScaleRejectionLayersLowSpinBox.maxValue = 6;
      this.lightLargeScaleRejectionLayersLowSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
      this.lightLargeScaleRejectionLayersLowSpinBox.onValueUpdated = function( value )
      {
         engine.lightsLargeScaleRejectionLayersLow = value;
      };

      this.lightLargeScaleRejectionLayersLabel.toolTip = this.lightLargeScaleRejectionLayersHighSpinBox.toolTip =
         this.lightLargeScaleRejectionLayersLowSpinBox.toolTip =
         "<p>Large-scale pixel rejection, number of protected small-scale wavelet layers. " +
         "Increase it to restrict large-scale rejection to larger structures of contiguous rejected pixels.</p>";

      //

      this.lightLargeScaleRejectionGrowthLabel = new Label( this );
      this.lightLargeScaleRejectionGrowthLabel.text = "Large-scale growth:";
      this.lightLargeScaleRejectionGrowthLabel.minWidth = IIlabelWidth;
      this.lightLargeScaleRejectionGrowthLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

      this.lightLargeScaleRejectionGrowthHighSpinBox = new SpinBox( this );
      this.lightLargeScaleRejectionGrowthHighSpinBox.minValue = 1;
      this.lightLargeScaleRejectionGrowthHighSpinBox.maxValue = 20;
      this.lightLargeScaleRejectionGrowthHighSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
      this.lightLargeScaleRejectionGrowthHighSpinBox.onValueUpdated = function( value )
      {
         engine.lightsLargeScaleRejectionGrowthHigh = value;
      };

      this.lightLargeScaleRejectionGrowthLowSpinBox = new SpinBox( this );
      this.lightLargeScaleRejectionGrowthLowSpinBox.minValue = 1;
      this.lightLargeScaleRejectionGrowthLowSpinBox.maxValue = 20;
      this.lightLargeScaleRejectionGrowthLowSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
      this.lightLargeScaleRejectionGrowthLowSpinBox.onValueUpdated = function( value )
      {
         engine.lightsLargeScaleRejectionGrowthLow = value;
      };

      this.lightLargeScaleRejectionGrowthLabel.toolTip = this.lightLargeScaleRejectionGrowthLowSpinBox.toolTip =
         "<p>Large-scale pixel rejection, growth of large-scale pixel rejection structures. " +
         "Increase to extend rejection to more adjacent pixels.</p>";

      this.lightLargeScaleRejectionSizer1 = new VerticalSizer;
      this.lightLargeScaleRejectionSizer1.spacing = 4;
      this.lightLargeScaleRejectionSizer1.add( this.lightLargeScaleRejectionLayersCheckboxesLabel );
      this.lightLargeScaleRejectionSizer1.add( this.lightLargeScaleRejectionLayersLabel );
      this.lightLargeScaleRejectionSizer1.add( this.lightLargeScaleRejectionGrowthLabel );

      this.lightLargeScaleRejectionSizer2 = new VerticalSizer;
      this.lightLargeScaleRejectionSizer2.spacing = 4;
      this.lightLargeScaleRejectionSizer2.add( this.lightLargeScaleRejectionHighCheckBox );
      this.lightLargeScaleRejectionSizer2.add( this.lightLargeScaleRejectionLayersHighSpinBox );
      this.lightLargeScaleRejectionSizer2.add( this.lightLargeScaleRejectionGrowthHighSpinBox );

      this.lightLargeScaleRejectionSizer3 = new VerticalSizer;
      this.lightLargeScaleRejectionSizer3.spacing = 4;
      this.lightLargeScaleRejectionSizer3.add( this.lightLargeScaleRejectionLowCheckBox );
      this.lightLargeScaleRejectionSizer3.add( this.lightLargeScaleRejectionLayersLowSpinBox );
      this.lightLargeScaleRejectionSizer3.add( this.lightLargeScaleRejectionGrowthLowSpinBox );

      this.lightLargeScaleRejectionSizer = new HorizontalSizer;
      this.lightLargeScaleRejectionSizer.add( this.lightLargeScaleRejectionSizer1 );
      this.lightLargeScaleRejectionSizer.addSpacing( 4 );
      this.lightLargeScaleRejectionSizer.add( this.lightLargeScaleRejectionSizer2 );
      this.lightLargeScaleRejectionSizer.addSpacing( 8 );
      this.lightLargeScaleRejectionSizer.add( this.lightLargeScaleRejectionSizer3 );
      this.lightLargeScaleRejectionSizer.addStretch();

      this.add( this.lightLargeScaleRejectionSizer );
   }

   this.updateControls = function()
   {
      this.combinationComboBox.currentItem = engine.combination[ this.imageType ];
      this.rejectionAlgorithmComboBox.currentItem = engine.rejectionIndex( engine.rejection[ this.imageType ] );
      this.percentileLowControl.setValue( engine.percentileLow[ this.imageType ] );
      this.percentileHighControl.setValue( engine.percentileHigh[ this.imageType ] );
      this.sigmaLowControl.setValue( engine.sigmaLow[ this.imageType ] );
      this.sigmaHighControl.setValue( engine.sigmaHigh[ this.imageType ] );
      this.linearFitLowControl.setValue( engine.linearFitLow[ this.imageType ] );
      this.linearFitHighControl.setValue( engine.linearFitHigh[ this.imageType ] );
      if ( ImageIntegration.prototype.Rejection_ESD )
      {
         this.ESD_OutliersControl.setValue( engine.ESD_Outliers[ this.imageType ] );
         this.ESD_SignificanceControl.setValue( engine.ESD_Significance[ this.imageType ] );
      }
      if ( ImageIntegration.prototype.Rejection_RCR )
      {
         this.RCR_Limit.setValue( engine.RCR_Limit[ this.imageType ] );
      }

      this.percentileLowControl.enabled = false;
      this.percentileHighControl.enabled = false;
      this.sigmaLowControl.enabled = false;
      this.sigmaHighControl.enabled = false;
      this.linearFitLowControl.enabled = false;
      this.linearFitHighControl.enabled = false;
      if ( ImageIntegration.prototype.Rejection_ESD )
      {
         this.ESD_OutliersControl.enabled = false;
         this.ESD_SignificanceControl.enabled = false;
      }
      if ( ImageIntegration.prototype.Rejection_RCR )
      {
         this.RCR_Limit.enabled = false;
      }

      switch ( engine.rejection[ this.imageType ] )
      {
         case ImageIntegration.prototype.PercentileClip:
            this.percentileLowControl.enabled = true;
            this.percentileHighControl.enabled = true;
            break;

         case ImageIntegration.prototype.WinsorizedSigmaClip:
            this.sigmaLowControl.enabled = true;
            this.sigmaHighControl.enabled = true;
            break;

         case ImageIntegration.prototype.LinearFit:
            this.linearFitLowControl.enabled = true;
            this.linearFitHighControl.enabled = true;
            break;

         case ImageIntegration.prototype.Rejection_ESD:
            this.ESD_OutliersControl.enabled = true;
            this.ESD_SignificanceControl.enabled = true;
            break;

         case ImageIntegration.prototype.Rejection_RCR:
            this.RCR_Limit.enabled = true;
            break;

         case ImageIntegration.prototype.auto:
            this.percentileLowControl.enabled = true;
            this.percentileHighControl.enabled = true;
            this.sigmaLowControl.enabled = true;
            this.sigmaHighControl.enabled = true;
            this.linearFitLowControl.enabled = true;
            this.linearFitHighControl.enabled = true;
            if ( ImageIntegration.prototype.Rejection_ESD )
            {
               this.ESD_OutliersControl.enabled = true;
               this.ESD_SignificanceControl.enabled = true;
            }
            if ( ImageIntegration.prototype.Rejection_RCR )
            {
               this.RCR_Limit.enabled = true;
            }
      }

      if ( this.imageType == ImageType.FLAT )
      {
         this.flatLargeScaleRejectionCheckBox.checked = engine.flatsLargeScaleRejection;
         this.flatLargeScaleRejectionLayersSpinBox.value = engine.flatsLargeScaleRejectionLayers;
         this.flatLargeScaleRejectionGrowthSpinBox.value = engine.flatsLargeScaleRejectionGrowth;

         let enabled = engine.rejection[ this.imageType ] != ImageIntegration.prototype.NoRejection;
         this.flatLargeScaleRejectionCheckBox.enabled = enabled;
         this.flatLargeScaleRejectionLayersSpinBox.enabled = enabled && engine.flatsLargeScaleRejection;
         this.flatLargeScaleRejectionGrowthSpinBox.enabled = enabled && engine.flatsLargeScaleRejection;
      }

      if ( this.imageType == ImageType.LIGHT )
      {
         this.lightLargeScaleRejectionHighCheckBox.checked = engine.lightsLargeScaleRejectionHigh;
         this.lightLargeScaleRejectionLayersHighSpinBox.value = engine.lightsLargeScaleRejectionLayersHigh;
         this.lightLargeScaleRejectionGrowthHighSpinBox.value = engine.lightsLargeScaleRejectionGrowthHigh;
         this.lightLargeScaleRejectionLowCheckBox.checked = engine.lightsLargeScaleRejectionLow;
         this.lightLargeScaleRejectionLayersLowSpinBox.value = engine.lightsLargeScaleRejectionLayersLow;
         this.lightLargeScaleRejectionGrowthLowSpinBox.value = engine.lightsLargeScaleRejectionGrowthLow;

         let enabled = engine.rejection[ this.imageType ] != ImageIntegration.prototype.NoRejection;
         this.lightLargeScaleRejectionHighCheckBox.enabled = enabled;
         this.lightLargeScaleRejectionLowCheckBox.enabled = enabled;
         this.lightLargeScaleRejectionLayersHighSpinBox.enabled = enabled && engine.lightsLargeScaleRejectionHigh;
         this.lightLargeScaleRejectionGrowthHighSpinBox.enabled = enabled && engine.lightsLargeScaleRejectionHigh;
         this.lightLargeScaleRejectionLayersLowSpinBox.enabled = enabled && engine.lightsLargeScaleRejectionLow;
         this.lightLargeScaleRejectionGrowthLowSpinBox.enabled = enabled && engine.lightsLargeScaleRejectionLow;
      }
   };
}

ImageIntegrationControl.prototype = new Control;

// ----------------------------------------------------------------------------

function LinearPatternSubtractionControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Linear Pattern Subtraction", parent, false, true );

   //

   this.onActivateStatusChanged = function( checked )
   {
      engine.linearPatternSubtraction = checked;
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.linearPatternSubtractionControl.updateControls();
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
   };

   //

   let toolTipRejectionLimit =
      "<p>Threshold to perform a bright pixel rejection in each column or " +
      "row of the small-scale component image. This will ensure a precise " +
      "calculation of statistics in each column or row, without a bias " +
      "toward bright pixels. The value is expressed in sigma units with " +
      "respect to background noise.</p>";

   this.rejectionLimit_Label = new Label( this );
   this.rejectionLimit_Label.text = "Rejection limit:";
   this.rejectionLimit_Label.toolTip = toolTipRejectionLimit;
   this.rejectionLimit_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.rejectionLimit_Label.minWidth = this.dialog.labelWidth1;

   this.rejectionLimit_SpinBox = new SpinBox( this );
   this.rejectionLimit_SpinBox.setRange( 0, 15 );
   this.rejectionLimit_SpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
   this.rejectionLimit_SpinBox.toolTip = toolTipRejectionLimit
   this.rejectionLimit_SpinBox.onValueUpdated = function( value )
   {
      engine.linearPatternSubtractionRejectionLimit = value;
   };

   this.rejectionLimit_Sizer = new HorizontalSizer;
   this.rejectionLimit_Sizer.spacing = 4;
   this.rejectionLimit_Sizer.add( this.rejectionLimit_Label );
   this.rejectionLimit_Sizer.add( this.rejectionLimit_SpinBox );
   this.rejectionLimit_Sizer.addStretch();

   //

   let toolTipMode = "<p>The type of correction to be applied.</p>";

   this.mode_Label = new Label( this );
   this.mode_Label.text = "Correction type:";
   this.mode_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.mode_Label.minWidth = this.dialog.labelWidth1;

   this.mode_ComboBox = new ComboBox( this );
   this.mode_ComboBox.addItem( "Columns" );
   this.mode_ComboBox.addItem( "Rows" );
   this.mode_ComboBox.addItem( "Columns and rows" );
   this.mode_ComboBox.toolTip = toolTipMode;
   this.mode_ComboBox.onItemSelected = function( item )
   {
      engine.linearPatternSubtractionMode = item;
   };

   this.mode_sizer = new HorizontalSizer;
   this.mode_sizer.spacing = 4;
   this.mode_sizer.add( this.mode_Label );
   this.mode_sizer.add( this.mode_ComboBox, 100 );

   //

   this.add( this.rejectionLimit_Sizer );
   this.add( this.mode_sizer );

   this.updateControls = () =>
   {
      if ( engine.linearPatternSubtraction )
         this.activate();
      else
         this.deactivate();

      this.rejectionLimit_SpinBox.value = engine.linearPatternSubtractionRejectionLimit;
      this.mode_ComboBox.currentItem = engine.linearPatternSubtractionMode;
   };
}

LinearPatternSubtractionControl.prototype = new Control;

// ----------------------------------------------------------------------------

function SubframesWeightsEditControl( parent, expand )
{
   this.__base__ = ParametersControl;
   this.__base__( "Weighting Formula parameters", parent, expand );

   this.presetsLabel = new Label( this );
   this.presetsLabel.text = "Preset:";
   this.presetsLabel.minWidth = this.dialog.labelWidth1 / 1.4;
   this.presetsLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.presetsComboBox = new ComboBox( this );
   this.presetsComboBox.addItem( "" );
   this.presetsComboBox.addItem( "Nebula" );
   this.presetsComboBox.addItem( "Galaxy" );
   this.presetsComboBox.addItem( "Cluster" );
   this.presetsComboBox.addItem( "By number of stars" );
   this.presetsComboBox.addItem( "Photometric" );
   this.presetsComboBox.addItem( "Photometric SNR" );
   this.presetsComboBox.currentItem = engine.subframeWeightingPreset;
   this.presetsComboBox.onItemSelected = ( item ) =>
   {
      engine.subframeWeightingPreset = item;
      // update sliders
      switch ( item )
      {
         case 1: // Nebula
            engine.FWHMWeight = 5;
            engine.eccentricityWeight = 10;
            engine.starsWeight = 0;
            engine.PSFSignalWeight = 0;
            engine.PSFSNRWeight = 0;
            engine.SNRWeight = 20;
            engine.pedestal = 65;
            break;
         case 2: // Galaxy
            engine.FWHMWeight = 20;
            engine.eccentricityWeight = 15;
            engine.starsWeight = 0;
            engine.PSFSignalWeight = 0;
            engine.PSFSNRWeight = 0;
            engine.SNRWeight = 25;
            engine.pedestal = 40;
            break;
         case 3: // Cluster
            engine.FWHMWeight = 35;
            engine.eccentricityWeight = 35;
            engine.starsWeight = 0;
            engine.PSFSignalWeight = 0;
            engine.PSFSNRWeight = 0;
            engine.SNRWeight = 20;
            engine.pedestal = 10;
            break;
         case 4: // By number of stars
            engine.FWHMWeight = 0;
            engine.eccentricityWeight = 0;
            engine.starsWeight = 40;
            engine.PSFSignalWeight = 0;
            engine.PSFSNRWeight = 0;
            engine.SNRWeight = 0;
            engine.pedestal = 60;
            break;
         case 5: // Photometric
            engine.FWHMWeight = 0;
            engine.eccentricityWeight = 0;
            engine.starsWeight = 0;
            engine.PSFSignalWeight = 100;
            engine.PSFSNRWeight = 0;
            engine.SNRWeight = 0;
            engine.pedestal = 0;
            break;
         case 6: // Photometric SNR
            engine.FWHMWeight = 0;
            engine.eccentricityWeight = 0;
            engine.starsWeight = 0;
            engine.PSFSignalWeight = 0;
            engine.PSFSNRWeight = 100;
            engine.SNRWeight = 0;
            engine.pedestal = 0;
            break;
      }
      this.updateControls();
   };

   this.presetsSizer = new HorizontalSizer;
   this.presetsSizer.spacing = 4;
   this.presetsSizer.add( this.presetsLabel );
   this.presetsSizer.add( this.presetsComboBox, 100 );

   //

   this.FWHMControl = new NumericControl( this );
   this.FWHMControl.label.text = "FWHM:";
   this.FWHMControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.FWHMControl.setRange( 0, 100 );
   this.FWHMControl.slider.setRange( 0, 100 );
   this.FWHMControl.slider.scaledMinWidth = 1;
   this.FWHMControl.setPrecision( 0 );
   this.FWHMControl.toolTip = "<p>Weight contribution of the FWHM.</p>";

   this.FWHMControl.onValueUpdated = ( value ) =>
   {
      engine.FWHMWeight = value;
      engine.subframeWeightingPreset = 0;
      this.updateControls();
   };

   this.FWHMSizer = new HorizontalSizer;
   this.FWHMSizer.spacing = 4;
   this.FWHMSizer.add( this.FWHMControl );

   //

   this.eccentricityControl = new NumericControl( this );
   this.eccentricityControl.label.text = "Eccentricity:";
   this.eccentricityControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.eccentricityControl.setRange( 0, 100 );
   this.eccentricityControl.slider.setRange( 0, 100 );
   this.eccentricityControl.slider.scaledMinWidth = 1;
   this.eccentricityControl.setPrecision( 0 );
   this.eccentricityControl.toolTip = "<p>Weight contribution of the stars eccentricity.</p>";

   this.eccentricityControl.onValueUpdated = ( value ) =>
   {
      engine.eccentricityWeight = value;
      engine.subframeWeightingPreset = 0;
      this.updateControls();
   };

   this.eccentricitySizer = new HorizontalSizer;
   this.eccentricitySizer.spacing = 4;
   this.eccentricitySizer.add( this.eccentricityControl );

   //

   this.SNRControl = new NumericControl( this );
   this.SNRControl.label.text = "SNR:";
   this.SNRControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.SNRControl.setRange( 0, 100 );
   this.SNRControl.slider.setRange( 0, 100 );
   this.SNRControl.slider.scaledMinWidth = 1;
   this.SNRControl.setPrecision( 0 );
   this.SNRControl.toolTip = "<p>Weight contribution of the SNR.</p>";

   this.SNRControl.onValueUpdated = ( value ) =>
   {
      engine.SNRWeight = value;
      engine.subframeWeightingPreset = 0;
      this.updateControls();
   };

   this.SNRSizer = new HorizontalSizer;
   this.SNRSizer.spacing = 4;
   this.SNRSizer.add( this.SNRControl );

   //

   this.starsControl = new NumericControl( this );
   this.starsControl.label.text = "Number of stars:";
   this.starsControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.starsControl.setRange( 0, 100 );
   this.starsControl.slider.setRange( 0, 100 );
   this.starsControl.slider.scaledMinWidth = 1;
   this.starsControl.setPrecision( 0 );
   this.starsControl.toolTip = "<p>Weight contribution of the number of stars.</p>";

   this.starsControl.onValueUpdated = ( value ) =>
   {
      engine.starsWeight = value;
      engine.subframeWeightingPreset = 0;
      this.updateControls();
   };

   this.starsSizer = new HorizontalSizer;
   this.starsSizer.spacing = 4;
   this.starsSizer.add( this.starsControl );

   //

   this.PSFSignalControl = new NumericControl( this );
   this.PSFSignalControl.label.text = "PSF Signal Weight:";
   this.PSFSignalControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.PSFSignalControl.setRange( 0, 100 );
   this.PSFSignalControl.slider.setRange( 0, 100 );
   this.PSFSignalControl.slider.scaledMinWidth = 1;
   this.PSFSignalControl.setPrecision( 0 );
   this.PSFSignalControl.toolTip = "<p>Weight contribution of the PSF Signal Weight estimator.</p>";

   this.PSFSignalControl.onValueUpdated = ( value ) =>
   {
      engine.PSFSignalWeight = value;
      engine.subframeWeightingPreset = 0;
      this.updateControls();
   };

   this.PSFSignalSizer = new HorizontalSizer;
   this.PSFSignalSizer.spacing = 4;
   this.PSFSignalSizer.add( this.PSFSignalControl );

   //

   this.PSFSNRControl = new NumericControl( this );
   this.PSFSNRControl.label.text = "PSF SNR Weight:";
   this.PSFSNRControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.PSFSNRControl.setRange( 0, 100 );
   this.PSFSNRControl.slider.setRange( 0, 100 );
   this.PSFSNRControl.slider.scaledMinWidth = 1;
   this.PSFSNRControl.setPrecision( 0 );

   this.PSFSNRControl.toolTip = "<p>Weight contribution of the PSF SNR estimator.</p>";
   this.PSFSNRControl.onValueUpdated = ( value ) =>
   {
      engine.PSFSNRWeight = value;
      engine.subframeWeightingPreset = 0;
      this.updateControls();
   };

   this.PSFSNRSizer = new HorizontalSizer;
   this.PSFSNRSizer.spacing = 4;
   this.PSFSNRSizer.add( this.PSFSNRControl );

   //

   this.pedestalControl = new NumericControl( this );
   this.pedestalControl.label.text = "Pedestal:";
   this.pedestalControl.label.minWidth = this.dialog.labelWidth1 / 1.4;
   this.pedestalControl.setRange( 0, 100 );
   this.pedestalControl.slider.setRange( 0, 100 );
   this.pedestalControl.slider.scaledMinWidth = 1;
   this.pedestalControl.setPrecision( 0 );
   this.pedestalControl.toolTip = "<p>Pedestal added to the weight.</p>";
   this.pedestalControl.onValueUpdated = ( value ) =>
   {
      engine.pedestal = value;
      this.updateControls();
   };
   this.pedestalSizer = new HorizontalSizer;
   this.pedestalSizer.spacing = 4;
   this.pedestalSizer.add( this.pedestalControl );

   //

   this.add( this.presetsSizer );
   this.add( this.FWHMSizer );
   this.add( this.eccentricitySizer );
   this.add( this.SNRSizer );
   this.add( this.starsSizer );
   this.add( this.PSFSignalSizer );
   this.add( this.PSFSNRSizer );
   this.add( this.pedestalSizer );

   //

   this.updateControls = function()
   {
      this.presetsComboBox.currentItem = engine.subframeWeightingPreset;
      this.FWHMControl.setValue( engine.FWHMWeight );
      this.eccentricityControl.setValue( engine.eccentricityWeight );
      this.starsControl.setValue( engine.starsWeight );
      this.PSFSignalControl.setValue( engine.PSFSignalWeight );
      this.PSFSNRControl.setValue( engine.PSFSNRWeight );
      this.SNRControl.setValue( engine.SNRWeight );
      this.pedestalControl.setValue( engine.pedestal );
   };

   this.updateControls();
}

SubframesWeightsEditControl.prototype = new Control;

// ----------------------------------------------------------------------------

function ImageRegistrationControl( parent, expand )
{
   this.__base__ = ParametersControl;
   this.__base__( "Image Registration", parent, expand );

   this.pixelInterpolationLabel = new Label( this );
   this.pixelInterpolationLabel.text = "Pixel interpolation:";
   this.pixelInterpolationLabel.minWidth = this.dialog.labelWidth1;
   this.pixelInterpolationLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.pixelInterpolationComboBox = new ComboBox( this );
   this.pixelInterpolationComboBox.addItem( "Nearest Neighbor" );
   this.pixelInterpolationComboBox.addItem( "Bilinear" );
   this.pixelInterpolationComboBox.addItem( "Bicubic Spline" );
   this.pixelInterpolationComboBox.addItem( "Bicubic B-Spline" );
   this.pixelInterpolationComboBox.addItem( "Lanczos-3" );
   this.pixelInterpolationComboBox.addItem( "Lanczos-4" );
   this.pixelInterpolationComboBox.addItem( "Lanczos-5" );
   this.pixelInterpolationComboBox.addItem( "Mitchell-Netravali Filter" );
   this.pixelInterpolationComboBox.addItem( "Catmul-Rom Spline Filter" );
   this.pixelInterpolationComboBox.addItem( "Cubic B-Spline Filter" );
   this.pixelInterpolationComboBox.addItem( "Auto" );
   this.pixelInterpolationComboBox.currentItem = engine.pixelInterpolation;
   this.pixelInterpolationComboBox.onItemSelected = function( item )
   {
      engine.pixelInterpolation = item;
   };

   this.pixelInterpolationSizer = new HorizontalSizer;
   this.pixelInterpolationSizer.spacing = 4;
   this.pixelInterpolationSizer.add( this.pixelInterpolationLabel );
   this.pixelInterpolationSizer.add( this.pixelInterpolationComboBox, 100 );

   //

   this.clampingThresholdControl = new NumericControl( this );
   this.clampingThresholdControl.label.text = "Clamping threshold:";
   this.clampingThresholdControl.label.minWidth = this.dialog.labelWidth1;
   this.clampingThresholdControl.setRange( 0, 1 );
   this.clampingThresholdControl.slider.setRange( 0, 1000 );
   this.clampingThresholdControl.slider.scaledMinWidth = 200;
   this.clampingThresholdControl.setPrecision( 2 );
   this.clampingThresholdControl.setValue( engine.clampingThreshold );
   this.clampingThresholdControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.clampingThresholdControl.toolTip = "<p>Clamping threshold for the bicubic spline and Lanczos interpolation algorithms.</p>";
   this.clampingThresholdControl.onValueUpdated = function( value )
   {
      engine.clampingThreshold = value;
   };

   //

   this.maxStarsLabel = new Label( this );
   this.maxStarsLabel.text = "Maximum stars:";
   this.maxStarsLabel.minWidth = this.dialog.labelWidth1;
   this.maxStarsLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.maxStarsSpinBox = new SpinBox( this );
   this.maxStarsSpinBox.minValue = 0; // <Auto>
   this.maxStarsSpinBox.maxValue = 262144;
   this.maxStarsSpinBox.minimumValueText = "<Auto>";
   this.maxStarsSpinBox.toolTip = "<p>Maximum number of stars allowed for image registration.</p>" +
      "<p>With the default &lt;Auto&gt; option, the WBPP script does not impose any restrictions on the maximum number of stars " +
      "used by the StarAlignment process for image registration. This means that the 2000 brightest stars will be used for each image " +
      "by default. If you want to speed up the image registration process you can specify a smaller amount for this parameter, " +
      "say 500 stars for example, which can be sufficient to achieve an accurate alignment for similar images without differential " +
      "distortions.</p>" +
      "<p>If your images suffer from field distortions and the data set includes significantly displaced or rotated frames, such " +
      "as frames affected by meridian flips for example, then you should enable the distortion correction option. In such case this " +
      "parameter will be ignored and the StarAlignment tool will select the required stars automatically.</p>";
   this.maxStarsSpinBox.onValueUpdated = function( value )
   {
      engine.maxStars = value;
   };

   this.maxStarsSizer = new HorizontalSizer;
   this.maxStarsSizer.spacing = 4;
   this.maxStarsSizer.add( this.maxStarsLabel );
   this.maxStarsSizer.add( this.maxStarsSpinBox );
   this.maxStarsSizer.addStretch();

   //

   this.distortionCorrectionCheckBox = new CheckBox( this );
   this.distortionCorrectionCheckBox.text = "Distortion correction";
   this.distortionCorrectionCheckBox.toolTip = "<p>Check this option to enable StarAlignment's arbitrary distortion correction " +
      "algorithms.</p>" +
      "<p>This feature is required to correct for nonlinear distorions such as barrel, pincushion and lateral chromatic aberration, " +
      "as well as correction of differential field curvature and optical distortions, which arise frequently with mosaics and " +
      "frames significantly displaced or rotated (e.g., frames subject to meridian flips).</p>" +
      "<p>Distortion correction requires the use of thin plate splines and a large number of PSF star fits, so it can be " +
      "significantly slower than normal registration with projective transformations. For difficult cases where you need more " +
      "control, disable image registration on this script and align your images manually after calibration.</p>";
   this.distortionCorrectionCheckBox.__parentControl__ = this;
   this.distortionCorrectionCheckBox.onCheck = function( checked )
   {
      engine.distortionCorrection = checked;
      this.__parentControl__.updateControls();
   };

   this.distortionCorrectionSizer = new HorizontalSizer;
   this.distortionCorrectionSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.distortionCorrectionSizer.add( this.distortionCorrectionCheckBox );
   this.distortionCorrectionSizer.addStretch();

   //

   this.structureLayersLabel = new Label( this );
   this.structureLayersLabel.text = "Detection scales:";
   this.structureLayersLabel.minWidth = this.dialog.labelWidth1;
   this.structureLayersLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.structureLayersSpinBox = new SpinBox( this );
   this.structureLayersSpinBox.minValue = 1;
   this.structureLayersSpinBox.maxValue = 8;
   this.structureLayersSpinBox.toolTip =
      "<p>Number of wavelet layers used for structure detection.</p>" +
      "<p>With more wavelet layers, larger stars (and perhaps also some nonstellar objects) will be detected.</p>";
   this.structureLayersSpinBox.onValueUpdated = function( value )
   {
      engine.structureLayers = value;
   };

   this.structureLayersSizer = new HorizontalSizer;
   this.structureLayersSizer.spacing = 4;
   this.structureLayersSizer.add( this.structureLayersLabel );
   this.structureLayersSizer.add( this.structureLayersSpinBox );
   this.structureLayersSizer.addStretch();

   //

   this.minStructureSizeLabel = new Label( this );
   this.minStructureSizeLabel.text = "Minimum structure size:";
   this.minStructureSizeLabel.minWidth = this.dialog.labelWidth1;
   this.minStructureSizeLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.minStructureSizeSpinBox = new SpinBox( this );
   this.minStructureSizeSpinBox.minValue = 0;
   this.minStructureSizeSpinBox.maxValue = 65535;
   this.minStructureSizeSpinBox.toolTip =
      "<p>Minimum size of a detectable star structure in square pixels.</p>" +
      "<p>This parameter can be used to prevent detection of small and bright image artifacts as stars. " +
      "This can be useful to work with uncalibrated or wrongly calibrated data, especially demosaiced CFA frames " +
      "where hot pixels have generated large bright artifacts that cannot be removed with a median filter.</p>" +
      "<p>Changing the default value of 0 this parameter should not be necessary with correctly acquired and " +
      "calibrated data. It may help, however, when working with poor quality data such as poorly tracked, poorly focused, " +
      "wrongly calibrated, low-SNR raw frames, for which our image registration algorithms and tools have not been " +
      "optimized.</p>";
   this.minStructureSizeSpinBox.onValueUpdated = function( value )
   {
      engine.minStructureSize = value;
   };

   this.minStructureSizeSizer = new HorizontalSizer;
   this.minStructureSizeSizer.spacing = 4;
   this.minStructureSizeSizer.add( this.minStructureSizeLabel );
   this.minStructureSizeSizer.add( this.minStructureSizeSpinBox );
   this.minStructureSizeSizer.addStretch();

   //

   this.noiseReductionFilterRadiusLabel = new Label( this );
   this.noiseReductionFilterRadiusLabel.text = "Noise reduction:";
   this.noiseReductionFilterRadiusLabel.minWidth = this.dialog.labelWidth1;
   this.noiseReductionFilterRadiusLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.noiseReductionFilterRadiusSpinBox = new SpinBox( this );
   this.noiseReductionFilterRadiusSpinBox.minValue = 0; // <Auto>
   this.noiseReductionFilterRadiusSpinBox.maxValue = 50;
   this.noiseReductionFilterRadiusSpinBox.minimumValueText = "<Disabled>";
   this.noiseReductionFilterRadiusSpinBox.toolTip =
      "<p>Size of the noise reduction filter.</p>" +
      "<p>This is the radius in pixels of a Gaussian convolution filter applied to the working image used for " +
      "calculation of star positions during the star detection phase. Use it only for very low SNR images, where " +
      "the star detector cannot find reliable stars with default parameters. Be aware that noise reduction will " +
      "modify star profiles and hence the way star positions are calculated. Under extreme low-SNR conditions, " +
      "however, this is probably better than working with the actual data anyway.</p>" +
      "<p>To disable noise reduction, set this parameter to zero.</p>";
   this.noiseReductionFilterRadiusSpinBox.onValueUpdated = function( value )
   {
      engine.noiseReductionFilterRadius = value;
   };

   this.noiseReductionFilterRadiusSizer = new HorizontalSizer;
   this.noiseReductionFilterRadiusSizer.spacing = 4;
   this.noiseReductionFilterRadiusSizer.add( this.noiseReductionFilterRadiusLabel );
   this.noiseReductionFilterRadiusSizer.add( this.noiseReductionFilterRadiusSpinBox );
   this.noiseReductionFilterRadiusSizer.addStretch();

   //

   this.sensitivityControl = new NumericControl( this );
   this.sensitivityControl.label.text = "Log(sensitivity):";
   this.sensitivityControl.label.minWidth = this.dialog.labelWidth1;
   this.sensitivityControl.setRange( -3, +3 ); // log10( 0.001 ), log10( 1000 )
   this.sensitivityControl.slider.setRange( -300, +300 );
   this.sensitivityControl.slider.scaledMinWidth = 200;
   this.sensitivityControl.setPrecision( 2 );
   this.sensitivityControl.setValue( Math.log10( engine.sensitivity ) );
   this.sensitivityControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.sensitivityControl.toolTip = "<p>Logarithm of the star detection sensitivity.</p>" +
      "<p>The sensitivity of the star detection algorithm is measured with respect to the <i>local background</i> " +
      "of each detected star. Given a star with estimated brightness <i>s</i> and local background <i>b</i>, " +
      "sensitivity is the minimum value of (<i>s</i> &ndash; <i>b</i>)/<i>b</i> necessary to trigger star detection.</p>" +
      "<p>Decrease this value to favor detection of fainter stars. Increase it to restrict detection to brighter stars, " +
      "which may accelerate the star matching process, but at the risk of increasing the probability of failure. " +
      "In general, you shouldn't need to change the default value of this parameter.</p>";
   this.sensitivityControl.onValueUpdated = function( value )
   {
      engine.sensitivity = Math.roundTo( Math.pow10( value ), 6 );
   };

   //

   this.peakResponseControl = new NumericControl( this );
   this.peakResponseControl.label.text = "Peak response:";
   this.peakResponseControl.label.minWidth = this.dialog.labelWidth1;
   this.peakResponseControl.setRange( 0, 1 );
   this.peakResponseControl.slider.setRange( 0, 100 );
   this.peakResponseControl.slider.scaledMinWidth = 200;
   this.peakResponseControl.setPrecision( 2 );
   this.peakResponseControl.setValue( engine.peakResponse );
   this.peakResponseControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.peakResponseControl.toolTip = "<p>Star peak response.</p>" +
      "<p>If you decrease this value, stars will need to have stronger (more prominent) peaks to be detected by " +
      "the star detection algorithm. This is useful to prevent detection of saturated stars, as well as small " +
      "nonstellar features. By increasing this parameter, the star detection algorithm will be more sensitive to " +
      "<i>peakedness</i>, and hence more tolerant with relatively <i>flat</i> image features.</p>";
   this.peakResponseControl.onValueUpdated = function( value )
   {
      engine.peakResponse = value;
   };

   //

   this.maxStarDistortionControl = new NumericControl( this );
   this.maxStarDistortionControl.label.text = "Maximum distortion:";
   this.maxStarDistortionControl.label.minWidth = this.dialog.labelWidth1;
   this.maxStarDistortionControl.setRange( 0, 1 );
   this.maxStarDistortionControl.slider.setRange( 0, 100 );
   this.maxStarDistortionControl.slider.scaledMinWidth = 200;
   this.maxStarDistortionControl.setPrecision( 2 );
   this.maxStarDistortionControl.setValue( engine.maxStarDistortion );
   this.maxStarDistortionControl.edit.setFixedWidth( this.dialog.numericEditWidth );
   this.maxStarDistortionControl.toolTip = "<p>Maximum star distortion.</p>" +
      "<p>Star distortion is measured with respect to a perfect square, whose distortion is 1. Lower values mean " +
      "more distortion. The distortion of a perfectly circular star is about 0.8 (actually, &pi;/4). Use this " +
      "parameter, if necessary, to control inclusion of elongated stars, multiple stars, and nonstellar image " +
      "features.</p>";
   this.maxStarDistortionControl.onValueUpdated = function( value )
   {
      engine.maxStarDistortion = value;
   };

   //

   this.useTriangleSimilarityCheckBox = new CheckBox( this );
   this.useTriangleSimilarityCheckBox.text = "Use triangle similarity";
   this.useTriangleSimilarityCheckBox.toolTip = "<p>If this option is checked, the image registration process will use " +
      "triangle similarity instead of polygonal descriptors for the star matching routine.</p>" +
      "<p>Polygonal descriptors are more robust and accurate, but cannot register images subject to specular transformations " +
      "(horizontal and vertical mirror). Triangle similarity works well under normal conditions and is able to register " +
      "mirrored images, so it is the default option in the WBPP script. If you need more control on image " +
      "registration parameters, check the <i>calibrate only</i> option and align your images manually after calibration.</p>";
   this.useTriangleSimilarityCheckBox.onCheck = function( checked )
   {
      engine.useTriangleSimilarity = checked;
   };

   this.useTriangleSimilaritySizer = new HorizontalSizer;
   this.useTriangleSimilaritySizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.useTriangleSimilaritySizer.add( this.useTriangleSimilarityCheckBox );
   this.useTriangleSimilaritySizer.addStretch();

   //

   this.add( this.pixelInterpolationSizer );
   this.add( this.clampingThresholdControl );
   this.add( this.maxStarsSizer );
   this.add( this.distortionCorrectionSizer );
   this.add( this.structureLayersSizer );
   this.add( this.minStructureSizeSizer );
   this.add( this.noiseReductionFilterRadiusSizer );
   this.add( this.sensitivityControl );
   this.add( this.peakResponseControl );
   this.add( this.maxStarDistortionControl );
   this.add( this.useTriangleSimilaritySizer );

   this.updateControls = function()
   {
      this.pixelInterpolationComboBox.currentItem = engine.pixelInterpolation;
      this.clampingThresholdControl.setValue( engine.clampingThreshold );
      this.maxStarsSpinBox.value = engine.maxStars;
      this.maxStarsSpinBox.enabled = !engine.distortionCorrection;
      this.distortionCorrectionCheckBox.checked = engine.distortionCorrection;
      this.structureLayersSpinBox.value = engine.structureLayers;
      this.minStructureSizeSpinBox.value = engine.minStructureSize;
      this.noiseReductionFilterRadiusSpinBox.value = engine.noiseReductionFilterRadius;
      this.sensitivityControl.setValue( Math.log10( engine.sensitivity ) );
      this.peakResponseControl.setValue( engine.peakResponse );
      this.maxStarDistortionControl.setValue( engine.maxStarDistortion );
      this.useTriangleSimilarityCheckBox.checked = engine.useTriangleSimilarity;
   };
}

ImageRegistrationControl.prototype = new Control;

// ----------------------------------------------------------------------------
function LocalNormalizationControl( parent, expand )
{
   this.__base__ = ParametersControl;
   this.__base__( "Local Normalization", parent, expand );

   let labelWidth = 20 * this.font.width( "M" );

   //

   let generateImagesToolTip = "<p>If checked, normalized images will be generated along with normalization .xnml files.</p>" +
      "<p>Local normalization functions, stored in .xnml files, can be used by the ImageIntegration and DrizzleIntegration " +
      "processes for normalization in the pixel rejection and/or integration output tasks. This means that the normalization " +
      "functions can be applied internally by these processes, so writing normalized images to disk files is generally not " +
      "necessary. In addition, the ImageIntegration and DrizzleIntegration processes apply normalization functions internally " +
      "without any truncation or rescaling of the data, so the entire data set is always integrated without any loss or " +
      "artificial alteration when local normalization files are used.</p>" +
      "<p>Generation of normalized image files can be useful in very difficult cases, for example when the data set includes " +
      "strong and large artifacts, such as big plane trails. In these cases you may want to inspect locally normalized images " +
      "manually with analysis tools such as Blink.</p>";

   this.generateCheckBox = new CheckBox( this );
   this.generateCheckBox.toolTip = generateImagesToolTip;
   this.generateCheckBox.onCheck = ( checked ) =>
   {
      engine.localNormalizationGenerateImages = checked;
   }

   this.generateLabel = new Label( this );
   this.generateLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.generateLabel.text = "Generate images:";
   this.generateLabel.toolTip = generateImagesToolTip;
   this.generateLabel.setFixedWidth( labelWidth );

   {
      let sizer = new HorizontalSizer;
      sizer.add( this.generateLabel );
      sizer.addSpacing( 4 );
      sizer.add( this.generateCheckBox );
      this.add( sizer );
   }

   //

   let referenceFrameGenerationToolTip = "<p>Available methods for automatic selection of the reference frame for local normalization.</p>" +
      "<p>The <b>single best frame</b> method uses as reference frame the one with the highest PSF signal weight estimate.</p>" +
      "<p>The <b>integration of best frames</b> method integrates a subset of best frames (from 3 to 20) using the ones with the highest " +
      "PSF signal weight values to generate a reference frame with a higher SNR.</p>";

   this.referenceFrameGenerationComboBox = new ComboBox( this );
   this.referenceFrameGenerationComboBox.addItem( "Single best frame" );
   this.referenceFrameGenerationComboBox.addItem( "Integration of best frames" )
   this.referenceFrameGenerationComboBox.toolTip = referenceFrameGenerationToolTip;
   this.referenceFrameGenerationComboBox.onItemSelected = ( item ) =>
   {
      engine.localNormalizationRerenceFrameGenerationMethod = item;
   }

   this.referenceFrameGenerationLabel = new Label( this );
   this.referenceFrameGenerationLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.referenceFrameGenerationLabel.text = "Reference frame generation:";
   this.referenceFrameGenerationLabel.toolTip = referenceFrameGenerationToolTip;
   this.referenceFrameGenerationLabel.setFixedWidth( labelWidth );

   {
      let sizer = new HorizontalSizer;
      sizer.add( this.referenceFrameGenerationLabel );
      sizer.addSpacing( 4 );
      sizer.add( this.referenceFrameGenerationComboBox );
      this.add( sizer );
   }

   //

   let psfTypeToolTip = "<p>Point spread function type used for PSF fitting and photometry.</p>" +
      "<p>In all cases elliptical functions are fitted to detected star structures, and PSF sampling regions are " +
      "defined adaptively using a median stabilization algorithm.</p>" +
      "<p>When the <b>Auto</b> option is selected, a series of different PSFs will be fitted for each source, and " +
      "the fit that leads to the least absolute difference among function values and sampled pixel values will be " +
      "used for scale estimation. Currently the following functions are tested in this special automatic mode: " +
      "Moffat functions with <i>beta</i> shape parameters equal to 2.5, 4, 6 and 10.</p>" +
      "<p>The rest of options select a fixed PSF type for all detected sources, which improves execution times at " +
      "the cost of a less adaptive, and hence potentially less accurate, relative scale measurement process.</p>";

   this.psfTypeComboBox = new ComboBox( this );
   this.psfTypeComboBox.addItem( "Gaussian​" );
   this.psfTypeComboBox.addItem( "Moffat beta = 1.5​" );
   this.psfTypeComboBox.addItem( "Moffat beta = 4​" );
   this.psfTypeComboBox.addItem( "Moffat beta = 6​" );
   this.psfTypeComboBox.addItem( "Moffat beta = 8​" );
   this.psfTypeComboBox.addItem( "Moffat beta = 10​" );
   this.psfTypeComboBox.addItem( "Auto​" );
   this.psfTypeComboBox.toolTip = psfTypeToolTip;
   this.psfTypeComboBox.onItemSelected = ( item ) =>
   {
      engine.localNormalizationPsfType = item;
   }

   this.psfTypeLabel = new Label( this );
   this.psfTypeLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.psfTypeLabel.text = "PSF type:";
   this.psfTypeLabel.toolTip = psfTypeToolTip;
   this.psfTypeLabel.setFixedWidth( labelWidth );

   {
      let sizer = new HorizontalSizer;
      sizer.add( this.psfTypeLabel );
      sizer.addSpacing( 4 );
      sizer.add( this.psfTypeComboBox );
      this.add( sizer );
   }

   //

   let psfMaxStarsToolTip = "<p>The maximum number of stars that can be measured to compute mean scale estimates.</p>" +
      "<p>PSF photometry will be performed for no more than the specified number of stars. The subset of measured stars will always start " +
      "at the beginning of the set of detected stars, sorted by the brightness in descending order</p>" +
      "<p>The default value imposes a generous limit of 24k stars. Limiting the number of photometrix samples can improve performance " +
      "for normalization of wide-field frames, where the number of detected stars can be very large. However, reducing the set of " +
      "measured sources too much will damage the accuracy of scale evaluation</p>";

   this.psfMaxStarsSpinBox = new SpinBox( this );
   this.psfMaxStarsSpinBox.minValue = DEFAULT_LOCALNORMALIZATION_PSF_MIN_STARS;
   this.psfMaxStarsSpinBox.maxValue = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
   this.psfMaxStarsSpinBox.toolTip = psfMaxStarsToolTip;

   this.psfMaxStarsLabel = new Label( this );
   this.psfMaxStarsLabel.text = "PSF maximum number of stars:";
   this.psfMaxStarsLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.psfMaxStarsLabel.toolTip = psfMaxStarsToolTip;
   this.psfMaxStarsLabel.setFixedWidth( labelWidth );

   {
      let sizer = new HorizontalSizer;
      sizer.add( this.psfMaxStarsLabel );
      sizer.addSpacing( 4 );
      sizer.add( this.psfMaxStarsSpinBox );
      sizer.addStretch();
      this.add( sizer );
   }

   //

   let methodToolTip = "<p>Available methods for evaluation of the global relative scale of the reference image with respect " +
      "to each normalization target image.</p>" +
      "<p>The <b>PSF flux evaluation</b> method detects stars in the images and fits a point spread function model to each detected " +
      "source. The fitted PSF parameters are then used to guide evaluation of the total flux of each source, and the fluxes of matched " +
      "pairs of stars in both images are used to compute a robust and precise scale factor. This method is usually the best choice for " +
      "normalization of deep-sky astronomical images where stars can be detected efficiently.</p>" +
      "<p>The <b>multiscale analysis</b> method uses wavelet transforms and morphological operations to isolate significant image " +
      "structures. The pixels gathered on the intersection between significant structures of the reference and target images are then " +
      "evaluated statistically to estimate the scale factor.</p>";

   this.methodComboBox = new ComboBox( this );
   this.methodComboBox.addItem( "PSF flux evaluation" );
   this.methodComboBox.addItem( "Multiscale analysis" );
   this.methodComboBox.toolTip = methodToolTip;
   this.methodComboBox.onItemSelected = ( item ) =>
   {
      engine.localNormalizationMethod = item;
   }

   this.methodLabel = new Label( this );
   this.methodLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.methodLabel.text = "Scale evaluation method:";
   this.methodLabel.toolTip = methodToolTip;
   this.methodLabel.setFixedWidth( labelWidth );

   {
      let sizer = new HorizontalSizer;
      sizer.add( this.methodLabel );
      sizer.addSpacing( 4 );
      sizer.add( this.methodComboBox );
      this.add( sizer );
   }

   //

   let scaleToolTip = "<p>LocalNormalization implements a multiscale normalization algorithm. This parameter is the size in pixels of " +
      "the sampling scale for local image normalization. The larger this parameter, the less locally adaptive will be the local " +
      "normalization function. Smaller values tend to reproduce variations among small-scale structures in the reference image. " +
      "Larger values tend to reproduce variations among large-scale structures.</p>" +
      "<p>To better understand the role of this parameter, suppose we applied the algorithm at the scale of one pixel. The result " +
      "would be an exact copy of the reference image. On the other hand, if we applied the algorithm at a scale similar to the size " +
      "of the whole image, the result would be a <i>global normalization</i>: a single linear function would be applied for " +
      "normalization of the entire target image.</p>" +
      "<p>The default scale is 512 pixels, which is quite appropriate for most deep-sky images. Suitable scales are generally in the " +
      "range from 128 to 512 pixels. Although the value of this parameter could in theory be set arbitrarily, for performance and " +
      "accuracy reasons the current implementation is limited to the scales of 32, 64, 128, 192, 256, 384, 512, 768 and 1024 pixels.</p>";

   this.scaleComboBox = new ComboBox( this );
   for ( let i = 0; i < WBPPLocalNormalizationScales.length; i++ )
      this.scaleComboBox.addItem( "" + WBPPLocalNormalizationScales[ i ] );
   this.scaleComboBox.toolTip = scaleToolTip;
   this.scaleComboBox.onItemSelected = function( item )
   {
      engine.localNormalizationScaleIndex = item;
   }

   this.scaleLabel = new Label( this );
   this.scaleLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.scaleLabel.text = "Normalization scale:";
   this.scaleLabel.toolTip = scaleToolTip;
   this.scaleLabel.setFixedWidth( labelWidth );

   {
      let sizer = new HorizontalSizer;
      sizer.add( this.scaleLabel );
      sizer.addSpacing( 4 );
      sizer.add( this.scaleComboBox );
      this.add( sizer );
   }

   //

   this.updateControls = function()
   {
      this.generateCheckBox.checked = engine.localNormalizationGenerateImages;
      this.methodComboBox.currentItem = engine.localNormalizationMethod;
      this.scaleComboBox.currentItem = engine.localNormalizationScaleIndex;
      this.referenceFrameGenerationComboBox.currentItem = engine.localNormalizationRerenceFrameGenerationMethod;
      this.psfTypeComboBox.currentItem = engine.localNormalizationPsfType;
      this.psfMaxStarsSpinBox.value = engine.localNormalizationPsfMaxStars;
   }
}

LocalNormalizationControl.prototype = new Control;

// ----------------------------------------------------------------------------

function LightsIntegrationControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Image Integration", parent, false, true );

   //

   this.onActivateStatusChanged = function( checked )
   {
      engine.integrate = checked;
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.lightsIntegrationControl.updateControls();
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
      this.dialog.updateControls();
   };

   //

   this.editButton = new PushButton( this );
   this.editButton.text = "Integration parameters...";
   this.editButton.icon = this.scaledResource( ":/icons/arrow-right.png" );
   this.editButton.toolTip = "<p>Edit image integration parameters.</p>";
   this.editButton.onClick = function()
   {
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.imageIntegrationControl.show();
   };

   this.editSizer = new HorizontalSizer;
   this.editSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.editSizer.add( this.editButton, 100 );

   //
   this.add( this.editSizer );

   this.updateControls = function()
   {
      if ( engine.integrate )
         this.activate();
      else
         this.deactivate();
      this.editButton.enabled = engine.integrate;
   };
}

LightsIntegrationControl.prototype = new Control;

// ----------------------------------------------------------------------------

function SubframesWeightingControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Subframe Weighting", parent, false, true );

   //

   this.onActivateStatusChanged = function( checked )
   {
      engine.subframeWeightingEnabled = checked;
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.subframesWeightingControl.updateControls();
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
   };

   let subframesWeightsTooltip = "<p>If this option is checked light frames will be analyzed and weighted " +
      "accordingly to the relative weights assigned to FWHM, eccentricity and SNR. " +
      "Weights are saved into the image as WBPPWGHT header key which will be used during the ImageIntegration process.</p>";

   this.subframesWeightsLabel = new Label( this );
   this.subframesWeightsLabel.text = "Weights: ";
   this.subframesWeightsLabel.minWidth = this.dialog.labelWidth1;
   this.subframesWeightsLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.subframesWeightsLabel.toolTip = subframesWeightsTooltip;

   this.subframesWeightsComboBox = new ComboBox( this );
   this.subframesWeightsComboBox.addItem( "PSF Signal Weight" );
   this.subframesWeightsComboBox.addItem( "PSF SNR" );
   this.subframesWeightsComboBox.addItem( "SNR Estimate" );
   this.subframesWeightsComboBox.addItem( "Weighting Formula" );
   this.subframesWeightsComboBox.toolTip = subframesWeightsTooltip;
   this.subframesWeightsComboBox.onItemSelected = function( itemIndex )
   {
      engine.subframesWeightsMethod = itemIndex;
      this.dialog.updateControls();
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
   };

   this.subframesWeightDataSizer = new HorizontalSizer;
   this.subframesWeightDataSizer.add( this.subframesWeightsLabel );
   this.subframesWeightDataSizer.addUnscaledSpacing( this.logicalPixelsToPhysical( 4 ) );
   this.subframesWeightDataSizer.add( this.subframesWeightsComboBox );
   this.subframesWeightDataSizer.addStretch();

   //

   this.editButton = new PushButton( this );
   this.editButton.text = "Weighting Formula parameters...";
   this.editButton.icon = this.scaledResource( ":/icons/arrow-right.png" );
   this.editButton.toolTip = "<p>Edit weighting Formula parameters.</p>";
   this.editButton.onClick = function()
   {
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.subframesWeightsEditControl.show();
   };

   this.editSizer = new HorizontalSizer;
   this.editSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.editSizer.add( this.editButton, 100 );

   this.add( this.subframesWeightDataSizer );
   this.add( this.editSizer );

   this.updateControls = () =>
   {
      if ( engine.subframeWeightingEnabled )
         this.activate();
      else
         this.deactivate();
      this.subframesWeightsComboBox.currentItem = engine.subframesWeightsMethod;
      this.editButton.enabled = engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA;
   };
}

SubframesWeightingControl.prototype = new Control;

// ----------------------------------------------------------------------------

function LightsRegistrationControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Image Registration", parent, false, true );

   //

   this.onActivateStatusChanged = function( checked )
   {
      engine.imageRegistration = checked;
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.lightsRegistrationControl.updateControls();
      this.dialog.referenceImageControl.updateControls();
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
      this.dialog.updateControls();
   };

   //

   this.generateDrizzleDataCheckBox = new CheckBox( this );
   this.generateDrizzleDataCheckBox.text = "Generate drizzle data";
   this.generateDrizzleDataCheckBox.toolTip = "<p>Generate .xdrz files in the image registration task. " +
      "These files can later be used with the ImageIntegration and DrizzleIntegration tools to " +
      "perform a drizzle integration process.</p>";
   this.generateDrizzleDataCheckBox.onCheck = function( checked )
   {
      engine.generateDrizzleData = checked;
   };

   this.generateDrizzleDataSizer = new HorizontalSizer;
   this.generateDrizzleDataSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.generateDrizzleDataSizer.add( this.generateDrizzleDataCheckBox );
   this.generateDrizzleDataSizer.addStretch();

   //

   this.editButton = new PushButton( this );
   this.editButton.text = "Registration parameters...";
   this.editButton.icon = this.scaledResource( ":/icons/arrow-right.png" );
   this.editButton.toolTip = "<p>Edit image registration parameters.</p>";
   this.editButton.onClick = function()
   {
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.imageRegistrationControl.show();
   };

   this.editSizer = new HorizontalSizer;
   this.editSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.editSizer.add( this.editButton, 100 );

   //

   this.add( this.generateDrizzleDataSizer );
   this.add( this.editSizer );

   this.updateControls = function()
   {
      if ( engine.imageRegistration )
         this.activate();
      else
         this.deactivate();
      this.generateDrizzleDataCheckBox.checked = engine.generateDrizzleData;
      this.generateDrizzleDataCheckBox.enabled = engine.imageRegistration;
      this.editButton.enabled = engine.imageRegistration;
   };
}

LightsRegistrationControl.prototype = new Control;


// ----------------------------------------------------------------------------

function LightsNormalizationControl( parent )
{
   this.__base__ = ParametersControl;
   this.__base__( "Local Normalization", parent, false, true );

   //

   this.onActivateStatusChanged = function( checked )
   {
      engine.localNormalization = checked;
      this.dialog.tabBox.pageControlByIndex( 5 ).redraw();
   };

   this.editButton = new PushButton( this );
   this.editButton.text = "Normalization parameters...";
   this.editButton.icon = this.scaledResource( ":/icons/arrow-right.png" );
   this.editButton.toolTip = "<p>Edit Local normalization parameters.</p>";
   this.editButton.onClick = function()
   {
      let lightsPage = this.dialog.tabBox.pageControlByIndex( ImageType.LIGHT );
      lightsPage.localNormalizationControl.show();
   };

   this.editSizer = new HorizontalSizer;
   this.editSizer.addUnscaledSpacing( this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.editSizer.add( this.editButton, 100 );

   this.add( this.editSizer )

   this.updateControls = function()
   {
      if ( engine.localNormalization )
         this.activate();
      else
         this.deactivate();
   }
}
LightsNormalizationControl.prototype = new Control;

// ----------------------------------------------------------------------------

function FileControl( parent, imageType )
{
   this.__base__ = Control;
   if ( parent )
      this.__base__( parent );
   else
      this.__base__();

   this.treeBox = new StyledTreeBox( this );
   this.treeBox.multipleSelection = true;
   this.treeBox.numberOfColumns = 1;
   this.treeBox.headerVisible = false;
   this.treeBox.setScaledMinWidth( 250 );

   //

   this.clearButton = new PushButton( this );
   this.clearButton.text = "Clear";
   this.clearButton.icon = this.scaledResource( ":/icons/clear.png" );
   this.clearButton.toolTip = "<p>Clear the current list of input files.</p>";
   this.clearButton.onClick = function()
   {
      this.dialog.clearTab( this.dialog.tabBox.currentPageIndex );
      engine.reconstructGroups();
      this.dialog.refreshTreeBoxes();
   };

   this.removeSelectedButton = new PushButton( this );
   this.removeSelectedButton.text = "Remove Selected";
   this.removeSelectedButton.icon = this.scaledResource( ":/icons/clear.png" );
   this.removeSelectedButton.toolTip = "<p>Remove selected items in the current list of input files.</p>";
   this.removeSelectedButton.onClick = function()
   {
      let tree = this.dialog.tabBox.pageControlByIndex( this.dialog.tabBox.currentPageIndex ).treeBox;
      let selected = tree.selectedNodes;
      let groups = engine.groupsManager.groups;
      for ( let step = 0; step < 2; ++step )
         for ( let i = 0; i < selected.length; ++i )
         {
            let node = selected[ i ];
            if ( step == 0 )
            {
               if ( node.nodeData_type == "FileItem" )
               {
                  // mark the file item as to be removed
                  groups[ node.nodeData_groupIndex ].fileItems[ node.nodeData_itemIndex ].__purged__ = true;
               }
            }
            else
            {
               if ( node.nodeData_type == "FrameGroup" )
               {
                  // mark children groups as to be removed
                  node.nodeData_indexes.forEach( i =>
                  {
                     groups[ i ].__purged__ = true;
                  } );
               }
            }
         }
      engine.reconstructGroups();
      this.dialog.refreshTreeBoxes();
   };

   this.invertSelectionButton = new PushButton( this );
   this.invertSelectionButton.text = "Invert Selection";
   this.invertSelectionButton.icon = this.scaledResource( ":/icons/select-invert.png" );
   this.invertSelectionButton.toolTip = "<p>Invert selected items in the current list of input files.</p>";
   this.invertSelectionButton.onClick = function()
   {
      function invertNodeSelection( node )
      {
         node.selected = !node.selected;
         for ( let i = 0; i < node.numberOfChildren; ++i )
            invertNodeSelection( node.child( i ) );
      }

      let tree = this.dialog.tabBox.pageControlByIndex( this.dialog.tabBox.currentPageIndex ).treeBox;
      for ( let i = 0; i < tree.numberOfChildren; ++i )
         invertNodeSelection( tree.child( i ) );
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.spacing = 6;
   this.buttonsSizer.add( this.clearButton );
   this.buttonsSizer.add( this.removeSelectedButton );
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.invertSelectionButton );

   //

   this.rightPanelSizer = new VerticalSizer;
   this.rightPanelSizer.add( this.buttonsSizer );
   this.rightPanelSizer.addSpacing( 8 );
   this.rightPanelSizer.addStretch();

   switch ( imageType )
   {
      case ImageType.BIAS:

         this.biasOverscanControl = new BiasOverscanControl( this );
         this.overscanControl = new OverscanControl( this, true /*expand*/ );

         this.rightPanelSizer.add( this.biasOverscanControl );
         this.rightPanelSizer.addSpacing( 8 );
         this.rightPanelSizer.add( this.overscanControl );

         this.imageIntegrationControl = new ImageIntegrationControl( this, ImageType.BIAS );
         this.rightPanelSizer.add( this.imageIntegrationControl );

         this.restyle();
         break;

      case ImageType.DARK:

         this.darkOptimizationThresholdControl = new NumericControl( this );
         this.darkOptimizationThresholdControl.label.text = "Optimization threshold:";
         this.darkOptimizationThresholdControl.label.minWidth = this.dialog.labelWidth1 +
            this.dialog.logicalPixelsToPhysical( 6 ); // + integration control margin
         this.darkOptimizationThresholdControl.setRange( 0, 10 );
         this.darkOptimizationThresholdControl.slider.setRange( 0, 200 );
         this.darkOptimizationThresholdControl.setPrecision( 4 );
         this.darkOptimizationThresholdControl.toolTip = "<p>Lower bound for the set of dark optimization pixels, " +
            "measured in sigma units from the median.</p>" +
            "<p>This parameter defines the set of dark frame pixels that will be used to compute dark optimization " +
            "factors adaptively. By restricting this set to relatively bright pixels, the optimization process can " +
            "be more robust to readout noise present in the master bias and dark frames. Increase this parameter to " +
            "remove more dark pixels from the optimization set.</p>";
         this.darkOptimizationThresholdControl.onValueUpdated = function( value )
         {
            engine.darkOptimizationLow = value;
         };

         this.rightPanelSizer.add( this.darkOptimizationThresholdControl );
         this.rightPanelSizer.addSpacing( 4 );

         this.darkExposureToleranceLabel = new Label( this );
         this.darkExposureToleranceLabel.text = "Exposure tolerance:";
         this.darkExposureToleranceLabel.minWidth = this.dialog.labelWidth1 +
            this.dialog.logicalPixelsToPhysical( 6 ); // + integration control margin
         this.dialog.logicalPixelsToPhysical( 6 ); // + integration control margin
         this.darkExposureToleranceLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

         this.darkExposureToleranceSpinBox = new SpinBox( this );
         this.darkExposureToleranceSpinBox.minValue = 0;
         this.darkExposureToleranceSpinBox.maxValue = 600;
         this.darkExposureToleranceSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
         this.darkExposureToleranceSpinBox.toolTip = "<p>Dark frames with exposure times differing less than this value " +
            "(in seconds) will be grouped together.</p>";
         this.darkExposureToleranceSpinBox.onValueUpdated = function( value )
         {
            engine.darkExposureTolerance = value;
            engine.reconstructGroups();
            parent.dialog.refreshTreeBoxes();
         };

         this.darkExposureToleranceSizer = new HorizontalSizer;
         this.darkExposureToleranceSizer.spacing = 4;
         this.darkExposureToleranceSizer.add( this.darkExposureToleranceLabel );
         this.darkExposureToleranceSizer.add( this.darkExposureToleranceSpinBox );
         this.darkExposureToleranceSizer.addStretch();

         this.rightPanelSizer.add( this.darkExposureToleranceSizer );
         this.rightPanelSizer.addSpacing( 8 );

         this.imageIntegrationControl = new ImageIntegrationControl( this, ImageType.DARK );
         this.rightPanelSizer.add( this.imageIntegrationControl );

         this.restyle();
         break;

      case ImageType.FLAT:

         this.imageIntegrationControl = new ImageIntegrationControl( this, ImageType.FLAT );
         this.rightPanelSizer.add( this.imageIntegrationControl );

         this.restyle();
         break;

      case ImageType.LIGHT:

         let lightExposureToleranceTooltip = "<p>Light frames with exposure times differing less than this value " +
            "(in seconds) will be grouped together during the calibration step.</p>";

         this.lightExposureToleranceLabel = new Label( this );
         this.lightExposureToleranceLabel.text = "Calibration exposure tolerance:";
         this.lightExposureToleranceLabel.minWidth = this.dialog.labelWidth1 + this.logicalPixelsToPhysical( 4 + 2 );
         this.lightExposureToleranceLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
         this.lightExposureToleranceLabel.toolTip = lightExposureToleranceTooltip;

         this.lightExposureToleranceSpinBox = new SpinBox( this );
         this.lightExposureToleranceSpinBox.minValue = 0;
         this.lightExposureToleranceSpinBox.maxValue = 3600;
         this.lightExposureToleranceSpinBox.setFixedWidth( this.dialog.numericEditWidth + this.logicalPixelsToPhysical( 16 ) );
         this.lightExposureToleranceSpinBox.toolTip = lightExposureToleranceTooltip;
         this.lightExposureToleranceSpinBox.onValueUpdated = function( value )
         {
            engine.lightExposureTolerance = value;
            engine.reconstructGroups();
            parent.dialog.refreshTreeBoxes();
         };

         this.groupLightsWithDifferentExposureSizer = new HorizontalSizer;
         this.groupLightsWithDifferentExposureSizer.spacing = 4;
         this.groupLightsWithDifferentExposureSizer.add( this.lightExposureToleranceLabel );
         this.groupLightsWithDifferentExposureSizer.add( this.lightExposureToleranceSpinBox );
         this.groupLightsWithDifferentExposureSizer.addStretch();

         this.linearPatternSubtractionControl = new LinearPatternSubtractionControl( this );
         this.lightsRegistrationControl = new LightsRegistrationControl( this );
         this.subframesWeightingControl = new SubframesWeightingControl( this );
         this.lightsNormalizationControl = new LightsNormalizationControl( this );
         this.subframesWeightsEditControl = new SubframesWeightsEditControl( this, true /*expand*/ );
         this.imageRegistrationControl = new ImageRegistrationControl( this, true /*expand*/ );
         this.localNormalizationControl = new LocalNormalizationControl( this, true )
         this.lightsIntegrationControl = new LightsIntegrationControl( this );
         this.imageIntegrationControl = new ImageIntegrationControl( this, ImageType.LIGHT, true /*expand*/ );

         this.rightPanelSizer.add( this.groupLightsWithDifferentExposureSizer );
         this.rightPanelSizer.addSpacing( 8 );
         this.rightPanelSizer.add( this.linearPatternSubtractionControl );
         this.rightPanelSizer.addSpacing( 8 );
         this.rightPanelSizer.add( this.subframesWeightsEditControl );
         this.rightPanelSizer.add( this.subframesWeightingControl );
         this.rightPanelSizer.addSpacing( 8 );
         this.rightPanelSizer.add( this.lightsRegistrationControl );
         this.rightPanelSizer.add( this.imageRegistrationControl );
         this.rightPanelSizer.addSpacing( 8 );
         this.rightPanelSizer.add( this.lightsNormalizationControl );
         this.rightPanelSizer.add( this.localNormalizationControl );
         this.rightPanelSizer.addSpacing( 8 );
         this.rightPanelSizer.add( this.lightsIntegrationControl );
         this.rightPanelSizer.add( this.imageIntegrationControl );

         this.restyle();
         this.lightsRegistrationControl.minWidth =
            this.dialog.tabBox.pageControlByIndex( ImageType.BIAS ).imageIntegrationControl.width;
         break;
   }

   //

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.treeBox );
   this.sizer.addSpacing( 12 );
   this.sizer.add( this.rightPanelSizer );
}

FileControl.prototype = new Control;

// ----------------------------------------------------------------------------

function ResetDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   //

   this.resetParametersRadioButton = new RadioButton( this );
   this.resetParametersRadioButton.text = "Reset all parameters to factory-default values";

   this.reloadSettingsRadioButton = new RadioButton( this );
   this.reloadSettingsRadioButton.text = "Reload settings stored since the last session";
   this.reloadSettingsRadioButton.checked = true;

   this.clearFileListsCheckBox = new CheckBox( this );
   this.clearFileListsCheckBox.text = "Clear all file lists";
   this.clearFileListsCheckBox.checked = false;

   //

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = "OK";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.cancelButton = new PushButton( this );
   this.cancelButton.text = "Cancel";
   this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancelButton.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.okButton );
   this.buttonsSizer.addSpacing( 8 );
   this.buttonsSizer.add( this.cancelButton );

   //

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add( this.resetParametersRadioButton );
   this.sizer.add( this.reloadSettingsRadioButton );
   this.sizer.add( this.clearFileListsCheckBox );
   this.sizer.add( this.buttonsSizer );

   this.adjustToContents();
   this.setFixedSize();

   this.windowTitle = "Reset Preprocessing Engine";
}

ResetDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

function SelectCustomFilesDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   let
   {
      extractNameAndExtension
   } = WBPPUtils.shared();

   this.imageType = ImageType.UNKNOWN;
   this.filter = "?"; // ### see StackEngine.addFile()
   this.binning = 0;
   this.exposureTime = 0;
   this.files = new Array;

   let labelWidth1 = this.font.width( "Exposure time (s):" + "M" );

   //

   this.fileListLabel = new Label( this );
   this.fileListLabel.text = "Selected Files";

   this.fileList = new StyledTreeBox( this );
   this.fileList.numberOfColumns = 1;
   this.fileList.headerVisible = false;
   this.fileList.setScaledMinSize( 400, 250 );

   this.addButton = new PushButton( this );
   this.addButton.text = "Files";
   this.addButton.icon = this.scaledResource( ":/icons/add.png" );
   this.addButton.toolTip = "<p>Add files to the input files list.</p>";
   this.addButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Images";
      ofd.loadImageFilters();
      if ( ofd.execute() )
      {
         for ( let i = 0; i < ofd.fileNames.length; ++i )
            this.dialog.files.push( ofd.fileNames[ i ] );
         this.dialog.updateFileList();
      }
   };

   this.clearButton = new PushButton( this );
   this.clearButton.text = "Clear";
   this.clearButton.icon = this.scaledResource( ":/icons/clear.png" );
   this.clearButton.toolTip = "<p>Clear the current list of input files.</p>";
   this.clearButton.onClick = function()
   {
      this.dialog.files = new Array;
      this.dialog.updateFileList();
   };

   this.fileButtonsSizer = new HorizontalSizer;
   this.fileButtonsSizer.addUnscaledSpacing( labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.fileButtonsSizer.add( this.addButton );
   this.fileButtonsSizer.addSpacing( 8 );
   this.fileButtonsSizer.add( this.clearButton );
   this.fileButtonsSizer.addStretch();

   //

   let imageTypeToolTip = "<p>Frame type. Select '?' to determine frame types automatically.</p>";

   this.imageTypeLabel = new Label( this );
   this.imageTypeLabel.text = "Image type:";
   this.imageTypeLabel.minWidth = labelWidth1;
   this.imageTypeLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.imageTypeLabel.toolTip = imageTypeToolTip;

   this.imageTypeComboBox = new ComboBox( this );
   this.imageTypeComboBox.addItem( "?" );
   this.imageTypeComboBox.addItem( "Bias frame" );
   this.imageTypeComboBox.addItem( "Dark frame" );
   this.imageTypeComboBox.addItem( "Flat field" );
   this.imageTypeComboBox.addItem( "Light frame" );
   this.imageTypeComboBox.currentItem = this.imageType + 1; // ImageType property -> combobox item
   this.imageTypeComboBox.toolTip = imageTypeToolTip;
   this.imageTypeComboBox.onItemSelected = function( item )
   {
      this.dialog.imageType = item - 1; // combobox item -> ImageType property
   };

   this.imageTypeSizer = new HorizontalSizer;
   this.imageTypeSizer.spacing = 4;
   this.imageTypeSizer.add( this.imageTypeLabel );
   this.imageTypeSizer.add( this.imageTypeComboBox, 100 );

   //

   let filterToolTip = "<p>Filter name. Specify a single question mark '?' to determine filters automatically.</p>";

   this.filterLabel = new Label( this );
   this.filterLabel.text = "Filter name:";
   this.filterLabel.minWidth = labelWidth1;
   this.filterLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.filterLabel.toolTip = filterToolTip;

   this.filterEdit = new Edit( this );
   this.filterEdit.text = this.filter;
   this.filterEdit.toolTip = filterToolTip;
   this.filterEdit.onEditCompleted = function()
   {
      this.text = this.dialog.filter = this.text.trim();
   };

   this.filterSizer = new HorizontalSizer;
   this.filterSizer.spacing = 4;
   this.filterSizer.add( this.filterLabel );
   this.filterSizer.add( this.filterEdit, 100 );

   //

   let binningToolTip = "<p>Pixel binning. Specify zero to determine binnings automatically.</p>";

   this.binningLabel = new Label( this );
   this.binningLabel.text = "Binning:";
   this.binningLabel.minWidth = labelWidth1;
   this.binningLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.binningLabel.toolTip = binningToolTip;

   this.binningSpinBox = new SpinBox( this );
   this.binningSpinBox.minValue = 0;
   this.binningSpinBox.maxValue = 4;
   this.binningSpinBox.value = this.binning;
   this.binningSpinBox.toolTip = binningToolTip;
   this.binningSpinBox.onValueUpdated = function( value )
   {
      this.dialog.binning = value;
   };

   this.binningSizer = new HorizontalSizer;
   this.binningSizer.spacing = 4;
   this.binningSizer.add( this.binningLabel );
   this.binningSizer.add( this.binningSpinBox );
   this.binningSizer.addStretch();

   //

   this.exposureTimeEdit = new NumericEdit( this );
   this.exposureTimeEdit.label.text = "Exposure time (s):";
   this.exposureTimeEdit.label.minWidth = labelWidth1;
   this.exposureTimeEdit.setRange( 0, 999999 );
   this.exposureTimeEdit.setPrecision( 2 );
   this.exposureTimeEdit.setValue( this.exposureTime );
   this.exposureTimeEdit.toolTip = "<p>Exposure time in seconds. Specify zero to determine exposure times automatically.</p>";
   this.exposureTimeEdit.sizer.addStretch();
   this.exposureTimeEdit.onValueUpdated = function( value )
   {
      this.dialog.exposureTime = value;
   };

   //

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = "OK";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.cancelButton = new PushButton( this );
   this.cancelButton.text = "Cancel";
   this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancelButton.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addUnscaledSpacing( labelWidth1 + this.logicalPixelsToPhysical( 4 ) );
   this.buttonsSizer.add( this.okButton );
   this.buttonsSizer.addSpacing( 8 );
   this.buttonsSizer.add( this.cancelButton );

   //

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add( this.fileListLabel );
   this.sizer.add( this.fileList, 100 );
   this.sizer.add( this.fileButtonsSizer );
   this.sizer.add( this.imageTypeSizer );
   this.sizer.add( this.filterSizer );
   this.sizer.add( this.binningSizer );
   this.sizer.add( this.exposureTimeEdit );
   this.sizer.add( this.buttonsSizer );

   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Custom Frames";

   this.updateFileList = function()
   {
      this.fileList.clear();
      for ( let i = 0; i < this.files.length; ++i )
      {
         let node = new TreeBoxNode;
         node.setText( 0, extractNameAndExtension( this.files[ i ] ) );
         node.setToolTip( 0, this.files[ i ] );
         this.fileList.add( node );
      }
   };
}

SelectCustomFilesDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

function StackDialog()
{
   this.__base__ = Dialog;
   this.__base__();

   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   this.labelWidth1 = this.font.width( "Minimum structure size:" + "M" );
   this.textEditWidth = 25 * this.font.width( "M" );
   this.numericEditWidth = 6 * this.font.width( "0" );
   this.suffixEditWidth = 10 * this.font.width( "M" );
   this.rightPanelWidth = this.logicalPixelsToPhysical( 210 );

   // Tree node icons.
   this.iconSize = this.resourcePixelRatio * this.font.height;
   this.masterFrameIcon = Bitmap.fromSVGFile( ":/bullets/bullet-star6-blue.svg", this.iconSize, this.iconSize );
   this.calibrationFrameIcon = Bitmap.fromSVGFile( ":/bullets/bullet-circle.svg", this.iconSize, this.iconSize );
   this.lightFrameIcon = Bitmap.fromSVGFile( ":/bullets/bullet-circle-blue.svg", this.iconSize, this.iconSize );

   // Force a reasonable minimum window height.
   this.setScaledMinHeight( 500 );

   // -------------------------------------------------------------------------
   // TAB BOX CONSTRUCTION
   // -------------------------------------------------------------------------

   // Force an update of all dialog styling properties (fonts, colors, margins,
   // etc.). This is necessary in this case to ensure proper styling of complex
   // child controls.
   this.restyle();

   //

   this.calibrationPanelPreProcess = new CalibrationPanel( this, WBPPGroupingMode.PRE );
   this.calibrationPanelPostProcess = new CalibrationPanel( this, WBPPGroupingMode.POST );

   //

   this.tabBox = new TabBox( this );

   this.tabBox.addPage( new FileControl( this, ImageType.BIAS ), "Bias" );
   this.tabBox.addPage( new FileControl( this, ImageType.DARK ), "Darks" );
   this.tabBox.addPage( new FileControl( this, ImageType.FLAT ), "Flats" );
   this.tabBox.addPage( new FileControl( this, ImageType.LIGHT ), "Lights" );
   this.tabBox.addPage( this.calibrationPanelPreProcess, "Calibration" );
   this.tabBox.addPage( this.calibrationPanelPostProcess, "Post-Calibration" );
   this.tabBox.currentPageIndex = 4;

   // Handle click on file name -> set registration reference image
   this.tabBox.pageControlByIndex( ImageType.LIGHT ).treeBox.onNodeDoubleClicked = function( node, column )
   {
      // We create a nodeData_filePath property for each TreeBox node to store
      // the full path of the corresponding frame group element.
      // -- See refreshTreeBoxes()
      if ( engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
         if ( node.nodeData_filePath )
            if ( !isEmptyString( node.nodeData_filePath ) )
            {
               engine.referenceImage = node.nodeData_filePath;
               this.dialog.referenceImageEdit.text = node.nodeData_filePath;
            }
   };

   // -------------------------------------------------------------------------
   // BOTTOM BUTTONS CONSTRUCTION
   // -------------------------------------------------------------------------

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.toolTip = "New Instance";
   this.newInstanceButton.onMousePress = function()
   {
      this.hasFocus = true;
      engine.exportParameters();
      this.pushed = false;
      this.dialog.newInstance();
   };

   //

   this.addFolderButton = new PushButton( this );
   this.addFolderButton.text = "Directory";
   this.addFolderButton.icon = this.scaledResource( ":/icons/add.png" );
   this.addFolderButton.toolTip = "<p>Add image files to the input files list by scanning all files and folders recursively under the selected root folder.</p>";
   this.addFolderButton.onClick = () =>
   {
      let gdd = new GetDirectoryDialog;
      gdd.initialPath = this.__addDirectoryPath__ ? this.__addDirectoryPath__ + "../" : engine.outputDirectory;
      gdd.caption = "Select Root Directory";
      if ( gdd.execute() )
      {
         let rootDir = gdd.directory;

         // get the list of compatible file extensions
         let openFileSupport = new OpenFileDialog;
         openFileSupport.loadImageFilters();
         let filters = openFileSupport.filters[ 0 ]; // all known format
         filters.shift();
         filters = filters.concat( filters.map( f => ( f.toUpperCase() ) ) );

         // perform the search
         engine.clearDiagnosticMessages();
         let filesFound = 0;
         let addedFiles = 0;
         let L = new FileList( rootDir, filters, false /*verbose*/ );
         L.files.forEach( filePath =>
         {
            engine.diagnosticMessages.push( "<b>found: </b>" + File.extractNameAndExtension( filePath ) );
            filesFound++;
            let result = engine.addFile( filePath );
            if ( result.success )
               addedFiles++;
         } );

         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();

         engine.diagnosticMessages.push( format( "=== %d frames found, %d added ===", filesFound, addedFiles ) );
         engine.showDiagnosticMessages();
         engine.clearDiagnosticMessages();
      }
   };

   this.fileAddButton = new PushButton( this );
   this.fileAddButton.text = "Files";
   this.fileAddButton.icon = this.scaledResource( ":/icons/add.png" );
   this.fileAddButton.toolTip = "<p>Add files to the input files list.</p>" +
      "<p>Image types will be selected automatically based on XISF image properties and/or FITS keywords.</p>";
   this.fileAddButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Images";
      ofd.loadImageFilters();
      if ( ofd.execute() )
      {
         let n = 0;
         for ( let i = 0; i < ofd.fileNames.length; ++i )
            if ( engine.addFile( ofd.fileNames[ i ] ) )
               ++n;
         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();

         if ( n < ofd.fileNames.length )
         {
            engine.diagnosticMessages.unshift( format( "=== %d of %d frames were added ===", n, ofd.fileNames.length ) );
            engine.showDiagnosticMessages();
            engine.clearDiagnosticMessages();
         }
      }
   };

   this.biasAddButton = new PushButton( this );
   this.biasAddButton.text = "Bias";
   this.biasAddButton.icon = this.scaledResource( ":/icons/add.png" );
   this.biasAddButton.toolTip = "<p>Add files to the input bias frames list.</p>" +
      "<p>Files will be added as bias frames unconditionally - no FITS header keyword checks will be performed.</p>";
   this.biasAddButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Bias Frames";
      ofd.loadImageFilters();
      if ( ofd.execute() )
      {
         let n = 0;
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            let result = engine.addBiasFrame( ofd.fileNames[ i ] );
            if ( result.success )
               ++n;
            else
               engine.diagnosticMessages.push( result.message );
         }
         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();
         this.dialog.tabBox.currentPageIndex = ImageType.BIAS;

         if ( n < ofd.fileNames.length )
         {
            engine.diagnosticMessages.unshift( format( "=== %d of %d bias frames were added ===", n, ofd.fileNames.length ) );
            engine.showDiagnosticMessages();
            engine.clearDiagnosticMessages();
         }
      }
   };

   this.darkAddButton = new PushButton( this );
   this.darkAddButton.text = "Darks";
   this.darkAddButton.icon = this.scaledResource( ":/icons/add.png" );
   this.darkAddButton.toolTip = "<p>Add files to the input dark frames list.</p>" +
      "<p>Files will be added as dark frames unconditionally - no FITS header keyword checks will be performed.</p>";
   this.darkAddButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Dark Frames";
      ofd.loadImageFilters();
      if ( ofd.execute() )
      {
         let n = 0;
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            let result = engine.addDarkFrame( ofd.fileNames[ i ] );
            if ( result.success )
               ++n;
            else
               engine.diagnosticMessages.push( result.message );
         }
         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();
         this.dialog.tabBox.currentPageIndex = ImageType.DARK;

         if ( n < ofd.fileNames.length )
         {
            engine.diagnosticMessages.unshift( format( "=== %d of %d dark frames were added ===", n, ofd.fileNames.length ) );
            engine.showDiagnosticMessages();
            engine.clearDiagnosticMessages();
         }
      }
   };

   this.flatAddButton = new PushButton( this );
   this.flatAddButton.text = "Flats";
   this.flatAddButton.icon = this.scaledResource( ":/icons/add.png" );
   this.flatAddButton.toolTip = "<p>Add files to the input flat frames list.</p>" +
      "<p>Files will be added as flat frames unconditionally - no no FITS header checks will be performed.</p>";
   this.flatAddButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Flat Frames";
      ofd.loadImageFilters();
      if ( ofd.execute() )
      {
         let n = 0;
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            let result = engine.addFlatFrame( ofd.fileNames[ i ] );
            if ( result.success )
               ++n;
            else
               engine.diagnosticMessages.push( result.message );
         }
         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();
         this.dialog.tabBox.currentPageIndex = ImageType.FLAT;

         if ( n < ofd.fileNames.length )
         {
            engine.diagnosticMessages.unshift( format( "=== %d of %d flat frames were added ===", n, ofd.fileNames.length ) );
            engine.showDiagnosticMessages();
            engine.clearDiagnosticMessages();
         }
      }
   };

   this.lightAddButton = new PushButton( this );
   this.lightAddButton.text = "Lights";
   this.lightAddButton.icon = this.scaledResource( ":/icons/add.png" );
   this.lightAddButton.toolTip = "<p>Add files to the input light frames list.</p>" +
      "<p>Files will be added as light frames unconditionally - no FITS header checks will be performed.</p>";
   this.lightAddButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Light Frames";
      ofd.loadImageFilters();
      if ( ofd.execute() )
      {
         let n = 0;
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            let result = engine.addLightFrame( ofd.fileNames[ i ] );
            if ( result.success )
               ++n;
            else
               engine.diagnosticMessages.push( result.message );
         }
         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();
         this.dialog.tabBox.currentPageIndex = ImageType.LIGHT;

         if ( n < ofd.fileNames.length )
         {
            engine.diagnosticMessages.unshift( format( "=== %d of %d light frames were added ===", n, ofd.fileNames.length ) );
            engine.showDiagnosticMessages();
            engine.clearDiagnosticMessages();
         }
      }
   };

   this.customAddButton = new PushButton( this );
   this.customAddButton.text = "Add Custom";
   this.customAddButton.icon = this.scaledResource( ":/icons/document-edit.png" );
   this.customAddButton.toolTip = "<p>Add custom files to the input custom frames list.</p>";
   this.customAddButton.onClick = function()
   {
      let d = new SelectCustomFilesDialog;
      if ( d.execute() )
      {
         let n = 0;
         for ( let i = 0; i < d.files.length; ++i )
         {
            let result = engine.addFile( d.files[ i ], d.imageType, d.filter, d.binning, d.exposureTime );
            if ( result.success )
               ++n;
            else
               engine.diagnosticMessages.push( result.message );
         }
         engine.reconstructGroups();
         this.dialog.refreshTreeBoxes();
         this.dialog.tabBox.currentPageIndex = d.imageType;

         if ( n < d.files.length )
         {
            engine.diagnosticMessages.unshift( format( "=== %d of %d custom frames were added ===", n, d.files.length ) );
            engine.showDiagnosticMessages();
            engine.clearDiagnosticMessages();
         }
      }
   };

   //

   this.resetButton = new PushButton( this );
   this.resetButton.text = "Reset";
   this.resetButton.icon = this.scaledResource( ":/icons/reload.png" );
   this.resetButton.toolTip = "<p>Perform optional reset and clearing actions.</p>";
   this.resetButton.onClick = function()
   {
      let d = new ResetDialog;
      if ( d.execute() )
      {
         if ( d.resetParametersRadioButton.checked )
            engine.setDefaultParameters();
         if ( d.reloadSettingsRadioButton.checked )
            engine.loadSettings();
         if ( d.clearFileListsCheckBox.checked )
         {
            engine.groupsManager.clear();
            engine.groupsManager.clearCache();
         }
         this.dialog.updateControls();
         this.dialog.keywordsEditor.reset();
      }
   };

   //

   this.diagnosticsButton = new PushButton( this );
   this.diagnosticsButton.defaultButton = true;
   this.diagnosticsButton.text = "Diagnostics";
   this.diagnosticsButton.icon = this.scaledResource( ":/icons/gear.png" );
   this.diagnosticsButton.toolTip = "<p>Check validity of selected files and processes.</p>";
   this.diagnosticsButton.onClick = () =>
   {
      engine.runDiagnostics();
      let action = engine.showDiagnosticMessages( false, true );
      engine.clearDiagnosticMessages();

      if ( action == StdDialogCode_GenerateScreenshots )
         this.generateScreenshots();
   };

   //

   this.runButton = new PushButton( this );
   this.runButton.text = "Run";
   this.runButton.icon = this.scaledResource( ":/icons/power.png" );
   this.runButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.exitButton = new PushButton( this );
   this.exitButton.text = "Exit";
   this.exitButton.icon = this.scaledResource( ":/icons/close.png" );
   this.exitButton.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.spacing = 6;
   this.buttonsSizer.add( this.newInstanceButton );
   this.buttonsSizer.add( this.addFolderButton );
   this.buttonsSizer.add( this.fileAddButton );
   this.buttonsSizer.add( this.biasAddButton );
   this.buttonsSizer.add( this.darkAddButton );
   this.buttonsSizer.add( this.flatAddButton );
   this.buttonsSizer.add( this.lightAddButton );
   this.buttonsSizer.add( this.customAddButton );
   this.buttonsSizer.addSpacing( 24 );
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.resetButton );
   this.buttonsSizer.addStretch();
   this.buttonsSizer.addSpacing( 24 );
   this.buttonsSizer.add( this.diagnosticsButton );
   this.buttonsSizer.add( this.runButton );
   this.buttonsSizer.add( this.exitButton );

   // -------------------------------------------------------------------------
   // RIGHT SIDE CONTENT - GLOBAL OPTIONS CONSTRUCTION
   // -------------------------------------------------------------------------

   //
   this.descriptionLabel = new Label( this );
   this.descriptionLabel.setMaxWidth( this.rightPanelWidth );
   this.descriptionLabel.margin = 4;
   this.descriptionLabel.wordWrapping = true;
   this.descriptionLabel.useRichText = true;
   this.descriptionLabel.font = new Font( FontFamily_SansSerif, 8 );
   this.descriptionLabel.text =
      "<p><b>A script for calibration and alignment of light frames</b>";

   //

   this.helpLabel = new Label( this );
   this.helpLabel.setMaxWidth( this.rightPanelWidth );
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.font = new Font( FontFamily_SansSerif, 6 );
   this.helpLabel.text =
      "Copyright &copy; 2019-2022 Roberto Sartori<br/>" +
      "Copyright &copy; 2020-2021 Adam Block<br/>" +
      "Copyright &copy; 2019 Tommaso Rubechi<br/>" +
      "Copyright &copy; 2012 Kai Wiechen<br/>" +
      "Copyright &copy; 2012-2022 Pleiades Astrophoto<br/>";

   //

   this.helpButton = new ToolButton( this );
   this.helpButton.icon = this.scaledResource( ":/icons/comment.png" );
   this.helpButton.setScaledFixedSize( 20, 20 );
   this.helpButton.toolTip =
      "<p><b>1.</b> Select the Bias tab and load either raw bias frames or a master bias.</p>" +
      "<p><b>2.</b> Select the Darks tab and repeat the process in step 1. Load flats that match filters of your Lights.</p>" +
      "<p><b>3.</b> Select the Flats tab and again repeat the process in step 1. Load flats for all the filters you will load in Lights.</p>" +
      "<p><b>4.</b> Select the Lights tab and load all the light frames you wish (you can load all filters in a single operation).</p>" +
      "<p><b>5.</b> Select a Light frame as the <i>registration reference</i>. You can select it by simply double-clicking on the list or you can flag the option for the automatic selection.</p>" +
      "<p><b>6.</b> Select an output directory.</p>" +
      "<p><b>7.</b> Select the Control Panel to set the correct settings/relationships between frames and look at the calibration diagram.</p>" +
      "<p><b>8.</b> Click Run and go get a cup of coffee or a glass of wine.</p>" +
      "<p><b>9.</b> The script will output the calibrated and registered light frames along with the generated master frames on the output directory, in subfolders named <i>\"calibrated\"</i>, <i>\"registered\"</i> and <i>\"master\"</i>.</p>";

   this.presetsControl = new PresetsControl( "Presets", this );
   this.presetsControl.setFixedWidth( this.rightPanelWidth );

   this.keywordsEditor = new KeywordsEditor( "Grouping Keywords", this );
   this.keywordsEditor.setFixedWidth( this.rightPanelWidth );
   engine.groupingKeywordsEnabled ? this.keywordsEditor.activate() : this.keywordsEditor.deactivate();
   this.keywordsEditor.onKeywordsUpdated = () =>
   {
      engine.reconstructGroups();
      this.updateControls();
   };

   //

   this.generateRejectionMapsCheckBox = new CheckBox( this );
   this.generateRejectionMapsCheckBox.setMaxWidth( this.rightPanelWidth );
   this.generateRejectionMapsCheckBox.text = "Generate rejection maps"
   this.generateRejectionMapsCheckBox.toolTip = "<p>Generate rejection maps for all instances that image integration is invoked.  " +
      "</p>" +
      "<p>Outputted images with rejection maps are stored as multiplexed XISF files.</p>";
   this.generateRejectionMapsCheckBox.onCheck = function( checked )
   {
      engine.generateRejectionMaps = checked;
   };

   //

   this.FITSCoordinateConventionComboBox = new ComboBox( this );
   this.FITSCoordinateConventionComboBox.addItem( "Global Pref" );
   this.FITSCoordinateConventionComboBox.addItem( "top-down" );
   this.FITSCoordinateConventionComboBox.addItem( "bottom-up" );
   this.FITSCoordinateConventionComboBox.toolTip = "<p>Defines the coordinate origin and orientation convention assumed by the WBPP " +
      "script for input FITS files.</p>" +
      "<p><b>Global Pref.</b>: Adopt the convention specified by global FITS format preferences.</p>" +
      "<p><b>top-down</b>: All input FITS files follow the <i>top-down</i> coordinate convention. The coordinate origin is at the " +
      "top left corner of the image, and vertical coordinates grow from top to bottom. This is the convention used by most amateur " +
      "camera control applications.</p>" +
      "<p><b>bottom-up</b>: All input FITS files follow the <i>bottom-up</i> coordinate convention. The coordinate origin is at the " +
      "bottom left corner of the image, and vertical coordinates grow from bottom to top.</p>" +
      "<p>Note that existing ROWORDER FITS keywords will always take precedence over this setting. Nonstandard ROWORDER keywords " +
      "have been supported by our FITS format module since PixInsight core version 1.8.8-6.</p>";
   this.FITSCoordinateConventionComboBox.onItemSelected = function( checked )
   {
      engine.fitsCoordinateConvention = checked;
   };

   this.FITSCoordinateConventionLabel = new Label( this );
   this.FITSCoordinateConventionLabel.text = "FITS orientation";
   this.FITSCoordinateConventionLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.fitsCoordianteConventionSizer = new HorizontalSizer;
   this.fitsCoordianteConventionSizer.spacing = 4;
   this.fitsCoordianteConventionSizer.add( this.FITSCoordinateConventionLabel );
   this.fitsCoordianteConventionSizer.add( this.FITSCoordinateConventionComboBox );

   this.includePathForMasterDetectionCheckBox = new CheckBox( this );
   this.includePathForMasterDetectionCheckBox.setMaxWidth( this.rightPanelWidth );
   this.includePathForMasterDetectionCheckBox.text = "Detect masters from file path";
   this.includePathForMasterDetectionCheckBox.toolTip = "<p>The automatic detection of a master file is performed by searching for the word <b>master</b> " +
      "in the FITS header IMAGETYP value; if not found then the full file path is scanned, including the drive name, folders, file name and extension, " +
      "looking for the word <b>master</b> (case insensitive).</p>" +
      "<p>Uncheck this option in case your computer drive name or a folder name contains the word <b>master</b> and unintentionally causes all files to " +
      "be recognized as masters; If unchecked the file path is ignored and only the file name is scanned.</p>";
   this.includePathForMasterDetectionCheckBox.onCheck = function( checked )
   {
      engine.detectMasterIncludingFullPath = checked;
      engine.reconstructGroups();
      this.dialog.refreshTreeBoxes();
   };

   //

   this.saveSessionCheckBox = new CheckBox( this );
   this.saveSessionCheckBox.setMaxWidth( this.rightPanelWidth );
   this.saveSessionCheckBox.text = "Save frame groups on exit";
   this.saveSessionCheckBox.toolTip = "<p>When this option is selected, frame groups will be saved before closing the dialog. " +
      "They will be reloaded on next launch.</p>" +
      "<p>Select this option if you need to close WBPP and later relaunch the script " +
      "without losing the loaded file groups.</p>";
   this.saveSessionCheckBox.onCheck = function( checked )
   {
      engine.saveFrameGroups = checked;
      this.dialog.updateControls();
   };

   //

   this.optionsSizer = new VerticalSizer;
   this.optionsSizer.spacing = 4;
   this.optionsSizer.add( this.fitsCoordianteConventionSizer );
   this.optionsSizer.add( this.includePathForMasterDetectionCheckBox );
   this.optionsSizer.add( this.generateRejectionMapsCheckBox );
   this.optionsSizer.add( this.saveSessionCheckBox );
   this.optionsSizer.addStretch();

   this.optionsControl = new ParametersControl( "Global Options", this, true, false, true );
   this.optionsControl.add( this.optionsSizer );
   this.optionsControl.setMaxWidth( this.rightPanelWidth );

   //

   this.referenceImageEdit = new Edit( this );
   this.referenceImageEdit.text = engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL ? engine.referenceImage : 'auto';
   this.referenceImageEdit.toolTip = "<p>Reference image for image registration.</p>" +
      "<p>Along with selecting an existing disk file, you can double-click one of the light frames in " +
      "the Lights list to select it as the registration reference image.</p>";
   this.referenceImageEdit.onEditCompleted = function()
   {
      engine.referenceImage = this.text = File.windowsPathToUnix( this.text.trim() );
   };

   this.referenceImageSelectButton = new ToolButton( this );
   this.referenceImageSelectButton.icon = this.scaledResource( ":/icons/select-file.png" );
   this.referenceImageSelectButton.setScaledFixedSize( 20, 20 );
   this.referenceImageSelectButton.toolTip = "<p>Select the image registration reference image file.</p>";
   this.referenceImageSelectButton.onClick = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = false;
      ofd.caption = "Select Registration Reference Image";
      ofd.loadImageFilters();
      let filters = ofd.filters;
      filters.push( [ "Comma Separated Value (CSV) files", ".csv", ".txt" ] );
      ofd.filters = filters;
      if ( ofd.execute() )
         this.dialog.referenceImageEdit.text = engine.referenceImage = ofd.fileName;
   };

   this.referenceImageSizer = new HorizontalSizer;
   this.referenceImageSizer.add( this.referenceImageEdit );
   this.referenceImageSizer.addSpacing( 2 );
   this.referenceImageSizer.add( this.referenceImageSelectButton );

   //
   this.subframesWeightUseBestReferenceLabel = new Label( this );
   this.subframesWeightUseBestReferenceLabel.text = "Mode: "
   this.subframesWeightUseBestReferenceLabel.setScaledFixedWidth( 32 );
   this.subframesWeightUseBestReferenceLabel.textAlignment = TextAlign_VertCenter;

   this.subframesWeightUseBestReferenceComboBox = new ComboBox( this );
   this.subframesWeightUseBestReferenceComboBox.toolTip = "<p>Selects the method to detect the reference frame to align the master light frames.<br>" +
      "<br><b>Manual</b>: manually assign the best reference frame by selecting an image file.<br>" +
      "<br><b>Auto</b>: one reference frame will be selected across all light frames and it will be used to align all master light frames.<br>" +
      "<br><b>Auto, by keyord</b>: one reference frame will be selected grouping all light frames that share the same keword value, a different reference frame will be selected for each keyword value.<br>" +
      "</p>";
   this.subframesWeightUseBestReferenceComboBox.onItemSelected = ( item ) =>
   {
      engine.setBestReferenceFrameMode( this.subframesWeightUseBestReferenceComboBox.currentItem )
      this.dialog.updateControls();
   };

   this.subframesWeightUseBestReferenceComboBox.initialize = function()
   {
      // retrieve the available options from the engine
      let options = engine.getBestReferenceFrameModes();
      // populate the combo box with the updated list
      this.clear();
      for ( let i = 0; i < options.length; i++ )
      {
         this.addItem( options[ i ] );
      }
      this.currentItem = engine.bestFrameRefernceMethod;
   }

   this.subframesWeightUseBestReferenceComboBox.updateList = () =>
   {
      let control = this.subframesWeightUseBestReferenceComboBox;
      // get the current selected text
      let currentText = control.itemText( control.currentItem );
      // retrieve the available options from the engine
      let options = engine.getBestReferenceFrameModes();
      // populate the combo box with the updated list
      control.clear();
      for ( let i = 0; i < options.length; i++ )
         control.addItem( options[ i ] );
      // set the index of the selected text, restore the fecault value if the selected
      // text does not exis anymore (keywords could be changed)
      let indx = options.indexOf( currentText );
      if ( indx > -1 )
         control.currentItem = indx;
      else
         control.currentItem = DEFAULT_BEST_REFERENCE_METHOD;
      engine.setBestReferenceFrameMode( control.currentItem )
      this.referenceImageControl.updateControls();
   };

   this.subframesBestReferenceDataSizer = new HorizontalSizer;
   this.subframesBestReferenceDataSizer.spacing = 8;
   this.subframesBestReferenceDataSizer.add( this.subframesWeightUseBestReferenceLabel );
   this.subframesBestReferenceDataSizer.add( this.subframesWeightUseBestReferenceComboBox );
   this.subframesBestReferenceDataSizer.addSpacing( 22 );

   //

   this.referenceImageControl = new ParametersControl( "Registration Reference Image", this, false /* expandable */ , false /* deactivable */ , true /* collapsable */ );
   this.referenceImageControl.setMaxWidth( this.rightPanelWidth );
   this.referenceImageControl.add( this.subframesBestReferenceDataSizer );
   this.referenceImageControl.add( this.referenceImageSizer );
   this.referenceImageControl.ebabled = engine.imageRegistration;
   this.referenceImageControl.updateControls = function()
   {
      this.enabled = engine.imageRegistration;
      this.parent.referenceImageEdit.enabled = engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL;
      this.parent.referenceImageEdit.text = this.parent.referenceImageEdit.enabled ? engine.referenceImage : 'auto';
   };

   //

   this.outputDirectoryEdit = new Edit( this );
   this.outputDirectoryEdit.text = engine.outputDirectory;
   this.outputDirectoryEdit.toolTip = "<p>Output root directory.</p>" +
      "<p>The WBPP script will generate all master, calibrated and registered images " +
      "populating a directory tree rooted at the specified output directory.</p>";
   this.outputDirectoryEdit.onEditCompleted = function()
   {
      let dir = File.windowsPathToUnix( this.text.trim() );
      if ( dir.endsWith( '/' ) )
         dir = dir.substring( 0, dir.length - 1 );
      engine.outputDirectory = this.text = dir;
   };

   this.outputDirSelectButton = new ToolButton( this );
   this.outputDirSelectButton.icon = this.scaledResource( ":/icons/select-file.png" );
   this.outputDirSelectButton.setScaledFixedSize( 20, 20 );
   this.outputDirSelectButton.toolTip = "<p>Select the output root directory.</p>";
   this.outputDirSelectButton.onClick = function()
   {
      let gdd = new GetDirectoryDialog;
      gdd.initialPath = engine.outputDirectory;
      gdd.caption = "Select Output Directory";
      if ( gdd.execute() )
      {
         let dir = gdd.directory;
         if ( dir.endsWith( '/' ) )
            dir = dir.substring( 0, dir.length - 1 );
         this.dialog.outputDirectoryEdit.text = engine.outputDirectory = dir;
      }
   };

   this.outputDirSizer = new HorizontalSizer;
   this.outputDirSizer.add( this.outputDirectoryEdit );
   this.outputDirSizer.addSpacing( 2 );
   this.outputDirSizer.add( this.outputDirSelectButton );

   this.outputDirControl = new ParametersControl( "Output Directory", this );
   this.outputDirControl.setMaxWidth( this.rightPanelWidth );
   this.outputDirControl.add( this.outputDirSizer );

   //

   this.settingsSizer = new VerticalSizer;
   this.settingsSizer.spacing = 8;
   this.settingsSizer.addSpacing( 20 );
   this.settingsSizer.add( this.descriptionLabel );
   this.settingsSizer.add( this.helpLabel );
   this.settingsSizer.add( this.helpButton );
   this.settingsSizer.addStretch();
   this.settingsSizer.add( this.presetsControl );
   this.settingsSizer.add( this.keywordsEditor );
   this.settingsSizer.add( this.optionsControl );
   this.settingsSizer.add( this.referenceImageControl );
   this.settingsSizer.add( this.outputDirControl );

   //

   this.mainSizer = new HorizontalSizer;
   this.mainSizer.spacing = 8;
   this.mainSizer.add( this.tabBox );
   this.mainSizer.add( this.settingsSizer );

   //

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;
   this.sizer.add( this.mainSizer );
   this.sizer.add( this.buttonsSizer );

   //

   this.windowTitle = TITLE + " v" + VERSION;
   this.adjustToContents();
   this.setMinSize( this.width / 1.2, this.height / 1.2 + this.logicalPixelsToPhysical( 8 ) );


   this.generateScreenshots = () =>
   {
      let
      {
         existingDirectory,
      } = WBPPUtils.shared();

      if ( engine.outputDirectory == 0 )
         return;

      let outputDir = existingDirectory( engine.outputDirectory + "/logs" );

      let curPage = this.tabBox.currentPageIndex;
      let w = this.width;
      let h = this.height;

      this.width = 1920 * 1.5;
      this.height = 1080 * 1.5;

      for ( let i = 0; i < this.tabBox.numberOfPages; i++ )
      {
         let page = this.tabBox.pageControlByIndex( i );
         this.tabBox.currentPageIndex = i;

         let bmp = this.render();
         let outputFile = outputDir + "/" + "0" + i + "_" + this.tabBox.pageLabel( i ) + ".jpg";
         bmp.save( outputFile, 80 );
      }

      this.width = w;
      this.height = h;
      this.tabBox.currentPageIndex = curPage;
   }

   // init controls
   this.subframesWeightUseBestReferenceComboBox.initialize();
}

StackDialog.prototype = new Dialog;

StackDialog.prototype.clearTab = function( index )
{
   if ( this.dialog.tabBox.pageControlByIndex( index ).treeBox )
      this.dialog.tabBox.pageControlByIndex( index ).treeBox.clear();
   engine.groupsManager.deleteFrameSet( index );
};

StackDialog.prototype.updateControls = function()
{
   this.refreshTreeBoxes();

   for ( let i = 0; i < 4; ++i )
   {
      let page = this.tabBox.pageControlByIndex( i );

      switch ( i )
      {
         case ImageType.BIAS:
            page.biasOverscanControl.updateControls();
            page.overscanControl.updateControls();
            break;

         case ImageType.DARK:
            page.darkOptimizationThresholdControl.setValue( engine.darkOptimizationLow );
            page.darkExposureToleranceSpinBox.value = engine.darkExposureTolerance;
            break;

         case ImageType.FLAT:
            break;

         case ImageType.LIGHT:
            page.lightExposureToleranceSpinBox.value = engine.lightExposureTolerance;
            page.linearPatternSubtractionControl.updateControls();
            page.lightsRegistrationControl.updateControls();
            page.lightsNormalizationControl.updateControls();
            page.imageRegistrationControl.updateControls();
            page.localNormalizationControl.updateControls();
            page.subframesWeightingControl.updateControls();
            page.subframesWeightsEditControl.updateControls();
            page.lightsIntegrationControl.updateControls();
            break;
      }

      page.imageIntegrationControl.updateControls();
   }

   this.tabBox.pageControlByIndex( 4 ).redraw();
   this.tabBox.pageControlByIndex( 5 ).redraw();

   this.generateRejectionMapsCheckBox.checked = engine.generateRejectionMaps;
   this.includePathForMasterDetectionCheckBox.checked = engine.detectMasterIncludingFullPath;
   this.FITSCoordinateConventionComboBox.currentItem = engine.fitsCoordinateConvention;
   this.saveSessionCheckBox.checked = engine.saveFrameGroups;
   this.outputDirectoryEdit.text = engine.outputDirectory;
   this.subframesWeightUseBestReferenceComboBox.updateList();
   this.referenceImageSelectButton.enabled = engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL
   this.referenceImageControl.updateControls();
   this.keywordsEditor.update();
};

StackDialog.prototype.refreshTreeBoxes = function()
{
   let
   {
      extractNameAndExtension,
      isEmptyString,
   } = WBPPUtils.shared();

   // reset all but the last tab
   for ( let j = 0; j <= ImageType.LIGHT; ++j )
      this.tabBox.pageControlByIndex( j ).treeBox.clear();

   // scan all groups BUT process only groups in pre-processing mode
   let groups = engine.groupsManager.groups;

   let tree = {};
   for ( let i = 0; i < groups.length; ++i )
   {
      let frameGroup = groups[ i ];

      // treeBox lists only pre-processing groups
      if ( frameGroup.mode != WBPPGroupingMode.PRE )
         continue;

      let imageType = frameGroup.imageType;
      let binning = frameGroup.binning.toString();
      let filter = frameGroup.filter;

      let node;
      let treeNode;

      if ( !tree.hasOwnProperty( imageType ) )
      {
         tree[ imageType ] = {};
         treeNode = tree[ imageType ];
      }
      else
      {
         treeNode = tree[ imageType ];
         node = treeNode.node;
      }

      if ( !treeNode.hasOwnProperty( binning ) )
      {
         node = new TreeBoxNode;
         this.tabBox.pageControlByIndex( imageType ).treeBox.add( node );
         node.expanded = true;
         node.setText( 0, "Binning " + binning );
         node.nodeData_type = "FrameGroup";
         node.nodeData_indexes = [ i ];
         treeNode[ binning ] = {};
         treeNode[ binning ].node = node;
         treeNode = treeNode[ binning ];
      }
      else
      {
         treeNode = treeNode[ binning ];
         node = treeNode.node;
         node.nodeData_indexes.push( i );
      }

      // BIASes and DARKs are not grouped by filter
      if ( imageType !== ImageType.BIAS )
      {
         if ( imageType !== ImageType.DARK )
         {
            let filterName = ( filter.length > 0 ) ? filter : 'NoFilter';
            if ( !treeNode.hasOwnProperty( filterName ) )
            {
               node = new TreeBoxNode( node );
               node.expanded = true;
               node.setText( 0, filterName );
               node.nodeData_type = "FrameGroup";
               node.nodeData_indexes = [ i ];
               treeNode[ filterName ] = {};
               treeNode[ filterName ].node = node;
               treeNode[ filterName ].inexes = [ i ];
               treeNode = treeNode[ filterName ];
            }
            else
            {
               treeNode = treeNode[ filterName ];
               node = treeNode.node;
               node.nodeData_indexes.push( i );
            }
         }

         let exposureTimesString = frameGroup.exposuresToString();
         if ( !treeNode.hasOwnProperty( exposureTimesString ) )
         {

            node = new TreeBoxNode( node );
            node.expanded = true;
            node.setText( 0, frameGroup.exposuresToExtendedString() );
            node.useRichText = true;
            node.nodeData_type = "FrameGroup";
            node.nodeData_indexes = [ i ];
            treeNode[ exposureTimesString ] = {};
            treeNode[ exposureTimesString ].node = node;
            treeNode[ exposureTimesString ].inexes = [ i ];
            treeNode = treeNode[ exposureTimesString ];
         }
         else
         {
            treeNode = treeNode[ exposureTimesString ];
            node = treeNode.node;
            node.nodeData_indexes.push( i );
         }
      }

      if ( engine.keywords.list.length > 0 )
      {
         // all groups are grouped by keywords
         let keywordsString = frameGroup.keywordsToString();
         if ( !treeNode.hasOwnProperty( keywordsString ) )
         {
            node = new TreeBoxNode( node );
            node.expanded = true;
            node.useRichText = true;
            node.setText( 0, ( keywordsString.length > 0 ) ? keywordsString : "[no matching keywords]" );
            node.nodeData_type = "FrameGroup";
            node.nodeData_indexes = [ i ];
            treeNode[ keywordsString ] = {};
            treeNode[ keywordsString ].node = node;
            treeNode[ keywordsString ].inexes = [ i ];
         }
         else
         {
            treeNode = treeNode[ keywordsString ];
            node = treeNode.node;
            node.nodeData_indexes.push( i );
         }
      }

      let rootNode = node;

      for ( let j = 0; j < frameGroup.fileItems.length; ++j )
      {
         let fileItem = frameGroup.fileItems[ j ];

         let node = new TreeBoxNode( rootNode );
         node.setText( 0, extractNameAndExtension( fileItem.filePath ) );

         let toolTip = "<p style=\"white-space:pre;\">" + fileItem.filePath;
         if ( fileItem.exposureTime > 0.004999 )
            toolTip += format( "<br/>Exposure: %.2f s", fileItem.exposureTime );
         toolTip += "</p>";
         node.setToolTip( 0, toolTip );

         if ( j === 0 && frameGroup.hasMaster )
         {
            node.setIcon( 0, this.masterFrameIcon );
            let f = node.font( 0 );
            f.bold = true;
            node.setFont( 0, f );
         }
         else switch ( frameGroup.imageType )
         {
            case ImageType.BIAS:
            case ImageType.DARK:
            case ImageType.FLAT:
               node.setIcon( 0, this.calibrationFrameIcon );
               break;
            case ImageType.LIGHT:
               node.setIcon( 0, this.lightFrameIcon );
               break;
         }

         node.nodeData_type = "FileItem";
         node.nodeData_itemIndex = j;
         node.nodeData_groupIndex = i;
         node.nodeData_filePath = fileItem.filePath;
      }
   }

   // once tree boxes has been refreshed the calibration panel should be updated
   this.calibrationPanelPreProcess.redraw();
   this.calibrationPanelPostProcess.redraw();
};

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-GUI.js - Released 2022-03-15T20:50:04Z
