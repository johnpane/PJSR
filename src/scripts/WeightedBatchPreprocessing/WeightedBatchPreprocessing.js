// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing.js - Released 2022-03-15T20:50:04Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.4.2
//
// Copyright (c) 2019-2022 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#feature-id    WeightedBatchPreprocessing : Batch Processing > WeightedBatchPreprocessing

#feature-info  A script for calibration and post-calibration tasks.<br/>\
               Original script written by Kai Wiechen (c) 2012,<br/>\
               Extended and maintained by Roberto Sartori (c) 2019-2022.

#feature-icon  @script_icons_dir/WeightedBatchPreprocessing.svg

if ( CoreApplication === undefined ||
     CoreApplication.versionRevision === undefined ||
     CoreApplication.versionMajor*1e11
   + CoreApplication.versionMinor*1e8
   + CoreApplication.versionRelease*1e5
   + CoreApplication.versionRevision*1e2 < 100800900000 )
{
   throw new Error( "This script requires PixInsight core version 1.8.9 or higher." );
}

#include <pjsr/Sizer.jsh>
#include <pjsr/DataType.jsh>
#include <pjsr/NumericControl.jsh>

#include "WeightedBatchPreprocessing-global.js" // global defines
#include "WeightedBatchPreprocessing-helper.js" // helper functions
#include "WeightedBatchPreprocessing-engine.js" // stack engine
#include "WeightedBatchPreprocessing-GUI.js"    // GUI part
/* beautify ignore:end */

/*
 * Script entry point
 */
function main()
{
   function runsInAutomationMode()
   {
      for ( let i = 0; i < jsArguments.length; ++i )
      {
         let items = jsArguments[ i ].split( "=" );
         if ( items.length == 2 && items[ 0 ] == "automationMode" && items[ 1 ].toLowerCase() == "true" )
            return true;
      }
      return false;
   }

   let existingMainWindowIDs = ImageWindow.windows.filter( W => W.isWindow ).map( W => W.mainView.uniqueId );

   function perform()
   {
      let
      {
         existingDirectory,
      } = WBPPUtils.shared();

      console.beginLog();

      try
      {
         console.show();

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "WeightedBatchPreprocessing " + VERSION );
         console.noteln( SEPARATOR );

         let T = new ElapsedTime;

         engine.cleanProcessLog();
         engine.initializeExecution();
         engine.doBias();
         engine.doDark();
         engine.doFlat();
         engine.doLight();

         console.writeln( "<end><cbr><br>* WeightedBatchPreprocessing: ", T.text );

         console.flush();
         console.hide();

         // show process logs
         if ( !engine.automationMode )
            engine.showProcessLogs();
      }
      catch ( x )
      {
         if ( !engine.automationMode )
            ( new MessageBox( x.message, TITLE + " " + VERSION, StdIcon_Error, StdButton_Ok ) ).execute();
         console.hide();
      }

      // save process logs
      {
         let logData = console.endLog();
         let logDate = new Date;
         let logPath = existingDirectory( engine.outputDirectory + "/logs" ) +
            format( "/%04d%02d%02d%02d%02d%02d.log",
               logDate.getUTCFullYear(), logDate.getUTCMonth() + 1, logDate.getUTCDate(),
               logDate.getUTCHours(), logDate.getUTCMinutes(), logDate.getUTCSeconds() );
         try
         {
            let file = File.createFileForWriting( logPath );
            file.write( logData );
            file.close();
         }
         catch ( x )
         {
            if ( !engine.automationMode )
               ( new MessageBox( x.message, TITLE + " " + VERSION, StdIcon_Error, StdButton_Ok ) ).execute();
         }
      }
   };

   console.hide();

   if ( Parameters.isViewTarget )
      throw new Error( TITLE + " can only be executed in the global context." );

   // handles the automated mode
   if ( runsInAutomationMode() )
   {
      console.noteln( "WBPP AUTOMATION MODE" );

      // injects the other parameters and extract the files and folders to be added
      engine.setDefaultParameters();
      // resets the groups
      engine.groupsManager.clear();
      // enable automationMode
      engine.automationMode = true;

      // parse arguments first
      for ( let i = 0; i < jsArguments.length; ++i )
      {
         let pItems = jsArguments[ i ].split( "=" )
         if ( pItems.length == 2 )
         {
            if ( pItems[ 0 ] == "file" )
            {
               console.noteln( "add file: ", pItems[ 1 ] );
               engine.addFile( pItems[ 1 ] );
            }
            else if ( pItems[ 0 ] == "dir" )
            {
               console.noteln( "add directory: ", pItems[ 1 ] );
               // get the list of compatible file extensions
               let openFileSupport = new OpenFileDialog;
               openFileSupport.loadImageFilters();
               let filters = openFileSupport.filters[ 0 ]; // all known format
               filters.shift();
               filters = filters.concat( filters.map( f => ( f.toUpperCase() ) ) );
               let L = new FileList( pItems[ 1 ], filters, false /*verbose*/ );
               L.files.forEach( filePath => engine.addFile( filePath ) );
            }
            else
            {
               console.noteln( pItems[ 0 ], " = ", pItems[ 1 ] );
               // this is used as a parameter value
               Parameters.set( pItems[ 0 ], pItems[ 1 ] );
            }
         }
      }

      // read the parameters set
      engine.importParameters();
      // reconstgruct group in case keywords have been specified
      engine.reconstructGroups();

      perform();
   }
   else
   {
      // present the GUI

      engine.importParameters();

      let dialog = new StackDialog();

      for ( ;; )
      {
         dialog.updateControls();

         if ( !dialog.execute() )
         {
            if ( engine.saveFrameGroups || ( new MessageBox( "Do you really want to exit " + TITLE + " ?", TITLE, StdIcon_Question, StdButton_No, StdButton_Yes ) ).execute() == StdButton_Yes )
               break;
            continue;
         }

         engine.runDiagnostics();
         if ( !engine.hasDiagnosticMessages() || engine.showDiagnosticMessages( true /*cancelButton*/ ) )
            perform();
         engine.clearDiagnosticMessages();

         processEvents();
         gc();
      }

      engine.saveSettings();
      engine = null;
   }

   // clean up residual image windows if needed
   ImageWindow.windows.filter( W => W.isWindow ).forEach( W =>
   {
      if ( existingMainWindowIDs.indexOf( W.mainView.uniqueId ) == -1 )
         W.forceClose();
   } );
}

main();

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing.js - Released 2022-03-15T20:50:04Z
