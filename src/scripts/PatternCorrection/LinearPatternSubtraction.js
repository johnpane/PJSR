// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// LinearPatternSubtraction.js - Released 2021-04-11T08:52:05Z
// ----------------------------------------------------------------------------
//
// Pattern Correction Scripts
//
// Copyright (c) 2019-2021 Vicent Peris (OAUV). All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#feature-id LinearPatternSubtraction : Utilities > LinearPatternSubtraction

#feature-info  <b>LinearPatternSubtraction version 1.0</b><br/>\
   <br/>\
   A script to correct residual column or row patterns in a list of images.<br/>\
   <br/>\
   Copyright &copy; 2019-2021 Vicent Peris (OAUV). All Rights Reserved.

#feature-icon  @script_icons_dir/LinearPatternSubtraction.svg

#ifndef __PI_ENCODED_VERSION__
#error This script requires PixInsight version 1.8.7 or higher.
#endif
#iflt __PI_ENCODED_VERSION__ "000100080007"
#error This script requires PixInsight version 1.8.7 or higher.
#endif

#define TITLE "LinearPatternSubtraction"
#define VERSION "1.01"

#include <pjsr/LinearPatternSubtraction.jsh>
#include <pjsr/ReadTextOptions.jsh>
#include <pjsr/UndoFlag.jsh>
#include "LinearPatternSubtractionGUI.js"

/* beautify ignore:end */

/*
 * LinearPatternSubtraction
 *
 * Script to correct residual column or row patterns in a list of images.
 *
 * This script reuses some code written by Georg Viehoever in the
 * CanonBandingReduction script and applies the column or row correction in the
 * multiscale context. The engine also includes outlier rejection for a more
 * robust statistical evaluation.
 *
 * The script corrects entire rows or columns, but it can also correct partial
 * rows or columns by reading a defect table generated by CosmeticCorrection.
 * Thus, if you want to correct partial rows or columns, you should first run
 * CosmeticCorrection, manually create the defect list, and save it to a text
 * file.
 */

//------------------------------------------------------------------------------

/*
 * Script parameters.
 */
function LPSParameters()
{
   this.__base__ = Object;
   this.__base__();

   this.reset = function()
   {
      this.inputFiles = LPS_DEFAULT_INPUT_FILES;
      this.targetIsActiveImage = LPS_DEFAULT_TARGET_IS_ACTIVE_IMAGE;
      this.closeFormerWorkingImages = LPS_DEFAULT_CLOSE_FORMER_WORKING_IMAGES;
      this.outputDir = LPS_DEFAULT_OUTPUT_DIR;
      this.correctColumns = LPS_DEFAULT_CORRECT_COLUMNS;
      this.correctEntireImage = LPS_DEFAULT_CORRECT_ENTIRE_IMAGE;
      this.defectTableFilePath = LPS_DEFAULT_DEFECT_TABLE_FILE_PATH;
      this.postfix = LPS_DEFAULT_POSTFIX;
      this.layersToRemove = LPS_DEFAULT_LAYERS_TO_REMOVE;
      this.rejectionLimit = LPS_DEFAULT_REJECTION_LIMIT;
      this.globalRejection = LPS_DEFAULT_GLOBAL_REJECTION;
      this.globalRejectionLimit = LPS_DEFAULT_GLOBAL_REJECTION_LIMIT;
      this.backgroundReferenceLeft = LPS_DEFAULT_BACKGROUND_REFERENCE_LEFT;
      this.backgroundReferenceTop = LPS_DEFAULT_BACKGROUND_REFERENCE_TOP;
      this.backgroundReferenceWidth = LPS_DEFAULT_BACKGROUND_REFERENCE_WIDTH;
      this.backgroundReferenceHeight = LPS_DEFAULT_BACKGROUND_REFERENCE_HEIGHT;
   };

   this.ensureValid = function()
   {
      let inputFiles = [];
      for ( let i = 0; i < this.inputFiles.length; ++i )
      {
         let inputFile = this.inputFiles[ i ].trim();
         if ( inputFile.length > 0 )
            inputFiles.push( inputFile );
      }
      this.inputFiles = inputFiles;
      this.outputDir.trim();
      this.defectTableFilePath.trim();
      this.postfix.trim();
      if ( this.postfix.length == 0 )
         this.postfix = POSTFIX;
   };

   this.import = function()
   {
      if ( Parameters.has( "inputFiles" ) )
         this.inputFiles = Parameters.get( "inputFiles" ).split( '|' );
      if ( Parameters.has( "targetIsActiveImage" ) )
         this.targetIsActiveImage = Parameters.getBoolean( "targetIsActiveImage" );
      if ( Parameters.has( "closeFormerWorkingImages" ) )
         this.closeFormerWorkingImages = Parameters.getBoolean( "closeFormerWorkingImages" );
      if ( Parameters.has( "outputDir" ) )
         this.outputDir = Parameters.get( "outputDir" );
      if ( Parameters.has( "correctColumns" ) )
         this.correctColumns = Parameters.getBoolean( "correctColumns" );
      if ( Parameters.has( "correctEntireImage" ) )
         this.correctEntireImage = Parameters.getBoolean( "correctEntireImage" );
      if ( Parameters.has( "defectTableFilePath" ) )
         this.defectTableFilePath = Parameters.get( "defectTableFilePath" );
      if ( Parameters.has( "postfix" ) )
         this.postfix = Parameters.get( "postfix" );
      if ( Parameters.has( "layersToRemove" ) )
         this.layersToRemove = Parameters.getInteger( "layersToRemove" );
      if ( Parameters.has( "rejectionLimit" ) )
         this.rejectionLimit = Parameters.getInteger( "rejectionLimit" );
      if ( Parameters.has( "globalRejection" ) )
         this.globalRejection = Parameters.getBoolean( "globalRejection" );
      if ( Parameters.has( "globalRejectionLimit" ) )
         this.globalRejectionLimit = Parameters.getInteger( "globalRejectionLimit" );
      if ( Parameters.has( "backgroundReferenceLeft" ) )
         this.backgroundReferenceLeft = Parameters.getInteger( "backgroundReferenceLeft" );
      if ( Parameters.has( "backgroundReferenceTop" ) )
         this.backgroundReferenceTop = Parameters.getInteger( "backgroundReferenceTop" );
      if ( Parameters.has( "backgroundReferenceWidth" ) )
         this.backgroundReferenceWidth = Parameters.getInteger( "backgroundReferenceWidth" );
      if ( Parameters.has( "backgroundReferenceHeight" ) )
         this.backgroundReferenceHeight = Parameters.getInteger( "backgroundReferenceHeight" );

      this.ensureValid();
   };

   this.export = function()
   {
      Parameters.set( "inputFiles", this.inputFiles.join( '|' ) );
      Parameters.set( "targetIsActiveImage", this.targetIsActiveImage );
      Parameters.set( "closeFormerWorkingImages", this.closeFormerWorkingImages );
      Parameters.set( "outputDir", this.outputDir );
      Parameters.set( "correctColumns", this.correctColumns );
      Parameters.set( "correctEntireImage", this.correctEntireImage );
      Parameters.set( "defectTableFilePath", this.defectTableFilePath );
      Parameters.set( "postfix", this.postfix );
      Parameters.set( "layersToRemove", this.layersToRemove );
      Parameters.set( "rejectionLimit", this.rejectionLimit );
      Parameters.set( "globalRejection", this.globalRejection );
      Parameters.set( "globalRejectionLimit", this.globalRejectionLimit );
      Parameters.set( "backgroundReferenceLeft", this.backgroundReferenceLeft );
      Parameters.set( "backgroundReferenceTop", this.backgroundReferenceTop );
      Parameters.set( "backgroundReferenceWidth", this.backgroundReferenceWidth );
      Parameters.set( "backgroundReferenceHeight", this.backgroundReferenceHeight );
   };

   this.reset();
}

LPSParameters.prototype = new Object;


//------------------------------------------------------------------------------

function main()
{
   if ( Parameters.isViewTarget )
      throw new Error( TITLE + " cannot be executed on views." );

   let parameters = new LPSParameters;
   parameters.import();

   let dialog = new LPSDialog( parameters );
   if ( dialog.execute() )
   {
      parameters = dialog.parameters;
      parameters.ensureValid();

      console.noteln( "<end><cbr><br>=================================" );
      console.noteln(               "LinearPatternSubtraction script" );
      console.noteln(               "(C) 2019-2021 Vicent Peris (OAUV)" );
      console.noteln(               "=================================" );
      console.writeln();
      console.writeln( "* Working parameters" );
      console.writeln( "Target is active image: " + parameters.targetIsActiveImage );
      console.writeln( "Close former working images: " + parameters.closeFormerWorkingImages );
      console.writeln( "Output directory: " + parameters.outputDir );
      console.writeln( "Correct columns: " + parameters.correctColumns );
      console.writeln( "Correct entire image: " + parameters.correctEntireImage );
      console.writeln( "Defect table file path: " + parameters.defectTableFilePath );
      console.writeln( "Output image postfix: " + parameters.postfix );
      console.writeln( "Layers to remove: " + parameters.layersToRemove );
      console.writeln( "Rejection limit: " + parameters.rejectionLimit );
      console.writeln( "Global rejection: " + parameters.globalRejection );
      console.writeln( "Global rejection limit: " + parameters.globalRejectionLimit );
      console.writeln( "Background reference left: " + parameters.backgroundReferenceLeft );
      console.writeln( "Background reference top: " + parameters.backgroundReferenceTop );
      console.writeln( "Background reference width: " + parameters.backgroundReferenceWidth );
      console.writeln( "Background reference height: " + parameters.backgroundReferenceHeight );

      console.show();
      processEvents();

      let engine = new LPSEngine();
      engine.targetIsActiveImage = parameters.targetIsActiveImage;
      engine.inputFiles = parameters.inputFiles;
      engine.outputDir = parameters.outputDir;
      engine.correctColumns = parameters.correctColumns;
      engine.correctEntireImage = parameters.correctEntireImage;
      engine.defectTableFilePath = parameters.defectTableFilePath;
      engine.layersToRemove = parameters.layersToRemove;
      engine.backgroundReferenceLeft = parameters.backgroundReferenceLeft;
      engine.backgroundReferenceTop = parameters.backgroundReferenceTop;
      engine.backgroundReferenceWidth = parameters.backgroundReferenceWidth;
      engine.backgroundReferenceHeight = parameters.backgroundReferenceHeight;
      engine.rejectionLimit = parameters.rejectionLimit;
      engine.globalRejection = parameters.globalRejection;
      engine.globalRejectionLimit = parameters.globalRejectionLimit;
      engine.closeFormerWorkingImages = parameters.closeFormerWorkingImages;

      let T = new ElapsedTime;

      engine.execute();

      console.writeln( "<end><cbr><br>Script processing time: " + T.text );
   }
}

main();

// ----------------------------------------------------------------------------
// EOF LinearPatternSubtraction.js - Released 2021-04-11T08:52:05Z
