// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// LinearDefectDetection.js - Released 2021-04-11T08:52:05Z
// ----------------------------------------------------------------------------
//
// Pattern Correction Scripts
//
// Copyright (c) 2019-2021 Vicent Peris (OAUV). All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#feature-id LinearDefectDetection : Utilities > LinearDefectDetection

#feature-info  <b>LinearDefectDetection version 1.0</b><br/>\
   <br/>\
   A script to detect defective columns or rows in a reference image.<br/>\
   <br/>\
   Copyright &copy; 2019-2021 Vicent Peris (OAUV). All Rights Reserved.

#feature-icon  @script_icons_dir/LinearDefectDetection.svg

#ifndef __PI_ENCODED_VERSION__
#error This script requires PixInsight version 1.8.7 or higher.
#endif
#iflt __PI_ENCODED_VERSION__ "000100080007"
#error This script requires PixInsight version 1.8.7 or higher.
#endif

#define TITLE "LinearDefectDetection"
#define VERSION "1.01"

#include <pjsr/LinearDefectDetection.jsh>
#include "LinearDefectDetectionGUI.js"
/* beautify ignore:end */

/*
 * LinearDefectDetection Script.
 *
 * Script to detect defective columns or rows in a reference image.
 */

// ----------------------------------------------------------------------------

/*
 * Script parameters.
 */
function LDDParameters()
{
   this.__base__ = Object;
   this.__base__();

   this.reset = function()
   {
      this.detectColumns = LDD_DEFAULT_DETECT_COLUMNS;
      this.detectPartialLines = LDD_DEFAULT_DETECT_PARTIAL_LINES;
      this.closeFormerWorkingImages = LDD_DEFAULT_CLOSE_FORMER_WORKING_IMAGES;
      this.layersToRemove = LDD_DEFAULT_LAYERS_TO_REMOVE;
      this.rejectionLimit = LDD_DEFAULT_REJECTION_LIMIT;
      this.detectionThreshold = LDD_DEFAULT_DETECTION_THRESHOLD;
      this.partialLineDetectionThreshold = LDD_DEFAULT_PARTIAL_LINE_DETECTION_THRESHOLD;
      this.imageShift = LDD_DEFAULT_IMAGE_SHIFT;
      this.outputDir = LDD_DEFAULT_OUTPUT_DIR;
   };

   this.ensureValid = function()
   {
      this.outputDir.trim();
      if ( this.layersToRemove < 7 )
         this.layersToRemove = 7;
   };

   this.import = function()
   {
      if ( Parameters.has( "detectColumns" ) )
         this.detectColumns = Parameters.getBoolean( "detectColumns" );
      if ( Parameters.has( "detectPartialLines" ) )
         this.detectPartialLines = Parameters.getBoolean( "detectPartialLines" );
      if ( Parameters.has( "closeFormerWorkingImages" ) )
         this.closeFormerWorkingImages = Parameters.getBoolean( "closeFormerWorkingImages" );
      if ( Parameters.has( "layersToRemove" ) )
         this.layersToRemove = Parameters.getInteger( "layersToRemove" );
      if ( Parameters.has( "rejectionLimit" ) )
         this.rejectionLimit = Parameters.getInteger( "rejectionLimit" );
      if ( Parameters.has( "detectionThreshold" ) )
         this.detectionThreshold = Parameters.getInteger( "detectionThreshold" );
      if ( Parameters.has( "partialLineDetectionThreshold" ) )
         this.partialLineDetectionThreshold = Parameters.getInteger( "partialLineDetectionThreshold" );
      if ( Parameters.has( "imageShift" ) )
         this.imageShift = Parameters.getInteger( "imageShift" );
      if ( Parameters.has( "outputDir" ) )
         this.outputDir = Parameters.get( "outputDir" );

      this.ensureValid();
   };

   this.export = function()
   {
      Parameters.set( "detectColumns", this.detectColumns );
      Parameters.set( "detectPartialLines", this.detectPartialLines );
      Parameters.set( "closeFormerWorkingImages", this.closeFormerWorkingImages );
      Parameters.set( "layersToRemove", this.layersToRemove );
      Parameters.set( "rejectionLimit", this.rejectionLimit );
      Parameters.set( "detectionThreshold", this.detectionThreshold );
      Parameters.set( "partialLineDetectionThreshold", this.partialLineDetectionThreshold );
      Parameters.set( "imageShift", this.imageShift );
      Parameters.set( "outputDir", this.outputDir );
   };

   this.reset();
}

LDDParameters.prototype = new Object;

// ----------------------------------------------------------------------------

/*
 * Output the list of detected lines to console and text file.
 */
function Output( detectColumns, detectedLines, threshold, outputDir )
{
   if ( detectedLines.detectedColumnOrRow.length > 0 )
   {
      let outputFileName, columnOrRow;
      if ( detectColumns )
      {
         outputFileName = "/detected-columns_" + threshold + "-sigma.txt";
         columnOrRow = "Col";
      }
      else
      {
         outputFileName = "/detected-rows_" + threshold + "-sigma.txt";
         columnOrRow = "Row";
      }

      let outputPath, defectListTable = null;
      if ( outputDir )
      {
         outputPath = outputDir + outputFileName;
         defectListTable = File.createFileForWriting( outputPath );
      }

      console.noteln( "<end><cbr><br>Detected lines" );
      console.noteln( "--------------" );
      for ( let i = 0; i < detectedLines.detectedColumnOrRow.length; ++i )
      {
         if ( defectListTable )
            defectListTable.outTextLn( columnOrRow + " " +
               detectedLines.detectedColumnOrRow[ i ] + " " +
               detectedLines.detectedStartPixel[ i ] + " " +
               detectedLines.detectedEndPixel[ i ] );
         console.noteln( columnOrRow + " " +
            detectedLines.detectedColumnOrRow[ i ] + " " +
            detectedLines.detectedStartPixel[ i ] + " " +
            detectedLines.detectedEndPixel[ i ] );
      }
      console.noteln( "<end><cbr><br>Detected defect lines: " + detectedLines.detectedColumnOrRow.length );
      if ( defectListTable )
         defectListTable.close();
      console.writeln();
      if ( defectListTable )
      {
         if ( !File.exists( outputPath ) )
            throw new Error( "*** File I/O Error: Could not output defect list to " + outputPath );
         console.writeln( "Defect list saved to " + outputPath );
      }
   }
   else
   {
      console.warningln( "<end><cbr><br>No defect was detected. Try lowering the threshold value." );
   }
}

// ----------------------------------------------------------------------------

function main()
{
   if ( Parameters.isViewTarget )
      throw new Error( TITLE + " cannot be executed on views." );

   let parameters = new LDDParameters;
   parameters.import();

   let dialog = new LDDDialog( parameters );
   if ( dialog.execute() )
   {
      parameters = dialog.parameters;
      parameters.ensureValid();

      console.noteln( "<end><cbr><br>=================================" );
      console.noteln(               "LinearDefectDetection script" );
      console.noteln(               "(C) 2019-2021 Vicent Peris (OAUV)" );
      console.noteln(               "=================================" );
      console.writeln();
      console.writeln( "* Working parameters" );
      console.writeln( "Detect columns: " + parameters.detectColumns );
      console.writeln( "Detect partial lines: " + parameters.detectPartialLines );
      console.writeln( "Image shift: " + parameters.imageShift );
      console.writeln( "Close former working images: " + parameters.closeFormerWorkingImages );
      console.writeln( "Layers to remove: " + parameters.layersToRemove );
      console.writeln( "Rejection limit: " + parameters.rejectionLimit );
      console.writeln( "Detection threshold: " + parameters.detectionThreshold );
      console.writeln( "Partial line detection threshold: " + parameters.partialLineDetectionThreshold );
      console.writeln( "Output directory: " + parameters.outputDir );

      console.show();
      processEvents();

      let T = new ElapsedTime;

      let engine = new LDDEngine();
      engine.detectColumns = parameters.detectColumns;
      engine.detectPartialLines = parameters.detectPartialLines;
      engine.layersToRemove = parameters.layersToRemove;
      engine.rejectionLimit = parameters.rejectionLimit;
      engine.imageShift = parameters.imageShift;
      engine.detectionThreshold = parameters.detectionThreshold;
      engine.partialLineDetectionThreshold = parameters.partialLineDetectionThreshold;
      engine.closeFormerWorkingImages = parameters.closeFormerWorkingImages;

      engine.execute();

      Output( parameters.detectColumns, engine, parameters.detectionThreshold, parameters.outputDir );

      processEvents();
      console.writeln( "<end><cbr><br>Script processing time: " + T.text );
   }
}

main();

// ----------------------------------------------------------------------------
// EOF LinearDefectDetection.js - Released 2021-04-11T08:52:05Z
