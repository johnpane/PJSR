// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer-GUI.js - Released 2022-03-04T15:42:10Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.0.0
//
// Copyright (c) 2022 Roberto Sartori
// Copyright (c) 2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include <pjsr/FontFamily.jsh>
#include <pjsr/KeyCodes.jsh>
#include "WeightsOptimizer-Plots.js"
#include "PreviewControl.js"
#include "WeightsOptimizer-FormulaEditor.js"
/* beautify ignore:end */

// The script's parameters dialog prototype.
function WeightsOptimizerDialog( engine )
{
   this.__base__ = Dialog;
   this.__base__();

   this.windowTitle = TITLE;

   let PLOT_TYPE = {
      NONE: 0,
      STATE: 1,
      ITERATIONS: 2
   }
   this.plotType = PLOT_TYPE.NONE;

   // ------------------------------------------------------------------------
   // Local vars
   // ------------------------------------------------------------------------
   // plot library
   let plotLib = new WeightsOptimizerPlots( engine.cache );

   this.updaterTimer = new Timer
   this.updaterTimer.interval = 1;
   this.updaterTimer.periodic = true;
   this.updaterTimer.dialog = this;
   this.updaterTimer.onTimeout = () =>
   {
      this.solverExecutionProcessEvents();
   }
   this.updaterTimer.stop();


   // CONTAINER FOR THE MIDDLE AND LEFT COLUMNS

   let midLeftParent = new Control( this );
   midLeftParent.setScaledFixedWidth( 600 );

   // PLOT THE ITERATIONS

   this.plotIterations = () =>
   {
      this.plotType = PLOT_TYPE.ITERATIONS;

      if ( this.iterationsTreeBox.selectedNodes.length == 1 )
      {
         this.highlightedIterationIndex = this.iterationsTreeBox.selectedNodes[ 0 ].__iterationIndex__;
      }

      let session = engine.params.iterations.getEnabledSession( this.highlightedIterationIndex );

      if ( session.iterations.length > 0 )
         plotLib.plotIterations(
            session.iterations,
            session.iterationIndex,
            this.plotArea,
            this.plotGain.checked,
            this.plotMeasure.checked );
      else
         this.plotArea.clear();
   }

   // ------------------------------------------------------------------------
   //                                    TITLE
   // ------------------------------------------------------------------------
   let titlePane = new Label( midLeftParent );
   with( titlePane )
   {
      frameStyle = FrameStyle_Box;
      margin = 4;
      wordWrapping = true;
      useRichText = true;
      text =
         "<p><b>" + TITLE + " v" + VERSION + "</b> &mdash; " +
         "This script computes the optimal weights to " +
         "optimize a measurement of the master integration image. \n" +
         "Optimal weights can be stored into the light images FITS header to be used by" +
         "the Image Integration process.";
   }

   // ------------------------------------------------------------------------
   //                           INPUT IMAGES GROUP
   // ------------------------------------------------------------------------
   let leftContainerControl = new Control( midLeftParent );
   leftContainerControl.sizer = new VerticalSizer;
   with( leftContainerControl.sizer )
   {
      spacing = WO_DEFAULT_SPACING / 2;
   }

   this.filesManagementGroupBox = new GroupBox( leftContainerControl );
   this.filesManagementGroupBox.title = "Input Files";
   this.filesManagementGroupBox.sizer = new VerticalSizer
   with( this.filesManagementGroupBox.sizer )
   {
      margin = WO_DEFAULT_MARGIN;
      spacing = WO_DEFAULT_SPACING;
   }

   this.inputFilesTreeBox = new TreeBox( this.filesManagementGroupBox )
   with( this.inputFilesTreeBox )
   {
      setMinHeight = 80;
      multipleSelection = true;
      rootDecoration = false;
      alternateRowColor = true;
      numberOfColumns = 2;
      headerVisible = true;
      setScaledMinHeight( 200 );
      adjustColumnWidthToContents( 0 );

      onNodeDoubleClicked = ( item, index ) =>
      {
         let window = ImageWindow.open( item.__filepath__ );
         if ( window.length > 0 )
         {
            window[ 0 ].bringToFront()
         }
      }

      setHeaderText( 0, "#" );
      setHeaderText( 1, "FILE" );
      setHeaderAlignment( 0, Align_Left | TextAlign_VertCenter );
      setHeaderAlignment( 1, Align_Left | TextAlign_VertCenter );
   }

   this.delesectAllNodes = () =>
   {
      this.highlightedIterationIndex = -1;
   }

   // -------------------------------
   // INPUT IMAGES - ADD BUTTON
   // -------------------------------

   this.filesAddButton = new PushButton( this.filesManagementGroupBox );
   this.filesAddButton.text = "Add";
   this.filesAddButton.icon = this.scaledResource( ":/icons/add.png" );
   this.filesAddButton.toolTip = "<p>Add image files to the input images list.</p>";
   this.filesAddButton.onClick = () =>
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select Images";
      ofd.loadImageFilters();

      if ( ofd.execute() )
      {
         engine.addInputFiles( ofd.fileNames );
      }

      this.delesectAllNodes();
      this.updateDialog();
      plotLib.plotState( engine.params.state, this.plotArea );
   };

   this.refreshInputFileList = () =>
   {
      this.inputFilesTreeBox.clear();
      this.inputFilesTreeBox.canUpdate = false;
      for ( let i = 0; i < engine.params.inputFiles.length; ++i )
      {
         let node = new TreeBoxNode( this.inputFilesTreeBox );
         node.setText( 0, "" + i );
         node.setText( 1, File.extractNameAndExtension( engine.params.inputFiles[ i ] ) );
         node.__filepath__ = engine.params.inputFiles[ i ];
         node.setAlignment( 1, Align_Left );
         node.setToolTip( 1, engine.params.inputFiles[ i ] );
      }
      this.inputFilesTreeBox.adjustColumnWidthToContents( 0 );
      this.inputFilesTreeBox.adjustColumnWidthToContents( 1 );
      this.inputFilesTreeBox.canUpdate = true;
   }

   // -------------------------------
   // INPUT IMAGES - CLEAR BUTTON
   // -------------------------------

   this.filesClearButton = new PushButton( this.filesManagementGroupBox );
   with( this.filesClearButton )
   {
      text = "Clear";
      icon = scaledResource( ":/icons/clear.png" );
      toolTip = "<p>Clear the list of input images.</p>";
      onClick = () =>
      {
         this.plotArea.clear();
         this.inputFilesTreeBox.clear();
         // removing all files means detaching the association with the image integration incon
         this.imageIntegrationComboBox.currentItem = 0;
         // note: this will remove all files and reset the state and the iterations
         engine.clearImageIntegrationIconName();
         // update the dialog
         this.updateDialog();
      };
   }

   // -------------------------------
   // INPUT IMAGES - SELECT IMAGE INTEGRATION INSTANCE
   // -------------------------------
   this.imageIntegrationInstanceLabel = new Label( this.filesManagementGroupBox );
   this.imageIntegrationInstanceLabel.text = "Use an Image Integration Icon:"
   this.imageIntegrationInstanceLabel.toolTip = "<p>Use an existing Image Integration icon process to determine" +
      "the list of files to be optimized and to perform the integrated file to be measured.</p>"
   this.imageIntegrationInstanceLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.imageIntegrationComboBox = new ComboBox( this.filesManagementGroupBox );

   // INIT THE LIST
   this.imageIntegrationComboBox.addItem( "" );
   this.iiIcons = ProcessInstance.iconsByProcessId( "ImageIntegration" );
   this.iiIcons.forEach( iconName =>
   {
      this.imageIntegrationComboBox.addItem( iconName );
   } );
   this.updateImageIntegrationComboBox = () =>
   {
      let i = this.imageIntegrationComboBox.findItem( engine.params.imageIntegrationIconName );
      if ( i > 0 )
         this.imageIntegrationComboBox.currentItem = i;
   }

   // onItemSelected CALLBACK
   this.imageIntegrationComboBox.onItemSelected = ( item ) =>
   {
      if ( item > 0 && engine.params.imageIntegrationIconName == "" )
         engine.setImageIntegrationIconName( this.iiIcons[ item - 1 ] );
      else if ( item == 0 && engine.params.imageIntegrationIconName != "" )
         engine.clearImageIntegrationIconName();
      this.updateDialog();
   }

   let iiSizer = new HorizontalSizer;
   iiSizer.spacing = WO_DEFAULT_SPACING;
   iiSizer.add( this.imageIntegrationInstanceLabel );
   iiSizer.add( this.imageIntegrationComboBox );

   this.updateFilesManagerControls = () =>
   {
      if ( engine.executing )
      {
         this.filesAddButton.enabled = false;
         this.filesClearButton.enabled = false;
         this.imageIntegrationComboBox.enabled = false;

      }
      else
      {
         this.updateImageIntegrationComboBox();
         this.filesAddButton.enabled = engine.params.imageIntegrationIconName == "";
         this.filesClearButton.enabled = true;
         this.imageIntegrationComboBox.enabled = engine.params.imageIntegrationIconName == "" && engine.params.inputFiles.length == 0;
      }
   }

   // LAYOUT

   let iiControlSizer = new HorizontalSizer;
   iiControlSizer.spacing = WO_DEFAULT_SPACING;
   iiControlSizer.add( this.filesAddButton );
   iiControlSizer.add( this.filesClearButton );

   let inputImagesButtonsSizer = new VerticalSizer;
   inputImagesButtonsSizer.spacing = WO_DEFAULT_SPACING;
   inputImagesButtonsSizer.add( iiControlSizer );
   inputImagesButtonsSizer.add( iiSizer );

   with( this.filesManagementGroupBox.sizer )
   {
      add( this.inputFilesTreeBox );
      add( inputImagesButtonsSizer );
   }

   // ------------------------------------------------------------------------
   //                           GLOBAL OPTIONS GROUP
   // ------------------------------------------------------------------------
   let configurationColumnWidth = 20 * this.font.width( "M" );
   let globalOptionsColumnWidth = 8 * this.font.width( "M" );

   this.configurationGroupBox = new GroupBox( leftContainerControl );
   this.configurationGroupBox.title = "Global Options"
   this.configurationGroupBox.sizer = new VerticalSizer
   with( this.configurationGroupBox.sizer )
   {
      margin = WO_DEFAULT_MARGIN;
      spacing = WO_DEFAULT_SPACING;
   }

   // ---------------------------------------
   // GLOBAL OPTIONS -Select the optimization objective
   // ---------------------------------------

   this.optimizationMetricLabel = new Label( this.configurationGroupBox );
   this.optimizationMetricLabel.margin = 4;
   this.optimizationMetricLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.optimizationMetricLabel.wordWrapping = true;
   this.optimizationMetricLabel.useRichText = true;
   this.optimizationMetricLabel.text = "<b>Objective:</b>";
   this.optimizationMetricLabel.setFixedWidth( globalOptionsColumnWidth );

   this.optimizationMetricComboBox = new ComboBox( this.configurationGroupBox );

   OPTIMIZATION_METRICS.forEach( item =>
   {
      this.optimizationMetricComboBox.addItem( item.label );
   } )
   this.optimizationMetricComboBox.currentItem = engine.params.optMetric;
   this.optimizationMetricComboBox.onItemSelected = ( index ) =>
   {
      engine.params.optMetric = index;
      this.updateDialog();
   }

   {
      let hSizer = new HorizontalSizer;
      hSizer.add( this.optimizationMetricLabel );
      hSizer.add( this.optimizationMetricComboBox );
      this.configurationGroupBox.sizer.add( hSizer );
   }

   // ----------------------------
   // GLOBAL OPTIONS - Optimization Edit Formula button
   // ----------------------------
   this.formulaLabel = new Label( this.configurationGroupBox );
   this.formulaLabel.text = "Formula: "
   this.formulaLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.formulaLabel.setFixedWidth( globalOptionsColumnWidth );

   this.formulaEditButton = new ToolButton( this.configurationGroupBox );
   this.formulaEditButton.icon = this.scaledResource( ":/icons/document-edit.png" );
   this.formulaEditButton.onClick = () =>
   {
      // OPEN THE MODAL EDITOR
      let formulaEditor = new FormulaEditor( engine.params.customFormula );
      formulaEditor.execute();
      engine.params.customFormula = formulaEditor.formula;

      this.updateDialog();
   }

   this.formulaTextEdit = new Edit( this.configurationGroupBox );
   this.formulaTextEdit.readOnly = true;

   this.formulaMinMaxComboBox = new ComboBox( this.configurationGroupBox );
   this.formulaMinMaxComboBox.addItem( "min" );
   this.formulaMinMaxComboBox.addItem( "max" );
   this.formulaMinMaxComboBox.onItemSelected = ( item ) =>
   {
      engine.params.customFormulaMinMax = item == 0 ? "min" : "max";
      this.updateDialog();
   }

   {
      let formulaSizer = new HorizontalSizer;
      formulaSizer.spacing = WO_DEFAULT_SPACING;
      formulaSizer.add( this.formulaMinMaxComboBox );
      formulaSizer.add( this.formulaTextEdit );
      formulaSizer.add( this.formulaEditButton );

      let sizer = new HorizontalSizer;
      sizer.add( this.formulaLabel );
      sizer.add( formulaSizer );
      this.configurationGroupBox.sizer.add( sizer );
   }
   // ----------------------------
   // GLOBAL OPTIONS - Optimization Subregion
   // ----------------------------

   // signal preview selector
   this.signalReferenceLabel = new Label( this.configurationGroupBox );
   this.signalReferenceLabel.margin = 4;
   this.signalReferenceLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.signalReferenceLabel.wordWrapping = true;
   this.signalReferenceLabel.useRichText = true;
   this.signalReferenceLabel.text = "Subregion:";
   this.signalReferenceLabel.toolTip = "<p>Select a preview to define the region to be optimized.</p>" +
      "<p>If no preview is selected then the entire image will be optimized.</p>"
   this.signalReferenceLabel.setFixedWidth( globalOptionsColumnWidth );

   this.signalPreviewViewList = new ViewList( this.configurationGroupBox );
   this.signalPreviewViewList.toolTip = this.signalReferenceLabel.toolTip;
   this.signalPreviewViewList.getPreviews();
   this.signalPreviewViewList.onViewSelected = ( view ) =>
   {
      for ( let i = 0; i < view.window.numberOfPreviews; i++ )
      {
         const preview = view.window.previews[ i ];
         if ( preview.uniqueId === view.uniqueId )
         {
            let x = preview.window.previewRect( preview ).x0;
            let y = preview.window.previewRect( preview ).y0;
            engine.setSignalROI( x, y, preview.image.width, preview.image.height );
         }
      }
      this.updateGlobalConfiguration();
   }

   {
      let hSizer = new HorizontalSizer;
      hSizer.add( this.signalReferenceLabel );
      hSizer.add( this.signalPreviewViewList );
      this.configurationGroupBox.sizer.add( hSizer );
   }

   // ROI EDITORS
   this.roiEditorsLabel = new Label( this.configurationGroupBox );
   this.roiEditorsLabel.margin = 4;
   this.roiEditorsLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.roiEditorsLabel.text = "";
   this.roiEditorsLabel.toolTip = "<p>Restrict the optimization to a subregion.</p>"
   this.roiEditorsLabel.setFixedWidth( globalOptionsColumnWidth );
   this.roiEditorsLabel.enabled = false;

   let isValidNumber = function( val )
   {
      return !isNaN( Number( val ) );
   }

   this.roiX0Label = new Label( this );
   this.roiX0Label.text = "X:"
   this.roiX0Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.roiX0Edit = new Edit( this );
   this.roiX0Edit.onEditCompleted = () =>
   {
      if ( isValidNumber( this.roiX0Edit.text ) )
         engine.params.roiX0 = Number( this.roiX0Edit.text );
      this.updateGlobalConfiguration();
   }

   this.roiY0Label = new Label( this );
   this.roiY0Label.text = " Y:"
   this.roiY0Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.roiY0Edit = new Edit( this );
   this.roiY0Edit.onEditCompleted = () =>
   {
      if ( isValidNumber( this.roiY0Edit.text ) )
         engine.params.roiY0 = Number( this.roiY0Edit.text );
      this.updateGlobalConfiguration();
   }

   this.roiWLabel = new Label( this );
   this.roiWLabel.text = " W:"
   this.roiWLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.roiWEdit = new Edit( this );
   this.roiWEdit.onEditCompleted = () =>
   {
      if ( isValidNumber( this.roiWEdit.text ) )
         engine.params.roiW = Number( this.roiWEdit.text );
      this.updateGlobalConfiguration();
   }

   this.roiHLabel = new Label( this );
   this.roiHLabel.text = " H:"
   this.roiHLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.roiHEdit = new Edit( this );
   this.roiHEdit.onEditCompleted = () =>
   {
      if ( isValidNumber( this.roiHEdit.text ) )
         engine.params.roiH = Number( this.roiHEdit.text );
      this.updateGlobalConfiguration();
   }

   this.roiClearButton = new ToolButton( this );
   this.roiClearButton.icon = this.scaledResource( ":/auto-hide/close.png" );
   this.roiClearButton.onClick = () =>
   {
      engine.params.roiX0 = 0;
      engine.params.roiY0 = 0;
      engine.params.roiW = 0;
      engine.params.roiH = 0;

      this.updateGlobalConfiguration();
   }

   {
      let hSizer = new HorizontalSizer;
      // hSizer.addSpacing(globalOptionsColumnWidth);
      hSizer.add( this.roiEditorsLabel );
      hSizer.add( this.roiX0Label );
      hSizer.add( this.roiX0Edit );
      // hSizer.addSpacing( WO_DEFAULT_SPACING );
      hSizer.add( this.roiY0Label );
      hSizer.add( this.roiY0Edit );
      // hSizer.addSpacing( WO_DEFAULT_SPACING );
      hSizer.add( this.roiWLabel );
      hSizer.add( this.roiWEdit );
      // hSizer.addSpacing( WO_DEFAULT_SPACING );
      hSizer.add( this.roiHLabel );
      hSizer.add( this.roiHEdit );
      hSizer.addSpacing( WO_DEFAULT_SPACING );
      // hSizer.add( this.roiX0Lbl );
      hSizer.add( this.roiClearButton );
      this.configurationGroupBox.sizer.add( hSizer );
   }

   // ------------------------------------
   //  GLOBAL OPTIONS - Select the working directory
   // ------------------------------------

   this.workingDirLabel = new Label( this.configurationGroupBox );
   this.workingDirLabel.margin = 4;
   this.workingDirLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.workingDirLabel.wordWrapping = true;
   this.workingDirLabel.useRichText = true;
   this.workingDirLabel.text = "Working Dir:";
   this.workingDirLabel.toolTip = "<p>The directory that will contain the files generated by the script.</p>"
   this.workingDirLabel.setFixedWidth( globalOptionsColumnWidth );

   this.workingDirectoryEdit = new Edit( this.configurationGroupBox );
   this.workingDirectoryEdit.text = engine.params.workingDir;
   this.workingDirectoryEdit.toolTip = this.workingDirLabel.toolTip;
   this.workingDirectoryEdit.enabled = false;

   this.workingDirSelectButton = new ToolButton( this.configurationGroupBox );
   this.workingDirSelectButton.icon = this.scaledResource( ":/icons/select-file.png" );
   this.workingDirSelectButton.setScaledFixedSize( 20, 20 );
   this.workingDirSelectButton.toolTip = "<p>Select the output root directory.</p>";
   this.workingDirSelectButton.onClick = () =>
   {
      let gdd = new GetDirectoryDialog;
      gdd.initialPath = engine.params.workingDir;
      gdd.caption = "Select Output Directory";
      if ( gdd.execute() )
      {
         let dir = gdd.directory;
         if ( dir.endsWith( '/' ) )
            dir = dir.substring( 0, dir.length - 1 );
         this.workingDirectoryEdit.text = engine.params.workingDir = dir;
      }
   };

   {
      let hSizer = new HorizontalSizer;
      hSizer.add( this.workingDirLabel );
      hSizer.addSpacing( 2 );
      hSizer.add( this.workingDirectoryEdit );
      hSizer.addSpacing( 2 );
      hSizer.add( this.workingDirSelectButton );
      this.configurationGroupBox.sizer.add( hSizer );
   }

   /**
    * UPDATE THE GLOBAL CONFIGURATION PANEL
    */
   this.updateGlobalConfiguration = () =>
   {
      this.optimizationMetricComboBox.currentItem = engine.params.optMetric;
      this.formulaMinMaxComboBox.currentItem = engine.params.customFormulaMinMax == "min" ? 0 : 1;
      this.formulaTextEdit.text = engine.params.customFormula;
      if ( engine.params.customFormula.length == 0 )
      {
         this.formulaTextEdit.toolTip = "Empty custom formula";
         this.formulaTextEdit.text = "<INSERT A FORMULA>";
      }
      else
      {
         this.formulaTextEdit.toolTip = "Formula: " + engine.params.customFormula;
         let MaxLen = 12;
         if ( engine.params.customFormula.length <= MaxLen )
            this.formulaTextEdit.text = engine.params.customFormula;
         else
            this.formulaTextEdit.text = engine.params.customFormula.slice( 0, MaxLen ) + "...";
      }
      this.workingDirectoryEdit.text = engine.params.workingDir;

      let useOptimizationFormula = engine.params.usingOptimizationFormula();
      this.formulaEditButton.enabled = useOptimizationFormula;
      this.formulaMinMaxComboBox.enabled = useOptimizationFormula;
      this.formulaLabel.textColor = this.formulaMinMaxComboBox.textColor;

      if ( !this.formulaEditButton.enabled )
         this.formulaTextEdit.text = "";

      // se the roi
      this.roiX0Edit.text = "" + engine.params.roiX0;
      this.roiY0Edit.text = "" + engine.params.roiY0;
      this.roiWEdit.text = "" + engine.params.roiW;
      this.roiHEdit.text = "" + engine.params.roiH;

      let previewSelected = this.signalPreviewViewList.currentView.id != "";
      this.roiX0Edit.enabled = !previewSelected;
      this.roiY0Edit.enabled = !previewSelected;
      this.roiWEdit.enabled = !previewSelected;
      this.roiHEdit.enabled = !previewSelected;

      // disable while the engine is running
      this.configurationGroupBox.enabled = !engine.executing;
   }

   // ------------------------------------------------------------------------
   //                              SOLVER PARAMETERS
   // ------------------------------------------------------------------------
   this.optimizerConfigurationGroupBox = new GroupBox( leftContainerControl );
   this.optimizerConfigurationGroupBox.title = "Solver Parameters"
   this.optimizerConfigurationGroupBox.sizer = new VerticalSizer
   with( this.optimizerConfigurationGroupBox.sizer )
   {
      margin = WO_DEFAULT_MARGIN;
      spacing = WO_DEFAULT_SPACING;
   }

   // ----------------------
   // SOLVER PARAMETERS - DELTA
   // ----------------------

   this.deltaControl = new NumericControl( this.optimizerConfigurationGroupBox );
   this.deltaControl.label.text = "Gradient Evaluation Step:";
   this.deltaControl.label.setFixedWidth( configurationColumnWidth );
   this.deltaControl.setRange( 0.001, 0.1 );
   this.deltaControl.slider.setRange( 0, 1000 );
   this.deltaControl.slider.scaledMinWidth = 1;
   this.deltaControl.setPrecision( 3 );

   this.deltaControl.toolTip = "<p>This is the amount of variation in the image weight used to compute " +
      "the partial derivative of the cost function along the i-th component.</p><p>The smaller the value " +
      "the more localized is the gradient evaluation, as a drawback if this step is too small the cost variation could " +
      "be too small to produce a significant gradient.</p><p>The default works well in the aveage case, you may need " +
      "to increase this value for a large set of images where a small change in a weight of a single image produce a " +
      "too small variation.</p>";
   this.deltaControl.onValueUpdated = ( value ) =>
   {
      engine.params.delta = value;
      this.updateOptimizerConfiguration();
   };
   this.optimizerConfigurationGroupBox.sizer.add( this.deltaControl );

   // ----------------------
   // SOLVER PARAMETERS - SPEED
   // ----------------------

   this.speedControl = new NumericControl( this.optimizerConfigurationGroupBox );
   this.speedControl.label.text = "Learning Rate:";
   this.speedControl.label.setFixedWidth( configurationColumnWidth );
   this.speedControl.setRange( 1, 5 );
   this.speedControl.slider.setRange( 1, 50 );
   this.speedControl.slider.scaledMinWidth = 1;
   this.speedControl.setPrecision( 1 );

   this.speedControl.toolTip = "<p>The learning rate represents the scale factor applied to the cost function's gradient " +
      "to determine the amplitude of the solution increment at each iteration.</p><p>Each time the gradient is computed the next weights are computed " +
      "by performing a descendant step along the gradient direction, the bigger the learning rate the faster the increment.</p>" +
      "<p>Increase this parameter to speed up the convergence for a large set of images if the convergence is too slow.</p>";
   this.speedControl.onValueUpdated = ( value ) =>
   {
      engine.params.speed = value;
      this.updateOptimizerConfiguration();
   };
   this.optimizerConfigurationGroupBox.sizer.add( this.speedControl );

   // -------------------------
   // SOLVER PARAMETERS - MAX AUTO SPEED REDUCTIONS
   // -------------------------

   this.speedReductionsControl = new NumericControl( this.optimizerConfigurationGroupBox );
   this.speedReductionsControl.label.text = "Maximum Learning Rate Halves:";
   this.speedReductionsControl.label.setFixedWidth( configurationColumnWidth );
   this.speedReductionsControl.setRange( 0, 24 );
   this.speedReductionsControl.slider.setRange( 0, 24 );
   this.speedReductionsControl.slider.scaledMinWidth = 1;
   this.speedReductionsControl.setPrecision( 0 );

   this.speedReductionsControl.toolTip = "<p>This represents the numer of time the learning rate gets halved because of an overshoot occurred.</p>" +
      "<p>An overshoot occurs when the cost function gets increased after a gradient descendant step, this occurs when the increment step is too big." +
      "When this condition is detected, the optimizer iteratively reduces the learning rate and attempts to find a stable increment by halving the " +
      "increment size, until the maximum number of halves is reached.</p>" +
      "<p>You may need to increase this parameter as long as the learning rate gets increased to converge a slightly better solution.</p>";
   this.speedReductionsControl.onValueUpdated = ( value ) =>
   {
      engine.params.maxSpeedReductions = value;
      this.updateOptimizerConfiguration();
   };
   this.optimizerConfigurationGroupBox.sizer.add( this.speedReductionsControl );

   // --------------------------
   // SOLVER PARAMETERS - COST CONVERGENCE THRESHOLD
   // --------------------------

   this.convergenceControl = new NumericControl( this.optimizerConfigurationGroupBox );
   this.convergenceControl.label.text = "Convergence Threshold (%): 10^";
   this.convergenceControl.label.setFixedWidth( configurationColumnWidth );
   this.convergenceControl.setRange( -4, -1 );
   this.convergenceControl.slider.setRange( 0, 4 );
   this.convergenceControl.slider.scaledMinWidth = 1;
   this.convergenceControl.setPrecision( 0 );

   this.convergenceControl.toolTip = "<p>The cost function convergence threshold.</p>" +
      "<p>This threshold defines the absolute percentual increment variation in the gradient direction under which the state " +
      "is considered converged to an optimum value.</p>" +
      "<p>Decrease this value to converge a more optimal solution, the lower the value the slower the convergence will be.</p>"
   this.convergenceControl.onValueUpdated = ( value ) =>
   {
      engine.params.convergence = value;
      this.updateOptimizerConfiguration();
      this.refreshIterationTable();
   };
   this.optimizerConfigurationGroupBox.sizer.add( this.convergenceControl );

   // --------------------------
   // SOLVER PARAMETERS - RESET BUTTON
   // --------------------------
   let configurationResetButton = new PushButton( this.optimizerConfigurationGroupBox );
   configurationResetButton.text = "Reset";
   configurationResetButton.toolTip = "<p>Reset the parameters to the default values.</p>"
   configurationResetButton.onClick = () =>
   {
      engine.params.optimizerReset();
      this.updateDialog();
   }

   let resetSSizer = new HorizontalSizer;
   resetSSizer.spacing = WO_DEFAULT_SPACING;
   resetSSizer.addStretch();
   resetSSizer.add( configurationResetButton );
   this.optimizerConfigurationGroupBox.sizer.add( resetSSizer );


   // LAUYOUT
   leftContainerControl.sizer.add( this.filesManagementGroupBox );
   leftContainerControl.sizer.add( this.configurationGroupBox );
   leftContainerControl.sizer.add( this.optimizerConfigurationGroupBox );

   // ------------------------------------------------------------------------

   this.updateOptimizerConfiguration = () =>
   {
      this.deltaControl.setValue( engine.params.delta );
      this.speedControl.setValue( engine.params.speed );
      this.speedReductionsControl.setValue( engine.params.maxSpeedReductions );
      this.convergenceControl.setValue( engine.params.convergence );

      // disable the controls during the execution
      this.optimizerConfigurationGroupBox.enabled = !engine.executing;
   }

   // ------------------------------------------------------------------------------------------
   //                                       STATE MANAGEMENT
   // ------------------------------------------------------------------------------------------

   this.stateManagementParentGroupBox = new GroupBox( midLeftParent );
   this.stateManagementParentGroupBox.title = "Current Weights";
   this.stateManagementParentGroupBox.setScaledFixedWidth( 240 );
   this.stateManagementParentGroupBox.sizer = new VerticalSizer;
   with( this.stateManagementParentGroupBox.sizer )
   {
      margin = WO_DEFAULT_MARGIN;
      spacing = WO_DEFAULT_SPACING;
   }

   this.stateTreeBox = new TreeBox( this.stateManagementParentGroupBox );
   this.stateTreeBox.alternateRowColor = true;
   this.stateTreeBox.multipleSelection = false;
   this.stateTreeBox.indentSize = 0;
   this.stateTreeBox.setScaledMinHeight( 120 );

   with( this.stateTreeBox )
   {
      setMinHeight = 80;
      multipleSelection = true;
      rootDecoration = false;
      alternateRowColor = true;
      numberOfColumns = 2;
      headerVisible = true;
      setScaledMinHeight( 200 );
      setHeaderText( 0, "#" );
      setHeaderText( 1, "WEIGHT" );
   }

   this.redrawStatus = () =>
   {
      this.stateTreeBox.clear();
      engine.params.state.forEach( ( weight, i ) =>
      {

         let node = new TreeBoxNode;
         node.setText( 0, "" + i );
         node.setAlignment( 0, TextAlign_Center );
         node.setText( 1, format( "%0.6f", weight ) );
         node.setAlignment( 0, TextAlign_Center );

         // store the associated metatada
         node.__i__ = i;
         node.__weight__ = weight;

         this.stateTreeBox.add( node );
      } )
      this.stateTreeBox.adjustColumnWidthToContents( 0 );
      this.stateTreeBox.adjustColumnWidthToContents( 1 );
   }

   // -------------------------------
   //    STATE MANAGEMENT - RESET BUTTON
   // -------------------------------
   this.stateResetButton = new PushButton( this.stateManagementParentGroupBox );
   this.stateResetButton.text = "Reset"
   this.stateResetButton.onClick = () =>
   {
      this.delesectAllNodes();
      engine.params.resetState();
      this.updateDialog();
      plotLib.plotState( engine.params.state, this.plotArea );
   }

   // -------------------------------
   // STATE MANAGEMENT - SAVE STATE
   // -------------------------------
   this.stateSaveButton = new PushButton( this.stateManagementParentGroupBox );
   this.stateSaveButton.text = "WRITE"
   this.stateSaveButton.onClick = () =>
   {

      this.filesManagementGroupBox.enabled = false;
      this.configurationGroupBox.enabled = false;
      this.optimizerConfigurationGroupBox.enabled = false;
      this.stateManagementParentGroupBox.enabled = false;

      this.undoToolButton.enabled = false;
      this.restartToolButton.enabled = false;
      this.stepToolButton.enabled = false;
      this.startToolButton.visible = false;
      this.stopToolButton.visible = true;

      // program a timer to run the store state
      let storeStateAsyncTimerEvent = new Timer( 0, false )
      storeStateAsyncTimerEvent.periodic = false;
      storeStateAsyncTimerEvent.dialog = this;
      storeStateAsyncTimerEvent.interval = 0;
      storeStateAsyncTimerEvent.onTimeout = () => engine.storeState( undefined /* current state */ , undefined, true /* force */ , this.solverExecutionProcessEvents /* run cbk */ , this.solverExecutionTerminated /* end cbk */ , true /* to input files*/ );
      storeStateAsyncTimerEvent.start();
   }

   this.saveKeywordEdit = new Edit( this.stateManagementParentGroupBox );
   this.saveKeywordEdit.text = engine.params.optKeyword;
   this.saveKeywordEdit.onTextUpdated = ( text ) =>
   {
      if ( text.indexOf( " " ) != -1 )
         this.saveKeywordEdit.text = this.saveKeywordEdit.text.replace( " ", "" );
   }
   this.saveKeywordEdit.onEditCompleted = () =>
   {
      if ( this.saveKeywordEdit.text.length > 0 )
         engine.params.optKeyword = this.saveKeywordEdit.text;
      else
         this.saveKeywordEdit.text = engine.params.optKeyword;
   }

   this.sateSaveSizer = new HorizontalSizer;
   this.sateSaveSizer.spacing = WO_DEFAULT_SPACING;
   this.sateSaveSizer.add( this.stateSaveButton );
   this.sateSaveSizer.add( this.saveKeywordEdit );

   // -------------------------------
   //    STATE MANAGEMENT - RESET BUTTON
   // -------------------------------
   this.randomStateButton = new PushButton( this.stateManagementParentGroupBox );
   this.randomStateButton.text = "Random"
   this.randomStateButton.onClick = () =>
   {
      this.delesectAllNodes();
      engine.params.randomState();
      this.updateDialog();
      plotLib.plotState( engine.params.state, this.plotArea );
   }

   // -------------------------------
   //    STATE MANAGEMENT - RESET BUTTON
   // -------------------------------
   this.deltaStateButton = new PushButton( this.stateManagementParentGroupBox );
   this.deltaStateButton.text = "Delta (%)"
   this.deltaStateButton.onClick = () =>
   {
      let numeric = parseFloat( this.deltaPercentageEdit.text );
      if ( !isNaN( numeric ) )
      {
         this.delesectAllNodes();
         engine.params.deltaState( numeric / 100 );
         this.updateDialog();
         plotLib.plotState( engine.params.state, this.plotArea );
      }
   }

   this.deltaPercentageEdit = new Edit( this.stateManagementParentGroupBox )
   this.deltaPercentageEdit.text = "20";
   this.deltaPercentageEdit.__prevValue__ = "20";
   this.deltaPercentageEdit.onEditCompleted = () =>
   {
      let numeric = parseFloat( this.deltaPercentageEdit.text );
      console.noteln( numeric )
      if ( !isNaN( numeric ) )
      {
         numeric = Math.max( 0, Math.min( 100, Math.round( numeric ) ) );
         this.deltaPercentageEdit.text = format( "%3i", numeric );
         this.deltaPercentageEdit.__prevValue__ = this.deltaPercentageEdit.text;
      }
      else
      {
         this.deltaPercentageEdit.text = this.deltaPercentageEdit.__prevValue__
      }
   }

   this.deltaStateSizer = new HorizontalSizer;
   this.deltaStateSizer.spacing = WO_DEFAULT_SPACING;
   this.deltaStateSizer.add( this.deltaPercentageEdit )

   // -------------------------------
   //    STATE MANAGEMENT - LOAD FROM KEYWORD
   // -------------------------------
   this.stateFromKeywordButton = new PushButton( this.stateManagementParentGroupBox );
   this.stateFromKeywordButton.text = "From Header"
   this.stateFromKeywordButton.onClick = () =>
   {
      if ( this.stateFromKeywordEdit.text.length > 0 )
      {
         this.delesectAllNodes();
         engine.loadStateFromKeyword( this.stateFromKeywordEdit.text );
         this.updateDialog();
         plotLib.plotState( engine.params.state, this.plotArea );
      }
   }

   this.stateFromKeywordEdit = new Edit;

   let stateLeftButtons = new VerticalSizer;
   stateLeftButtons.spacing = WO_DEFAULT_SPACING;
   stateLeftButtons.add( this.stateFromKeywordButton );
   stateLeftButtons.add( this.deltaStateButton );
   stateLeftButtons.add( this.stateResetButton );

   let stateRightButtons = new VerticalSizer;
   stateRightButtons.spacing = WO_DEFAULT_SPACING;
   stateRightButtons.add( this.stateFromKeywordEdit );
   stateRightButtons.add( this.deltaStateSizer );
   stateRightButtons.add( this.randomStateButton );

   let stateButtonsSizer = new HorizontalSizer;
   stateButtonsSizer.spacing = WO_DEFAULT_SPACING;
   stateButtonsSizer.add( stateLeftButtons );
   stateButtonsSizer.add( stateRightButtons );


   // -------------------------------
   // STATE MANAGEMENT - PLOT STATE
   // -------------------------------
   this.plotStateButton = new PushButton( this.stateManagementParentGroupBox );
   this.plotStateButton.text = "PLOT"
   this.plotStateButton.onClick = () =>
   {
      this.delesectAllNodes();
      this.updateDialog();
      plotLib.plotState( engine.params.state, this.plotArea );
   }

   // LAYOUT
   this.stateManagementParentGroupBox.sizer.add( this.stateTreeBox );
   this.stateManagementParentGroupBox.sizer.add( stateButtonsSizer );
   this.stateManagementParentGroupBox.sizer.add( this.plotStateButton );
   this.stateManagementParentGroupBox.sizer.add( this.sateSaveSizer );

   /**
    * UPDATE THE STATE MANAGEMENT CONTROLS
    */
   this.updateStateManagement = () =>
   {
      this.stateResetButton.enabled = !engine.executing;
      this.stateSaveButton.enabled = !engine.executing;
      this.saveKeywordEdit.enabled = !engine.executing;
      this.randomStateButton.enabled = !engine.executing;
      this.deltaStateButton.enabled = !engine.executing;
      this.deltaPercentageEdit.enabled = !engine.executing;
      this.stateFromKeywordButton.enabled = !engine.executing;
      this.stateFromKeywordEdit.enabled = !engine.executing;
      this.plotStateButton.enabled = !engine.executing;
   }

   // ------------------------------------------------------------------------------------------
   //                                  ITERATIONS AND REPORTS
   // ------------------------------------------------------------------------------------------
   let iterationsAndReportGroupBox = new GroupBox( this );
   iterationsAndReportGroupBox.title = "Solver Iterations"
   iterationsAndReportGroupBox.setScaledMinWidth( 440 );
   iterationsAndReportGroupBox.sizer = new VerticalSizer;
   with( iterationsAndReportGroupBox.sizer )
   {
      margin = WO_DEFAULT_MARGIN;
      spacing = WO_DEFAULT_SPACING;
   }

   // -------------------------------
   // ITERATIONS AND REPORTS - ITERATIONS TABLE
   // -------------------------------
   this.iterationsTreeBox = new TreeBox( iterationsAndReportGroupBox );
   // this.iterationsTreeBox.alternateRowColor = true;
   this.iterationsTreeBox.multipleSelection = false;
   this.stateTreeBox.indentSize = 0;
   this.iterationsTreeBox.setScaledFixedHeight( 240 );
   this.iterationsTreeBox.setVariableWidth();
   this.highlightedIterationIndex = -1;
   this.markedRows = [];
   this.hintPromotedRow = -1;

   this.iterationsTreeBox.onKeyRelease = ( keyCode ) =>
   {

      this.iterationsTreeBox.enabled = false;
      if (
         keyCode == Key_Up ||
         keyCode == Key_Down ||
         keyCode == Key_PageUp ||
         keyCode == Key_PageDown
      )
      {
         if ( this.plotType == PLOT_TYPE.ITERATIONS || this.plotType == PLOT_TYPE.NONE )
            this.plotIterations();
      }
      this.iterationsTreeBox.enabled = true;
      this.iterationsTreeBox.hasFocus = true;
   }

   this.iterationsTreeBox.onNodeClicked = ( node ) =>
   {
      let nodeSelected = this.iterationsTreeBox.selectedNodes.length > 0;
      let sameNodeSelected = nodeSelected && this.iterationsTreeBox.selectedNodes[ 0 ].__iterationIndex__ == this.highlightedIterationIndex;
      // the following unexpected behavior occurs when a node is checheck/unchecked: onNodeClicked is called the first time but no nodes are selected
      if ( !sameNodeSelected && this.iterationsTreeBox.selectedNodes[ 0 ] == undefined )
         return

      this.highlightedIterationIndex = sameNodeSelected ? -1 : this.iterationsTreeBox.selectedNodes[ 0 ].__iterationIndex__;
      if ( sameNodeSelected )
      {
         this.iterationsTreeBox.selectedNodes[ 0 ].selected = false;
      }
      this.updateIterationNotes();
      this.updateDialog();
      this.plotIterations();
   }

   this.iterationsTreeBox.onNodeUpdated = ( node ) =>
   {
      engine.params.iterations.setEnable( node.__iterationIndex__, node.checked );
      this.plotIterations();
      // refresh the dialog later
      this.updateDialogTimer.start();
   }

   this.NprefixHeaders = 5;

   this.initIterationsTable = () =>
   {

      let N = engine.params.state.length + this.NprefixHeaders;
      this.iterationsTreeBox.numberOfColumns = N;

      // set the standard headers
      this.iterationsTreeBox.setHeaderText( 0, "# | Session" );
      this.iterationsTreeBox.setHeaderText( 1, "OPTIMIZE" );
      this.iterationsTreeBox.setHeaderText( 2, "VALUE" );
      this.iterationsTreeBox.setHeaderText( 3, "GAIN %" );
      this.iterationsTreeBox.setHeaderText( 4, "NOTE" );
      for ( let i = 0; i < engine.params.state.length; i++ )
         this.iterationsTreeBox.setHeaderText( this.NprefixHeaders + i, "" + i );

      // stylize the header
      for ( let i = 0; i < N; i++ )
      {
         this.iterationsTreeBox.setHeaderAlignment( i, TextAlign_Center );
         this.iterationsTreeBox.adjustColumnWidthToContents( i );
      }
      this.iterationsTreeBox.__clicked_index = -1;
   }

   /**
    * Returns TRUE if a session is currently selected
    */
   this.sessionIsSelected = () => (
      this.highlightedIterationIndex != -1 //&& engine.params.iterations[ this.highlightedIterationIndex ].type == ITERATION_TYPE.INIT
   )

   this.refreshIterationTable = () =>
   {
      if ( this.highlightedIterationIndex >= engine.params.iterations.length )
         this.delesectAllNodes();

      // value formatter - truncate instead of round, plot 9 digits and remove the last char
      let val = ( value, precision ) => ( format( "%.6f", value ).slice( 0, ( ( precision != undefined ) ? ( -6 + precision ) : undefined ) ) )

      // configure the table header
      this.initIterationsTable();

      // clear and reconstruct the table
      this.iterationsTreeBox.clear();

      // check if session is selected: a session is selected when the starting node is highlighted
      let selectedSession = undefined;
      if ( this.sessionIsSelected() )
         selectedSession = engine.params.iterations[ this.highlightedIterationIndex ].sessionID;

      let convergenceDigits = -engine.params.convergence;

      let parentNode;
      for ( let i = 0; i < engine.params.iterations.length; i++ )
      {
         let rowIsHintedForPromotion = this.hintPromotedRow == i;

         let it = engine.params.iterations[ i ];
         let treeNode = new TreeBoxNode( this.iterationsTreeBox );
         let sessionID = engine.params.iterations[ i ].sessionID;
         if ( i == 0 || engine.params.iterations[ i ].sessionID != engine.params.iterations[ i - 1 ].sessionID )
            parentNode = treeNode

         treeNode.setText( 0, "" + ( it.index != undefined ? it.index : "" ) + " | " + sessionID );

         treeNode.checkable = true;
         treeNode.checked = engine.params.iterations[ i ].enabled;

         if ( rowIsHintedForPromotion )
            treeNode.setIcon( 0, this.scaledResource( ":/bullets/bullet-ball-glass-green.png" ) )
         else
            switch ( it.type )
            {
               case ITERATION_TYPE.INIT:
                  treeNode.setIcon( 0, this.scaledResource( ":/bullets/bullet-triangle-green.png" ) )
                  break;
               case ITERATION_TYPE.DESCEND:
                  treeNode.setIcon( 0, this.scaledResource( ":/bullets/bullet-ball-glass-yellow.png" ) )
                  break;
               case ITERATION_TYPE.SPEED_REDUCTIONS_STOP:
                  treeNode.setIcon( 0, this.scaledResource( ":/bullets/bullet-ball-glass-green.png" ) )
                  break;
               case ITERATION_TYPE.CONVERGED:
                  treeNode.setIcon( 0, this.scaledResource( ":/bullets/bullet-ball-glass-green.png" ) )
                  break;
            }

         treeNode.setText( 1, OPTIMIZATION_METRICS[ it.optMetric ].label );
         treeNode.setAlignment( 1, 3 );
         if ( OPTIMIZATION_METRICS[ it.optMetric ].label == WO_CUSTOM_FORMULA_LBL )
            treeNode.setToolTip( 1, "" + it.customFormula );
         treeNode.setText( 2, "" + val( it.measure ) );

         treeNode.setText( 3, "" + val( it.mvp * 100.0, convergenceDigits ) );
         treeNode.setAlignment( 3, 2 );
         treeNode.setText( 4, "" + it.note );
         treeNode.setToolTip( 4, it.note );
         treeNode.__iterationIndex__ = i;
         for ( let j = 0; j < it.state.length; j++ )
         {
            treeNode.setText( this.NprefixHeaders + j, val( it.state[ j ] ) );
         }

         // handle the marked rows: content replaced with "-"
         let rowIsMarked = false;
         for ( let j = 0; j < this.markedRows.length; j++ )
            if ( this.markedRows[ j ] == i )
            {
               rowIsMarked = true;
               break;
            }

         if ( rowIsMarked )
            for ( let j = 0; j < it.state.length + this.NprefixHeaders; j++ )
            {
               treeNode.setAlignment( j, 3 );
               treeNode.setText( j, "-" )
            }
         let colorInt = it.colorInt;

         if ( selectedSession != undefined )
         {
            let color = 0x00CCCCCC;
            if ( selectedSession == sessionID )
            {
               color = colorInt || 0x00000000;
            }
            for ( let j = 0; j < it.state.length + this.NprefixHeaders; j++ )
               treeNode.setTextColor( j, color );
         }
         else
         {
            if ( colorInt )
               for ( let j = 0; j < it.state.length + this.NprefixHeaders; j++ )
                  treeNode.setTextColor( j, colorInt );
         }

         // disabeld rows always override any font color previously assigned
         if ( !it.enabled )
            for ( let j = 0; j < it.state.length + this.NprefixHeaders; j++ )
               treeNode.setTextColor( j, 0x00CCCCCC );


         // bold / regular font
         let bold = it.type == ITERATION_TYPE.CONVERGED || it.type == ITERATION_TYPE.SPEED_REDUCTIONS_STOP || rowIsHintedForPromotion;
         for ( let j = 0; j < it.state.length + this.NprefixHeaders; j++ )
         {
            let newFont = new Font( FontFamily_Monospace, 8, "pt" );
            newFont.bold = bold;
            treeNode.setFont( j, newFont );
         }

         this.iterationsTreeBox.add( treeNode );
      }

      let N = engine.params.state.length + this.NprefixHeaders;
      for ( let j = 0; j < N; j++ )
         this.iterationsTreeBox.adjustColumnWidthToContents( j );

      if ( this.highlightedIterationIndex != -1 )
         this.iterationsTreeBox.child( this.highlightedIterationIndex ).selected = true;
   }

   // -------------------------------
   // ITERATIONS AND REPORTS - ITERATION NOTES
   // -------------------------------
   this.iterationNotes = new TextBox( iterationsAndReportGroupBox );
   this.iterationNotes.setScaledFixedHeight( 240 );
   this.iterationNotes.setScaledFixedWidth( 100 );

   this.iterationNotes.onTextUpdated = () =>
   {
      if ( this.highlightedIterationIndex == -1 )
         return;

      engine.params.iterations[ this.highlightedIterationIndex ].note = this.iterationNotes.text;
      this.refreshIterationTable();
   }

   this.updateIterationNotes = () =>
   {
      if ( this.highlightedIterationIndex != -1 )
      {
         this.iterationNotes.enabled = true;
         this.iterationNotes.text = engine.params.iterations[ this.highlightedIterationIndex ].note || ""
      }
      else
      {
         this.iterationNotes.enabled = false;
         this.iterationNotes.text = ""
      }
   };

   // LAYOUT ITERATIONS AND SOLVER
   this.iterationsAndSolverSizer = new HorizontalSizer;
   this.iterationsAndSolverSizer.spacing = WO_DEFAULT_SPACING;
   this.iterationsAndSolverSizer.add( this.iterationsTreeBox );
   this.iterationsAndSolverSizer.add( this.iterationNotes );


   // ------------------------------------------------------------------------------------------
   //                           ITERATIONS AND REPORTS - TOOLBAR
   // ------------------------------------------------------------------------------------------
   this.toolBar = new Control( iterationsAndReportGroupBox )
   this.toolBar.sizer = new HorizontalSizer;
   with( this.toolBar.sizer )
   {
      spacing = WO_DEFAULT_SPACING;
   }

   // -------------------------------
   // TOOLBAR - SET STATUS
   // -------------------------------
   this.setStatusToolButton = new ToolButton( this.toolBar );
   this.setStatusToolButton.icon = this.scaledResource( ":/icons/document-arrow-left.png" );
   this.setStatusToolButton.setScaledFixedSize( 24, 24 );
   this.setStatusToolButton.focusStyle = FocusStyle_NoFocus;
   this.setStatusToolButton.toolTip = "<p>Sets the current selected row's state as the current state.</p>"
   this.setStatusToolButton.onClick = () =>
   {
      if ( this.highlightedIterationIndex == -1 )
         return;
      engine.params.state = engine.params.iterations[ this.highlightedIterationIndex ].state;
      this.updateDialog();
   };

   // -------------------------------
   // TOOLBAR - SAVE
   // -------------------------------
   this.loadSaveSeparator = new Label( this.toolBar );
   this.loadSaveSeparator.text = " | "
   this.loadSaveSeparator.textAlignment = TextAlign_Center;

   this.saveToolButton = new ToolButton( this.toolBar );
   this.saveToolButton.icon = this.scaledResource( ":/icons/save.png" );
   this.saveToolButton.setScaledFixedSize( 24, 24 );
   this.saveToolButton.focusStyle = FocusStyle_NoFocus;
   this.saveToolButton.toolTip = "<p>Saves the script status into an external file.</p>"
   this.saveToolButton.onClick = () =>
   {
      let sfd = new SaveFileDialog;
      sfd.initialPath = engine.params.workingDir;
      sfd.caption = "Save Weight Optimizer Status";
      sfd.overwritePrompt = true;
      if ( sfd.execute() )
         engine.saveToFile( sfd.fileName );
   };

   // -------------------------------
   // TOOLBAR - LOAD
   // -------------------------------
   this.loadToolButton = new ToolButton( this.toolBar );
   this.loadToolButton.icon = this.scaledResource( ":/icons/folder-open.png" );
   this.loadToolButton.setScaledFixedSize( 24, 24 );
   this.loadToolButton.focusStyle = FocusStyle_NoFocus;
   this.loadToolButton.toolTip = "<p>Loads the script status from an external file.</p>"
   this.loadToolButton.onClick = () =>
   {
      let ofd = new OpenFileDialog;
      ofd.initialPath = engine.params.workingDir;
      ofd.multipleSelection = false;
      ofd.caption = "Load Weight Optimizer Status";
      if ( ofd.execute() )
      {
         engine.loadFromFile( ofd.fileName );
         this.updateDialog();
         this.plotIterations();
      }
   };

   // -------------------------------
   // TOOLBAR - UNDO SOLVER
   // -------------------------------
   this.undoToolButton = new ToolButton( this.toolBar );
   this.undoToolButton.icon = this.scaledResource( ":/icons/undo.png" );
   this.undoToolButton.setScaledFixedSize( 24, 24 );
   this.undoToolButton.focusStyle = FocusStyle_NoFocus;
   this.undoToolButton.toolTip = "<p>Undo the last iteration.</p>"
   this.undoToolButton.onClick = () =>
   {
      engine.undoITeration();
      this.updateDialog();
   };

   this.undoToolButton.onEnter = () =>
   {
      if ( engine.executing )
         return;
      this.markedRows = [ engine.params.iterations.length - 1 ];
      this.refreshIterationTable();
   }

   this.undoToolButton.onLeave = () =>
   {
      this.markedRows = [];
      this.refreshIterationTable();
   }

   // -------------------------------
   // TOOLBAR - RESTART SOLVER
   // -------------------------------
   this.restartToolButton = new ToolButton( this.toolBar );
   this.restartToolButton.icon = this.scaledResource( ":/icons/reload.png" );
   this.restartToolButton.setScaledFixedSize( 24, 24 );
   this.restartToolButton.focusStyle = FocusStyle_NoFocus;
   this.restartToolButton.toolTip = "<p>Clear the iterations data and restart the solver.</p>"
   this.restartToolButton.onClick = () =>
   {
      engine.clearIterations();
      this.updateDialog();
      this.plotIterations();
   };

   // -------------------------------
   // TOOLBAR - STEP SOLVER
   // -------------------------------
   this.stepToolButton = new ToolButton( this.toolBar );
   this.stepToolButton.icon = this.scaledResource( ":/icons/step-forward.png" );
   this.stepToolButton.setScaledFixedSize( 24, 24 );
   this.stepToolButton.focusStyle = FocusStyle_NoFocus;
   this.stepToolButton.toolTip = "<p>Runs a single iteration.</p>"
   this.stepToolButton.onClick = () =>
   {
      // deselect any row
      this.delesectAllNodes();
      this.updateDialog();

      this.updaterTimer.start();
      engine.execute( 1, undefined /*this.solverExecutionProcessEvents*/ , this.solverExecutionIterationCompleted, this.solverExecutionTerminated );
      this.refreshToolBar();
   };

   // -------------------------------
   // TOOLBAR - START SOLVER
   // -------------------------------
   this.startToolButton = new ToolButton( this.toolBar );
   this.startToolButton.icon = this.scaledResource( ":/icons/play.png" );
   this.startToolButton.setScaledFixedSize( 24, 24 );
   this.startToolButton.focusStyle = FocusStyle_NoFocus;
   this.startToolButton.toolTip = "<p>Runs the solver until the optimal solution is found.</p>"
   this.startToolButton.onClick = () =>
   {
      // deselect any row
      this.delesectAllNodes();
      this.updateDialog();

      // start the dialog updater timer
      this.updaterTimer.start()
      // refresh the dialog later to enable/disable the controls during the execution
      this.updateDialogTimer.start();
      engine.execute( undefined /* max iterations not specified */ , this.solverExecutionProcessEvents, this.solverExecutionIterationCompleted, this.solverExecutionTerminated );
      this.refreshToolBar();
   };

   // // -------------------------------
   // // TOOLBAR - STOP SOLVER
   // // -------------------------------
   // disabling the dialog on a scheduled timer
   this.stopTimer = new Timer( 0 /*interval*/ , false /*periodic*/ );
   this.stopTimer.onTimeout = () =>
   {
      this.setStatusLabel( "<b>STOPPING...</b>" );
      this.enabled = false;
   };

   this.stopToolButton = new ToolButton( this.toolBar );
   this.stopToolButton.icon = this.scaledResource( ":/icons/stop.png" );
   this.stopToolButton.setScaledFixedSize( 24, 24 );
   this.stopToolButton.focusStyle = FocusStyle_NoFocus;
   this.stopToolButton.toolTip = "<p>Stops the solver.</p>"
   this.stopToolButton.visible = false;
   this.stopToolButton.onClick = () =>
   {
      engine.stopRequested = true;
      this.stopTimer.start();
   };

   // -------------------------------
   // TOOLBAR - DELETE ITERATION
   // -------------------------------
   this.sessionEditSeparator = new Label( this.toolBar );
   this.sessionEditSeparator.text = " | "
   this.sessionEditSeparator.textAlignment = TextAlign_Center;

   this.deleteIterationToolButton = new ToolButton( this.toolBar );
   this.deleteIterationToolButton.icon = this.scaledResource( ":/icons/clock-delete.png" );
   this.deleteIterationToolButton.setScaledFixedSize( 24, 24 );
   this.deleteIterationToolButton.focusStyle = FocusStyle_NoFocus;
   this.deleteIterationToolButton.toolTip = "<p>Remove the selected iteration.</p>"
   this.deleteIterationToolButton.onClick = () =>
   {
      if ( this.highlightedIterationIndex == -1 )
         return;

      engine.params.iterations.deleteIteration( this.highlightedIterationIndex );
      if ( this.highlightedIterationIndex > engine.params.iterations.length - 1 )
         this.highlightedIterationIndex = engine.params.iterations.length - 1;

      this.updateDialog();
      this.plotIterations();
   }

   this.deleteIterationToolButton.onEnter = () =>
   {
      if ( engine.executing )
         return;
      this.markedRows = [ this.highlightedIterationIndex ];
      this.refreshIterationTable();
   }

   this.deleteIterationToolButton.onLeave = () =>
   {
      this.markedRows = [];
      this.refreshIterationTable();
   }

   // -------------------------------
   // TOOLBAR - SQUEEZE ITERATIONS
   // -------------------------------
   this.squeezeIterationToolButton = new ToolButton( this.toolBar );
   this.squeezeIterationToolButton.icon = this.scaledResource( ":/icons/clock-warn.png" );
   this.squeezeIterationToolButton.setScaledFixedSize( 24, 24 );
   this.squeezeIterationToolButton.focusStyle = FocusStyle_NoFocus;
   this.squeezeIterationToolButton.toolTip = "<p>Squeeze the sessions by removing all intermediate converging iterations.</p>"
   this.squeezeIterationToolButton.onClick = () =>
   {
      engine.params.iterations.squeezeSessionContaining( this.highlightedIterationIndex );
      this.delesectAllNodes();
      this.updateDialog();
      this.plotIterations();
   }

   this.squeezeIterationToolButton.onEnter = () =>
   {
      if ( engine.executing )
         return;
      if ( this.highlightedIterationIndex >= 0 )
      {
         let id = engine.params.iterations[ this.highlightedIterationIndex ].sessionID;
         this.markedRows = engine.params.iterations.filter( it => ( it.sessionID == id && it.type == ITERATION_TYPE.DESCEND ) ).map( it => it.index );
         this.refreshIterationTable();
      }
   }

   this.squeezeIterationToolButton.onLeave = () =>
   {
      this.markedRows = [];
      this.refreshIterationTable();
   }

   // -------------------------------
   // TOOLBAR - KEEP ONLY CONVERGED
   // -------------------------------
   this.keepOnlyConvergedToolButton = new ToolButton( this.toolBar );
   this.keepOnlyConvergedToolButton.icon = this.scaledResource( ":/icons/clock-ok.png" );
   this.keepOnlyConvergedToolButton.setScaledFixedSize( 24, 24 );
   this.keepOnlyConvergedToolButton.focusStyle = FocusStyle_NoFocus;
   this.keepOnlyConvergedToolButton.toolTip = "<p>Keeps only converged iterations.</p>"
   this.keepOnlyConvergedToolButton.onClick = () =>
   {
      engine.params.iterations.KeepResultsOfSessionContaining( this.highlightedIterationIndex );
      this.delesectAllNodes();
      this.updateDialog();
      this.plotIterations();
   }

   this.keepOnlyConvergedToolButton.onEnter = () =>
   {
      if ( engine.executing )
         return;
      if ( this.highlightedIterationIndex >= 0 )
      {
         let id = engine.params.iterations[ this.highlightedIterationIndex ].sessionID;
         this.markedRows = engine.params.iterations.filter( it => ( it.sessionID == id && ( it.type != ITERATION_TYPE.CONVERGED && it.type != ITERATION_TYPE.SPEED_REDUCTIONS_STOP ) ) ).map( it => it.index );
         this.refreshIterationTable();
      }
   }

   this.keepOnlyConvergedToolButton.onLeave = () =>
   {
      this.markedRows = [];
      this.refreshIterationTable();
   }

   // -------------------------------
   // TOOLBAR - PROMOTE AN ITERATION
   // -------------------------------
   this.promoteToConvergedToolButton = new ToolButton( this.toolBar );
   this.promoteToConvergedToolButton.icon = this.scaledResource( ":/icons/clock-add.png" );
   this.promoteToConvergedToolButton.setScaledFixedSize( 24, 24 );
   this.promoteToConvergedToolButton.focusStyle = FocusStyle_NoFocus;
   this.promoteToConvergedToolButton.toolTip = "<p>Mark the active iteration as converged.</p>"
   this.promoteToConvergedToolButton.onClick = () =>
   {
      engine.params.iterations.promote( this.highlightedIterationIndex );
      this.delesectAllNodes();
      this.updateDialog();
      this.plotIterations();
   }

   this.promoteToConvergedToolButton.onEnter = () =>
   {
      if ( engine.executing )
         return;
      if ( this.highlightedIterationIndex >= 0 )
      {
         this.hintPromotedRow = this.highlightedIterationIndex;
         this.refreshIterationTable();
      }
   }

   this.promoteToConvergedToolButton.onLeave = () =>
   {
      this.hintPromotedRow = -1;
      this.refreshIterationTable();
   }

   this.hintPromotedRow

   // UPDATE TOOLBAR
   this.refreshToolBar = () =>
   {
      // create the status flags
      let selectedSession = this.sessionIsSelected();

      // configure the buttons
      this.setStatusToolButton.enabled = selectedSession;

      // enable the session edit buttons only if a session is selected
      this.setStatusToolButton.enabled = selectedSession;
      this.squeezeIterationToolButton.enabled = selectedSession;
      this.deleteIterationToolButton.enabled = selectedSession;
      this.keepOnlyConvergedToolButton.enabled = selectedSession;
      this.promoteToConvergedToolButton.enabled = selectedSession;

      this.plotGain.checked = engine.params.plotGain;
      this.plotMeasure.checked = engine.params.plotMeasure;

      // disable all tool buttons when the solver is running
      this.setStatusToolButton.enabled = this.setStatusToolButton.enabled && !engine.executing;
      this.saveToolButton.enabled = !engine.executing;
      this.loadToolButton.enabled = !engine.executing;
      this.undoToolButton.enabled = !engine.executing;
      this.restartToolButton.enabled = !engine.executing;
      this.stepToolButton.enabled = !engine.executing;

      this.deleteIterationToolButton.enabled = this.deleteIterationToolButton.enabled && !engine.executing;
      this.squeezeIterationToolButton.enabled = this.squeezeIterationToolButton.enabled && !engine.executing;
      this.keepOnlyConvergedToolButton.enabled = this.keepOnlyConvergedToolButton.enabled && !engine.executing;
      this.promoteToConvergedToolButton.enabled = this.promoteToConvergedToolButton.enabled && !engine.executing;

      this.saveCSV.enabled = !engine.executing;
      this.savePlot.enabled = !engine.executing;

      // when executing hide the start button and show the stop button
      this.startToolButton.visible = !engine.executing;
      this.stopToolButton.visible = engine.executing;
   }

   // -------------------------------
   // TOOLBAR - STATUS LABEL
   // -------------------------------
   this.statusLabel = new Label( this.toolBar );
   this.statusLabel.useRichText = true;
   this.statusLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.statusLabel.setVariableWidth();
   this.statusLabel.setVariableHeight();
   this.statusLabel.visible = false;

   this.setStatusLabel = ( txt ) =>
   {
      this.statusLabel.text = txt;
      this.statusLabel.visible = txt.length > 0;
   }
   this.setStatusLabel( "" );

   this.statusLabelSizer = new HorizontalSizer;
   this.statusLabelSizer.addStretch();
   this.statusLabelSizer.add( this.statusLabel );
   this.statusLabelSizer.addStretch();

   // -------------------------------
   // TOOLBAR - plot Gain / Measure
   // -------------------------------
   this.plotsOptionsSeparator = new Label( this.toolBar );
   this.plotsOptionsSeparator.text = " | "
   this.plotsOptionsSeparator.textAlignment = TextAlign_Center;

   this.plotGain = new CheckBox( this.toolBar );
   this.plotGain.text = "Gain";
   this.plotGain.onClick = () =>
   {
      engine.params.plotGain = this.plotGain.checked;
      this.plotIterations();
   }

   this.plotMeasure = new CheckBox( this.toolBar );
   this.plotMeasure.text = "Measure";
   this.plotMeasure.onClick = () =>
   {
      engine.params.plotMeasure = this.plotMeasure.checked;
      this.plotIterations();
   }

   // -------------------------------
   // TOOLBAR - PLOT ITERATIONS
   // -------------------------------
   this.plotAllToolButton = new ToolButton( this.toolBar );
   this.plotAllToolButton.icon = this.scaledResource( ":/icons/chart.png" );
   this.plotAllToolButton.setScaledFixedSize( 24, 24 );
   this.plotAllToolButton.focusStyle = FocusStyle_NoFocus;
   this.plotAllToolButton.toolTip = "<p>Plots the whole iterations progress.</p>"
   this.plotAllToolButton.onClick = () =>
   {
      if ( engine.params.iterations.length > 0 )
         this.plotIterations()
   };

   // -------------------------------
   // TOOLBAR - SAVE TABLE AND PLOT ITERATIONS
   // -------------------------------
   this.saveCSV = new ToolButton( this.toolBar );
   this.saveCSV.icon = this.scaledResource( ":/icons/document-csv.png" );
   this.saveCSV.setScaledFixedSize( 24, 24 );
   this.saveCSV.focusStyle = FocusStyle_NoFocus;
   this.saveCSV.toolTip = "<p>Exports the current table as a CSV document.</p>"
   this.saveCSV.onClick = () =>
   {

      let sfd = new SaveFileDialog;
      sfd.initialPath = engine.params.workingDir;
      sfd.caption = "Export CSV";
      sfd.filters = [
         [ "CSV files", ".csv" ]
      ];
      sfd.overwritePrompt = true;
      if ( sfd.execute() )
      {
         // export the table content
         let nCols = this.iterationsTreeBox.numberOfColumns;
         // header: manually split the first column between iteration and session
         let values = [ "#", "session" ];
         for ( let i = 1; i < nCols; i++ )
            values.push( this.iterationsTreeBox.headerText( i ) );
         let csv = values.join( ";" ) + "\n";
         let nRows = this.iterationsTreeBox.numberOfChildren;
         for ( let i = 0; i < nRows; i++ )
         {
            let node = this.iterationsTreeBox.child( i );
            let indexes = node.text( 0 )
            values = indexes.split( "|" );
            for ( let j = 1; j < nCols; j++ )
               values.push( node.text( j ) );
            csv += values.join( ";" ) + "\n";
         }

         File.writeTextFile( sfd.fileName, csv );
      }
   };

   this.savePlot = new ToolButton( this.toolBar );
   this.savePlot.icon = this.scaledResource( ":/icons/picture-export.png" );
   this.savePlot.setScaledFixedSize( 24, 24 );
   this.savePlot.focusStyle = FocusStyle_NoFocus;
   this.savePlot.toolTip = "<p>Saves the current plot.</p>"
   this.savePlot.onClick = () =>
   {

      let sfd = new SaveFileDialog;
      sfd.initialPath = engine.params.workingDir;
      sfd.caption = "Save Plot";
      sfd.filters = [
         [ "BMP files", ".bmp" ]
      ];
      sfd.overwritePrompt = true;
      if ( sfd.execute() )
         this.plotArea.saveAs( sfd.fileName + ".bmp" )
   };

   // LAYOUT TOOLBAR
   this.toolBar.sizer.add( this.setStatusToolButton );
   this.toolBar.sizer.add( this.saveToolButton );
   this.toolBar.sizer.add( this.loadToolButton );
   this.toolBar.sizer.add( this.loadSaveSeparator );
   this.toolBar.sizer.add( this.undoToolButton );
   this.toolBar.sizer.add( this.stepToolButton );
   this.toolBar.sizer.add( this.startToolButton );
   this.toolBar.sizer.add( this.stopToolButton );

   this.toolBar.sizer.add( this.sessionEditSeparator );
   this.toolBar.sizer.add( this.restartToolButton );
   this.toolBar.sizer.add( this.deleteIterationToolButton );
   this.toolBar.sizer.add( this.squeezeIterationToolButton );
   this.toolBar.sizer.add( this.keepOnlyConvergedToolButton );
   this.toolBar.sizer.add( this.promoteToConvergedToolButton );

   this.toolBar.sizer.addStretch();
   this.toolBar.sizer.add( this.plotGain );
   this.toolBar.sizer.add( this.plotMeasure );
   this.toolBar.sizer.add( this.plotAllToolButton );
   this.toolBar.sizer.add( this.plotsOptionsSeparator );
   this.toolBar.sizer.add( this.savePlot );
   this.toolBar.sizer.add( this.saveCSV );

   this.toolBarAndStatusSizer = new VerticalSizer;
   this.toolBarAndStatusSizer.spacing = WO_DEFAULT_SPACING;
   this.toolBarAndStatusSizer.add( this.toolBar );
   this.toolBarAndStatusSizer.add( this.statusLabelSizer );

   // -------------------------------
   // ITERATIONS AND REPORTS - PLOT AREA
   // -------------------------------
   this.plotArea = new PreviewControl( iterationsAndReportGroupBox );
   this.plotArea.setVariableWidth();

   // -------------------------------
   // ITERATIONS AND REPORTS - CONTRAST SLIDER
   // -------------------------------
   this.contrastSlider = new VerticalSlider( iterationsAndReportGroupBox );
   this.contrastSlider.setFixedWidth( 10 );
   this.contrastSlider.minValue = 1;
   this.contrastSlider.maxValue = 100;
   this.contrastSlider.stepSize = 1;
   this.contrastSlider.value = 50;

   // implement debouncing
   this.contrastSliderTimer = new Timer;
   this.contrastSliderTimer.periodic = false;
   this.contrastSliderTimer.interval = 0.1;
   this.contrastSliderTimer.dialog = this;
   this.contrastSliderTimer.onTimeout = () =>
   {
      this.plotIterations();
   }
   this.contrastSlider.onValueUpdated = () =>
   {
      plotLib.iterationsContrastValue = 1 - this.contrastSlider.normalizedValue;
      this.contrastSliderTimer.start();
   }

   this.plotAreaSizer = new HorizontalSizer;
   this.plotAreaSizer.spacing = WO_DEFAULT_SPACING;
   this.plotAreaSizer.add( this.plotArea );
   this.plotAreaSizer.add( this.contrastSlider );

   // LAYOUT

   iterationsAndReportGroupBox.sizer.add( this.iterationsAndSolverSizer );
   iterationsAndReportGroupBox.sizer.add( this.toolBarAndStatusSizer );
   iterationsAndReportGroupBox.sizer.add( this.plotAreaSizer );

   //------------------------------------------
   // BOTTOM BAR
   //------------------------------------------
   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.toolTip = "Drag & Drop to desktop to create a Process Icon";
   this.newInstanceButton.onMousePress = () =>
   {

      engine.params.export();
      this.newInstance();
   };

   this.browseDocumentationButton = new ToolButton( this );
   this.browseDocumentationButton.icon = this.scaledResource( ":/process-interface/browse-documentation.png" );
   this.browseDocumentationButton.setScaledFixedSize( 24, 24 );
   this.browseDocumentationButton.toolTip =
      "<p>Opens a browser to view the script's documentation.</p>";
   this.browseDocumentationButton.onClick = () =>
   {
      Dialog.browseScriptDocumentation( "WeightsOptimizer" );
      return;
   };

   this.resetButton = new ToolButton( this );
   this.resetButton.icon = this.scaledResource( ":/process-interface/reset.png" );
   this.resetButton.setScaledFixedSize( 24, 24 );
   this.resetButton.toolTip =
      "<p>Resets the dialog's parameters.</p>" +
      "<p>Saved settings are also cleared.</p>";
   this.resetButton.onClick = () =>
   {
      engine.reset();
      this.plotArea.clear();
      this.updateDialog();
   };

   let bottomBarSizer = new HorizontalSizer;
   bottomBarSizer.spacing = WO_DEFAULT_SPACING;
   bottomBarSizer.add( this.newInstanceButton );
   bottomBarSizer.add( this.browseDocumentationButton );
   bottomBarSizer.add( this.resetButton );
   bottomBarSizer.addStretch();

   // -------------------------------------------
   // EXECUTION CALLBACKS
   // -------------------------------------------
   this.solverExecutionProcessEvents = () =>
   {
      let elapsedTimeFormatted = ( secs ) =>
      {
         secs = Math.round( secs );
         var hours = Math.floor( secs / ( 60 * 60 ) );

         var divisor_for_minutes = secs % ( 60 * 60 );
         var minutes = Math.floor( divisor_for_minutes / 60 );

         var divisor_for_seconds = divisor_for_minutes % 60;
         var seconds = Math.ceil( divisor_for_seconds );

         return ( hours > 0 ? hours + "h " : "" ) + ( minutes > 0 ? minutes + "m " : ( hours > 0 ? "0m " : "" ) ) + ( seconds > 0 ? seconds + "s " : ( hours > 0 || minutes > 0 ? "0s " : "" ) )
      };

      let msg = ( message ) =>
      {
         return "<b>Iteration #" + engine.currentIteration + ":</b> " + message;
      };

      if ( !engine.stopRequested )
      {
         switch ( engine.params.status.value )
         {
            case OPTIMIZATION_SATES.INIT:
               if ( engine.isCropping )
               {
                  this.setStatusLabel( "<b>Initializing the solver, cropping " + engine.isCroppingProgress + "</b>" );
               }
               else
               {
                  this.setStatusLabel( "<b>Initializing the solver...</b>" );
               }
               break;
            case OPTIMIZATION_SATES.GRADIENT:
               let remainingTime = elapsedTimeFormatted( ( engine.gradientEndTime - Date.now() ) / 1000 );
               let eta = ( engine.gradientEndTime && ( remainingTime.length > 0 ) ) ? " | remaining " + elapsedTimeFormatted( ( engine.gradientEndTime - Date.now() ) / 1000 ) : "";
               this.setStatusLabel( msg( "Computing the gradient [ " + ( engine.params.status.progress + 1 ) + " / " + engine.params.status.tot + " ]" + eta ) );
               break;
            case OPTIMIZATION_SATES.REDUCING_STEP:
               this.setStatusLabel( msg( "Reducing the learning rate to 1/" + Math.pow( 2, engine.params.status.speedReductionsCount ) ) );
               break;
            case OPTIMIZATION_SATES.ITERATION_COMPLETED:
               this.setStatusLabel( msg( "Iteration completed" ) );
               break;
            case OPTIMIZATION_SATES.WRITING_WEIGHTS:
               this.setStatusLabel( "<b>writing weights in file " + engine.params.status.progress + "/" + engine.params.status.tot + "</b>" );
         }
      }

      processEvents();
   };

   this.solverExecutionIterationCompleted = () =>
   {
      this.delesectAllNodes();
      this.updateDialog();
      this.plotIterations();
   };

   this.solverExecutionTerminated = () =>
   {
      // restore the play button and clear the progression label
      this.undoToolButton.enabled = true;
      this.restartToolButton.enabled = true;
      this.stepToolButton.enabled = true;
      this.startToolButton.visible = true;
      this.stopToolButton.visible = false;

      this.setStatusLabel( "" );
      this.enabled = true;

      this.startToolButton.visible = true;
      this.stopToolButton.visible = false;
      this.filesManagementGroupBox.enabled = true;
      this.configurationGroupBox.enabled = true;
      this.optimizerConfigurationGroupBox.enabled = true;
      this.stateManagementParentGroupBox.enabled = true;
      this.updaterTimer.stop()
      engine.stopRequested = false;
      this.updateDialogTimer.start();
   };

   // ------------------------------------------------------------------------------------------
   //                                    TERMINATE THE ENGINE ON HIDE
   // ------------------------------------------------------------------------------------------
   this.onHide = () =>
   {
      engine.stopRequested = true;
   };

   // ------------------------------------------------------------------------------------------
   //                                    CONSTRUCT THE MAIN VIEW
   // ------------------------------------------------------------------------------------------

   // MID SIZER
   let midSizer = new VerticalSizer;
   with( midSizer )
   {
      spacing = WO_DEFAULT_SPACING;
      add( this.stateManagementParentGroupBox );
   }
   // LEFT + MID SIZER
   let leftMidSizer = new HorizontalSizer;
   with( leftMidSizer )
   {
      spacing = WO_DEFAULT_SPACING;
      add( leftContainerControl )
      add( midSizer )
   }

   // TITLE + LEFT AND MID SIZER
   midLeftParent.sizer = new VerticalSizer;
   with( midLeftParent.sizer )
   {
      spacing = WO_DEFAULT_SPACING / 2;
      add( leftMidSizer );
      add( titlePane );
   }

   // MAIN CONTENT
   let mainSizer = new HorizontalSizer;
   with( mainSizer )
   {
      spacing = WO_DEFAULT_SPACING;
      add( midLeftParent );
      add( iterationsAndReportGroupBox );
   };

   this.sizer = new VerticalSizer;
   with( this.sizer )
   {
      margin = WO_DEFAULT_MARGIN;
      spacing = WO_DEFAULT_SPACING;
      add( mainSizer );
      add( bottomBarSizer )
   };

   this.updateDialog = () =>
   {
      this.refreshInputFileList();
      this.updateFilesManagerControls();
      this.updateGlobalConfiguration();
      this.updateOptimizerConfiguration();
      this.updateStateManagement();
      this.refreshIterationTable();
      this.refreshToolBar();
      this.updateIterationNotes();
      this.redrawStatus();
   };

   // dynamic window sizing
   this.updateDialogTimer = new Timer( 0 /*interval*/ , false /*periodic*/ );
   this.updateDialogTimer.onTimeout = () =>
   {
      this.updateDialog();
   };

   this.updateDialog();
   // this.adjustToContents();
   this.plotIterations();
}

WeightsOptimizerDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer-GUI.js - Released 2022-03-04T15:42:10Z
