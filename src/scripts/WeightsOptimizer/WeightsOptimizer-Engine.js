// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer-Engine.js - Released 2022-03-04T15:42:10Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.0.0
//
// Copyright (c) 2022 Roberto Sartori
// Copyright (c) 2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include "WeightsOptimizer-Helpers.js"
/* beautify ignore:end */

/*
 * Iterations object.
 * This function extends the interations array with a set of convenient helpers.
 */
function WOIterations()
{
   this.qualitativePalette = [
      "0x001F77B4",
      "0x002CA02C",
      "0x00D62728",
      "0x009467BD",
      "0x008C564B",
      "0x00E377C2",
      "0x00BCBD22",
      "0x0017BECF",
      "0x00FF7F0E"
   ];

   this.qualitativePaletteInt = [
      0x1F77B4,
      0x2CA02C,
      0xD62728,
      0x9467BD,
      0x8C564B,
      0xE377C2,
      0xBCBD22,
      0x17BECF,
      0xFF7F0E
   ];

   /**
    * Reassign the colors to the converged iterations
    */
   this.updatePaletteColors = () =>
   {
      let count = 0;
      for ( let i = 0; i < this.length; i++ )
      {
         let converged = this[ i ].type == ITERATION_TYPE.CONVERGED || this[ i ].type == ITERATION_TYPE.SPEED_REDUCTIONS_STOP;
         let colorInt;
         let colorHex;
         if ( converged )
         {
            colorHex = this.qualitativePalette[ count ];
            colorInt = this.qualitativePaletteInt[ count ];
            count = ( count + 1 ) % this.qualitativePalette.length;
         }
         this[ i ].colorHex = colorHex;
         this[ i ].colorInt = colorInt;
      }
   };

   /**
    * Defines the JSON representation of the iterations object.
    *
    * @return {*}
    */
   this.toJSON = () =>
   {
      // avoid using map and filter
      let result = [];
      for ( let i = 0; i < this.length; i++ )
      {
         if ( this[ i ] != null )
         {
            result.push( this[ i ] );
         }
      }
      return result;
   };

   /**
    * Returns the last element or undefined if the array is empty.
    *
    * @return {*}
    */
   this.lastSessionID = () =>
   {
      return this.length == 0 ? 0 : ( this[ this.length - 1 ].sessionID || 0 );
   };

   /**
    * Adds the iteration object provided.
    *
    * @param {*} newItem
    */
   this.add = ( newItem ) =>
   {
      // session ID is computed automatically, we keep
      // - zero for the very first iteration
      // - the same session ID for all iterations that are not INIT
      // - increment the session ID if the iteration is INIT
      let sessionID = this.lastSessionID();
      if ( this.length > 0 && newItem.type == ITERATION_TYPE.INIT )
      {
         sessionID += 1;
      };

      // define a set of default values
      let item = {
         type: ITERATION_TYPE.INIT,
         i: this.length,
         cost: 0,
         mvp: 0,
         measure: 0,
         state: OPTIMIZATION_SATES.INIT,
         convergenceReached: false,
         maxSpeedReductionReached: false,
         note: "",
         sessionID: sessionID,
         enabled: true
      };
      // override defaults with the given object values
      Object.keys( newItem ).forEach( key =>
      {
         if ( newItem[ key ] != undefined )
            item[ key ] = newItem[ key ];
         else
            console.warningln( "undefined iteration value for key ", key );
      } );
      // adds the new iteration
      this.push( item );

      // regenerate the index
      this.generateIndex();

      // update the palette colors
      this.updatePaletteColors();

      // recompute relative values
      this.recalculate();
   };

   /**
    * Since iterations could be deleted or reordered then the Gain field gets recalculated
    * to reflect the gain from the previous iteration.
    */
   this.recalculate = () =>
   {
      let curSession = -1;
      this.forEach( ( it, i ) =>
      {
         if ( i == 0 )
            it.gain = 0;
         else if ( it.sessionID == curSession )
            it.gain = ( it.cost - this[ i - 1 ].cost ) / this[ i - 1 ].cost;
         curSession = it.sessionID;
      } );
   };

   /**
    * Validates and adds the iterations provided by the input array.
    * Each item is validated before being added, this would make the parsing more safe
    * in case of corrupted data.
    *
    * @param {*} array list of iterations.
    * @return {*}
    */
   this.fromArray = ( array ) =>
   {
      if ( array == undefined )
         return false;

      for ( let indx in array )
      {
         if ( this.isValid( array[ indx ] ) )
            this.add( array[ indx ] );
      }
      return true;
   };

   /**
    * Enable/Disable the iteration at the given index
    *
    * @param {*} index
    * @param {*} enabled
    */
   this.setEnable = ( index, enabled ) =>
   {
      if ( this[ index ] )
      {
         this[ index ].enabled = enabled;
         // recompute the index
         this.generateIndex();
      }
   };

   /**
    * Mark the iteration at the given index as promoted
    *
    * @param {*} index
    */
   this.promote = ( index ) =>
   {
      if ( this[ index ] )
         if ( this[ index ].type == ITERATION_TYPE.DESCEND || this[ index ].type == ITERATION_TYPE.INIT )
         {
            this[ index ].type = ITERATION_TYPE.CONVERGED;
            this.updatePaletteColors();
         }
   };

   /**
    * Generate the index property for the iterations. The index is a progressive number
    * that enumerates only the enabled iterations.
    *
    */
   this.generateIndex = () =>
   {
      let index = 0;
      for ( let i = 0; i < this.length; i++ )
         if ( this[ i ].enabled )
         {
            this[ i ].index = index;
            index += 1;
         }
      else
         this[ i ].index = undefined;
   };

   /**
    * Delete a session containing the given iteration Index.
    *
    * @param {*} iterationIndex
    */
   this.deleteSessionContaining = ( iterationIndex ) =>
   {
      if ( iterationIndex == -1 )
         return;

      let id = this[ iterationIndex ].sessionID;
      for ( let i = this.length - 1; i >= 0; i-- )
         if ( this[ i ].sessionID == id )
            this.splice( i, 1 );

      // regenerate the index
      this.generateIndex();
   };

   /**
    * Deletes the iteration at the given index
    *
    * @param {*} iterationIndex
    * @return {*}
    */
   this.deleteIteration = ( iterationIndex ) =>
   {
      if ( iterationIndex == -1 || iterationIndex >= this.length )
         return;

      this.splice( iterationIndex, 1 );

      // regenerate the index
      this.generateIndex();
   };

   /**
    * Squeezes a session containing the given iteration index by removing all converging steps
    * in the session.
    *
    * @param {*} iterationIndex
    */
   this.squeezeSessionContaining = ( iterationIndex ) =>
   {
      if ( iterationIndex == -1 )
         return;

      let id = this[ iterationIndex ].sessionID;
      for ( let i = this.length - 1; i >= 0; i-- )
         if ( this[ i ].sessionID == id && this[ i ].type == ITERATION_TYPE.DESCEND )
            this.splice( i, 1 );

      // regenerate the index
      this.generateIndex();
   };

   /**
    * Removes all iterations from a session containing the given iteration except the converget ones
    *
    * @param {*} iterationIndex
    */
   this.KeepResultsOfSessionContaining = ( iterationIndex ) =>
   {
      if ( iterationIndex == -1 )
         return;

      let id = this[ iterationIndex ].sessionID;
      for ( let i = this.length - 1; i >= 0; i-- )
         if ( this[ i ].sessionID == id && ( this[ i ].type != ITERATION_TYPE.CONVERGED && this[ i ].type != ITERATION_TYPE.SPEED_REDUCTIONS_STOP ) )
            this.splice( i, 1 );

      // regenerate the index
      this.generateIndex();
   };

   /**
    * Accepts an iteration object and returns true if the object is a valid iteration descriptor.
    *
    * @param {*} item the item to be validated as an iteration descriptor
    * @return {*}
    */
   this.isValid = ( item ) =>
   {
      // -- TYPE
      // type must be assigned
      if ( item.type == undefined )
      {
         console.warningln( "validation failed: type == undefined" );
         return false;
      }

      // type must have a valid value
      let validTypeValues = [
         ITERATION_TYPE.INIT,
         ITERATION_TYPE.CONVERGED,
         ITERATION_TYPE.DESCEND,
         ITERATION_TYPE.SPEED_REDUCTIONS_STOP
      ];
      if ( validTypeValues.indexOf( item.type ) == -1 )
      {
         console.warningln( "validation failed: type value not recognized: ", item.type );
         return false;
      }

      // -- I
      // the current iteration must be positive if provided
      if ( item.i == undefined || item.i < 0 )
      {
         console.warningln( "validation failed: negative index value" );
         return false;
      }

      // -- COST, MVP, MEASURE
      // must be provided and be numeric
      let numericLabels = [ "cost", "mvp", "measure" ];
      // cost must be provided and be numeric
      for ( let i = 0; i < numericLabels.length; i++ )
      {
         if ( item[ numericLabels[ i ] ] === undefined )
         {
            console.warningln( "validation failed: undefined numeric value for key '", numericLabels[ i ], "' found at index ", i );
            console.warningln( JSON.stringify( item, null, 2 ) );
            return false;
         }
         if ( typeof item[ numericLabels[ i ] ] != typeof 0.1 )
         {
            console.warningln( "validation failed: invalid numeric value for key '", numericLabels[ i ], "' found at index ", i, ": ", item[ numericLabels[ i ] ] );
            return false;
         }
      }

      // -- STATE
      // state must be provieded and have a valid value
      if ( item.state == undefined )
      {
         console.warningln( "validation failed: state not defined" );
         return false;
      }

      // --BOOLEANS
      let booleans = [ item.convergenceReached, item.maxSpeedReductionReached ];
      // must be provided and be booleans
      for ( let i = 0; i < booleans.length; i++ )
      {
         if ( booleans[ i ] == undefined )
         {
            console.warningln( "validation failed: undefined bool value at index ", i );
            return false;
         }
         if ( typeof booleans[ i ] != typeof true )
         {
            console.warningln( "validation failed: invalid boolean value found at index ", i, ": ", booleans[ i ] );
            return false;
         }
      }

      // -- NOTES
      // optional, if provided must be string
      if ( item.notes != undefined && ( typeof item.notes != typeof "" ) )
      {
         console.warningln( "validation failed: invalid notes type:", typeof item.notes );
         return false;
      }

      // all vlidation passed
      return true;
   };

   /**
    * Remove all iterations.
    *
    */
   this.clear = () =>
   {
      this.length = 0;
   };

   /**
    * Returns the list of enabled iterations belonging to the session that starts with the iterationIndex.
    * If none are found then all iterations will be returned.
    *
    * @param
    {
      *
    }
    iterationIndx
    */
   this.getEnabledSession = ( iterationIndx ) =>
   {
      let findIndex = function( iterations, iteration )
      {
         for ( let i = 0; i < iterations.length; i++ )
            if ( iterations[ i ].i == iteration.i )
               return i;
         return -1;
      };

      let iterations;

      // if no interaction is selected or no specific session is selected than return all enabled iterations
      if ( iterationIndx == -1 /*|| ( iterationIndx >= 0 && this[ iterationIndx ].type != ITERATION_TYPE.INIT ) */ )
         iterations = this.filter( it => it.enabled );
      else
         iterations = this.filter( it => it.enabled && it.sessionID == this[ iterationIndx ].sessionID );

      return {
         iterations: iterations,
         iterationIndex: ( iterationIndx == -1 ) ? undefined : findIndex( iterations, this[ iterationIndx ] )
      };
   };
}

WOIterations.prototype = new Array;

function WOEngineParametersPrototype()
{
   // -------------------------------------
   //     Engine parameters management
   // -------------------------------------

   /**
    * Resets the optimization parameters to the defaults.
    *
    */
   this.optimizerReset = () =>
   {
      this.delta = DELTA;
      this.speed = SPEED;
      this.maxSpeedReductions = MAX_SPEED_REDUCTIONS;
      this.gradTolerance = GRAD_TOLERANCE;
      this.minWeight = MIN_WEIGHT;
      this.convergence = COST_PERC_CONVERGENCE;
      this.maxIterations = MAX_ITERATIONS;
   };

   /**
    * Resets the state to 1.
    *
    */
   this.resetState = () =>
   {
      this.state = this.state.map( () => 1 );
   };

   /**
    * Generates a random state assigning uniform random values in [0,1] range.
    *
    */
   this.randomState = () =>
   {
      this.state = this.state.map( () => Math.random() );
   };

   /**
    * Generates a random state adding an uniform random value  in the range [-delta/2, delta/2] to each
    * current state variables.
    *
    * @param {*} delta
    */
   this.deltaState = ( delta ) =>
   {
      let newState = this.state.map( ( value ) => value * ( 1 + ( 2 * Math.random() * delta - delta ) ) );
      let M = newState.reduce( ( acc, val ) => Math.max( acc, val ), newState[ 0 ] );
      this.state = newState.map( s => s / M );
   };

   /**
    * Parameters initialization routine.
    *
    */
   this.initialize = () =>
   {
      this.inputFiles = [];
      this.state = [];
      this.iterations = new WOIterations();
      this.status = {
         value: OPTIMIZATION_SATES.STOP
      };
      this.imageIntegrationIconName = "";
      this.version = VERSION;
      this.workingDir = File.systemTempDirectory;
      this.optMetric = 0; // first optimization parameter in the list
      this.useROI = () => ( this.roiW != 0 && this.roiH != 0 );
      this.plotGain = false;
      this.plotMeasure = true;
      this.customFormula = "PSFSignal";
      this.customFormulaMinMax = "max";
      this.roiX0 = 0;
      this.roiY0 = 0;
      this.roiW = 0;
      this.roiH = 0;
      this.optKeyword = FIT_WEIGHT_KEY;

      this.optimizerReset();
   };

   /**
    * Returns true if a custom formula is used as optimization objective
    *
    */
   this.usingOptimizationFormula = () =>
   {
      return ( OPTIMIZATION_METRICS[ this.optMetric ].label == WO_CUSTOM_FORMULA_LBL );
   };

   /**
    * Parameters import
    *
    */
   this.import = () =>
   {
      let GET = ( typeOf, loader ) =>
      {
         WO_PERSISTED_PRAMS.filter( p => p.type === typeOf ).forEach( p =>
         {
            if ( Parameters.has( p.name ) )
               this[ p.name ] = Parameters[ loader ]( p.name );
         } );
      };

      let GET_JSON = ( key ) =>
      {
         // get the list of files
         if ( Parameters.has( key ) )
         {
            try
            {
               let JSONdata = ByteArray.fromBase64( Parameters.getString( key ) ).toString();
               let data = JSON.parse( JSONdata );
               this[ key ] = data;
            }
            catch ( e )
            {}
         }
      };

      let GET_JSON_OBJ = ( key ) =>
      {
         // get the list of files
         let data;
         if ( Parameters.has( key ) )
         {
            try
            {
               let JSONdata = ByteArray.fromBase64( Parameters.getString( key ) ).toString();
               data = JSON.parse( JSONdata );
            }
            catch ( e )
            {}
         }
         return data;
      };

      // get booleans
      GET( "boolean", "getBoolean" );
      // get integers
      GET( "integer", "getInteger" );
      // get floats
      GET( "float", "getReal" );
      // get strings
      GET( "string", "getString" );

      // get the list of files
      GET_JSON( "inputFiles" );
      GET_JSON( "state" );

      this.iterations.fromArray( GET_JSON_OBJ( "iterations" ) );
   };

   /**
    * Parameters export
    *
    */
   this.export = () =>
   {
      let SET = ( typeOf ) =>
      {
         WO_PERSISTED_PRAMS.filter( p => p.type == typeOf ).forEach( p =>
         {
            Parameters.set( p.name, this[ p.name ] );
         } );
      };
      let SET_JSON = ( key ) =>
      {
         Parameters.set( key, new ByteArray( JSON.stringify( this[ key ] ) ).toBase64() );
      };

      // get booleans
      SET( "boolean" );
      // get integers
      SET( "integer" );
      // get floats
      SET( "float" );
      // get strings
      SET( "string" );

      SET_JSON( "inputFiles" );
      SET_JSON( "state" );
      SET_JSON( "iterations" );
   };

   /**
    * Persist the parameters acroiss sessions
    *
    */
   this.save = () =>
   {
      function save( key, type, value )
      {
         try
         {
            Settings.write( "WO_" + key, type, value );
         }
         catch ( e )
         {
            console.noteln( "FAILED to save ", key, " of type ", type, " for value ", value );
         }
      }

      let SAVE = ( typeOf ) =>
      {
         WO_PERSISTED_PRAMS.filter( p => p.saveDataType == typeOf ).forEach( p =>
         {
            save( p.name, typeOf, this[ p.name ] )
         } );
      };

      let SAVE_JSON = ( key ) =>
      {
         save( key, DataType_String, JSON.stringify( this[ key ] ) );
      };

      // save booleans
      SAVE( DataType_Boolean );
      // save integers
      SAVE( DataType_Int32 );
      // save floats
      SAVE( DataType_Float );
      // save strings
      SAVE( DataType_String );

      SAVE_JSON( "inputFiles" );
      SAVE_JSON( "state" );
      SAVE_JSON( "iterations" );
   };

   /**
    * Load persisted parameters
    *
    */
   this.load = () =>
   {
      function load( key, type )
      {
         return Settings.read( "WO_" + key, type );
      }

      let o;

      let LOAD = ( typeOf ) =>
      {
         WO_PERSISTED_PRAMS.filter( p => p.saveDataType == typeOf ).forEach( p =>
         {
            if ( ( o = load( p.name, typeOf ) ) != null )
               this[ p.name ] = o;
         } )
      }

      let LOAD_JSON = ( key ) =>
      {
         if ( ( o = load( key, DataType_String ) ) != null )
         {
            let data = JSON.parse( o );
            if ( data )
               this[ key ] = data;
         }
      }

      let LOAD_JSON_OBJ = ( key ) =>
      {
         let data;
         if ( ( o = load( key, DataType_String ) ) != null )
            data = JSON.parse( o );
         return data;
      }

      // save booleans
      LOAD( DataType_Boolean );
      // save integers
      LOAD( DataType_Int32 );
      // save floats
      LOAD( DataType_Float );
      // save strings
      LOAD( DataType_String );

      LOAD_JSON( "inputFiles" );
      LOAD_JSON( "state" );
      this.iterations.fromArray( LOAD_JSON_OBJ( "iterations" ) );
   };

   this.importFromJSON = ( json ) =>
   {
      Object.keys( json ).forEach( key =>
      {
         if ( key == "iterations" )
            this.iterations.fromArray( json[ key ] );
         else
            this[ key ] = json[ key ];
      } );
   };
}

/**
 * This class tracks the temporary files to be removed at exit
 *
 */
function WOEngineCache()
{
   this.files = {};

   this.reset = () =>
   {
      this.files = {};
   };

   this.addTemporaryFilePath = ( filePath ) =>
   {
      // avoid duplicates
      if ( this.files[ filePath ] )
         return;
      this.files[ filePath ] = true;
   };

   this.deleteTemporaryFiles = () =>
   {
      /*  */
      Object.keys( this.files ).forEach( filePath =>
      {
         if ( File.exists( filePath ) )
            File.remove( filePath );
      } )
      this.reset();
   };
}

// The script's process prototype.
function WeightsOptimizerEngine()
{
   // the engine parameters
   this.params = new WOEngineParametersPrototype();
   this.params.initialize();

   // state
   this.executing = false;
   this.callbacks = {};

   // cache and temp files
   this.cache = new WOEngineCache();

   // -----------------------------------
   //              CONFIG
   // -----------------------------------
   this.reset = () =>
   {
      this.params.initialize();
   };

   // -----------------------------------
   //          LOG FUNCTIONS
   // -----------------------------------
   this.logString = "";

   this.writeln = ( ...args ) =>
   {
      console.writeln( this.log.apply( null, args ) );
   };

   this.noteln = ( ...args ) =>
   {
      console.noteln( this.log.apply( null, args ) );
   };

   this.saveLog = () =>
   {
      let logDate = new Date;
      let timestamp = format( "%04d%02d%02d%02d%02d%02d",
         logDate.getUTCFullYear(), logDate.getUTCMonth() + 1, logDate.getUTCDate(),
         logDate.getHours(), logDate.getUTCMinutes(), logDate.getUTCSeconds() );
      let fname = this.params.workingDir + "/weights_optimizer_" + timestamp + ".log";
      File.writeTextFile( fname, this.logString );
   };

   this.log = ( ...args ) =>
   {
      let joined = args.reduce( ( acc, item ) => ( acc + item ), "" );
      this.logString = this.logString + joined + "\n";
      return joined;
   };

   // -----------------------------------
   //            SAVE / LOAD
   // -----------------------------------

   /**
    * Reads the state from the files FITS header using the providede keyword
    *
    * @param {*} keyword
    */
   this.loadStateFromKeyword = ( keyword ) =>
   {
      let kup = keyword.toUpperCase();
      this.params.inputFiles.forEach( ( file, i ) =>
      {
         let keywords = LoadFITSKeywords( file );
         for ( let j = 0; j < keywords.length; j++ )
         {
            let k = keywords[ j ];
            if ( k.name.toUpperCase() == kup )
            {
               let value = parseFloat( k.value );
               if ( value != NaN )
               {
                  this.params.state[ i ] = value;
                  break;
               }
               else
                  console.warningln( "keyword ", k.name, " not a valid number: ", k.value, " in file ", File.extractNameAndExtension( file ) );
            }
         }
      } );
      this.params.state = this.normalize( this.params.state );
   };

   this.saveToFile = ( fname ) =>
   {
      let data = JSON.stringify( this.params, null, 2 );
      File.writeTextFile( fname, data );
   };

   this.loadFromFile = ( fname ) =>
   {
      let file = File.openFileForReading( fname );
      if ( !file.isOpen )
      {
         console.warningln( "unable to open ", fname );
         return;
      }

      let s = file.read( DataType_ByteArray, file.size );
      file.close();
      let str = s.toString();
      try
      {
         let json = JSON.parse( str );
         if ( json )
            // load persisted prams from JSON
            this.params.importFromJSON( json );
         else
            console.warningln( "failed to parse ", fname );
      }
      catch ( e )
      {
         console.warningln( "failed to parse ", fname );
      }
   };

   // -----------------------------------
   //          FILE ACCESS
   // -----------------------------------

   this.readImage = ( filePath ) =>
   {
      let ext = File.extractExtension( filePath );
      let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
      if ( F.isNull )
         throw new Error( "No installed file format can read \'" + ext + "\' files." ); // shouldn't happen

      let f = new FileFormatInstance( F );
      if ( f.isNull )
         throw new Error( "Unable to instantiate file format: " + F.name );

      let d = f.open( filePath );
      if ( d.length < 1 )
         throw new Error( "Unable to open file: " + filePath );
      if ( d.length > 1 )
         throw new Error( "Multi-image files are not supported by this script: " + filePath );

      let window = new ImageWindow( 1, 1, 1, /*numberOfChannels*/ 32, /*bitsPerSample*/ true /*floatSample*/ );

      let view = window.mainView;
      view.beginProcess( UndoFlag_NoSwapFile );

      if ( !f.readImage( view.image ) )
         throw new Error( "Unable to read file: " + filePath );

      if ( F.canStoreImageProperties )
         if ( F.supportsViewProperties )
         {
            let info = view.importProperties( f );
            if ( !info || length( info ) == 0 )
               console.criticalln( "<end><cbr>*** Error reading image properties:\n", info );
         }

      if ( F.canStoreKeywords )
         window.keywords = f.keywords;

      view.endProcess();

      f.close();

      return window;
   };

   this.addInputFiles = ( fileItems ) =>
   {
      let newWeights = fileItems.map( () => ( 1 ) );
      this.params.inputFiles = this.params.inputFiles.concat( fileItems );
      this.params.state = this.params.state.concat( newWeights );
   };

   this.addInputFile = ( inputFile ) =>
   {
      this.addInputFiles( [ inputFile ] );
   };

   this.clearInputFiles = () =>
   {
      this.params.inputFiles = [];
      this.params.state = [];
      this.clearIterations();
   };

   this.setImageIntegrationIconName = ( name ) =>
   {
      let ii = ProcessInstance.fromIcon( name );
      if ( ii instanceof ImageIntegration )
      {
         this.clearIterations();
         this.params.imageIntegrationIconName = name;

         this.addInputFiles( ii.images.map( ( image ) =>
         {
            return image[ 1 ];
         } ) );
      }
   };

   this.clearImageIntegrationIconName = () =>
   {
      this.params.imageIntegrationIconName = "";
      this.clearInputFiles();
      this.clearIterations();
   };

   this.setSignalROI = ( x, y, w, h ) =>
   {
      this.params.roiX0 = x;
      this.params.roiY0 = y;
      this.params.roiW = w;
      this.params.roiH = h;
   };

   this.cropImagesIfNeeded = () =>
   {

      let useIIInstance = this.params.imageIntegrationIconName != "";
      if ( !useIIInstance && this.params.useROI() )
      {
         this.noteln( "DO CROP IMAGES" );

         let LM = -1;
         let TM = -1;
         let RM = -1;
         let BM = -1;

         let P = new Crop;
         P.mode = Crop.prototype.AbsolutePixels;
         P.xResolution = 72.000;
         P.yResolution = 72.000;
         P.metric = false;
         P.forceResolution = false;
         P.red = 0.000000;
         P.green = 0.000000;
         P.blue = 0.000000;
         P.alpha = 1.000000;
         P.noGUIMessages = true;
         let newInputFiles = [];

         this.isCropping = true;
         for ( let i = 0; i < this.params.inputFiles.length; i++ )
         {
            // return if stop has been requested
            if ( this.stopRequested )
            {
               this.isCropping = false;
               return;
            }

            this.isCroppingProgress = "" + ( i + 1 ) + "/" + this.params.inputFiles.length;
            let filePath = this.params.inputFiles[ i ];

            let imageWindow = this.readImage( filePath );

            if ( LM == -1 )
            {
               let W = imageWindow.mainView.image.width;
               let H = imageWindow.mainView.image.height;
               LM = Math.min( W - 1, this.params.roiX0 );
               TM = Math.min( H - 1, this.params.roiY0 );
               RM = Math.max( 0, W - ( this.params.roiX0 + this.params.roiW ) );
               BM = Math.max( 0, H - ( this.params.roiY0 + this.params.roiH ) );
            }

            P.leftMargin = -LM;
            P.topMargin = -TM;
            P.rightMargin = -RM;
            P.bottomMargin = -BM;

            this.noteln( "cropping to: X0=", LM, " Y0=", TM, " width=", RM, " height=", BM );

            P.executeOn( imageWindow.mainView );

            this.noteln( "Cropped image size: ", imageWindow.mainView.image.width, ", ", imageWindow.mainView.image.height );

            // save the image as xisf and update the input files if needed
            let fname = File.extractName( filePath );
            fname = fname + "_crop";
            let newFilePath = this.params.workingDir + "/" + fname + ".xisf";
            if ( File.exists( newFilePath ) )
            {
               File.remove( newFilePath );
            }
            imageWindow.saveAs( newFilePath, false, false, false, false );
            imageWindow.forceClose();
            newInputFiles[ i ] = newFilePath;
            this.cache.addTemporaryFilePath( newFilePath );
         }
         this.params.workingFiles = newInputFiles.slice();
         this.isCropping = false;
      }
      else
      {
         this.params.workingFiles = this.params.inputFiles.slice();
      }
   };

   // -------------------------------------
   //         MANAGEMENT FUNCTIONS
   // -------------------------------------

   this.undoITeration = () =>
   {
      this.params.iterations.pop();
   };

   this.clearIterations = () =>
   {
      this.params.iterations.clear();
   };

   this.sanityCheck = () =>
   {
      while ( 1 )
      {
         // if an Image Integration process icon is loaded then the icon must be present otherwise we reset the state
         if ( this.params.imageIntegrationIconName == "" )
         {
            break;
         }

         let ii = ProcessInstance.fromIcon( this.params.imageIntegrationIconName );
         if ( ii == null || !( ii instanceof ImageIntegration ) )
         {
            console.noteln( "icon name not an ImageIntegration instance" )
            this.clearImageIntegrationIconName();
            break;
         }

         // all input files must match
         let iifiles = ii.images.map( img => img[ 1 ] ).join( " " );
         let engineFiles = this.params.inputFiles.join( " " );
         if ( iifiles != engineFiles )
         {
            console.noteln( "files are different:" )
            console.noteln( " " )
            console.noteln( "iifiles: ", iifiles )
            console.noteln( " " )
            console.noteln( "engineFiles: ", engineFiles )
            this.clearImageIntegrationIconName();
            break;
         }

         break;
      }
   };

   // -------------------------------------
   //            SOLVER FUNCTIONS
   // -------------------------------------

   // performs the integration
   this.integrate = ( fname, useROI, state ) =>
   {
      let usesIIInstance = this.params.imageIntegrationIconName != "";
      let P;
      if ( usesIIInstance )
         P = ProcessInstance.fromIcon( this.params.imageIntegrationIconName );
      else
         P = new ImageIntegration;

      if ( usesIIInstance )
      {
         // during optimization we use the Image Integration instance selected
         // by the way we avoid to perform some operations that
         // are meaningless for our optimization purposes in order to improve the overall
         // computation time
         P.weightMode = ImageIntegration.prototype.KeywordWeight;
         P.weightKeyword = FIT_WEIGHT_KEY;
         P.showImages = false;
         P.noGUIMessages = true;
         P.generateRejectionMaps = false;
         P.generateDrizzleData = false;
         P.closePreviousImages = false;
         P.reportRangeRejection = false
         P.evaluateNoise = false;

         // override the ROI if a custom one is defined
         if ( useROI )
         {
            P.useROI = true;
            P.roiX0 = this.params.roiX0;
            P.roiY0 = this.params.roiX0;
            P.roiX1 = this.params.roiX0 + this.params.roiW;
            P.roiY1 = this.params.roiY0 + this.params.roiH;
         }
      }
      else
      {
         P.images = this.params.workingFiles.map( filePath =>
            // enabled, path, drizzlePath, localNormalizationDataPath
            {
               return [ true, filePath, "", "" ]
            }
         );

         P.inputHints = "fits-keywords normalize raw cfa signed-is-physical";
         P.combination = ImageIntegration.prototype.Average;
         P.weightMode = ImageIntegration.prototype.KeywordWeight;
         P.weightKeyword = FIT_WEIGHT_KEY;
         P.weightScale = ImageIntegration.prototype.WeightScale_BWMV;
         P.adaptiveGridSize = 16;
         P.adaptiveNoScale = false;
         P.ignoreNoiseKeywords = false;
         P.normalization = ImageIntegration.prototype.NoNormalization;
         P.rejection = ImageIntegration.prototype.WinsorizedSigmaClip;
         P.rejectionNormalization = ImageIntegration.prototype.NoRejectionNormalization;
         P.minMaxLow = 1;
         P.minMaxHigh = 1;
         P.pcClipLow = 0.200;
         P.pcClipHigh = 0.100;
         P.sigmaLow = 4.000;
         P.sigmaHigh = 3.000;
         P.winsorizationCutoff = 5.000;
         P.linearFitLow = 5.000;
         P.linearFitHigh = 4.000;
         P.esdOutliersFraction = 0.30;
         P.esdAlpha = 0.05;
         P.esdLowRelaxation = 1.50;
         P.ccdGain = 1.00;
         P.ccdReadNoise = 10.00;
         P.ccdScaleNoise = 0.00;
         P.clipLow = true;
         P.clipHigh = true;
         P.rangeClipLow = true;
         P.rangeLow = 0.000000;
         P.rangeClipHigh = false;
         P.rangeHigh = 0.980000;
         P.mapRangeRejection = true;
         P.reportRangeRejection = false;
         P.largeScaleClipLow = false;
         P.largeScaleClipLowProtectedLayers = 2;
         P.largeScaleClipLowGrowth = 2;
         P.largeScaleClipHigh = false;
         P.largeScaleClipHighProtectedLayers = 2;
         P.largeScaleClipHighGrowth = 2;
         P.generate64BitResult = false;
         P.generateRejectionMaps = false;
         P.generateIntegratedImage = true;
         P.generateDrizzleData = false;
         P.closePreviousImages = false;
         P.bufferSizeMB = 16;
         P.stackSizeMB = 1024;
         P.autoMemorySize = true;
         P.autoMemoryLimit = 0.75;
         P.useCache = true;
         P.evaluateNoise = false;
         P.mrsMinDataFraction = 0.010;
         P.subtractPedestals = false;
         P.truncateOnOutOfRange = false;
         P.noGUIMessages = true;
         P.showImages = false;
         P.useFileThreads = true;
         P.fileThreadOverload = 1.00;
         P.useBufferThreads = true;
         P.maxBufferThreads = 0;
      }

      if ( state )
      {
         P.csvWeights = state.join( "," );
      }

      this.log( " " );
      this.log( "--- Image Integration ---" );
      this.log( P.toSource() );
      this.log( "-------------------------" );
      this.log( " " );

      P.executeGlobal();

      // save the image
      if ( !fname )
      {
         fname = this.params.workingDir + "/OptimalIntegration.xisf"
      }
      let window = ImageWindow.windowById( "integration" )
      if ( File.exists( fname ) )
      {
         File.remove( fname );
      }
      window.saveAs( fname, false, false )
      window.forceClose()
      this.cache.addTemporaryFilePath( fname );
      return fname
   };

   // write the weights in the file fits header
   this.storeState = ( state, iImage, force, runcbk, endcbk, toInputFiles ) =>
   {
      let fnames = ( toInputFiles ? this.params.inputFiles : this.params.workingFiles ).slice();
      // we write the weights only if externally forced
      if ( force )
      {
         // save the current satus, it will be restored at the end
         let currentStatus = this.params.status

         if ( state == undefined )
         {
            state = this.params.state;
         }

         for ( let i = 0; i < fnames.length; i++ )
         {
            let filePath = fnames[ i ];

            if ( this.stopRequested )
            {
               console.noteln( "end requested" )
               break;
            }

            // save the status for all images or only of the image specified by iImage parameter
            if ( ( iImage == undefined ) || ( iImage == i ) )
            {
               // load the i-th image
               let imageWindow = this.readImage( filePath );

               if ( imageWindow )
               {
                  // set the keyword
                  imageWindow.keywords = imageWindow.keywords.filter( keyword =>
                  {
                     return keyword.name != this.params.optKeyword;
                  } ).concat(
                     new FITSKeyword(
                        this.params.optKeyword,
                        format( "%.5e", state[ i ] ).replace( "e", "E" ),
                        "Weight Optimizer weight"
                     ) );

                  // overwrite the input files if IamgeIntegration is given or if save is forced
                  let newFilePath;
                  if ( this.params.imageIntegrationIconName != "" || force )
                  {
                     let extension = File.extractExtension( filePath );
                     if ( extension != "xisf" )
                     {
                        newFilePath = filePath.replace( extension, ".xisf" )
                        console.warningln( "WARNING: new file will be written storing the weights ", newFilePath )
                     }
                     else
                     {
                        newFilePath = filePath;
                     }
                  }
                  else
                  {
                     let fname = File.extractName( filePath );
                     newFilePath = this.params.workingDir + "/" + fname + ".xisf";
                     if ( File.exists( newFilePath ) )
                        File.remove( newFilePath );
                     this.params.workingFiles[ i ] = newFilePath;
                     this.cache.addTemporaryFilePath( newFilePath );
                  }
                  imageWindow.saveAs( newFilePath, false, false, false, false );
                  imageWindow.forceClose();
               }
            }

            // update the status with the current progress and call the running callback to refresh the UI
            this.params.status = {
               value: OPTIMIZATION_SATES.WRITING_WEIGHTS,
               progress: i,
               tot: fnames.length
            }
            if ( runcbk )
               runcbk();
         }

         // restore the current status
         this.params.status = currentStatus;
      }

      // invoke the end callback if provided
      if ( endcbk )
         endcbk();
   };

   // compute the cost function associated to the given state
   this.computeCostFunction = ( state ) =>
   {
      if ( this.stopRequested )
      {
         return {
            measure: 0,
            cost: 0
         };
      }

      //
      this.noteln( "computeCostFunction: integrating..." )
      let ii = this.integrate( undefined /* standarad output file */ , this.params.useROI(), state );

      if ( this.stopRequested )
      {
         return {
            measure: 0,
            cost: 0
         };
      }
      let measure = this.measure( ii )
      let cost;
      if ( this.params.usingOptimizationFormula() )
         cost = this.params.customFormulaMinMax == "max" ? ( 1 / measure ) : measure;
      else
         cost = OPTIMIZATION_METRICS[ this.params.optMetric ].criteria == "max" ? ( 1 / measure ) : measure;

      this.noteln( " " )
      this.noteln( "computeCostFunction: measured: ", measure )
      this.noteln( "computeCostFunction: cost: ", cost )
      this.noteln( " " )
      return {
         measure: measure,
         cost: cost
      };
   };

   // measures the integrated image and returns the quality metric
   this.measure = ( imagePath ) =>
   {
      // use SS to measure the image
      let P = new SubframeSelector;
      P.routine = SubframeSelector.prototype.MeasureSubframes;
      P.nonInteractive = true;
      P.subframes = [
         [ true, imagePath ]
      ];
      P.fileCache = true;
      P.subframeScale = 1.0000;
      P.cameraGain = 1.0000;
      P.cameraResolution = SubframeSelector.prototype.Bits16;
      P.siteLocalMidnight = 24;
      P.scaleUnit = SubframeSelector.prototype.ArcSeconds;
      P.dataUnit = SubframeSelector.prototype.Electron;
      P.trimmingFactor = 0.10;
      P.structureLayers = 5;
      P.noiseLayers = 0;
      P.hotPixelFilterRadius = 1;
      P.applyHotPixelFilter = false;
      P.noiseReductionFilterRadius = 0;
      P.sensitivity = 0.1000;
      P.peakResponse = 0.8000;
      P.maxDistortion = 0.5000;
      P.upperLimit = 1.0000;
      P.backgroundExpansion = 3;
      P.xyStretch = 1.5000;
      P.psfFit = SubframeSelector.prototype.Moffat4;
      P.psfFitCircular = false;
      P.roiX0 = 0;
      P.roiY0 = 0;
      P.roiX1 = 0;
      P.roiY1 = 0;
      P.pedestalMode = SubframeSelector.prototype.Pedestal_Keyword;
      P.pedestal = 0;
      P.pedestalKeyword = "";
      P.inputHints = "";
      P.outputHints = "";
      P.outputDirectory = "";
      P.outputExtension = ".xisf";
      P.outputPrefix = "";
      P.outputPostfix = "_a";
      P.outputKeyword = "SSWEIGHT";
      P.overwriteExistingFiles = false;
      P.onError = SubframeSelector.prototype.Continue;
      P.approvalExpression = "";
      P.weightingExpression = this.params.customFormula;
      P.sortProperty = SubframeSelector.prototype.Index;
      P.graphProperty = SubframeSelector.prototype.PSFSignalWeight;
      P.auxGraphProperty = SubframeSelector.prototype.Weight;
      P.useFileThreads = true;
      P.fileThreadOverload = 1.00;
      P.maxFileReadThreads = 0;
      P.maxFileWriteThreads = 0;

      P.executeGlobal();
      this.log( " " );
      this.log( " --- Subframe Selector result ---" );
      this.log( P.toSource() );
      this.log( " --------------------------------" );
      this.log( " " );

      let data;
      if ( this.params.usingOptimizationFormula() )
      {
         let customFormula = new SSCustomFormula( P.measurements, this.log );
         return customFormula.computeFormula( this.params.customFormula );
      }
      else
      {
         data = P.measurements;
         // return the metric needed
         let iIndex = OPTIMIZATION_METRICS[ this.params.optMetric ].indx;
         return data[ 0 ][ iIndex ];
      }
   };

   // compute the state gradient direction given the current cost value at the given state and the delta to be computed
   this.computeGradient = ( prevCostData, state, delta ) =>
   {
      let gradient = [];
      let maxGradient = 0;
      let T0 = Date.now();
      if ( this.gradientStepDuration )
         this.gradientEndTime = T0 + this.gradientStepDuration * state.length;
      else
         this.gradientEndTime = undefined;
      for ( let i = 0; i < state.length; i++ )
      {
         if ( this.stopRequested )
            break;

         this.params.status = {
            value: OPTIMIZATION_SATES.GRADIENT,
            progress: i,
            tot: state.length
         }

         if ( state[ i ] == this.params.minWeight )
         {
            gradient[ i ] = 0;
         }
         else
         {
            // give a delta to the cost
            let dState = state.slice();
            dState[ i ] = dState[ i ] + delta;
            this.storeState( dState, i );
            if ( this.stopRequested )
               break;

            let newCostData = this.computeCostFunction( dState );
            if ( this.stopRequested )
               break;

            this.storeState( state, i );
            if ( this.stopRequested )
               break;

            if ( newCostData.cost == prevCostData.cost || delta == 0 )
               gradient[ i ] = 0;
            else
               gradient[ i ] = ( newCostData.cost - prevCostData.cost ) / delta;

            if ( isNaN( gradient[ i ] ) )
            {
               this.params.error = true;
               this.params.errorMsg = "gradient at index " + i + " is NaN.";
            }
            else
            {
               maxGradient = Math.max( maxGradient, Math.abs( gradient[ i ] ) );
            }

            let dT = Date.now() - T0;
            this.gradientEndTime = T0 + dT / ( i + 1 ) * state.length;
         }
      }
      this.params.maxGradient = maxGradient;
      return gradient;
   };

   this.computeNewState = ( state, gradient, speed ) =>
   {
      let newState = [];
      let modGrad = 0;
      gradient.forEach( ( g ) =>
      {
         modGrad = modGrad + g * g;
      } );
      modGrad = Math.sqrt( modGrad );

      state.forEach( ( x, i ) =>
      {
         if ( modGrad > 0 )
            newState[ i ] = Math.max( this.params.minWeight, x - speed * gradient[ i ] / modGrad );
         else
            newState[ i ] = state[ i ]
      } )

      return newState;
   };

   this.normalize = ( state ) =>
   {
      let M = state[ 0 ];
      state.forEach( val =>
      {
         if ( val > M )
            M = val;
      } )
      return state.map( val => ( val / M ) );
   };

   // Initialize the solver parameters and set the initial status
   this.initializeSolver = () =>
   {
      // initialize solution
      this.params.error = false;
      this.params.errorMsg = "";

      this.cropImagesIfNeeded();

      // reutrn if stop has been requested
      if ( this.stopRequested )
         return;

      this.noteln( "source files: ", this.params.workingFiles );
      this.storeState( this.params.state );

      // reutrn if stop has been requested
      if ( this.stopRequested )
         return;

      this.params.costData = this.computeCostFunction( this.params.state );
   };

   // OPTIMIZATION PROCEDURE
   this.optimize = ( maxIterations ) =>
   {
      this.params.status = {
         value: OPTIMIZATION_SATES.INIT
      };
      this.initializeSolver();

      // reutrn if stop has been requested
      if ( this.stopRequested )
         return;

      this.currentIteration = this.params.iterations.length;
      let firstIteration = this.currentIteration;
      let maxSpeedReductionReached = false;
      let speed = this.params.speed;
      let speedReductionsCount = 0;
      let initialMeasure = this.params.costData.measure;
      let initialCost = this.params.costData.cost;

      this.log( "PARAMETERS:\n", JSON.stringify( this.params, null, 2 ) );

      this.T0 = Date.now();

      // insert the initial iteration if:
      // - this is the very first execution i.e. no iterations exist yet
      // - the state has changed since the previous iteration
      // - the objective has changed, so one of the following changed
      //    - the objective selected by the dropdown
      //    - the custom formula
      //    - the option to minimize/maximize the custom formula
      let it = this.params.iterations;
      let lastIteration = it.length > 0 ? it[ it.length - 1 ] :
      {
         state: this.params.state.slice()
      };
      let stateChangedSinceLastIteration = this.params.state.reduce( ( acc, value, i ) =>
         {
            return acc || ( value != lastIteration.state[ i ] );
         },
         false );

      let optimizationObjectiveChanged = firstIteration == 0 ? false : (
         this.params.optMetric != lastIteration.optMetric ||
         this.params.customFormula != lastIteration.customFormula ||
         this.params.customFormulaMinMax != lastIteration.customFormulaMinMax );

      if ( firstIteration == 0 || stateChangedSinceLastIteration || optimizationObjectiveChanged )
      {
         this.params.iterations.add(
         {
            type: ITERATION_TYPE.INIT,
            i: this.currentIteration,
            cost: initialCost,
            mvp: 0,
            measure: initialMeasure,
            state: this.params.state.slice(),
            convergenceReached: false,
            maxSpeedReductionReached: false,
            note: "",
            optMetric: this.params.optMetric,
            optLbl: OPTIMIZATION_METRICS[ this.params.optMetric ].label,
            customFormula: this.params.customFormula,
            customFormulaMinMax: this.params.customFormulaMinMax
         } );
         this.currentIteration += 1;
         firstIteration += 1;
         this.execIterationCompleted();
      }

      // -
      // ITERATOR LOOP
      // -
      let newCostData;
      let newState;
      while ( true )
      {
         // -
         // skip all steps if stop is requested
         // -
         if ( this.stopRequested )
         {
            break;
         }

         // -
         // compute the gradient
         // -
         this.noteln( " -------------------------------------------------- " );
         this.noteln( "              Begin of iteration #", this.currentIteration );
         this.noteln( " -------------------------------------------------- " );

         let gradient = this.computeGradient( this.params.costData, this.params.state, this.params.delta );

         this.noteln( " " );
         this.noteln( " -------------------" );
         this.noteln( "  COMPUTED GRADIENT" );
         this.noteln( " -------------------" );
         this.noteln( " " );
         gradient.forEach( ( x, i ) => this.noteln( "[", i, "]: ", x ) )
         this.noteln( " " );

         // -
         // NEW STATE - SPEED REDUCTION LOOP
         // -
         while ( true )
         {
            // -
            // skip all steps if stop is requested
            // -
            if ( this.stopRequested )
               break;

            if ( this.params.error )
            {
               this.writeln( "Iteration [", this.currentIteration, "] FAILED" );
               this.writeln( "  ", this.params.err.errorMsg );
               break;
            }

            // -
            // Speed reduction max cound reached? if yes then stop the iterations
            // -
            if ( speedReductionsCount >= this.params.maxSpeedReductions )
            {
               maxSpeedReductionReached = true;
               break;
            }

            // -
            // compute the new normalized state
            // -
            newState = this.computeNewState( this.params.state, gradient, speed );

            // -
            // skip all steps if stop is requested
            // -
            if ( this.stopRequested )
               break;

            newState = this.normalize( newState );

            this.noteln( " -----------" );
            this.noteln( "  NEW STATE" );
            this.noteln( " -----------" );
            newState.forEach( ( x, i ) =>
            {
               this.noteln( "[", i, "]: ", x );
            } );
            this.noteln( " " );

            // -
            // compute the cost in the new state
            // -
            this.storeState( newState );

            // -
            // skip all steps if stop is requested
            // -
            if ( this.stopRequested )
               break;

            newCostData = this.computeCostFunction( newState );

            // -
            // skip all steps if stop is requested
            // -
            if ( this.stopRequested )
               break;

            // -
            // check if an inprovememt has been made, otherwise reduce the speed step
            // -
            if ( newCostData.cost < this.params.costData.cost )
               // iteration completed
               break;
            else
            {
               // -
               // step didn't reduce the cost, halve the speed step
               // -
               speedReductionsCount += 1;
               speed = speed / 2;
               this.writeln( "Cost increased, reduce the learning rate to ", speed );
               this.writeln( "" );
               this.params.status = {
                  value: OPTIMIZATION_SATES.REDUCING_STEP,
                  speedReductionsCount: speedReductionsCount
               }
            }
         }

         // -

         if ( this.stopRequested )
         {
            break;
         }

         // -
         // iteration completed, notify it
         // -
         this.params.status = {
            value: OPTIMIZATION_SATES.ITERATION_COMPLETED,
            i: this.currentIteration
         };

         // -
         // log the iteration result
         // -
         this.noteln( "" );
         this.noteln( " ---------------------" );
         this.noteln( "  ITERATION COMPLETED" );
         this.noteln( " ---------------------" );

         let measure_lbl = OPTIMIZATION_METRICS[ this.params.optMetric ].label
         let PL = 25
         let padded = ( str ) =>
         {
            let padLen = Math.max( Math.min( PL, PL - str.length ), 0 );
            return "  " + str + " ".repeat( padLen );
         };

         let mvp = ( Math.abs( newCostData.measure - this.params.costData.measure ) / this.params.costData.measure )
         let mvp_th = Math.pow( 10, this.params.convergence );
         this.writeln( "Iteration #" + this.currentIteration );
         this.writeln( padded( measure_lbl + " initial" ) + " : " + this.params.costData.measure );
         this.writeln( padded( measure_lbl + " final" ) + " : " + newCostData.measure );
         this.writeln( padded( measure_lbl + " increment" ) + " : " + ( newCostData.measure - this.params.costData.measure ) );
         this.writeln( padded( "cost" ) + " : " + newCostData.cost );
         this.writeln( padded( "measure variation perc" ) + " : " + format( "%0.04f %%", mvp * 100 ) );
         this.writeln( padded( "max gradient" ) + " : " + this.params.maxGradient );
         this.writeln( padded( "learning rate" ) + " : " + speed );
         this.writeln( padded( "variation perc thr." ) + " : " + format( "%0.04f %%", mvp_th ) );
         this.writeln( " " );

         // -
         // finalize the iteration depending on the iteration result type
         // -

         let costToleranceCheck = ( 100 * mvp ) < mvp_th;
         let convergenceReached = costToleranceCheck;

         let iterationType;
         if ( convergenceReached )
            iterationType = ITERATION_TYPE.CONVERGED;
         else
         {
            if ( maxSpeedReductionReached )
               iterationType = ITERATION_TYPE.SPEED_REDUCTIONS_STOP;
            else
               iterationType = ITERATION_TYPE.DESCEND;
         }

         let lastIteration = this.params.iterations[ this.params.iterations.length - 1 ];
         // non-converged iterations reports data from the last iteration
         let iterationData = {
            type: iterationType,
            i: this.currentIteration,
            // for a non-converged iteration the cost reported is the one of the previous iteration
            cost: maxSpeedReductionReached ? lastIteration.cost : newCostData.cost,
            // for a non-converged iteration the mvp is 0
            mvp: maxSpeedReductionReached ? 0 : mvp,
            // for a non-converged iteration the measurement reported is the one of the previous iteration
            measure: maxSpeedReductionReached ? lastIteration.measure : newCostData.measure,
            // for a non-converged iteration the state reported is the same of the previous iteration
            state: ( maxSpeedReductionReached ? lastIteration.state : newState ).slice(),
            convergenceReached: convergenceReached,
            maxSpeedReductionReached: maxSpeedReductionReached,
            note: "",
            optMetric: this.params.optMetric,
            optLbl: OPTIMIZATION_METRICS[ this.params.optMetric ].label,
            customFormula: this.params.customFormula,
            customFormulaMinMax: this.params.customFormulaMinMax
         };
         this.params.iterations.add( iterationData );

         // -
         // save progression
         // -
         this.params.state = iterationData.state.slice();
         this.params.costData = {
            measure: iterationData.measure,
            cost: iterationData.cost
         }
         this.execIterationCompleted();

         // -
         // termination conditions
         // -
         if ( convergenceReached )
         {
            this.writeln( "tolerance reached" );
            break;
         }

         if ( maxSpeedReductionReached )
         {
            this.writeln( "Unable to make further improvements, maximum reached" );
            break;
         }

         // -
         // next iteration ready
         // -
         this.currentIteration += 1;

         // stop if the max number of iterations has been reached
         if ( this.currentIteration - firstIteration >= maxIterations )
         {
            this.writeln( "Max iterations reached, stop" );
            break;
         }
      }

      // -
      // Do not report if solver has been manually terminated
      // -
      if ( this.stopRequested )
         return;

      // -
      // SOLVER TERMINATION REPORT
      // -
      let measure_lbl = OPTIMIZATION_METRICS[ this.params.optMetric ].label
      this.writeln( "OPTIMAL VALUES FOUND: " );
      this.params.state.forEach( ( x, i ) =>
      {
         let filename = File.extractName( this.params.inputFiles[ i ] );
         this.writeln( "[", i, "]: ", format( "%.04f", x ), " ; ", filename );
      } );

      this.noteln( " List" );
      let M = 0;
      this.params.state.forEach( x =>
      {
         M = Math.max( M, x )
      } );
      this.params.state.forEach( x =>
      {
         this.writeln( format( "%.04f", x / M ) );
      } );

      this.noteln( " Initial measurement of " + measure_lbl + " :", initialMeasure );
      this.noteln( " Optimal measurement of " + measure_lbl + " :", newCostData.measure );
      this.noteln( " " );
      this.noteln( " Initial cost:", initialCost );
      this.noteln( " Minimum cost:", newCostData.cost );
   };

   this.execute = ( maxIterations, execProcessEvents, execIterationCompleted, execTerminated ) =>
   {
      // set the execution status and stop flag
      this.executing = true;
      this.stopRequested = false;
      // this.callbacks.execProcessEvents = execProcessEvents;
      this.callbacks.execIterationCompleted = execIterationCompleted;
      this.callbacks.execTerminated = execTerminated;

      //run he solver loop
      try
      {
         this.optimize( maxIterations || this.params.maxIterations );
         if ( this.stopRequested )
         {
            this.noteln( "SOLVER STOPPED" );
         }
      }
      catch ( e )
      {
         this.noteln( "TERMINATED WITH ERROR: ", e );
      }
      this.execTerminated();
      this.executing = false;
      this.stopRequested = false;
      this.callbacks.execProcessEvents = undefined;
      this.callbacks.execIterationCompleted = undefined;
      this.callbacks.execTerminated = undefined;
      this.saveLog();
   };

   // configure the execution callbacks
   this.execProcessEvents = () =>
   {
      if ( this.callbacks.execProcessEvents )
      {
         this.callbacks.execProcessEvents();
      }
      else
      {
         processEvents();
      }
   };

   this.execIterationCompleted = () =>
   {
      if ( this.callbacks.execIterationCompleted )
      {
         this.callbacks.execIterationCompleted();
      }
      else
      {
         processEvents();
      }
   };

   this.execTerminated = () =>
   {
      if ( this.callbacks.execTerminated )
      {
         this.callbacks.execTerminated();
      }
      else
      {
         processEvents();
      }
   };
}

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer-Engine.js - Released 2022-03-04T15:42:10Z
