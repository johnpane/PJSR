// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer-Globals.js - Released 2022-03-04T15:42:10Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.0.0
//
// Copyright (c) 2022 Roberto Sartori
// Copyright (c) 2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#define FIT_WEIGHT_KEY          "OPTWGHT"
#define DELTA                   0.05
#define SPEED                   1
#define MAX_SPEED_REDUCTIONS    12
#define GRAD_TOLERANCE          0.01
#define MIN_WEIGHT              0.0001
#define COST_PERC_CONVERGENCE   -2
#define MAX_ITERATIONS          100

#define WO_DEFAULT_MARGIN 8
#define WO_DEFAULT_SPACING 8

#define WO_CUSTOM_FORMULA_LBL "Formula"

/* beautify ignore:end */

let OPTIMIZATION_METRICS = [
{
   label: "PSF Signal",
   indx: 7,
   criteria: "max"
},
{
   label: "PSF SNR",
   indx: 28,
   criteria: "max"
},
{
   label: "SNR Estimate",
   indx: 9,
   criteria: "max"
},
{
   label: WO_CUSTOM_FORMULA_LBL,
   indx: 4,
   criteria: "max"
} ];

let OPTIMIZATION_SATES = {
   STOP: 0,
   INIT: 1,
   GRADIENT: 2,
   REDUCING_STEP: 3,
   ITERATION_COMPLETED: 4,
   WRITING_WEIGHTS: 5,
}

let ITERATION_TYPE = {
   INIT: 0,
   DESCEND: 1,
   CONVERGED: 2,
   SPEED_REDUCTIONS_STOP: 3
}

/* DECLARES THE ENGINE PARAMETERS THAT WILL BE AUTOMATICALLY LOADED/SAVED
AND IMPORTED/EXPORTED
*/
let WO_PERSISTED_PRAMS = [
{
   name: "version",
   type: "string",
   saveDataType: DataType_String
},
{
   name: "workingDir",
   type: "string",
   saveDataType: DataType_String
},
{
   name: "imageIntegrationIconName",
   type: "string",
   saveDataType: DataType_String
},
{
   name: "customFormula",
   type: "string",
   saveDataType: DataType_String
},
{
   name: "customFormulaMinMax",
   type: "string",
   saveDataType: DataType_String
},
{
   name: "optKeyword",
   type: "string",
   saveDataType: DataType_String
},
{
   name: "delta",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "speed",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "maxSpeedReductions",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "gradTolerance",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "minWeight",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "convergence",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "maxIterations",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "roiX0",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "roiY0",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "roiW",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "roiH",
   type: "float",
   saveDataType: DataType_Float
},
{
   name: "optMetric",
   type: "integer",
   saveDataType: DataType_Int32
},
{
   name: "plotGain",
   type: "boolean",
   saveDataType: DataType_Boolean
},
{
   name: "plotMeasure",
   type: "boolean",
   saveDataType: DataType_Boolean
} ];

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer-Globals.js - Released 2022-03-04T15:42:10Z
