// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer.js - Released 2022-03-04T15:42:10Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.0.0
//
// Copyright (c) 2022 Roberto Sartori
// Copyright (c) 2022 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#define TITLE "Weights Optimizer"
#define VERSION "1.0.0"

#feature-id WeightsOptimizer : Utilities > WeightsOptimizer

#feature-info Image Integration weights optimizer.<br/> \
    <br/> \
    This script computes the optimal weights to be assigned to light frames in order \
    to maximize a specific quality metric of the master integration image. \
    <br/> \
    Copyright & copy; 2022 Roberto Sartori. All Rights Reserved.

#feature-icon  @script_icons_dir/WeightsOptimizer.svg

#include <pjsr/ColorSpace.jsh>
#include <pjsr/DataType.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/SectionBar.jsh>
#include "WeightsOptimizer-Globals.js"
#include "WeightsOptimizer-Engine.js"
#include "WeightsOptimizer-GUI.js"

/* beautify ignore:end */

function main()
{
   console.show();

   let WOEngine = new WeightsOptimizerEngine();

   if ( Parameters.isViewTarget )
   {
      // Script cannot be executed on a target image
      console.criticalln( "Weight Optimizer cannot be executed on a view target." )
      return;
   }

   if ( Parameters.isGlobalTarget )
   {
      // load parameters from the icon instance
      WOEngine.params.import();
   }
   else
   {
      // load last saved parameters
      WOEngine.params.load();
   }
   WOEngine.sanityCheck();


   // Prepare the dialog
   let parametersDialog = new WeightsOptimizerDialog( WOEngine );
   parametersDialog.execute();

   if ( !Parameters.isGlobalTarget )
   {
      // save the current execution if not launched from an icon
      WOEngine.params.save();
   }

   // clean up the temporary files
   WOEngine.cache.deleteTemporaryFiles();
}

main();

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer.js - Released 2022-03-04T15:42:10Z
