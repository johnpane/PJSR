//     ____       __ _____  ____
//    / __ \     / // ___/ / __ \
//   / /_/ /__  / / \__ \ / /_/ /
//  / ____// /_/ / ___/ // _, _/   PixInsight JavaScript Runtime
// /_/     \____/ /____//_/ |_|    PJSR Version 1.0
// ----------------------------------------------------------------------------
// pjsr/LinearDefectDetection.jsh - Released 2022-03-02T11:24:32Z
// ----------------------------------------------------------------------------
// This file is part of the PixInsight JavaScript Runtime (PJSR).
// PJSR is an ECMA-262-5 compliant framework for development of scripts on the
// PixInsight platform.
//
// Copyright (c) 2003-2022 Pleiades Astrophoto S.L. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __PJSR_LinearDefectDetection_jsh
#define __PJSR_LinearDefectDetection_jsh

#include <pjsr/ImageOp.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/UndoFlag.jsh>

#define LDD_DEFAULT_DETECT_COLUMNS                    true
#define LDD_DEFAULT_DETECT_PARTIAL_LINES              true
#define LDD_DEFAULT_CLOSE_FORMER_WORKING_IMAGES       true
#define LDD_DEFAULT_LAYERS_TO_REMOVE                  9
#define LDD_DEFAULT_REJECTION_LIMIT                   3
#define LDD_DEFAULT_DETECTION_THRESHOLD               5
#define LDD_DEFAULT_PARTIAL_LINE_DETECTION_THRESHOLD  5
#define LDD_DEFAULT_IMAGE_SHIFT                       50
#define LDD_DEFAULT_OUTPUT_DIR                        ""

/*
 * LinearDefectDetection.
 *
 * Implements the procedure to detect defective columns or rows in a reference image.
 */
function LDDEngine()
{

   // set the default values
   this.detectColumns = LDD_DEFAULT_DETECT_COLUMNS;
   this.detectPartialLines = LDD_DEFAULT_DETECT_PARTIAL_LINES;
   this.closeFormerWorkingImages = LDD_DEFAULT_CLOSE_FORMER_WORKING_IMAGES;
   this.layersToRemove = LDD_DEFAULT_LAYERS_TO_REMOVE;
   this.rejectionLimit = LDD_DEFAULT_REJECTION_LIMIT;
   this.detectionThreshold = LDD_DEFAULT_DETECTION_THRESHOLD;
   this.partialLineDetectionThreshold = LDD_DEFAULT_PARTIAL_LINE_DETECTION_THRESHOLD;
   this.imageShift = LDD_DEFAULT_IMAGE_SHIFT;
   this.outputDir = LDD_DEFAULT_OUTPUT_DIR;

   // results
   this.detectedColumnOrRow = new Array;
   this.detectedStartPixel = new Array;
   this.detectedEndPixel = new Array;

   // ----------------------------------------------------------------------------

   /*
    * Function to subtract the large-scale components from an image using the
    * median wavelet transform.
    */
   this.multiscaleIsolation = ( image, LSImage, layersToRemove ) =>
   {
      // Generate the large-scale components image.
      // First we generate the array that defines
      // the states (enabled / disabled) of the scale layers.
      let scales = new Array;
      for ( let i = 0; i < layersToRemove; ++i )
         scales.push( 1 );

      // The scale layers are an array of images.
      // We use the medianWaveletTransform. This algorithm is less prone
      // to show vertical patterns in the large-scale components.
      let multiscaleTransform = new Array;
      multiscaleTransform = image.medianWaveletTransform( layersToRemove - 1, 0, scales );
      // We subtract the last layer to the image.
      // Please note that this image has negative pixel values.
      image.apply( multiscaleTransform[ layersToRemove - 1 ], ImageOp_Sub );
      // Generate a large-scale component image
      // if the respective input image is not null.
      if ( LSImage != null )
         LSImage.apply( multiscaleTransform[ layersToRemove - 1 ] );
      // Remove the multiscale layers from memory.
      for ( let i = 0; i < multiscaleTransform.length; ++i )
         multiscaleTransform[ i ].free();
   }

   // ----------------------------------------------------------------------------

   /*
    * Function to create a list of vertical or horizontal lines in an image. It
    * can combine entire rows or columns and fragmented ones, if an array of
    * partial sections is specified in the input parameters. This list is used to
    * input the selected regions in the IterativeStatistics function.
    */
   this.lineList = ( correctEntireImage, partialColumnOrRow, partialStartPixel, partialEndPixel, maxPixelPara, maxPixelPerp ) =>
   {
      let LL = {};

      LL.columnOrRow = new Array;
      LL.startPixel = new Array;
      LL.endPixel = new Array;

      if ( !correctEntireImage )
      {
         LL.columnOrRow = partialColumnOrRow;
         LL.startPixel = partialStartPixel;
         LL.endPixel = partialEndPixel;
      }
      else
      {
         if ( partialColumnOrRow.length == 0 )
            partialColumnOrRow.push( maxPixelPerp + 1 );

         let iPartial = 0;
         for ( let i = 0; i <= maxPixelPerp; ++i )
         {
            if ( iPartial < partialColumnOrRow.length )
            {
               if ( i < partialColumnOrRow[ iPartial ] && correctEntireImage )
               {
                  LL.columnOrRow.push( i );
                  LL.startPixel.push( 0 );
                  LL.endPixel.push( maxPixelPara );
               }
               else
               {
                  // Get the partial column or row.
                  LL.columnOrRow.push( partialColumnOrRow[ iPartial ] );
                  LL.startPixel.push( partialStartPixel[ iPartial ] );
                  LL.endPixel.push( partialEndPixel[ iPartial ] );
                  if ( partialStartPixel[ iPartial ] > 0 )
                  {
                     LL.columnOrRow.push( partialColumnOrRow[ iPartial ] );
                     LL.startPixel.push( 0 );
                     LL.endPixel.push( partialStartPixel[ iPartial ] - 1 );
                  }
                  if ( partialEndPixel[ iPartial ] < maxPixelPara )
                  {
                     LL.columnOrRow.push( partialColumnOrRow[ iPartial ] );
                     LL.startPixel.push( partialEndPixel[ iPartial ] + 1 );
                     LL.endPixel.push( maxPixelPara );
                  }
                  // In some cases, there can be more than one section of
                  // the same column or row in the partial defect list.
                  // In that case, i (which is the current column or row number)
                  // shouldn't increase because we are repeating
                  // the same column or row.
                  i = partialColumnOrRow[ iPartial ];
                  ++iPartial;
               }
            }
            else if ( correctEntireImage )
            {
               LL.columnOrRow.push( i );
               LL.startPixel.push( 0 );
               LL.endPixel.push( maxPixelPara );
            }
         }
      }

      return LL;
   }

   // ----------------------------------------------------------------------------

   /*
    * Function to calculate the median and MAD of a selected image area with
    * iterative outlier rejection in the high end of the distribution. Useful to
    * reject bright objects in a background-dominated image, especially if the
    * input image is the output image of MultiscaleIsolation.
    */
   this.iterativeStatistics = ( image, rectangle, rejectionLimit ) =>
   {
      let IS = {};

      image.selectedRect = rectangle;
      let formerHighRejectionLimit = 1000;
      // The initial currentHighRejectionLimit value is set to 0.99 because
      // the global rejection sets the rejected pixels to 1. This way, those
      // pixels are already rejected in the first iteration.
      let currentHighRejectionLimit = 0.99;
      let j = 0;
      while ( formerHighRejectionLimit / currentHighRejectionLimit > 1.001 || j < 10 )
      {
         // Construct the statistics object to rectangle statistics.
         // These statistics are updated with the new high rejection limit
         // calculated at the end of the iteration.
         let iterativeRectangleStatistics = new ImageStatistics;
         with( iterativeRectangleStatistics )
         {
            medianEnabled = true;
            lowRejectionEnabled = false;
            highRejectionEnabled = true;
            rejectionHigh = currentHighRejectionLimit;
         }
         iterativeRectangleStatistics.generate( image );
         IS.median = iterativeRectangleStatistics.median;
         IS.MAD = iterativeRectangleStatistics.mad;
         formerHighRejectionLimit = currentHighRejectionLimit;
         currentHighRejectionLimit = parseFloat( IS.median + ( iterativeRectangleStatistics.mad * 1.4826 * rejectionLimit ) );
         ++j;
      }
      image.resetSelections();

      return IS;
   }

   // ----------------------------------------------------------------------------

   /*
    * Function to detect defective partial columns or rows in an image.
    */
   this.partialLineDetection = ( detectColumns, image, layersToRemove, imageShift, threshold) =>
   {
      let PLD = {};

      if ( ( detectColumns ? image.height : image.width ) < imageShift * 4 )
         throw new Error( "imageShift parameter too high for the current image size" );


      // Create a small-scale component image and its image window.
      // SSImage will be the main view of the small-scale component
      // image window because we need to apply a
      // MorphologicalTransformation instance to it.
      PLD.SSImageWindow = new ImageWindow( image.width,
         image.height,
         image.numberOfChannels,
         32, true, false,
         "partial_line_detection" );

      // The initial small-scale component image is the input image.
      PLD.SSImage = new Image( image.width,
         image.height,
         image.numberOfChannels,
         image.colorSpace,
         image.bitsPerSample,
         SampleType_Real );

      PLD.SSImage.apply( image );

      // Subtract the large-scale components to the image.
      console.noteln( "<end><cbr><br>* Isolating small-scale image components..." );
      console.flush();
      this.multiscaleIsolation( PLD.SSImage, null, layersToRemove );

      // The clipping mask is an image to reject the highlights
      // of the processed small-scale component image. The initial
      // state of this image is the small-scale component image
      // after removing the large-scale components. We simply
      // binarize this image at 5 sigmas above the image median.
      // This way, the bright structures are white and the rest
      // of the image is pure black. We'll use this image
      // at the end of the processing.
      let clippingMask = new Image( image.width,
         image.height,
         image.numberOfChannels,
         image.colorSpace,
         image.bitsPerSample,
         SampleType_Real );

      clippingMask.apply( PLD.SSImage );
      clippingMask.binarize( clippingMask.MAD() * 5 );

      // Apply a morphological transformation process
      // to the small-scale component image.
      // The structuring element is a line in the direction
      // of the lines to be detected.
      console.noteln( "<end><cbr><br>* Processing small-scale component image..." );
      console.flush();
      let structure;
      if ( detectColumns )
         structure = [
            [
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ]
         ];
      else
         structure = [
            [
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
            ]
         ];

      console.writeln( "<end><cbr>Applying morphological median transformation..." );
      console.flush();
      for ( let i = 0; i < 5; ++i )
         PLD.SSImage.morphologicalTransformation( 4, structure, 0, 0, 1 );

      // Shift a clone of the small-scale component image
      // after the morphological transformation. We then subtract
      // the shifted image from its parent image. In the resulting
      // image, those linear structures with a sudden change
      // of contrast over the column or row will result in a bright
      // line at the origin of the defect. This lets us
      // to detect the defective partial columns or rows.
      let shiftedSSImage = new Image( image.width,
         image.height,
         image.numberOfChannels,
         image.colorSpace,
         32, SampleType_Real );

      shiftedSSImage.apply( PLD.SSImage );
      detectColumns ? shiftedSSImage.shiftBy( 0, -imageShift ) :
         shiftedSSImage.shiftBy( imageShift, 0 );
      PLD.SSImage.apply( shiftedSSImage, ImageOp_Sub );
      shiftedSSImage.free();

      // Subtract again the large-scale components
      // of this processed small-scale component image.
      // This will give a cleaner result before binarizing.
      console.writeln( "<end><cbr>Isolating small-scale image components..." );
      console.flush();
      this.multiscaleIsolation( PLD.SSImage, null, layersToRemove - 3 );

      // Binarize the image to isolate the partial line detection structures.
      console.writeln( "<end><cbr>Isolating partial line defects..." );
      console.flush();
      let imageMedian = PLD.SSImage.median();
      let imageMAD = PLD.SSImage.MAD();
      PLD.SSImage.binarize( imageMedian + imageMAD * threshold );
      // Now, we subtract the binarized the clipping mask from this processed
      // small-scale component image. This removes the surviving linear structures
      // coming from bright objects in the image.
      PLD.SSImage.apply( clippingMask, ImageOp_Sub );
      PLD.SSImage.truncate( 0, 1 );

      // We apply a closure operation with the same structuring element.
      // This process removes short surviving lines coming from
      // the image noise while keeping the long ones
      console.writeln( "<end><cbr>Applying morphological closure transformation..." );
      console.flush();
      PLD.SSImage.morphologicalTransformation( 2, structure, 0, 0, 1 );

      // Detect the defective partial rows or columns. We select
      // those columns or rows having a minimum number of white pixels.
      // The minimum is half of the image shift and it is calculated
      // by comparing the mean pixel value to the length of the line.
      // Then, we find the maximum position to set the origin of the defect.
      // The maximum position is the start of the white line but the origin
      // of the defect is the end of the white line. To solve this,
      // we first mirror the image.
      console.noteln( "<end><cbr><br>* Detecting partial line defects..." );
      console.flush();
      let maxPixelPerp, maxPixelPara, lineRect;
      if ( detectColumns )
      {
         PLD.SSImage.mirrorVertical();
         maxPixelPerp = PLD.SSImage.width - 1;
         maxPixelPara = PLD.SSImage.height - 1;
         lineRect = new Rect( 1, PLD.SSImage.height );
      }
      else
      {
         PLD.SSImage.mirrorHorizontal();
         maxPixelPerp = PLD.SSImage.height - 1;
         maxPixelPara = PLD.SSImage.width - 1;
         lineRect = new Rect( PLD.SSImage.width, 1 );
      }

      PLD.columnOrRow = new Array;
      PLD.startPixel = new Array;
      PLD.endPixel = new Array;
      for ( let i = 0; i <= maxPixelPerp; ++i )
      {
         detectColumns ? lineRect.moveTo( i, 0 ) :
            lineRect.moveTo( 0, i );

         var lineMeanPixelValue = PLD.SSImage.mean( lineRect );
         // The equation at right sets the minimum length of the line
         // to trigger a defect detection.
         if ( lineMeanPixelValue > ( imageShift / ( ( maxPixelPara + 1 - imageShift * 2 ) * 2 ) ) )
         {
            PLD.columnOrRow.push( i )
            detectColumns ? PLD.startPixel.push( maxPixelPara - parseInt( PLD.SSImage.maximumPosition( lineRect ).toArray()[ 1 ] ) ) :
               PLD.startPixel.push( maxPixelPara - parseInt( PLD.SSImage.maximumPosition( lineRect ).toArray()[ 0 ] ) );
            PLD.endPixel.push( maxPixelPara );
         }
      }

      detectColumns ? PLD.SSImage.mirrorVertical() : PLD.SSImage.mirrorHorizontal();

      PLD.SSImageWindow.mainView.beginProcess();
      PLD.SSImageWindow.mainView.image.apply( PLD.SSImage );
      PLD.SSImageWindow.mainView.endProcess();

      return PLD;
   }

   // ----------------------------------------------------------------------------

   /*
    * These are the image windows and images that will be used by the script
    * engine.
    */
   this.defineWindowsAndImages = ( detectPartialLines ) =>
   {
      let WI = {}

      // Define the working image windows and images.
      WI.referenceImageWindow = ImageWindow.activeWindow;

      WI.referenceImage = new Image( WI.referenceImageWindow.mainView.image.width,
         WI.referenceImageWindow.mainView.image.height,
         WI.referenceImageWindow.mainView.image.numberOfChannels,
         WI.referenceImageWindow.mainView.image.colorSpace,
         32, SampleType_Real );

      WI.referenceImage.apply( WI.referenceImageWindow.mainView.image );

      if ( detectPartialLines )
      {
         WI.referenceImageCopy = new Image( WI.referenceImageWindow.mainView.image.width,
            WI.referenceImageWindow.mainView.image.height,
            WI.referenceImageWindow.mainView.image.numberOfChannels,
            WI.referenceImageWindow.mainView.image.colorSpace,
            32, SampleType_Real );

         WI.referenceImageCopy.apply( WI.referenceImageWindow.mainView.image );
      }

      WI.referenceSSImage = new Image( WI.referenceImage.width,
         WI.referenceImage.height,
         WI.referenceImage.numberOfChannels,
         WI.referenceImage.colorSpace,
         32, SampleType_Real );

      WI.referenceSSImage.apply( WI.referenceImage );

      WI.lineModelWindow = new ImageWindow( WI.referenceImage.width,
         WI.referenceImage.height,
         WI.referenceImage.numberOfChannels,
         32, true, false, "line_model" );

      WI.lineModelImage = new Image( WI.referenceImage.width,
         WI.referenceImage.height,
         WI.referenceImage.numberOfChannels,
         WI.referenceImage.colorSpace,
         32, SampleType_Real );

      WI.lineDetectionWindow = new ImageWindow( WI.referenceImage.width,
         WI.referenceImage.height,
         WI.referenceImage.numberOfChannels,
         32, true, false, "line_detection" );

      WI.lineDetectionImage = new Image( WI.referenceImage.width,
         WI.referenceImage.height,
         WI.referenceImage.numberOfChannels,
         WI.referenceImage.colorSpace,
         32, SampleType_Real );

      return WI;
   }

   // define the execution function that performs the method
   this.execute = () =>
   {
      // close former working images if needed
      if ( this.closeFormerWorkingImages )
      {
         if ( !ImageWindow.windowById( "partial_line_detection" ).isNull )
            ImageWindow.windowById( "partial_line_detection" ).forceClose();
         if ( !ImageWindow.windowById( "line_model" ).isNull )
            ImageWindow.windowById( "line_model" ).forceClose();
         if ( !ImageWindow.windowById( "line_detection" ).isNull )
            ImageWindow.windowById( "line_detection" ).forceClose();
      }

      let WI = this.defineWindowsAndImages( this.detectPartialLines );

      // Generate the small-scale image by subtracting
      // the large-scale components of the image.
      this.multiscaleIsolation( WI.referenceSSImage, null, this.layersToRemove );

      // Build a list of lines in the image.
      // This can include entire or partial rows or columns.
      if ( this.layersToRemove < 7 )
         this.layersToRemove = 7;
      let partialLines;
      if ( this.detectPartialLines )
         partialLines = this.partialLineDetection( this.detectColumns, WI.referenceImageCopy,
            this.layersToRemove - 3, this.imageShift,
            this.partialLineDetectionThreshold );

      let maxPixelPara, maxPixelPerp;
      if ( this.detectColumns )
      {
         maxPixelPara = WI.referenceImage.height - 1;
         maxPixelPerp = WI.referenceImage.width - 1;
      }
      else
      {
         maxPixelPara = WI.referenceImage.width - 1;
         maxPixelPerp = WI.referenceImage.height - 1;
      }

      let lines;
      if ( this.detectPartialLines )
         lines = this.lineList( true,
            partialLines.columnOrRow,
            partialLines.startPixel,
            partialLines.endPixel,
            maxPixelPara, maxPixelPerp );
      else
         lines = this.lineList( true, [], [], [], maxPixelPara, maxPixelPerp );

      // Calculate the median value of each line in the image.
      // Create a model image with the lines filled
      // by their respective median values.
      console.writeln( "<end><cbr><br>Analyzing " + lines.columnOrRow.length + " lines in the image<br>" );
      let lineValues = new Array;
      for ( let i = 0; i < lines.columnOrRow.length; ++i )
      {
         let lineRect;
         if ( this.detectColumns )
         {
            lineRect = new Rect( 1, lines.endPixel[ i ] - lines.startPixel[ i ] + 1 );
            lineRect.moveTo( lines.columnOrRow[ i ], lines.startPixel[ i ] );
         }
         else
         {
            lineRect = new Rect( lines.endPixel[ i ] - lines.startPixel[ i ] + 1, 1 );
            lineRect.moveTo( lines.startPixel[ i ], lines.columnOrRow[ i ] );
         }

         let lineStatistics = this.iterativeStatistics( WI.referenceSSImage, lineRect, this.rejectionLimit );
         WI.lineModelImage.selectedRect = lineRect;
         WI.lineModelImage.apply( lineStatistics.median );
         lineValues.push( lineStatistics.median );
      }
      WI.referenceSSImage.resetSelections();
      WI.lineModelImage.resetSelections();

      // Build the detection map image
      // and the list of detected line defects.
      this.detectedColumnOrRow = new Array;
      this.detectedStartPixel = new Array;
      this.detectedEndPixel = new Array;

      let lineModelMedian = WI.lineModelImage.median();
      let lineModelMAD = WI.lineModelImage.MAD();
      let lineRect;
      for ( let i = 0; i < lineValues.length; ++i )
      {
         if ( this.detectColumns )
         {
            lineRect = new Rect( 1, lines.endPixel[ i ] - lines.startPixel[ i ] + 1 );
            lineRect.moveTo( lines.columnOrRow[ i ], lines.startPixel[ i ] );
         }
         else
         {
            lineRect = new Rect( lines.endPixel[ i ] - lines.startPixel[ i ] + 1, 1 );
            lineRect.moveTo( lines.startPixel[ i ], lines.columnOrRow[ i ] );
         }

         WI.lineDetectionImage.selectedRect = lineRect;
         let sigma = Math.abs( lineValues[ i ] - lineModelMedian ) / ( lineModelMAD * 1.4826 );
         WI.lineDetectionImage.apply( parseInt( sigma ) / ( this.detectionThreshold + 1 ) );
         if ( sigma >= this.detectionThreshold )
         {
            this.detectedColumnOrRow.push( lines.columnOrRow[ i ] );
            this.detectedStartPixel.push( lines.startPixel[ i ] );
            this.detectedEndPixel.push( lines.endPixel[ i ] );
         }
      }

      // Transfer the resulting images to their respective windows.
      WI.lineDetectionImage.resetSelections();
      WI.lineDetectionImage.truncate( 0, 1 );
      WI.lineModelImage.apply( WI.referenceImage.median(), ImageOp_Add );

      WI.lineModelWindow.mainView.beginProcess();
      WI.lineModelWindow.mainView.image.apply( WI.lineModelImage );
      WI.lineModelWindow.mainView.endProcess();

      WI.lineDetectionWindow.mainView.beginProcess();
      WI.lineDetectionWindow.mainView.image.apply( WI.lineDetectionImage );
      WI.lineDetectionWindow.mainView.endProcess();

      // Free memory space taken by working images.
      WI.referenceImage.free();
      WI.referenceSSImage.free();
      WI.lineModelImage.free();
      WI.lineDetectionImage.free();
      if ( this.detectPartialLines )
         WI.referenceImageCopy.free();

      if (this.closeFormerWorkingImages) {
         WI.lineModelWindow.forceClose();
         WI.lineDetectionWindow.forceClose();
         partialLines.SSImageWindow.forceClose();
      } else {
         WI.lineModelWindow.show();
         WI.lineDetectionWindow.show();
         partialLines.SSImageWindow.show();
      }
   }
}

#endif   // __PJSR_LinearDefectDetection_jsh

// ----------------------------------------------------------------------------
// EOF pjsr/LinearDefectDetection.jsh - Released 2022-03-02T11:24:32Z
